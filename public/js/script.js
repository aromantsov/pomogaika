$(document).ready(function () {
    function logSpecialist() {
        if($(window).width() < 641) {
            if(!$('.other_link_header').children('.link_cart').length > 0){
                $('.search_top_line_link').css({
                    left: "auto",
                    right: "0",
                });
            }
            else{
                $('.search_top_line_link').css({
                    left: "calc(100% - 92px)",
                    right: "auto",
                });
            }
        }
    }
    logSpecialist();
    $(window).on('resize', logSpecialist);
    $('ul.tabs__caption').each(function(i) {
      var storage = localStorage.getItem('tab' + i);
      if (storage) {
        $(this).find('li').removeClass('active').eq(storage).addClass('active')
        .closest('div.tabs').find('div.tabs__content').removeClass('active').eq(storage).addClass('active');
      }
    });
 
    $('ul.tabs__caption').on('click', 'li:not(.active)', function() {
      $(this)
      .addClass('active').siblings().removeClass('active')
      .closest('div.tabs').find('div.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
      var ulIndex = $('ul.tabs__caption').index($(this).parents('ul.tabs__caption'));
      localStorage.removeItem('tab' + ulIndex);
      localStorage.setItem('tab' + ulIndex, $(this).index());
    });

    $('#update-button-form').on('click', function(){
        $('#update-form').submit();
    });

    if ($(".slider_home_page").length > 0) {
        $(".slider_home_page").slick({
            slidesToShow: 1,
            infinite: true,
            dots: true,
            arrows: false,
            autoplay: true,
            autoplaySpeed: 2000
        });

    }

    if ($(".line_image_link").length > 0) {
        $(".line_image_link").slick({
            slidesToShow: 2,
            infinite: true,
            dots: true,
            arrows: false,
            responsive: [
                {
                    breakpoint: 641,
                    settings: {
                        slidesToShow: 1,
                        autoplaySpeed: 2000
                    }
                },
            ]
        });

    }

    if ($(".category-slider").length > 0) {
        $(".category-slider").slick({
            slidesToShow: 2,
            infinite: true,
            dots: true,
            arrows: false,
            autoplaySpeed: 2000
        });
    }

    $slickElement = $('.slider_product_block');
    if ($slickElement.length > 0) {
        $slickElement.slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            infinite: false,
            dots: true,
            arrows: true,
            responsive: [
                {
                    breakpoint: 1501,
                    settings: {
                        slidesToShow: 4,
                    }
                },
                {
                    breakpoint: 1251,
                    settings: {
                        slidesToShow: 3,
                    }
                },
                {
                    breakpoint: 641,
                    settings: {
                        slidesToShow: 2,
                    }
                },
            ],
            customPaging: function (slider, i) {
                if (!i) {
                    $('.new_product_line .title_product_block').append('<div class="custom_pagging"><span>5</span>/' + $(slider.$slides).length);
                    var detachButton = $('.slick-arrow');
                    $('.slick-arrow').remove();
                    $('.custom_pagging').append(detachButton);
                }
                if ($('.custom_pagging').length > 1) {
                    $('.custom_pagging').each(function (index, elem) {
                        $(elem).remove();
                    });
                }
            },
        });
        $slickElement.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
            var slickCurrent = +$('.slick-current').length;
            var i = (currentSlide ? currentSlide : 0) + 5;
            $('.custom_pagging span').text(i);
        });
    }
    // if ($('.xzoom, .xzoom-gallery').length > 0) {
    //     $('.xzoom, .xzoom-gallery').xzoom({
    //         zoomWidth: 400,
    //         title: false,
    //         tint: '#333',
    //         Xoffset: 15
    //     });
    // }
    // if ($('.sort_select').length > 0) {
    //     $('.sort_select').selectric({
    //         maxHeight: 200
    //     });
    // }

    $('.more_about_company').click(function () {    
        var thisBlock = $(this);    
        $('.hide_slide_reviews').slideUp(100);  
        thisBlock.siblings('.hide_slide').slideToggle(100); 
        thisBlock.parent('.wrap_slide_about_company').toggleClass('open');  
        if (thisBlock.children('span').text() == 'Подробнее') { 
            thisBlock.children('span').text('Свернуть');    
        } else {    
            thisBlock.children('span').text('Подробнее');   
        }   
    }); 
    $('.read_slide_review').click(function (e) {    
        e.preventDefault(); 
        var thisBlock = $(this);    
        $('.hide_slide').slideUp(100);  
        thisBlock.parents('.rating_reviews_link').siblings('.hide_slide_reviews').slideToggle(100); 
    }); 
    $('.wrap_link_category > a, .wrap_submenu_section').hover(function () { 
        $('.content').addClass('show_overlay'); 
    }, function () {    
        $('.content').removeClass('show_overlay');  
    }); 
    $('.link_dropdown_menu > a.blue_button_link').click(function (e) {  
        e.preventDefault(); 
        $(this).toggleClass('open');    
        $(this).siblings('.a_side_category').fadeToggle(100);   
    }); 
    if ($(".js-range-slider").length > 0) { 
        $(".js-range-slider").ionRangeSlider({  
            onFinish: function (data) { 
                $('.from_input').val(data.from);    
                $('.to_input').val(data.to);    
            }   
        }); 
        let my_range = $(".js-range-slider").data("ionRangeSlider");    
        $('.from_input, .to_input').change(function () {    
            my_range.update({   
                from: $('.from_input').val(),    
                to: $('.to_input').val()    
            }); 
        }); 
    }   
    $('.name_slide_label').click(function () {  
        $(this).toggleClass('open').siblings('.wrap_slide_content').slideToggle(300);   
    }); 
    $('.text_open').click(function () { 
        $('.append_text').text($(this).attr('data-text'));  
        $('.overlay').fadeIn(100, function () { 
            $('.text_hint').fadeIn(100);    
        }); 
    }); 
    $('.cart-add-text').click(function () { 
        $('.append_text').text($(this).attr('data-text'));  
        $('.overlay').fadeIn(100, function () { 
            $('.text_hint').fadeIn(100);    
        }); 
    }); 
    $('.close_modal, .close_success_modal, .overlay').click(function () { 
        $('.modal_block').fadeOut(100, function () {    
            $('.overlay').fadeOut(100); 
            $('.append_text').text(''); 
        }); 
    }); 
    $('.change_password_button').click(function () {    
        $('.overlay').fadeIn(100, function () { 
            $('.change_password').fadeIn(100);  
        }); 
    }); 
    $('.xlsx-import-button').click(function () {    
        $('.overlay').fadeIn(100, function () { 
            $('.xlsx-import').fadeIn(100);  
        }); 
    }); 
    $('.open_modal_login').click(function () { 
        $('#regform').fadeOut(100);  
        $('.overlay').fadeIn(100, function () {  
            $('.enter_form').fadeIn(100);  
        }); 
    }); 
    $('.open_modal_register').click(function () {   
        $('.overlay').fadeIn(100, function () { 
            $('.registration_form').fadeIn(100);  
        }); 
    }); 
    $('.link_add_review').click(function (event) {    
        $('.overlay').fadeIn(100, function () {
            $('.create_reviews').fadeIn(100);  
        });
        return false; 
    }); 
    $('#add_review').click(function () { 
        console.log('test');
        $('.overlay').fadeIn(100, function () {
            $('.create_reviews').fadeIn(100);  
        });
        return false; 
    }); 
    $('.user_image_upload input').on('change', function (evt) { 
        var file = evt.target.files;    
        var f = file[0];    
        if (!f.type.match('image.*')) { 
            alert("К загрузке допустимы только изображения...");    
        }   
        var reader = new FileReader();  
        reader.onload = (function (theFile) {   
            return function (e) {   
                $('.user_image_upload img').remove();   
                $('.user_image_upload').append('<img title="' + escape(theFile.name) + '" src="' + e.target.result + '" />')    
            };  
        })(f);  
        reader.readAsDataURL(f);    
    }); 
    $('#button_add1').click(function () {    
        var thisValue = $(this).siblings('input').val();    
        if (thisValue.length == 0) return;  
        $(this).siblings('input').val('').focus();  
        var inputVar = '<div class="line_new_inpt"><div>';  
        inputVar += '<label><input type="checkbox" name="shops[]" value="' + thisValue + '" checked><span></span>' + thisValue;  
        inputVar += '</label></div><div class="remove_blocks"></div>';  
        inputVar += '</div>';   
        $(this).parents('.input_add_addres').siblings('.append_new_input').append(inputVar);    
    }); 
    $('#button_add2').click(function () {
        var cost =  $('#cost').val();     
        var radius = $('#radius').val();   
        if ((cost.length == 0) || (radius.length == 0)) return;    
        var inputVar = '<li class="one_address"><input type="checkbox" name="delivery_addresses[]" value="' + +cost +'/' + +radius + '" checked><span>' + cost + ' грн</span><span>' + radius + ' км</span><div class="remove_blocks2"></div></li>';  
        $('.add_point').append(inputVar);    
    }); 
    $('body').on('click', '.remove_blocks', function () {   
        $(this).parent('.line_new_inpt').remove();  
    }); 
    $('body').on('click', '.remove_blocks2', function () {   
        $(this).parent('.one_address').remove();  
    }); 
    $('.button_open_load_modal').click(function () {    
        $('.overlay').fadeIn(100, function () { 
            $('.upload_file').fadeIn(100);  
        }); 
    }); 
    $('.pseudo_input input').on('change', function (evt) {  
        var file = evt.target.files;    
        var f = file[0];    
        var reader = new FileReader();  
        reader.onload = (function (theFile) {   
            return function (e) {   
                $('.pseudo_input span').text(escape(theFile.name)); 
            };  
        })(f);  
        reader.readAsDataURL(f);    
    }); 
    $('.input_multiple_file_load input').on('change', function (evt) {  
        $('.one_append_imagess').remove();  
        var file = evt.target.files;    
        for (var i = 0, f; f = file[i]; i++) {  
            if (!f.type.match('image.*')) { 
                alert("К загрузке допустимы только изображения...");    
            }   
            var reader = new FileReader();  
            reader.onload = (function (theFile) {   
                return function (e) {   
                    $('.insert_multiupload_image').prepend('<div class="one_append_imagess"><img title="' + escape(theFile.name) + '" src="' + e.target.result + '" /></div>'); 
                };  
            })(f);  
            reader.readAsDataURL(f);    
        }   
    }); 
    $('.login_enter_account a').click(function (e) {    
        e.preventDefault(); 
        $('.overlay').fadeIn(100, function () { 
            $('.registration_form').fadeIn(100);    
        }); 
    });

    $('.name_category_click, .rewiews_about_company_click .name_reviews_product').click(function () {
        $(this).toggleClass('open');
        $(this).siblings('.product_wishlist_line, .account_my_reviews, .review_about_company_slide').slideToggle(100);
    });
    $('.click_down_orders').click(function(){
        $(this).parents('.one_wrap_orders').addClass('hide');
        $(this).parents('.line_order_header').siblings('.toggle_slide_orders_details').slideDown(100);
    });
    $('.slide_up_orders').click(function(){
        $(this).parents('.one_wrap_orders').removeClass('hide');
        $(this).parent('.toggle_slide_orders_details').slideUp(100);
    });
    
    $('.click_more_about p').click(function(){
        $(this).toggleClass('rotate');
        var thisTextOpen = $(this).attr('data-text-open');
        var thisTextClose = $(this).attr('data-text-close');
        if($(this).hasClass('rotate')){
            $(this).children('span').text(thisTextClose);
        } else {
            $(this).children('span').text(thisTextOpen);
        }
        console.log($('.toggle_block_more').html());
        $(this).parent().siblings('.table_product_comparement').children('.toggle_block_more').slideToggle(100);
        
    }); 

     $('.line_radio_toggle input').change(function(){
        var thisParentIndex = $(this).parent().index();
        $('.toggle_forms form').hide().eq(thisParentIndex).show();
    });
    $('.top_click_settings input').change(function(){
        var thisParentIndex = $(this).parent().index() - 1;
        $(this).parents('.top_click_settings').siblings('.change_delivery_type').children('.delivery_self').hide().eq(thisParentIndex).show();
    });
    $('.detail').on('click', function(e) {
        $(this).parent().parent().find('.order_hidden').slideToggle(300);
    });
    $('.hidden').on('click', function(e) {
        $(this).parent().slideToggle(300);
    });
    $('.select_block').on('click', function() {
        $(this).find('.dropdown_menu').slideToggle(300);
    });
    $('.dropdown_menu').on('click', 'li', function() {
        $(this).parent().parent().find('.select_value').html($(this).html());
    });

    function category () {
        if($(window).width() < 969) {
            $('.a_side_category').on('click', function() {
                $(this).find('.link_cats_a_side').slideToggle(300);
                $(this).find('.all_category_link').toggleClass('active');
            });
        }
    };
    category();
    // $('.rating_options.stars').on('click', 'label', function (e) {
    //     e.preventDefault();
    //     var previos = $(e.target).prevAll('input');
    //     $(e.target).parent().each(function (index, elem) {
    //         $($(elem).find('input')).removeClass('active');
    //         $($(elem).find('input')).removeAttr('checked');
    //     });
    //     $(e.target).prev().attr('checked', 'true');
    //     previos.addClass('active');
    // });
    $('.filter_button').on('click', function() {
        $('.catalog_filter_line .left_filter').slideDown(200);
        $('.content').addClass('show_overlay');
    });
    $('.closed_filter').on('click', function() {
        $('.catalog_filter_line .left_filter').slideUp(200);
        $('.content').removeClass('show_overlay');
    });
    $('.mobile_menu').on('click', function() {
        $('.mobile_menu_hidden_block').fadeIn(100);
    });
    $('.close_mobile').on('click', function() {
        $('.mobile_menu_hidden_block').fadeOut(100);
    });
    $('.stats_button').on('click', function() {
        $(this).parent().next().slideToggle(300);
        $(this).toggleClass('active');
        if($(this).hasClass('active')) {
            $(this).find('span').text('Свернуть');
        }
        else{
            $(this).find('span').text('Подробнее');
        }
    });
    (function($) {
        //ripple-effect
        var parent, ink, d, x, y;
        $(".ripple").click(function(e){
            parent = $(this).parent();

            //создаём .ink элемент, если еще не создан
            if (parent.find(".ink").length == 0)
                parent.prepend("<span class='ink'></span>");

            ink = parent.find(".ink");
            //сбрасываем анимацию
            ink.removeClass("animate");

            //рассчитываем размеры .ink элемента
            //они определяются размерами родительского контейнера
            if(!ink.height() && !ink.width())
            {
                d = Math.max(parent.outerWidth(), parent.outerHeight());
                ink.css({height: d, width: d});
            }

            //получим начальные координаты,
            //центр .ink нужно поместить в точку клика
            x = ink.width()/50;
            y = ink.height()/50;

            //установим координаты и запустим анимацию
            ink.css({top: y+'px', left: x+'px'}).addClass("animate");
        })
    })(jQuery);
     $('.select_mounth p').on('click', function () {
        $(this).next().slideToggle(300);
    });
    $('.select_mounth ul li').on('click', function () {
        $('.select_mounth p').text($(this).text());
        $('.select_val').val($(this).attr('data-val'));
        $('.select_mounth ul').slideUp(300);
    });
    if ($(window).width() < 641) {
        $('.menu_search_link_header .search_form button.search_1').on('click', function (e) {
            e.preventDefault();
            $('.search_2').fadeToggle(300);
            $('.search_form input[name="search"]').fadeToggle(300);
        });
    }

    setInterval(function(){
        $('.alert-success').fadeOut(300);
    }, 5000);

    setInterval(function(){
        $('.alert-danger').fadeOut(300);
    }, 5000);
});
    