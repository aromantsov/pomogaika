var compare = {
	'add': function(product_id) {
		$.ajax({
			url: '/addcompare',
			type: 'post',
			data: {
				'product_id': product_id,
			},
			dataType: 'json',
			success: function(json) {
				// $('.alert-dismissible').remove();

				if (json['success']) {
				   $('.alert-success').css('display', 'block').html(json['success']); 

				// 	$('#compare-total').html(json['total']);

				// 	$('html, body').animate({ scrollTop: 0 }, 'slow');
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	},
	'remove': function() {

	}
}