<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['prefix' => App\Http\Middleware\LocaleMiddleware::getLocale()], function(){

    Route::group(['prefix'=>'admin', 'namespace'=>'Admin', 'middleware'=>['admin']], function(){
      Route::get('/', 'DashboardController@dashboard')->name('admin.index');
      Route::resource('/category', 'CategoryController', ['as'=>'admin']);
      Route::resource('/product', 'ProductController', ['as'=>'admin']);
      Route::resource('/attribute', 'AttributeController', ['as'=>'admin']);
      Route::resource('/tag', 'TagController', ['as'=>'admin']);
      Route::resource('/news', 'NewsController', ['as'=>'admin']);
      Route::resource('/newsimage', 'NewsimageController', ['as'=>'admin']);
      Route::resource('/orders', 'OrderController', ['as'=>'admin']);
      Route::resource('/manufacturers', 'ManufacturerController', ['as'=>'admin']);
      Route::resource('/info', 'InfoController', ['as'=>'admin']);
      Route::resource('/review', 'ReviewController', ['as'=>'admin']);
      Route::resource('/vendorreview', 'VendorReviewController', ['as'=>'admin']);
      Route::resource('/callback', 'CallbackController', ['as'=>'admin']);
      Route::post('/product/offer', 'ProductController@saveOffer')->name('admin.product.offer');
      Route::post('/product/deleteimage', 'ProductController@deleteImage')->name('admin.product.deleteimage');
      Route::post('/product/deletepicture', 'ProductController@deletePicture')->name('admin.product.deletepicture');
      Route::post('/product/autocomplete', 'ProductController@autocomplete')->name('admin.product.autocomplete');
      Route::post('/product/autocomplete2', 'ProductController@autocomplete2')->name('admin.product.autocomplete2');
      Route::put('/property/saveproperty', 'PropertyController@saveProperty')->name('admin.property.saveproperty'); 
      Route::post('/category/deleteicon', 'CategoryController@deleteIcon')->name('admin.category.deleteicon');
      Route::post('/category/deleteimage', 'CategoryController@deleteImage')->name('admin.category.deleteimage');
      Route::post('/news/deletepicture', 'NewsController@deletePicture')->name('admin.news.deletepicture');
      Route::post('/order/savepurchase', 'OrderController@savePurchase')->name('admin.order.savepurchase');
      Route::post('/order/autocomplete', 'OrderController@autocomplete')->name('admin.order.autocomplete');
      Route::post('/order/savepurchase', 'OrderController@savePurchase')->name('admin.order.savepurchase');
      Route::post('/order/setprice', 'OrderController@setPrice');

      Route::get('/import', 'ImportController@index')->name('import');
      Route::post('/upload', 'ImportController@upload')->name('admin.upload');

      Route::group(['prefix'=>'user_management', 'namespace'=>'UserManagement'], function(){
        Route::resource('/user', 'UserController', ['as'=>'admin.user_management']);
        Route::resource('/role', 'UserRoleController', ['as'=>'admin.user_management']);
        Route::resource('/orderplan', 'OrdersPlanController', ['as'=>'admin.user_management']);
        Route::resource('/adv', 'AdvController', ['as'=>'admin.user_management']);
      });
    });

    Route::get('/', 'WelcomeController@main')->name('main');
    Route::post('/uploadfile', 'ProductController@upload')->name('upload');
    Route::post('/addtowishlist', 'ProductController@add_wishlist');
    Route::post('/addcompare', 'ProductController@add_compare');
    Route::get('/compare', 'ProductController@compare')->name('compare');
    Route::get('/removecompare/{id}', 'ProductController@removeCompare');
    Route::get('/search/product', 'ProductController@search')->name('search.product');
    
    Route::get('/cart', 'CartController@index')->name('cart');
    Route::post('/add_to_cart', 'CartController@add')->name('add_to_cart');
    Route::post('/map', 'CartController@map');
    Route::delete('/remove_from_cart/{cart}', 'CartController@remove');
    Route::put('/update_cart/{cart}', 'CartController@update');
    Route::post('/setshipping/{vendor}', 'CartController@setShippingMethod');
    Route::get('/preorder', 'CartController@preorder')->name('preorder');
    Route::post('/setorder', 'OrderController@setOrder');
    Route::get('/success/{id}', 'OrderController@confirm');
    Route::get('/order_info/{id}', 'OrderController@info')->name('order_info');
    Route::post('/shopaddresses', 'OrderController@getDelivery');
    Route::post('/costdelivery', 'OrderController@costDelivery');
    Route::post('/sessioncost', 'OrderController@sessionCost');
    Route::post('/mincost', 'OrderController@mincost');

    Auth::routes();

    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/homeedit', 'HomeController@edit')->name('edithome');
    Route::put('/homeedit/{user}', 'HomeController@update')->name('updatehome');
    Route::post('/changepassword', 'HomeController@changePassword')->name('changepassword');
    Route::put('/homeeditvendor/{user}', 'HomeController@updatevendor')->name('updatehomevendor');
    Route::post('/orderplan', 'HomeController@orderPlan');
    Route::post('/orderadv', 'HomeController@orderAdv')->name('orderadv');
    Route::put('/selectstatus', 'HomeController@selectStatus')->name('selectstatus');
    Route::post('/deleteavatar', 'HomeController@deleteAvatar');
    Route::post('/addvendorreview', 'HomeController@addReview')->name('addvendorreview');
    Route::delete('/deletevendorreview', 'HomeController@deleteVendorReview');
    Route::delete('/deleteproductreview', 'HomeController@deleteProductReview');
    Route::get('/cron', 'HomeController@cron');
    Route::delete('/removeshop', 'HomeController@removeShop');
    Route::post('/addshop', 'HomeController@addShop');
    Route::post('/adddelivery', 'HomeController@addDelivery');
    Route::put('/activeshop', 'HomeController@setActiveShop');
    Route::delete('/removedelivery', 'HomeController@removeDelivery');
    Route::post('/sendactivate', 'HomeController@sendMessage')->name('sendactivate');
    Route::get('/categories', 'CategoryController@index')->name('categories');
    Route::get('/filters/{id}', 'CategoryController@filters')->name('filters');
    Route::get('/category/{id}', 'CategoryController@show')->name('category');
    Route::get('/product/{id}', 'ProductController@show')->name('product');
    Route::post('/addproduct', 'ProductController@toModeration')->name('addproduct');
    Route::get('/is_sale', 'ProductController@is_sale')->name('is_sale');
    Route::get('/by_vendor/{id}', 'ProductController@byVendor')->name('by_vendor');

    Route::post('/addreview', 'ReviewController@add')->name('addreview');
    Route::post('/vlike', 'ReviewController@v_like');
    Route::post('/vdislike', 'ReviewController@v_dislike');
    Route::post('/plike', 'ReviewController@p_like');
    Route::post('/pdislike', 'ReviewController@p_dislike');

    Route::get('/newslist', 'NewsController@index')->name('newslist');
    Route::get('/news/{id}', 'NewsController@show')->name('news');
    Route::get('/tag/{id}', 'NewsController@tag')->name('tag');
    Route::get('/search/news', 'NewsController@search')->name('searchnews');

    Route::get('/charity', 'CharityController@index')->name('charity');

    Route::get('/info/{id}', 'InfoController@index')->name('info');
    Route::get('/contact', 'InfoController@contact')->name('contact');
    Route::post('/callback', 'InfoController@callback')->name('callback');

    Route::get('/api_login', 'ApiController@login');
    Route::post('/api_register', 'ApiController@register');
    Route::get('/api_products/{id}', 'ApiController@getProducts');
    Route::post('/api_result', 'ApiController@result');
    Route::post('/api_reset_password', 'ApiController@resetPassword');
    Route::post('/api_update_password', 'ApiController@updatePassword');

    Route::resource('/photos', 'PhotostockController');
});


Route::get('setlocale/{lang}', function ($lang) {

    $referer = Redirect::back()->getTargetUrl();; //URL предыдущей страницы
    $parse_url = parse_url($referer, PHP_URL_PATH); //URI предыдущей страницы

    //разбиваем на массив по разделителю
    $segments = explode('/', $parse_url);

    //Если URL (где нажали на переключение языка) содержал корректную метку языка
    if (in_array($segments[1], App\Http\Middleware\LocaleMiddleware::$languages)) {

        unset($segments[1]); //удаляем метку
    } 
    
    //Добавляем метку языка в URL (если выбран не язык по-умолчанию)
    if($lang != 'uk'){
      array_splice($segments, 1, 0, $lang); 
    }
    

    //формируем полный URL
    $url = Request::root().implode("/", $segments);
    
    //если были еще GET-параметры - добавляем их
    if(parse_url($referer, PHP_URL_QUERY)){
        $url = $url.'?'. parse_url($referer, PHP_URL_QUERY);
    }
    return redirect($url); //Перенаправляем назад на ту же страницу                            

})->name('setlocale');

