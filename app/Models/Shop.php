<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Shop extends Model
{
    protected $guarded = [];

    public function users()
    {
    	return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function deliveries()
    { 
        return $this->hasMany(Delivery::class);
    }
}
