<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Review extends Model
{
    protected $guarded = [];

    public function products()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }

    public function users()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public static function getReviews($product_id)
    {
        return Review::where('product_id', $product_id)->get();
    }

    public static function getReviewsByUser($user_id)
    {
        $product_data = json_decode(json_encode(Review::with('products')->where('user_id', $user_id)->groupBy('product_id')->get(), JSON_UNESCAPED_UNICODE), true);

        $products = [];

        foreach($product_data as $product){

            $reviews = Review::where('user_id', $user_id)->where('product_id', $product['product_id'])->get();
            $product['reviews'] = $reviews;
            $products[] = $product; 
        }

        return $products;
    }
}
