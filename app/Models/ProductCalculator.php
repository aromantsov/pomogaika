<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductCalculator extends Model
{

    protected $table = 'product_calculator';

    protected $guarded = [];

    public function products()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }
}
