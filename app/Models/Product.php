<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use App\User;
use App\Models\Offer;
use DB;

class Product extends Model
{

    protected $guarded = [];

    public function categories()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    public function scopeLatestproducts($query, $count)
    {
    	return $query->orderBy('created_at', 'desc')->take($count)->get();
    }

    public function scopePublished($query, $published)
    {
        return $query->where('published', $published)->take(5);
    }

    public function orderByCreated($query)
    {
        return $query->orderBy('created_at');
    }

    public function offers()
    {
        return $this->hasMany(Offer::class);
    }

    public function pictures()
    {
        return $this->hasMany(Picture::class);
    }

    public function properties()
    {
        return $this->hasMany(Property::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public static function getProductsFromWishlistByCategory($cat_id, $user_id)
    {
        $products = Product::select('*', 'product_user.product_id as product_id')->leftJoin('product_user', 'product_user.product_id', '=', 'products.id')->where('products.category_id', $cat_id)->where('product_user.user_id', $user_id)->where('products.published', 1)->get();

        $products_data = [];
        foreach($products as $product){
            $product = json_decode(json_encode($product, JSON_UNESCAPED_UNICODE) , true);
            $product['rating'] = Review::where('product_id', $product['id'])->avg('rating');
            $product['reviews'] = Review::where('product_id', $product['product_id'])->count();
            $product['price'] = Offer::select('price')->where('product_id', $product['product_id'])->avg('price');
            $products_data[] = $product;
        }
        return $products_data;
    }

    public function setPrice($product_id)
    {
        $offers = Offer::where('product_id', $product_id)->get();

        $sum_price = 0;
        $sum_trade_price = 0;

        foreach($offers as $offer){
            $sum_price += $offer->price;
            $sum_trade_price += $offer->trade_price;;
        }

        $price = $sum_price / count($offers);
        $trade_price = $sum_trade_price / count($offers);

        Product::update(['price' => $price, 'trade_price' => $trade_price]);
    }

    public function reviews()
    {
        return $this->hasMany(Review::class);
    }

    public function manufacturers()
    {
        return $this->belongsTo(Manufacturer::class, 'manufacturer_id', 'id');
    }

    public static function getProducts($filter, $category_id, $min_price, $max_price, $sort_by)
    {
        if($filter['manufacturer']){
            $manufacturer_array = $filter['manufacturer'];
        }else{
            $manufacturer_array = [];
            $manufacturers = Product::select('manufacturer_id')->where('category_id', $category_id)->distinct()->get();

            foreach($manufacturers as $manufacturer){
                $manufacturer_array[] = $manufacturer->manufacturer_id;
            }
        }


        if($filter['vendor']){
            $vendor_array = $filter['vendor'];
        }else{
            $vendor_array = [];
            $vendors = Product::join('offers', 'offers.product_id', '=', 'products.id')->select('offers.user_id')->where('products.category_id', $category_id)->where('products.published', 1)->distinct()->get();

            foreach($vendors as $vendor){
                $vendor_array[] = $vendor->user_id;
            }
        }

        if(!$filter['from_price']){
            $filter['from_price'] = 0;
        }

        if(!$filter['to_price']){
            $filter['to_price'] = 0;
        }

        if($sort_by == 'popular'){
            $products = DB::select(sprintf("SELECT *, p.id AS id, p.price as price, SUM(pr.quantity) as quantity FROM products p LEFT JOIN offers o ON (p.id = o.product_id) LEFT JOIN purchases pr ON (pr.offer_id = o.id) WHERE category_id = %d AND p.price <= %d AND p.price >= %d AND p.manufacturer_id IN (" . implode(', ', $manufacturer_array) . ") AND o.user_id IN (" . implode(', ', $vendor_array) . ") GROUP BY o.product_id ORDER BY quantity DESC", $category_id, $max_price, $min_price));
            //$products = Product::select('*', 'products.price as price', 'products.id as id')->leftJoin('offers', 'offers.product_id', '=', 'products.id')->where('products.category_id', $category_id)->where('products.price', '>=', $min_price)->where('products.price', '<=', $max_price)->whereIn('products.manufacturer_id', $manufacturer_array)->whereIn('offers.user_id', $vendor_array)->groupBy('offers.product_id')->orderBy('products.popular', 'desc')->get();
        }elseif($sort_by == 'price_asc'){
            $products = Product::select('*', 'products.price as price', 'products.id as id')->leftJoin('offers', 'offers.product_id', '=', 'products.id')->where('products.category_id', $category_id)->where('products.price', '>=', $min_price)->where('products.price', '<=', $max_price)->whereIn('products.manufacturer_id', $manufacturer_array)->whereIn('offers.user_id', $vendor_array)->groupBy('offers.product_id')->orderBy('products.price', 'asc')->get();
        }elseif($sort_by == 'price_desc'){
            $products = Product::select('*', 'products.price as price', 'products.id as id')->leftJoin('offers', 'offers.product_id', '=', 'products.id')->where('products.category_id', $category_id)->where('products.price', '>=', $min_price)->where('products.price', '<=', $max_price)->whereIn('products.manufacturer_id', $manufacturer_array)->whereIn('offers.user_id', $vendor_array)->groupBy('offers.product_id')->orderBy('products.price', 'desc')->get();
        }else{
            $products = Product::select('*', 'products.price as price', 'products.id as id')->leftJoin('offers', 'offers.product_id', '=', 'products.id')->where('products.category_id', $category_id)->where('products.price', '>=', $min_price)->where('products.price', '<=', $max_price)->whereIn('products.manufacturer_id', $manufacturer_array)->whereIn('offers.user_id', $vendor_array)->groupBy('offers.product_id')->orderBy('products.price', 'asc')->get();
        } 

        $products_data = [];
        foreach($products as $product){
            $product = json_decode(json_encode($product, JSON_UNESCAPED_UNICODE) , true);
            $product['rating'] = Review::where('product_id', $product['id'])->avg('rating');
            $product['reviews'] = Review::where('product_id', $product['id'])->count();
            $product['price'] = Offer::select('price')->where('product_id', $product['id'])->avg('price');
            $products_data[] = $product;
        }
        return $products_data;     
    }

    public static function getSales()
    {
        $products = Product::orderBy('created_at', 'desc')->where('published', 1)->where('is_sale', 1)->limit(8)->get();

        $products_data = [];
        foreach($products as $product){
            $product = json_decode(json_encode($product, JSON_UNESCAPED_UNICODE) , true);
            $product['rating'] = Review::where('product_id', $product['id'])->avg('rating');
            $product['reviews'] = Review::where('product_id', $product['id'])->count();
            $product['price'] = Offer::select('price')->where('product_id', $product['id'])->avg('price');
            $products_data[] = $product;
        }
        return $products_data;
    }

    public static function getCategoryProducts($id, $sort_by)
    {
        if($sort_by == 'popular'){
            $products = DB::select(sprintf("SELECT *, p.id AS id, p.price as price, SUM(pr.quantity) as quantity FROM products p LEFT JOIN offers o ON (p.id = o.product_id) LEFT JOIN purchases pr ON (pr.offer_id = o.id) WHERE category_id = %d AND published = %d GROUP BY o.product_id ORDER BY quantity DESC", $id, 1));
        }elseif($sort_by == 'price_asc'){
            $products = Product::where('category_id', $id)->where('published', 1)->orderBy('price', 'asc')->get();
        }elseif($sort_by == 'price_desc'){
            $products = Product::where('category_id', $id)->where('published', 1)->orderBy('price', 'desc')->get();
        }else{
            $products = Product::where('category_id', $id)->where('published', 1)->orderBy('price', 'asc')->get();
        }    
        
        $products_data = [];
        foreach($products as $product){
            $product = json_decode(json_encode($product, JSON_UNESCAPED_UNICODE) , true);
            $product['rating'] = Review::where('product_id', $product['id'])->avg('rating');
            $product['reviews'] = Review::where('product_id', $product['id'])->count();
            $product['price'] = Offer::select('price')->where('product_id', $product['id'])->avg('price');
            $products_data[] = $product;
        }
        return $products_data;
    }

    public static function getSearchProducts($search)
    {
        $products = Product::where('name_ru', 'like', '%' . $search . '%')->orWhere('name_uk', 'like', '%' . $search . '%')->where('published', 1)->get();
        $products_data = [];
        foreach($products as $product){
            $product = json_decode(json_encode($product, JSON_UNESCAPED_UNICODE) , true);
            $product['rating'] = Review::where('product_id', $product['id'])->avg('rating');
            $product['reviews'] = Review::where('product_id', $product['id'])->count();
            $product['price'] = Offer::select('price')->where('product_id', $product['id'])->avg('price');
            $products_data[] = $product;
        }
        return $products_data;
    }

    public static function getSearchProductsWithSort($search, $sort)
    {
        if($sort == 'price_asc'){
            $products = Product::where('name_ru', 'like', '%' . $search . '%')->orWhere('name_uk', 'like', '%' . $search . '%')->where('published', 1)->orderBy('price', 'asc')->get();
            $products_data = [];
            foreach($products as $product){
                $product = json_decode(json_encode($product, JSON_UNESCAPED_UNICODE) , true);
                $product['rating'] = Review::where('product_id', $product['id'])->avg('rating');
                $product['reviews'] = Review::where('product_id', $product['id'])->count();
                //$product['price'] = Offer::select('price')->where('product_id', $product['id'])->avg('price');
                $products_data[] = $product;
            }
            return $products_data;
        }elseif($sort == 'price_desc'){
            $products = Product::where('name_ru', 'like', '%' . $search . '%')->orWhere('name_uk', 'like', '%' . $search . '%')->where('published', 1)->orderBy('price', 'desc')->get();
            $products_data = [];
            foreach($products as $product){
                $product = json_decode(json_encode($product, JSON_UNESCAPED_UNICODE) , true);
                $product['rating'] = Review::where('product_id', $product['id'])->avg('rating');
                $product['reviews'] = Review::where('product_id', $product['id'])->count();
                //$product['price'] = Offer::select('price')->where('product_id', $product['id'])->avg('price');
                $products_data[] = $product;
            }
            return $products_data;
        }elseif($sort == 'popular'){
            $products = DB::select(sprintf("SELECT *, p.id AS id, p.price as price, SUM(pr.quantity) as quantity FROM products p LEFT JOIN offers o ON (p.id = o.product_id) LEFT JOIN purchases pr ON (pr.offer_id = o.id) WHERE p.name_ru LIKE '%s' OR p.name_uk LIKE '%s' GROUP BY o.product_id ORDER BY quantity DESC", '%' . $search . '%', '%' . $search . '%'));
            $products_data = [];
            foreach($products as $product){
                $product = json_decode(json_encode($product, JSON_UNESCAPED_UNICODE) , true);
                $product['rating'] = Review::where('product_id', $product['id'])->avg('rating');
                $product['reviews'] = Review::where('product_id', $product['id'])->count();
                //$product['price'] = Offer::select('price')->where('product_id', $product['id'])->avg('price');
                $products_data[] = $product;
            }
            return $products_data; 
        }elseif($sort == 'rating'){
            $products = DB::select(sprintf("SELECT *, p.id AS id, AVG(r.rating) AS rating, p.price as price FROM products p LEFT JOIN reviews r ON (p.id = r.product_id)  WHERE p.name_ru LIKE '%s' OR p.name_uk LIKE '%s' GROUP BY p.id ORDER BY AVG(r.rating) DESC", '%' . $search . '%', '%' . $search . '%'));
            $products_data = []; 
            foreach($products as $product){
                $product = json_decode(json_encode($product, JSON_UNESCAPED_UNICODE) , true);
                //$product['rating'] = Review::where('product_id', $product['id'])->avg('rating');
                $product['reviews'] = Review::where('product_id', $product['id'])->count();
                //$product['price'] = Offer::select('price')->where('product_id', $product['id'])->avg('price');
                $products_data[] = $product;
            }
            return $products_data;
        }elseif($sort == 'created_at'){
            $products = Product::where('name_ru', 'like', '%' . $search . '%')->orWhere('name_uk', 'like', '%' . $search . '%')->where('published', 1)->orderBy('created_at', 'desc')->get();
            $products_data = []; 
            foreach($products as $product){
                $product = json_decode(json_encode($product, JSON_UNESCAPED_UNICODE) , true);
                $product['rating'] = Review::where('product_id', $product['id'])->avg('rating');
                $product['reviews'] = Review::where('product_id', $product['id'])->count();
                //$product['price'] = Offer::select('price')->where('product_id', $product['id'])->avg('price');
                $products_data[] = $product;
            }
            return $products_data;             
        }else{
            return self::getSearchProducts($search);
        }
    }

    public static function allGetSales()
    {
        $products = Product::orderBy('created_at', 'desc')->where('published', 1)->where('is_sale', 1)->get();

        $products_data = [];
        foreach($products as $product){
            $product = json_decode(json_encode($product, JSON_UNESCAPED_UNICODE) , true);
            $product['rating'] = Review::where('product_id', $product['id'])->avg('rating');
            $product['reviews'] = Review::where('product_id', $product['id'])->count();
            $product['price'] = Offer::select('price')->where('product_id', $product['id'])->avg('price');
            $products_data[] = $product;
        }
        return $products_data;
    }

    public function calculators()
    {
        return $this->hasMany(ProductCalculator::class);
    }

    public static function getNewProducts()
    {
        $products = Product::orderBy('created_at', 'desc')->where('published', 1)->where('is_new', 1)->limit(14)->get();

        foreach($products as $product){

            $product->price = Offer::select('price')->where('product_id', $product->id)->avg('price');

            $new_products[] = $product;
        }

        return $new_products;
    }
}
