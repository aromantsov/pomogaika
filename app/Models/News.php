<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = 'news';

    protected $guarded = [];

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public function similar()
    {
        return $this->belongsToMany(News::class, 'news_similar', 'news_id', 'similar_id');
    }
}
