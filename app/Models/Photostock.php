<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Photostock extends Model
{
    protected $table = 'photostock';
    
    protected $guarded = [];
    
    public function users()
    {
    	return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
