<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    protected $guarded = [];

    public function products()
    {
    	return $this->belongsTo(Product::class, 'product_id', 'id');
    }

    public function attributes()
    {
    	return $this->belongsTo(Attribute::class, 'attribute_id', 'id');
    }
}
