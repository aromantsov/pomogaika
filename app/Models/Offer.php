<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Purchase;
use App\User;
use DB;

class Offer extends Model
{
    protected $guarded = [];

    public function products()
    {
    	return $this->belongsTo(Product::class, 'product_id', 'id');
    }

    public function users()
    {
    	return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public static function getOffer($cart, $vendor_id)
    {
    	$offers = DB::select(sprintf("SELECT *, of.price AS price FROM offers of LEFT JOIN products p ON (of.product_id = p.id) LEFT JOIN cart c ON (c.offer_id = of.id) WHERE of.user_id = %d AND c.session_id = '%s'", $vendor_id, $cart));
    	return $offers;
    }

    public function purchases()
    {
        return $this->hasMany(Purchase::class, 'user_id', 'id');
    }

    public static function getProductOffers($product_id, $sort_by)
    {
        if($sort_by == 'popular'){
            $offers = Offer::where('product_id', $product_id)->get();
        }elseif($sort_by == 'price_asc'){
            $offers = Offer::where('product_id', $product_id)->orderBy('price', 'asc')->get();
        }elseif($sort_by == 'price_desc'){
            $offers = Offer::where('product_id', $product_id)->orderBy('price', 'desc')->get();
        }else{
            $offers = Offer::where('product_id', $product_id)->get();
        }
        return $offers;
    }

    public static function productsByVendor($vendor_id)
    {
        $products = Offer::select('*', 'offers.id as offer_id')->join('products', 'offers.product_id', '=', 'products.id')->where('offers.user_id', $vendor_id)->get();
        $products_data = [];

        foreach($products as $product){
            $product = json_decode(json_encode($product, JSON_UNESCAPED_UNICODE) , true);
            $product['rating'] = Review::where('product_id', $product['id'])->avg('rating');
            $product['reviews'] = Review::where('product_id', $product['id'])->count();
            $products_data[] = $product;
        }

        return $products_data;
    }

}
