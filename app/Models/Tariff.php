<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tariff extends Model
{
    protected $guarded = [];

    public function plans()
    {
    	$this->hasMany(Plan::class);
    }
}
