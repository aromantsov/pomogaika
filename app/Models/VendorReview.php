<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class VendorReview extends Model
{
    protected $guarded = [];

    public function vendors()
    {
    	return $this->belongsTo(User::class, 'vendor_id', 'id');
    }

    public function users()
    {
    	return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public static function getReviewsByUser($user_id)
    {
    	$vendors_data = json_decode(json_encode(VendorReview::with('vendors')->where('user_id', $user_id)->groupBy('vendor_id')->get(), JSON_UNESCAPED_UNICODE), true);

    	$vendors = [];

    	foreach($vendors_data as $vendor){

    		$reviews = VendorReview::where('user_id', $user_id)->where('vendor_id', $vendor['vendor_id'])->get();
    		$vendor['reviews'] = $reviews;
    		$vendors[] = $vendor; 
    	}

    	return $vendors;
    }
}
