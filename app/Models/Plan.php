<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Plan extends Model
{
    protected $guarded = [];

    public function tariffs()
    {
    	return $this->belongsTo(Tariff::class, 'tariff_id', 'id');
    }

    public function users()
    {
    	return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
