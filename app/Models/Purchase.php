<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    protected $guarded = [];

    public function orders()
    {
    	return $this->belongsTo(Order::class, 'order_id', 'id');
    }

    public function offers()
    {
    	return $this->belongsTo(Offer::class, 'offer_id', 'id');
    }
}
