<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['title_ru', 'title_uk', 'parent_id', 'published', 'created_by', 'modified_by'];

    public function children()
    {
    	return $this->hasMany(self::class, 'parent_id');
    }

    public function scopeLatestCategories($query, $count)
    {
    	return $query->orderBy('created_at', 'desc')->take($count)->get();
    }

    public function products()
    {
      return $this->hasMany(Product::class);
    }
}
