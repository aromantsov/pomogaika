<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Cart extends Model
{
    protected $table = 'cart';
    protected $guarded = [];

    public static function countOffers()
    {
        $count = 0;

    	$cart = Cart::where('session_id', session('cart'))->get();

    	foreach($cart as $offer){
    		$count += $offer->quantity;
    	}

    	return $count;
    }

    public function users()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
