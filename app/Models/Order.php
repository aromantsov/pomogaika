<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Purchase;
use App\Models\Offer;

use App\User;
use DB;

class Order extends Model
{
    protected $guarded = [];

    public function purchases(){
    	$this->hasMany(Purchase::class);
    }

    public function users(){
    	$this->belongsTo(User::class, 'user_id', 'id');
    }

    public static function getOrders($user_id)
    {
    	$orders = json_decode(json_encode(Order::select('*', 'orders.id as order_id', 'orders.created_at as order_time')->leftJoin('order_statuses', 'orders.order_status_id', '=', 'order_statuses.id')->where('orders.user_id', $user_id)->orderBy('orders.id', 'desc')->get(), JSON_UNESCAPED_UNICODE), true);

        for($i = 0; $i < count($orders); $i++){
            $orders[$i]['vendors'] = json_decode(json_encode(DB::table('users')->join('offers', 'offers.user_id', '=', 'users.id')->join('purchases', 'purchases.offer_id', '=', 'offers.id')->where('purchases.order_id', $orders[$i]['order_id'])->groupBy('users.id')->get(), JSON_UNESCAPED_UNICODE), true);


            for($j = 0; $j < count($orders[$i]['vendors']); $j++){
                $orders[$i]['vendors'][$j]['products'] = Purchase::select('*', 'purchases.total as total')->leftJoin('offers', 'purchases.offer_id', '=', 'offers.id')->join('orders', 'purchases.order_id', '=', 'orders.id')->join('products', 'products.id', '=', 'offers.product_id')->where('orders.id', $orders[$i]['order_id'])->where('offers.user_id', $orders[$i]['vendors'][$j]['user_id'])->get();
            }
        }
        return $orders;
    }

    public static function getActualOrders($user_id)
    {
        $orders = json_decode(json_encode(Order::select('*', 'orders.id as order_id', 'orders.created_at as order_time')->leftJoin('order_statuses', 'orders.order_status_id', '=', 'order_statuses.id')->where('orders.user_id', $user_id)->where('orders.order_status_id', '!=', 10)->orderBy('orders.id', 'desc')->get(), JSON_UNESCAPED_UNICODE), true);

        for($i = 0; $i < count($orders); $i++){
            $orders[$i]['vendors'] = json_decode(json_encode(DB::table('users')->join('offers', 'offers.user_id', '=', 'users.id')->join('purchases', 'purchases.offer_id', '=', 'offers.id')->where('purchases.order_id', $orders[$i]['order_id'])->groupBy('users.id')->get(), JSON_UNESCAPED_UNICODE), true);


            for($j = 0; $j < count($orders[$i]['vendors']); $j++){
                $orders[$i]['vendors'][$j]['products'] = Purchase::select('*', 'purchases.total as total')->leftJoin('offers', 'purchases.offer_id', '=', 'offers.id')->join('orders', 'purchases.order_id', '=', 'orders.id')->join('products', 'products.id', '=', 'offers.product_id')->where('orders.id', $orders[$i]['order_id'])->where('offers.user_id', $orders[$i]['vendors'][$j]['user_id'])->get();
            }
        }
        return $orders;
    }

    public static function getOrdersByVendor($vendor_id)
    {
    	$orders = json_decode(json_encode(Order::select('*', 'orders.id as order_number', 'orders.created_at as order_time', 'orders.total as total')->leftJoin('order_statuses', 'orders.order_status_id', '=', 'order_statuses.id')->join('purchases', 'purchases.order_id', '=', 'orders.id')->join('offers', 'offers.id', '=', 'purchases.offer_id')->where('offers.user_id', $vendor_id)->groupBy('orders.id')->orderBy('order_id', 'desc')->get(), JSON_UNESCAPED_UNICODE), true);

    	for($i = 0; $i < count($orders); $i++){
    		$orders[$i]['products'] = Product::select('*', 'purchases.total as total')->leftJoin('offers', 'products.id', '=', 'offers.product_id')->join('purchases', 'offers.id', '=', 'purchases.offer_id')->join('orders', 'purchases.order_id', '=', 'orders.id')->where('orders.id', $orders[$i]['order_id'])->where('offers.user_id', $vendor_id)->get();
    	}
    	return $orders;
    }

    public function orderStatuses()
    {
        $this->belongsTo(OrderStatus::class, 'order_status_id', 'id');
    }

    public function offers()
    {
        return $this->hasManyThrough(Offer::class, Purchase::class);
    }

    public static function getOrderVendor($order_id)
    {
        $purchase = Purchase::where('order_id', $order_id)->first();

        $offer_id = $purchase->offer_id ?? 0;

        $offer = Offer::find($offer_id);

        return $offer->user_id ?? 0;
    }
}
