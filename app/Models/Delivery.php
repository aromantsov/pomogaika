<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Delivery extends Model
{
    protected $table = 'delivery';

    protected $guarded = [];

    public function shops()
    {
    	return $this->belongsTo(Shop::class, 'shop_id', 'id');
    }
}
