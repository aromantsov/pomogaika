<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    protected $guarded = [];

    public function prorerties()
    {
        return $this->hasMany(Property::class);
    }
}
