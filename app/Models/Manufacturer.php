<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Manufacturer extends Model
{
    protected $guarded = [];

    public function products()
    {
    	return $this->hasMany(Product::class);
    }

    public static function getFilterManufacturers($category_id)
    {
        return Manufacturer::select('manufacturers.id', 'manufacturers.brand')->join('products', 'products.manufacturer_id', '=', 'manufacturers.id')->where('products.category_id', $category_id)->distinct()->get();
    }
}
