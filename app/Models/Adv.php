<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Adv extends Model
{
    protected $table = 'adv';

    protected $guarded = [];

    public function users()
    {
    	return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
