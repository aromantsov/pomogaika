<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\Property;

class IsValidProperty implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    private $product_id;
    private $attribute_id;
     
    public function __construct($product_id, $attribute_id)
    {
        $this->product_id = $product_id;
        $this->attribute_id = $attribute_id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $other_properties = Property::where('product_id', $this->product_id)->where('attribute_id', $this->attribute_id)->first();
        
        if(!$other_properties){
            return true;
        }
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Данная характеристика уже существует';
    }
}