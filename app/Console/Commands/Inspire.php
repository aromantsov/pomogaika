<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Inspire extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'inspire';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $plans = Plan::where('status', 'active')->get();

            foreach($plans as $plan){
                if(Carbon::now() > $plan->term){
                    $plan->tariff_id = 1;
                    $plan->term = NULL;
                    $plan->save();
                }
            }

            $advs = Adv::where('status', 'active')->get();

            foreach($advs as $adv){
                if(Carbon::now() > $adv->term){
                    $adv->delete();
                }
            }
    }
}
