<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Carbon\Carbon;
use App\Models\Plan;
use App\Models\Adv;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\Inspire::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //$schedule->call(function () {
            $plans = Plan::where('status', 'active')->get();

            foreach($plans as $plan){
                if(Carbon::now() > $plan->term){
                    $plan->tariff_id = 1;
                    $plan->term = NULL;
                    $plan->save();
                }
            }

            $advs = Adv::where('status', 'active')->get();

            foreach($advs as $adv){
                if(Carbon::now() > $adv->term){
                    $adv->delete();
                }
            }
        //})->cron('40 10 * * *');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
