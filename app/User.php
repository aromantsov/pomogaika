<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\UserRole;
use App\Models\Product;
use App\Models\Offer;
use App\Models\Cart;
use App\Models\Order;
use App\Models\Shop;
use App\Models\Review;
use App\Models\VendorReview;
use App\Models\Plan;
use App\Models\Photostock;
use App\Models;
use Auth;
use DB;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function user_roles()
    {
        return $this->belongsTo(UserRole::class, 'user_role_id', 'id');
    }

    public function isAdmin()
    {
        return $this->user_roles->id == 1 || $this->user_roles->id == 2;
    }

    public function offers()
    {
        return $this->hasMany(Offer::class);
    }

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    public function cart()
    {
        return $this->hasMany(Cart::class);
    }

    public static function getVendors($cart)
    {
        $vendors = DB::select(sprintf("SELECT DISTINCT *, u.id AS vendor_id FROM users u LEFT JOIN offers of ON (of.user_id = u.id) LEFT JOIN cart c ON (c.offer_id = of.id) LEFT JOIN shops s ON (s.user_id = u.id) WHERE user_role_id = 3 AND c.session_id = '%s' GROUP BY vendor_id", $cart));

        $vendors_data = [];
        foreach($vendors as $vendor){
            $vendor->shops = Shop::where('user_id', $vendor->vendor_id)->get();
            $vendor->plan = Plan::with('tariffs')->where('user_id', $vendor->vendor_id)->where('status', 'active')->first();
            $vendors_data[] = $vendor;
        }
        return $vendors_data;
    }

     public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public static function getClients($vendor_id, $search_form)
    {
        if(isset($search_form['name'])){
            $name_array = explode(' ', $search_form['name']);
            $search = " AND u.name = '" . $name_array[0] . "' AND u.lastname = '" . $name_array[1] . "'";
        }else{
            $search = '';
        }

        if(isset($search_form['phone'])){
            $search .= " AND u.phone = '" . $search_form['phone'] . "'";
        }else{
            $search .= '';
        }

        if((isset($search_form['gender'])) && ($search_form['gender'] != 'all')) {
            $search .= " AND u.gender = '" . $search_form['gender'] . "'";
        }else{
            $search .= "";
        }

        if(isset($search_form['age'])){
            $agepicker = time() - $search_form['age'] * 365.2421896 * 24 * 60 * 60;
            $agepicker_date = date('Y-m-d', $agepicker);
            $search .=  " AND u.birthdate <= '" . $agepicker_date . "' AND DATE_ADD(u.birthdate, INTERVAL 1 YEAR) > '" . $agepicker_date . "'";
        }else{
            $search .= "";
        }

        if(isset($search_form['sort'])){
            if($search_form['sort'] == 'name'){
                $sort = " u.name ASC";
            }elseif($search_form['sort'] == 'reviews'){
                $sort = " COUNT(vr.id) DESC";    
            }elseif($search_form['sort'] == 'age'){
                $sort = " u.birthdate DESC";
            }else{
                $sort = " o.created_at DESC";
            }
        }else{
            $sort = " o.created_at DESC";
        }

        return DB::select(sprintf("SELECT u.name as name, u.lastname as lastname, u.phone as phone, u.gender as gender, u.birthdate as birthdate, u.avatar as avatar, o.created_at AS order_date, MAX(o.created_at) as last_order, COUNT(o.id) AS orders, COUNT(vr.id) as reviews FROM users u JOIN orders o ON (o.user_id = u.id) LEFT JOIN purchases p ON (p.order_id = o.id) LEFT JOIN vendor_reviews vr ON (vr.vendor_id = u.id) JOIN offers of ON (p.offer_id = of.id) WHERE u.user_role_id != 3 AND of.user_id = %d %s GROUP BY o.user_id ORDER BY %s", $vendor_id, $search, $sort));
    }

    public static function getAges()
    {
        return DB::select("SELECT DISTINCT(YEAR(CURRENT_DATE)-YEAR(`birthdate`))-(RIGHT(CURRENT_DATE,5)<RIGHT(`birthdate`,5)) AS age FROM users WHERE user_role_id != 3 AND (YEAR(CURRENT_DATE)-YEAR(`birthdate`)) >= 1");
    }

    public function shops()
    {
        return $this->hasMany(Shop::class);
    }

    public function rewiews()
    {
        return $this->hasMany(Review::class);
    }

    public function plans()
    {
        return $this->hasMany(Plan::class);
    }

    public static function getFilterVendors($category_id)
    {
        return User::select('users.id', 'users.name')->join('offers', 'offers.user_id', '=', 'users.id')->join('products', 'products.id', '=', 'offers.product_id')->where('products.category_id', $category_id)->where('users.user_role_id', 3)->distinct()->get();
    }

    public function adv()
    {
        return $this->hasMany(Adv::class);
    }

    public function vendor_reviews()
    {
        return $this->hasMany(User::class);
    }

    public static function getVendorsByProduct($product_id)
    {
        return $vendors = User::leftJoin('offers', 'offers.user_id','=' , 'users.id')->where('offers.product_id', $product_id)->get();
    }

     public static function getVendorsWithSort($product_id, $sort)
    {
        if($sort == 'price_asc'){
            return $vendors = User::leftJoin('offers', 'offers.user_id','=' , 'users.id')->where('offers.product_id', $product_id)->orderBy('offers.price', 'asc')->get();
        }elseif($sort == 'price_desc'){
            return $vendors = User::leftJoin('offers', 'offers.user_id','=' , 'users.id')->where('offers.product_id', $product_id)->orderBy('offers.price', 'desc')->get();
        }else{
            return $vendors = User::leftJoin('offers', 'offers.user_id','=' , 'users.id')->where('offers.product_id', $product_id)->get();
        }
        
    }

    public function photostock()
    {
        return $this->hasMany(Photostock::class);
    }

    public function isPremiumVendor()
    {
        $plan = Plan::where('status', 'active')->where('user_id', Auth::id())->first();
        return $this->user_roles->id == 3 && $plan->tariff_id == 3;
    }
}
