<?php

namespace App\Imports;

use App\Models\Product;
use App\Models\Offer;
use App\Models\Category;
use App\Models\Manufacturer;
use Maatwebsite\Excel\Concerns\ToModel;

class ProductsImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {

        $product = Product::where('model', $row[1])->first();

        if($product){
            $product->sku = $row[1];
            $product->manufacturer_id = $this->setManufacturer($row[2]);
            $product->category_id = $this->setCategory($row[3], $row[4]);
            $product->image = $this->prepareProductImage($row[10]);
            $product->video = $this->prepareVideo($row[12]);
            $product->name_uk = $row[6] ?? $row[7];
            $product->name_ru = $row[7] ?? $row[6];
            $product->description_uk = $row[8] ?? $row[9] ?? '';
            $product->description_ru = $row[9] ?? $row[8] ?? '';
            $product->price = $row[13];
            $product->unit = $row[14];
            // $product->meta_name_ru = $row[12];
            // $product->meta_description_ru = $row[13];
            $product->save();

            // $offer = Offer::where('product_id', $product->id)->where('user_id', 3)->first();
            // if($offer){
            //     $offer->price = $row[13] ?? 0;
            //     $offer->save();
            // }
            return;
        }
        
        if($row[1]){
        return new Product([
            'model' => $row[1],
            'sku' => $row[1] ?? '',
            'manufacturer_id' => $this->setManufacturer($row[2]),
            'category_id' => $this->setCategory($row[3], $row[4]),
            'image' => $this->prepareProductImage($row[10]), 
            'video' => $this->prepareVideo($row[12]),
            'name_uk' => $row[6] ?? $row[7],
            'name_ru' => $row[7] ?? $row[6],
            'description_uk' => $row[8] ?? $row[9] ?? '',
            'description_ru' => $row[9] ?? $row[8] ?? '',
            'unit' => $row[14],
            // 'meta_name_ru' => $row[12] ?? '',
            // 'meta_description_ru' => $row[13] ?? '',
            'price' => $this->setPrice($row[1], (float)$row[13] ?? 0)
        ]);
        }else{
            echo '<p>Файл успешно импортирован</p>';
            echo '<a href="/admin/import">Назад</a>';

            exit();
            //return redirect()->back()->with(['success' => 'Файл успешно импортирован']);
        }
    }

    private function setCategory($ex_cat, $ex_subcat)
    {
        if(!empty($ex_subcat)){
            $category = Category::where('title_uk', $ex_subcat)->first();

            if($category){
                return $category->id;
            }

            $parent = Category::where('title_uk', $ex_cat)->first();

            if(!$parent){
                $parent = new Category;
                $parent->title_uk = $ex_cat  ?? '';
                $parent->title_ru = $ex_cat  ?? '';
                $parent->parent_id = 0;
                $parent->save();
            }

            $category = new Category;
            $category->title_uk = $ex_subcat ?? '';
            $category->title_ru = $ex_subcat  ?? '';
            $category->parent_id = $parent->id;
            $category->save();
            return $category->id;
        }

        $category = Category::where('title_uk', $ex_cat)->first();

        if($category){
            return $category->id;
        }

        $category = new Category;
        $category->title_uk = $ex_cat ?? '';
        $category->title_ru = $ex_cat ?? '';
        $category->parent_id = 0;
        $category->save();
        return $category->id;
    }

    private function setManufacturer($brand)
    {
        $manufacturer = Manufacturer::where('brand', $brand)->first();

        if($manufacturer){
            return $manufacturer->id;
        }

        $manufacturer = new Manufacturer;
        $manufacturer->brand = $brand;
        $manufacturer->save();
        return $manufacturer->id;
    }

    private function prepareProductImage($image_url){

        $url_check = explode('://', $image_url);

        if(($url_check[0] != 'http') && ($url_check[0] != 'https')){
            return;
        }

        $ch = curl_init ();
        curl_setopt ($ch , CURLOPT_URL , $image_url);
        curl_setopt ($ch , CURLOPT_USERAGENT , "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.47 Safari/536.11");
        curl_setopt ($ch , CURLOPT_HEADER , 0);
        curl_setopt ($ch , CURLOPT_RETURNTRANSFER , 1 );
        curl_setopt ($ch , CURLOPT_BINARYTRANSFER , 1);
        $image = curl_exec($ch);
        curl_close($ch);

        $image_url = explode('/', $image_url);
        $image_link = $image_url[count($image_url) - 1];


        file_put_contents(public_path('/img/product/') . $image_link, $image);

        return '/img/product/'. $image_link;
    }

    private function setPrice($model, $price)
    {
        $price = (float)$price ?? 0;
        $product = Product::where('model', $model)->first();
        //$offer = new Offer;
        if($product){
            // $offer = Offer::create([
            //     'product_id' => $product->id,
            //     'user_id' => 3,
            //     'vendor_model' => $model, 
            //     'price' => $price ?? 0
            // ]);
            $product->price = $price ?? 0;
            $product->save();
        }
        
    }

    private function prepareVideo($url)
    {
        $array = explode('/', $url);

        if($array([count($array) - 1]) == ''){
            return $array([count($array) - 2]);
        }else{
            return $array([count($array) - 1]);
        }
    }
}