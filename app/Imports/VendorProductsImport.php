<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToModel;
use App\Models\Product;
use App\Models\Category;
use App\Models\Offer;
use App\Models\Manufacturer;
use App\Models\Picture;

class VendorProductsImport implements ToModel
{
    private $vendor_id;

	public function __construct($vendor_id)
	{
		$this->vendor_id = $vendor_id;
	}
    /**
    * @param Collection $collection
    */
    public function model(array $row)
    {
        if($row[1] == 'Артикултовара') return;

        $product = Product::where('sku', $row[1])->first();

        if($product){

            // $product->sku = $row[1];
            // $product->manufacturer_id = $this->setManufacturer($row[2]);
            // $product->category_id = $this->setCategory($row[3], $row[4]);
            // $product->image = $this->prepareProductImage($row[10]);
            // $product->name_uk = $row[6] ?? $row[7];
            // $product->name_ru = $row[7] ?? $row[6];
            // $product->description_uk = $row[8] ?? $row[9] ?? '';
            // $product->description_ru = $row[9] ?? $row[8] ?? '';
            // $product->price = $row[13];
            // $product->save();

            $offer = Offer::where('product_id', $product->id)->where('user_id', $this->vendor_id)->first();

            if($offer){
            	$offer->price = (float)$row[13] ?? 0;
            	$offer->save();
            }else{
            	Offer::create([
                    'product_id' => $product->id,
                    'user_id' => $this->vendor_id,
                    'price' => (float)$row[13] ?? 0
            	]);
            }
            $model_product = new Product;
            $model_product->setPrice($product->id);
            return;
        }else{
            if($row[1]){
            $product = Product::create([
                'model' => $row[1],
                'sku' => $row[1] ?? '',
                'manufacturer_id' => $this->setManufacturer($row[2]),
                'category_id' => $this->setCategory($row[3], $row[4]),
                'image' => $this->prepareProductImage($row[10], 'product'), 
                'video' => $this->prepareVideo($row[12]),
                'name_uk' => $row[6] ?? $row[7],
                'name_ru' => $row[7] ?? $row[6],
                'description_uk' => $row[8] ?? $row[9] ?? '',
                'description_ru' => $row[9] ?? $row[8] ?? '',
                // 'meta_name_ru' => $row[12] ?? '',
                // 'meta_description_ru' => $row[13] ?? '',
                'price' => (float)$row[13] ?? 0,
                'unit' => $row[14] ?? ''
            ]);

            if(!empty($row[11])){
                $images = explode(',', $row[11]);

                foreach($images as $image){
                    Picture::create([
                        'product_id' => $product->id,
                        'image' => $this->prepareProductImage(ltrim($image), 'product_pictures') ?? ''
                    ]);
                }
            }

            Offer::create([
                'product_id' => $product->id,
                'user_id' => $this->vendor_id,
                'price' => (float)$row[13] ?? 0
            ]);

            // if($this->vendor_id != 3){
            //     Offer::create([
            //         'product_id' => $product->id,
            //         'user_id' => 3,
            //         'price' => (float)$row[13] ?? 0
            //     ]);
            // }

            $model_product = new Product;
            $model_product->setPrice($product->id);
            }else{
                request()->session()->flash('success', 'Товары успешно загружены');
                //return redirect()->route('home');
                header('Location: /home');
                
                //exit();
            }
        }


        return;
    }

    private function setCategory($ex_cat, $ex_subcat)
    {
        if(!empty($ex_subcat)){
            $category = Category::where('title_uk', $ex_subcat)->first();

            if($category){
                return $category->id;
            }

            $parent = Category::where('title_uk', $ex_cat)->first();

            if(!$parent){
                $parent = new Category;
                $parent->title_uk = $ex_cat  ?? '';
                $parent->title_ru = $ex_cat ?? '';
                $parent->parent_id = 0;
                $parent->save();
            }

            $category = new Category;
            $category->title_uk = $ex_subcat ?? '';
            $category->title_ru = $ex_subcat ?? '';
            $category->parent_id = $parent->id;
            $category->save();
            return $category->id;
        }

        $category = Category::where('title_uk', $ex_cat)->first();

        if($category){
            return $category->id;
        }

        $category = new Category;
        $category->title_uk = $ex_cat ?? '';
        $category->title_ru = $ex_cat ?? '';
        $category->parent_id = 0;
        $category->save();
        return $category->id;
    }

    private function setManufacturer($brand)
    {
        $manufacturer = Manufacturer::where('brand', $brand)->first();

        if($manufacturer){
            return $manufacturer->id;
        }

        $manufacturer = new Manufacturer;
        $manufacturer->brand = $brand;
        $manufacturer->save();
        return $manufacturer->id;
    }

    private function prepareProductImage($image_url, $dir){

        $url_check = explode('://', $image_url);

        if(($url_check[0] != 'http') && ($url_check[0] != 'https')){
            return;
        }

        try{
            $ch = curl_init ();
            curl_setopt ($ch , CURLOPT_URL , $image_url);
            curl_setopt ($ch , CURLOPT_USERAGENT , "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.47 Safari/536.11");
            curl_setopt ($ch , CURLOPT_HEADER , 0);
            curl_setopt ($ch , CURLOPT_RETURNTRANSFER , 1 );
            curl_setopt ($ch , CURLOPT_BINARYTRANSFER , 1);
            $image = curl_exec($ch);
            curl_close($ch);
        } catch (Exception $e) {
            var_dump($e->getMessage());
        }

        $image_url = explode('/', $image_url);
        $image_link = $image_url[count($image_url) - 1];

        file_put_contents(public_path('/img/' . $dir . '/') . $image_link, $image);

        return '/img/' . $dir . '/'. $image_link;
    }

    private function prepareVideo($url)
    {
        $array = explode('/', $url);

        if($array[count($array) - 1] == ''){
            return $array[count($array) - 2] ?? '';
        }else{
            return $array[count($array) - 1];
        }
    }
}
