<?php

namespace App\Http\Controllers;

use App\Models\ProductCalculator;
use App\Models\Product;
use App\Models\Property;
use App\Models\Attribute;
use App\Models\Offer;
use App\Models\Cart;
use App\User;
use Illuminate\Http\Request;
use Auth;
use App;

class ApiController extends Controller
{
    public function login()
    {
    	echo 'api находится в разработке';
    }

    public function register(Request $request)
    {
        $api_phone = str_replace(')-', ')', $request['phone']);
        $api_phone = str_replace('(', ' (', $api_phone);
    	return User::where('phone', $api_phone)->get();
    }

    public function getProducts($id)
    {
    	$product_info = ProductCalculator::with('products')->where('calculator', $id)->get();
        
        $products = [];

        foreach($product_info as $product){
            $product_data['id'] = $product->products->id;
            $product_data['name_ru'] = $product->products->name_ru;
            $product_data['name_uk'] = $product->products->name_uk;
            $product_data['price'] = $product->products->price;

            $prs = Property::with('attributes')->where('product_id', $product->products->id)->get();

            $props = [];

            foreach($prs as $pr){
                $pr_data['key'] = $pr->attributes->name_ru;
                $pr_data['value'] = $pr->value_ru;

                $props[] = $pr_data;
            }

            $product_data['properties'] = $props;

            $products[] = $product_data;

        }
        echo json_encode($products, JSON_UNESCAPED_UNICODE);
    }

    public function result(Request $request)
    {
        $products = [];
        foreach($request['data_product'] as $key => $field){
            $product_data = Product::find($key);
            $product['id'] = $key;
            $product['model'] = $product_data['model'];
            $product['name'] = $product_data['name_' . App::getLocale()];
            $product['quantity'] = $field;

            $product['vendors'] = User::getVendorsByProduct($key);
            $products[] = $product;
        }
        session(['api' => $products]);
        return redirect()->route('preorder');
        // session(['bonus' => $request['bonus'] ?? 2]);
        // foreach($request['data_product'] as $key => $field){

        //     $offer_info = Offer::with('products')->where('product_id', $key)->where('user_id', 3)->first();

        //     $offer_id = $offer_info->id;

        //     if($offer_info){
        //         $quantity = $field ?? 1;
        //     }

        //     if(!session('cart')){
        //         session(['cart' => substr(bin2hex(openssl_random_pseudo_bytes(26)), 0, 26)]);
        //     }

        //     $total = Cart::where('user_id', Auth::id() ?? 0)->where('session_id', session('cart'))->where('offer_id', $offer_id)->first();

        //     if(!$total){
        //         Cart::create(['api_id' => 0, 'user_id' => Auth::id() ?? 0, 'session_id' => session('cart'), 'offer_id' => $offer_id, 'quantity' => $quantity]);
        //     }else{
        //         $total->api_id = 0;
        //         $total->user_id = Auth::id() ?? 0;
        //         $total->session_id = session('cart');
        //         $total->offer_id = $offer_id;
        //         $total->quantity += $quantity;
        //         $total->save();
        //     }   
        // }

        // sleep(1);
        // return redirect()->route('cart'); 
    }

    public function resetPassword(Request $request)
    {
        $api_phone = str_replace(')-', ')', $request['phone']);
        $api_phone = str_replace('(', ' (', $api_phone);
        return User::select('password')->where('phone', $api_phone)->get();
    }

    public function updatePassword(Request $request)
    {
        $api_phone = str_replace(')-', ')', $request['phone']);
        $api_phone = str_replace('(', ' (', $api_phone);
        $user = User::where('phone', $api_phone)->get();
        $user->password = $request['password'];
        $user->save();
    }
}
