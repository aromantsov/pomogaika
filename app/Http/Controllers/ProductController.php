<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Product;
use App\Models\Offer;
use App\Models\Picture;
use App\Models\Property;
use App\Models\Review;
use App\Models\Plan;
use App;
use Validator;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\VendorProductsImport;
use Auth;
use App\User;

class ProductController extends Controller
{
    public function show($id, Request $request)
    {
    	return view('product', [
            'get' => $request,
    		'product' => Product::where('id', $id)->with('manufacturers')->where('published', 1)->firstOrFail(),
    		'pictures' => Picture::where('product_id', $id)->get(),
            'categories' => Category::with('children')->where('parent_id', '0')->where('published', '1')->get(),
            //'offers' => Offer::where('product_id', $id)->get(),
            'offers' => Offer::getProductOffers($id, $request['sort_by']),
            'reviews' => Review::getReviews($id),
            'properties' => Property::with('attributes')->where('product_id', $id)->get(),
            'visitor' => User::find(Auth::id()),
            'rating' => (int)Review::where('product_id', $id)->avg('rating'),
            'reviews_count' => Review::where('product_id', $id)->count(),
    		'local' => App::getLocale()
    	]);
    }

    public function upload(Request $request)
    {
        if(App::getLocale() == 'ru'){
            $messages = [
                'mimes' => 'Файл должен быть в формате xls или csv'
            ];
        }else{
            $messages = [
                'mimes' => 'Файл має бути в форматі xls або csv'
            ];
        }
        

        $validator = Validator::make($request->all(), [
            'file' => 'required|max:100000|mimes:xlsx,xls,csv'
        ], $messages);

        if(!$request['file']){
            request()->session()->flash('error', 'Файл не выбран');
            return redirect()->back();
        }

        \Excel::import(new VendorProductsImport(Auth::id()), request()->file('file'));
        //request()->session()->flash('success', 'Товары успешно загружены');

        return redirect()->back();
    }

    public function add_wishlist(Request $request){

        // $messages = [
        //     'min' => 'Добавлять товары в список желаний могут только авторизованные пользователи'
        // ];

        if($request['user_id'] == 0){
            return 'error';
        }

        $user = User::find($request['user_id']);

        if($user->user_role_id == 3){
            return 'error2';
        }

        foreach($user->products as $product){
            if($product->id == $request['product_id']){
                $user->products()->detach($product);
                return 'success_del';
            }
        }

        $product = Product::find($request['product_id']);
        $product->users()->attach($user);
        return 'success';
    }

    public function add_compare(Request $request)
    {
        $json = [];

        if (session('compare') === null) {
            session(['compare' => []]);
        }

        if (isset($request['product_id'])) {
            $product_id = $request['product_id'];
        } else {
            $product_id = 0;
        }

        $product_info = Product::find($product_id);

        if($product_info){
            if(!in_array($request['product_id'], session('compare'))){
                // if(count(session('compare')) >= 4){
                //     array_shift(session('compare'));
                // }

                $sess = session('compare');
                $sess[] = $request['product_id'];
                session(['compare' => $sess]);
            }
            if(App::getLocale() == 'ru'){
                $json['success'] = sprintf('Товар <a href="%s">%s</a> добавлен в ваш <a href="%s">список сравнения</a>!', '/ru/product' . $request['product_id'], $product_info['name_' . App::getLocale()], '/ru/compare');

                $json['total'] = sprintf('Сравнение товаров (%s)', (session('compare') !== null) ? count(session('compare')) : 0);
            }else{
                $json['success'] = sprintf('Товар <a href="%s">%s</a> додано до вашого <a href="%s">списку порівняння</a>!', '/product' . $request['product_id'], $product_info['name_' . App::getLocale()], '/compare');

                $json['total'] = sprintf('Порівняння товарів (%s)', (session('compare') !== null) ? count(session('compare')) : 0);
            }
            return $json;
        }
    }

    public function compare(Request $request)
    {
        $session = $request->session()->all();

        if(!isset($session['compare'])){
            if(App::getLocale() == 'ru'){
                request()->session()->flash('error', 'Список сравнения пустой');
            }else{
                request()->session()->flash('error', 'Список порівняння пустий');
            }
            
            return redirect()->back();
        }

    	$products = Product::find($session['compare']);

        $cats_array = [];
        $cats = [];
        $prods = [];
        $prods_id = [];
        $prop = [];

        foreach($products as $product){
            if(!in_array($product->category_id, $cats_array)){
                $cats_array[] = $product->category_id;
                $cats[] = Category::find($product->category_id);

                $prods[$product->category_id] = [];

                foreach($cats as $cat){

                    if($cat == null){
                        $prods[$product->category_id] = Product::with('manufacturers')->where('category_id', 0)->whereIn('id', (array)$session['compare'])->get();
                        $prods_id[$product->category_id] = Product::select('id')->where('category_id', 0)->whereIn('id', (array)$session['compare'])->get();
                    }else{
                        $prods[$product->category_id] = Product::with('manufacturers')->where('category_id', $cat->id)->whereIn('id', (array)$session['compare'])->get();
                        $prods_id[$product->category_id] = Product::select('id')->where('category_id', $cat->id)->whereIn('id', (array)$session['compare'])->get();
                    }
                    
                }
            }
        }
        $pr = [];
        foreach($cats as $cat){
            $cat_prods = Product::select('id')->where('category_id', $cat->id)->whereIn('id', (array)$session['compare'])->get();

            $cat_prod_ids = [];

            foreach($cat_prods as $prod){
                $cat_prod_ids[] = $prod['id'];
            }

            $pr[$cat->id ?? 0] = Property::with('attributes')->leftJoin('products', 'product_id', '=', 'properties.product_id')->whereIn('properties.product_id', $cat_prod_ids)->where('products.category_id', $cat->id ?? 0)->groupBy('properties.attribute_id')->get();
        }

        return view('compare', [
            'categories' => Category::with('children')->where('parent_id', '0')->where('published', '1')->get(),
            'products' => $prods,
            'cats' => $cats,
            'prids' => $prods_id,
            //'properties' => Property::with('attributes')->whereIn('product_id', (array)$session['compare'])->groupBy('attribute_id')->get(),
            'props' => $pr,
            'local' => App::getLocale()
        ]);
    }

    public function removeCompare($product_id)
    {
        if(session('compare') != null){
            $session = session('compare');

            for($i = 0; $i < count($session); $i++){
                if($session[$i] == $product_id){
                    $session[$i] = null;
                }
            }

            session(['compare' => $session]);
            request()->session()->flash('success', 'Товар удален из списка сравнений');
            return redirect()->route('compare');
        }
    }

    public function search(Request $request)
    {
        if($request['sort']){
            $products = Product::getSearchProductsWithSort($request['search'], $request['sort']);
        }else{
            $products = Product::getSearchProducts($request['search']);
        }

        $product_ids = [];
        $prices = [];

        foreach($products as $product){
            $product_ids[] = $product['id'];
            $prices[] = $product['price'];
        }

        if($prices){
            $min_price = min($prices);
            $max_price = max($prices);
        }else{
            $min_price = 0;
            $max_price = 0;
        }

        return view('search.product', [
            'categories' => Category::with('children')->where('parent_id', '0')->where('published', '1')->get(),
            'products' => $products,
            'search' => $request['search'],
            'count' => count($products),
            'count_vendors' => count(Offer::whereIn('offers.product_id', $product_ids)->groupBy('user_id')->get()),
            'min_price' => $min_prices ?? 0,
            'max_price' => $max_prices ?? 0,
            'get' => $request, 
            'local' => App::getLocale()
        ]);
    }

    public function toModeration(Request $request)
    {

        $messages = [
            'required' => 'Поля название (на двух языках) и цена обязательны к заполнению',
            'mimes' => 'Файл должен быть формата jpeg, png или svg',
            'max' => 'Максимальній размнер файла 8 Мб'

        ];

        $validator = $this->validate($request, [
            'name_ru' => 'required|max:191',
            'name_uk' => 'required|max:191',
            'image' => 'nullable|mimes:jpg,jpeg,png,svg|max:8096',
            'price' => 'required'
        ], $messages);

        $product = Product::create([
            'name_ru' => $request['name_ru'],
            'name_uk' => $request['name_uk'],
            'category_id' => $request['category_id'] ?? 0,
            'price' => $request['price'] ?? 0,
            'manufacturer_id' => $request['manufacturer_id'] ?? 0,
            'description_ru' => $request['comment'] ?? '',
            'description_uk' => $request['comment'] ?? '',
        ]);

        Offer::create([
            'product_id' => $product->id,
            'user_id' => Auth::id(),
            'price' => $request['price'],
        ]);

        // if(Auth::id() != 3){
        //     Offer::create([
        //         'product_id' => $product->id,
        //         'user_id' => 3,
        //         'price' => $request['price'],
        //     ]);
        // }

        if($request['option']){
            Property::create([
                'attribute_id' => 6,
                'product_id' => $product->id,
                'value_ru' => $request['option'],
                'value_uk' => $request['option'],
            ]);
        }

        if($request['type']){
            Property::create([
                'attribute_id' => 7,
                'product_id' => $product->id,
                'value_ru' => $request['type'],
                'value_uk' => $request['type'],
            ]);
        }

        if($request['packaging']){
            Property::create([
                'attribute_id' => 8,
                'product_id' => $product->id,
                'value_ru' => $request['packaging'],
                'value_uk' => $request['packaging'],
            ]);
        }

        if($request['package']){
            Property::create([
                'attribute_id' => 9,
                'product_id' => $product->id,
                'value_ru' => $request['package'],
                'value_uk' => $request['package'],
            ]);
        }

        $user = User::find(Auth::id());

        if(App::getLocale() == 'ru'){
            $title = 'Новый товар';
            $msg = 'Пользователь ' . $user->name . ' добавил товар ' . $product->name_ru;
        }else{
            $title = 'Новий товар';
            $msg = 'Користувач ' . $user->name . ' додав товар ' . $product->name_uk;
        }

        $admins = User::where('user_role_id', 1)->get();

        foreach($admins as $admin){
            mail($admin->email, $title, $msg);
        } 

        $pictures = $request->file(['pictures']);
        if($pictures){
            $image = array_shift($pictures);
        }
        
        if(isset($image)){
            $new_name = $image->getClientOriginalName();
            $image->move($_SERVER['DOCUMENT_ROOT'] . '/img/product/', $new_name);
            if(isset($new_name)){
                $product->image = '/img/product/' . $new_name;
            }
            
            $product->save();
        }
        
        if($pictures){
            foreach ($pictures as $key => $f) {
                $new_name = $f->getClientOriginalName();
                $f->move($_SERVER['DOCUMENT_ROOT'] . '/img/product_pictures/', $new_name);
                Picture::create([
                    'product_id' => $product->id,
                    'image' => '/img/product_pictures/' . $new_name
                ]);
            }
        }

        request()->session()->flash('success', 'Товар успешно создан, находится на модерации');
        return redirect()->route('home');
    }

    public function is_sale()
    {
        return view('sale', [
            'categories' => Category::with('children')->where('parent_id', '0')->where('published', '1')->get(),
            'products' => Product::allGetSales(),
            'local' => App::getLocale()
        ]);
    }

    public function byVendor($id)
    {
        $plan = Plan::with('tariffs')->where('user_id', $id)->where('status', 'active')->first();

        if(($plan === null) || (!$plan->tariffs->public_profile)){
            request()->session()->flash('error', 'У данного поставщика нет публичного профиля');
            return redirect()->route('main');
        }

        return view('vendor', [
            'categories' => Category::with('children')->where('parent_id', '0')->where('published', '1')->get(),
            'products' => Offer::productsByVendor($id),
            'vendor' => User::find($id),
            'local' => App::getLocale()
        ]);
    }
}
