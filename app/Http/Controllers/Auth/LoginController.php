<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';
    private $param;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        session(['url.intended' => url()->previous()]);
        $this->redirectTo = session()->get('url.intended');

        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        $ar = explode('@', $request['login']);

        if(count($ar) == 2){
            $this->param = 'email';
            $request['email'] = $request['login'];
        }else{
            $this->param = 'phone';
            $request['phone'] = $this->phone_number($request['login']);
        }

        if(isset($request['modal'])){
            session(['open_modal' => true]);
        }else{
            session(['open_modal' => null]);
        }

        $phone = User::where('phone', $request['phone'])->first();    

        if(!$phone){
            $api_phone = str_replace(' (', '(', $request['phone']);
            $api_phone = str_replace(') ', ')', $api_phone);
            $api_phone = str_replace(')', ')-', $api_phone);
            $user_data = json_decode(file_get_contents('http://0c0e686efac0.ngrok.io/api/users/getUserByPhone/' . $api_phone . '?token=bfe9d1c7-7da7-4a5d-9c07-911cb821fa05'), true);
            if($user_data){
                $user_data['password'] = file_get_contents('http://0c0e686efac0.ngrok.io/api/users/getPasswordByUserId/' . $user_data['id'] . '?token=bfe9d1c7-7da7-4a5d-9c07-911cb821fa05');

                $user_phone = str_replace('(', ' (', $user_data['phone']);
                $user_phone = str_replace(')-', ') ', $user_phone);

                $user = User::create([
                    'name' => $user_data['name'],
                    'phone' => $user_phone ?? '',
                    'address' => $user_data['address'],
                    'email' => $user_data['email'],
                    'password' => trim($user_data['password'], '\"'),
                    'user_role_id' => 4,
                    'is_activate' => 1
                ]);
            }

        }

        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if (method_exists($this, 'hasTooManyLoginAttempts') &&
            $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    public function username()
    {
        return $this->param;
    }

    private function phone_number($sPhone)
    {
        if(strlen($sPhone) == 19) return $sPhone;
        if(strlen($sPhone) == 17){
            $sPhone = str_replace('(', ' (', $sPhone);
            $sPhone = str_replace(')', ') ', $sPhone);
            return $sPhone;
        }
        if(strlen($sPhone) == 18){
            $sPhone = str_replace('(', ' (', $sPhone);
            $sPhone = str_replace(')-', ') ', $sPhone);
            return $sPhone;
        }
        $sPhone = preg_replace("[^0-9]",'',$sPhone);
        if(strlen($sPhone) == 10){
            $sPhone = '+38' . $sPhone;
        }
        if(strlen($sPhone) == 12){
            $sPhone = '+' . $sPhone;
        }
        $sCode = substr($sPhone, 0, 3);
        $sPrefix = substr($sPhone,3,3);
        $sNumber1 = substr($sPhone,6,3);
        $sNumber2 = substr($sPhone,9,2);
        $sNumber3 = substr($sPhone,11,2);
        $sPhone = $sCode . " (".$sPrefix.") ".$sNumber1."-".$sNumber2."-".$sNumber3;
        return $sPhone;
    }
}
