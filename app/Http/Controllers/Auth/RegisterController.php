<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Events\Registered;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home?new_user=true';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

     public function register(Request $request)
    {
        if(isset($request['modal_reg'])){
            session(['open_modal2' => true]);
        }else{
            session(['open_modal2' => null]);
        }

        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        $this->guard()->login($user);

        return $this->registered($request, $user)
                        ?: redirect($this->redirectPath());
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $messages = [
            'conditions.required' => "Пожалуйста, подтвердите условия использования",
        ];

        return Validator::make($data, [
            'phone' => ['required', 'string', 'unique:users'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'conditions' => ['required']
        ], $messages);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        session(['new' => Hash::make($data['email'])]);
        mail($data['email'], 'Регистрация на Pomogaika', 'Благодарим за регистрацию. Пожалуйста, пройдите по ссылке, чтобы подтвердить аккаунт: ' . $_SERVER["HTTP_REFERER"] . '/home?hash=' . session('new') . '. Ссылка действительна 30 минут');
        $user = User::create([
            'phone' => $data['phone'],
            'email' => $data['email'],
            'user_role_id' => $data['user_role_id'] ?? 4,
            'password' => Hash::make($data['password']),
        ]);
        $api_phone = str_replace(' (', '(', $data['phone']);
        $api_phone = str_replace(') ', ')', $api_phone);
        $api_phone = str_replace(')', ')-', $api_phone);
        //file_get_contents('https://pomogayka.ua/api_register/' . $api_phone);
        return $user;
    }
}
