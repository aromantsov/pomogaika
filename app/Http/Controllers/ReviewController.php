<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Review;
use App\Models\Offer;
use App\Models\VendorReview;
use App\User;
use Auth;
use Validator;
use App;

class ReviewController extends Controller
{
    public function add(Request $request)
    {
        if(App::getLocale() == 'ru'){
            if(!Auth::id()){
                request()->session()->flash('error', 'Только зарегистрированные пользователи могут оставлять отзывы');
                return redirect()->back();
            }
        }else{
            if(!Auth::id()){
                request()->session()->flash('error', 'Тільки зареєстровані користувачі можуть залишати відгуки');
                return redirect()->back();
            }
        }
        

        $user = User::find(Auth::id());
        Review::create([
        	'user_id' => Auth::id(),
        	'product_id' => $request['product_id'],
            'username' => $user['name'] . ' ' . $user['lastname'],
        	'positives' => $request['positives'],
        	'negatives' => $request['negatives'],
        	'text' => $request['text'],
        	'rating' => $request['sociability'] ?? 5
        ]);

        if(App::getLocale() == 'ru'){
            request()->session()->flash('success', 'Отзыв добавлен');
        }else{
            request()->session()->flash('success', 'Відгук додано');
        }

        return redirect()->back();
    }

    public function v_like(Request $request)
    {
        $review_id = $request['review_id'];

        $review = VendorReview::find($review_id);
        if(!session('v_like-' . $review_id)){
            $review->likes++;
            $review->save();
            session(['v_like-' . $review_id => true]);
            if(session(('v_dislike-' . $review_id))){
                $review->dislikes--;
                $review->save();
                session(['v_dislike-' . $review_id => null]);
            }
        }else{
            $review->likes--;
            $review->save();
            session(['v_like-' . $review_id => null]);
        }
        
        return json_encode(['likes' => $review->likes, 'dislikes' => $review->dislikes]);
    }

    public function v_dislike(Request $request)
    {
        $review_id = $request['review_id'];

        $review = VendorReview::find($review_id);
        if(!session('v_dislike-' . $review_id)){
            $review->dislikes++;
            $review->save();
            session(['v_dislike-' . $review_id => true]);
            if(session(('v_like-' . $review_id))){
                $review->likes--;
                $review->save();
                session(['v_like-' . $review_id => null]);
            }
        }else{
            $review->dislikes--;
            $review->save();
            session(['v_dislike-' . $review_id => null]);
        }
        
        return json_encode(['likes' => $review->likes, 'dislikes' => $review->dislikes]);
    }

    public function p_like(Request $request)
    {
        $review_id = $request['review_id'];

        $review = Review::find($review_id);
        if(!session('p_like-' . $review_id)){
            $review->likes++;
            $review->save();
            session(['p_like-' . $review_id => true]);
            if(session(('p_dislike-' . $review_id))){
                $review->dislikes--;
                $review->save();
                session(['p_dislike-' . $review_id => null]);
            }
        }else{
            $review->likes--;
            $review->save();
            session(['p_like-' . $review_id => null]);
        }
        
        return json_encode(['likes' => $review->likes, 'dislikes' => $review->dislikes]);
    }

    public function p_dislike(Request $request)
    {
        $review_id = $request['review_id'];

        $review = Review::find($review_id);
        if(!session('p_dislike-' . $review_id)){
            $review->dislikes++;
            $review->save();
            session(['p_dislike-' . $review_id => true]);
            if(session(('p_like-' . $review_id))){
                $review->likes--;
                $review->save();
                session(['p_like-' . $review_id => null]);
            }
        }else{
            $review->dislikes--;
            $review->save();
            session(['p_dislike-' . $review_id => null]);
        }
        
        return json_encode(['likes' => $review->likes, 'dislikes' => $review->dislikes]);
    }
}
