<?php

namespace App\Http\Controllers;

use App\User;
use Auth;
use App\Models\Category;
use App\Models\Product;
use App\Models\Offer;
use App\Models\Order;
use App\Models\OrderStatus;
use App\Models\Shop;
use App\Models\VendorReview;
use App\Models\Review;
use App\Models\Tariff;
use App\Models\Plan;
use App\Models\Adv;
use App\Models\Manufacturer;
use App\Models\Delivery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $user = User::find(Auth::id());

        if($user->user_role_id == 3){
            header('Cache-Control: no-cache, no-store, must-revalidate'); // HTTP 1.1.
            header('Cache-Control: post-check=0, pre-check=0', FALSE);
            header('Pragma: no-cache'); // HTTP 1.0.
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Proxies.
            header("Last-Modified:".gmdate("D,d M Y H:i:s"));
            $offers = Offer::where('user_id', Auth::id())->get();
            $offers = json_decode(json_encode($offers, JSON_UNESCAPED_UNICODE), true);

            $product_info = [];
            
            foreach($offers as $offer){

                if($request->search_name){
                    $s_name = $request->search_name;
                }

                if($request->search_model){
                    $s_model = $request->search_model;
                }

                if($request->search_category){
                    $cat = Category::find($request->search_category);

                    if($cat->parent_id){
                        $s_category = $cat->parent_id;
                    }else{
                        $s_category = $request->search_category;
                    }
                }else{
                    $request->search_subcategory = 0;
                }

                if($request->search_subcategory){
                    $s_category = $request->search_subcategory;
                } 

                $product = Product::where('id', $offer['product_id'])->where('name_' . App::getLocale(), ($request->search_name) ? '=' : '<>', $s_name ?? null)->where('model', ($request->search_model) ? '=' : '<>', $s_model ?? null)->where('category_id', (isset($s_category)) ? '=' : '<>', $s_category ?? 0)->first();

                if(!$product){
                    continue;
                }
                $offer['name_ru'] = $product->name_ru;
                $offer['name_uk'] = $product->name_uk;
                $offer['model'] = $product->model;
                $offer['published'] = $product->published;
                $offer['image'] = $product->image;
                $category = Category::find($product->category_id);
                if($category->parent_id == 0){
                    $offer['category_id'] = $category->id;
                    $offer['category_ru'] = $category->title_ru;
                    $offer['category_uk'] = $category->title_uk;
                }else{
                    $parent = Category::find($category->parent_id);
                    $offer['category_id'] = $parent->id;
                    $offer['category_ru'] = $parent->title_ru;
                    $offer['category_uk'] = $parent->title_uk;
                    $offer['subcategory_id'] = $category->id;
                    $offer['subcategory_ru'] = $category->title_ru;
                    $offer['subcategory_uk'] = $category->title_uk;
                }
                $product_info[] = $offer;
            }

            if($request['search_category']){
                $search_parent = $request['search_category'];
            }

            $sc = $this->searchClients($request);

            $or_adv = Adv::where('user_id', Auth::id())->where('status', 'ordered')->get() ?? false;

            $adv_array = [];

            if($or_adv){
                foreach($or_adv as $value){
                    $adv_array[] = $value->advplace_id;
                }
            }

            $active_shop = Shop::where('user_id', Auth::id())->where('is_active', 1)->first();

            if(!$active_shop){
                $active_shop = Shop::where('user_id', Auth::id())->first();
            }

            if($request['new_user']){
                $new_user = true;
            }

            if($request['hash'] && ($request['hash'] == session('new'))){
                $user->is_activate = 1;
                $user->save();
                $hash = session('new');
            }

            $params = [
                'user' => $user,
                'categories' => Category::with('children')->where('parent_id', '0')->where('published', '1')->get(),
                'products' => $product_info,
                'search_categories' => Category::where('parent_id', '0')->get(),
                'search_parent' => $search_parent ?? 0,
                'search_subcategories' => isset($search_parent) ? (Category::with('children')->where('parent_id', $search_parent)->get()) : [],
                'get' => $request,
                'ages' => User::getAges(),
                'shops' => Shop::where('user_id', Auth::id())->get(),
                'active_shop' => $active_shop,
                'delivery_addresses' => Delivery::select('*', 'delivery.id as delivery_id')->join('shops', 'delivery.shop_id', '=', 'shops.id')->where('shops.user_id', Auth::id())->where('shops.is_active', 1)->get(),
                'clients' => User::getClients($user->id, $sc),
                'orders' => Order::getOrdersByVendor(Auth::id()),
                'order_statuses' => OrderStatus::all(),
                'vendor_reviews' => VendorReview::with('users')->where('vendor_id', Auth::id())->orderBy('created_at', 'desc')->get(),
                'rating' => (int)VendorReview::where('vendor_id', Auth::id())->avg('rating'),
                'tariffs' => Tariff::all(),
                'plan' => Plan::with('tariffs')->where('user_id', Auth::id())->where('status', 'active')->first(),
                'ordered_plan' => Plan::with('tariffs')->where('user_id', Auth::id())->where('status', 'ordered')->first() ?? false,
                'ordered_adv' => $adv_array,
                'new_user' => $new_user ?? null,
                'hash' => $hash ?? null,
                'local' => App::getLocale()
            ];

            return view('account_company_info', $params);
        }else{
            $products = $user->products;

            $cats_array = [];
            $cats = [];
            $prods = [];

            foreach($products as $product){
                if(!in_array($product->category_id, $cats_array)){
                    $cats_array[] = $product->category_id;
                    $cats[] = Category::find($product->category_id);

                    $prods[$product->category_id] = [];

                    foreach($cats as $cat){
                        $prods[$product->category_id] = Product::getProductsFromWishlistByCategory($cat->id, Auth::id());
                    }
                }
            }

            $orders = Order::getOrders(Auth::id());

            $vendor_reviews = VendorReview::getReviewsByUser(Auth::id());

            if($request['new_user']){
                $new_user = true;
            }

            if($request['hash'] && ($request['hash'] == session('new'))){
                $user->is_activate = 1;
                $user->save();
                $hash = session('new');
            }

            $params = [
                'user' => $user,
                'categories' => Category::with('children')->where('parent_id', '0')->where('published', '1')->get(),
                'cats' => $cats,
                'products' => $prods,
                'orders' => $orders,
                'get' => $request,
                'vendor_reviews' => $vendor_reviews,
                'reviews' => Review::getReviewsByUser(Auth::id()),
                'actual' => Order::getActualOrders(Auth::id()),
                'new_user' => $new_user ?? null,
                'hash' => $hash ?? null,
                'local' => App::getLocale()
            ];
            return view('account_user_info', $params);
        }

        return view('home', ['role' => $user->user_role_id]);
    }

    public function edit(Request $request)
    {
        $user = User::find(Auth::id());

        if($user->user_role_id == 3){

            header('Cache-Control: no-cache, no-store, must-revalidate'); // HTTP 1.1.
            header('Pragma: no-cache'); // HTTP 1.0.
            header('Expires: 0'); // Proxies.

            $or_adv = Adv::where('user_id', Auth::id())->where('status', 'ordered')->get() ?? false;

            $adv_array = [];

            if($or_adv){
                foreach($or_adv as $value){
                    $adv_array[] = $value->advplace_id;
                }
            }

            $active_shop = Shop::where('user_id', Auth::id())->where('is_active', 1)->first();

            if(!$active_shop){
                $active_shop = Shop::where('user_id', Auth::id())->first();
            }
            
            $params = [
                'user' => $user,
                'orders' => Order::getOrdersByVendor(Auth::id()),
                'order_statuses' => OrderStatus::all(),
                'get' => $request,
                'ages' => User::getAges(),
                'clients' => User::getClients($user->id, $this->searchClients($request)),
                'vendor_reviews' => VendorReview::with('users')->where('vendor_id', Auth::id())->orderBy('created_at', 'desc')->get(),
                'tariffs' => Tariff::all(),
                'rating' => (int)VendorReview::where('vendor_id', Auth::id())->avg('rating'),
                'plan' => Plan::with('tariffs')->where('user_id', Auth::id())->where('status', 'active')->first(),
                'categories' => Category::with('children')->where('parent_id', '0')->where('published', '1')->get(),
                'manufacturers' => Manufacturer::all(),
                'shops' => Shop::where('user_id', Auth::id())->get(),
                'active_shop' => $active_shop,
                'delivery_addresses' => Delivery::select('*', 'delivery.id as delivery_id')->join('shops', 'delivery.shop_id', '=', 'shops.id')->where('shops.user_id', Auth::id())->where('shops.is_active', 1)->get(),
                'ordered_plan' => Plan::with('tariffs')->where('user_id', Auth::id())->where('status', 'ordered')->first() ?? false,
                'ordered_adv' => $adv_array,
                'local' => App::getLocale()
            ];

            return view('account_company_edit', $params);
        }else{

            $products = $user->products;

            $cats_array = [];
            $cats = [];
            $prods = [];

            foreach($products as $product){
                if(!in_array($product->category_id, $cats_array)){
                    $cats_array[] = $product->category_id;
                    $cats[] = Category::find($product->category_id);

                    $prods[$product->category_id] = [];

                    foreach($cats as $cat){
                        $prods[$product->category_id] = Product::getProductsFromWishlistByCategory($cat->id, Auth::id());
                    }
                }
            }

            $orders = Order::getOrders(Auth::id());

            $params = [
                'user' => $user,
                'categories' => Category::with('children')->where('parent_id', '0')->where('published', '1')->get(),
                'cats' => $cats,
                'products' => $prods,
                'orders' => $orders,
                'reviews' => Review::getReviewsByUser(Auth::id()),
                'vendor_reviews' => VendorReview::getReviewsByUser(Auth::id()),
                'actual' => Order::getActualOrders(Auth::id()),
                'local' => App::getLocale()
            ];
            return view('account_user_edit', $params);
        }
    }

    public function update(User $user, Request $request)
    {
        session(['open_modal' => null]);
        session(['open_modal2' => null]);

        $validator = $this->validate($request, [
            'name' => 'required|string|max:255',
            'lastname' => 'nullable|string|max:255',
            'patronimic' => 'nullable|string|max:255',
            'address' => 'nullable|string|max:255',
            'email' => ['required', 'string', 'email', 'max:255', \Illuminate\Validation\Rule::unique('users')->ignore($user->id)],
        ]);

        if($request->file()){

            $messages = [
                'mimes' => 'Файл должен быть формата jpeg, png или svg'
            ];

            $validator = $this->validate($request, [
                'avatar' => 'mimes:jpg,jpeg,png,svg'
            ], $messages);

            $avatar_del = User::find(Auth::id());

            $files = $request->file();

            if(($avatar_del->avatar) && (isset($files['avatar']))){
                unlink(public_path($avatar_del->avatar));
            }
        }

        $user->name = $request['name'];
        $user->lastname = $request['lastname'];
        $user->patronimic = $request['patronimic'];
        $user->gender = $request['gender_input'];
        $user->phone = $request['phone'];
        $user->email = $request['email'];
        $user->birthdate = $request['birthdate'];
        $user->address = $request['address'];
        $user->save();

        if($request->file()){

            foreach ($request->file() as $key => $f) {
                $new_name[$key] = $f->getClientOriginalName();
                $f->move($_SERVER['DOCUMENT_ROOT'] . '/img/users/', $new_name[$key]);
            }
            if(isset($new_name['avatar'])){
                $user->avatar = '/img/users/' . $new_name['avatar'];
            }
            
            $user->save();
        }
        return redirect()->route('home');
    }

    public function updatevendor(User $user, Request $request)
    {
        session(['open_modal' => null]);
        session(['open_modal2' => null]);
        $validator = $this->validate($request, [
            'name' => 'required|string|max:255',
            'lastname' => 'nullable|string|max:255',
            'address' => 'nullable|string|max:255',
        ]);
        if($request->file()){
          
            $messages = [
                'mimes' => 'Файл должен быть формата jpeg, png или svg'
            ];

            $validator = $this->validate($request, [
                'avatar' => 'mimes:jpg,jpeg,png,svg'
            ], $messages);

            $avatar_del = User::find(Auth::id());

            $files = $request->file();

            if(($avatar_del->avatar) && (isset($files['avatar']))){
                unlink(public_path($avatar_del->avatar));
            }
        }

        $user->name = $request['name'];
        $user->lastname = $request['site'];

        if(!$request['delivery_type']){
            $request['delivery_type'] = [];
        }

        if(count($request['delivery_type']) == 2){
            $user->gender = 'both';
        }elseif(count($request['delivery_type']) == 1){
            $user->gender = $request['delivery_type'][0];
        }

        $user->phone = $request['phone'];
        $user->address = $request['address'];
        $user->description = $request['description'];
        $user->save();
        
        if($request->file()){
            
            foreach ($request->file() as $key => $f) {
                $new_name[$key] = $f->getClientOriginalName();
                $f->move($_SERVER['DOCUMENT_ROOT'] . '/img/vendors/', $new_name[$key]);
            }
            if(isset($new_name['avatar'])){
                $user->avatar = '/img/vendors/' . $new_name['avatar'];
            }
            
            $user->save();
        }

        if($request['shops']){

            $shops = Shop::where('user_id', Auth::id())->get();

            $active = Shop::where('user_id', Auth::id())->where('is_active', 1)->first();

            if(!$active){
                $active = Shop::where('user_id', Auth::id())->first();

            }

        
            for($i = 0; $i < count($request['shops']); $i++){
                $shop = Shop::where('address', $request['shops'][$i])->first();
                
                if(!$shop){
                    $shop = Shop::create([
                        'user_id' => Auth::id(),
                        'address' => $request['shops'][$i]
                    ]);
                } 

                if((isset($active)) && ($shop->address == $active->address)){
                    $shop->is_active = 1;
                    $shop->save();
                }
            }
        }

        if($request['delivery_addresses']){

            $active = Shop::where('user_id', Auth::id())->where('is_active', 1)->first();

            if(!$active){
                $active = Shop::where('user_id', Auth::id())->first();

            }

            Delivery::where('shop_id', $active->id)->delete();

            foreach($request['delivery_addresses'] as $address){
                $explode = explode('/', $address);
                $delivery = Delivery::create([
                    'shop_id' => $active->id,
                    'price' => $explode[0],
                    'distance' => $explode[1],
                ]);
                
            }

        }
       
        return redirect()->route('home');
    }

    public function changePassword(Request $request)
    {
        if(App::getLocale() == 'ru'){
           $messages = [
            'required' => 'Пожалуйста, заполните все поля',
            'current_password' => 'Текущий пароль не верен',
            'min:8' => 'Минимальный размер пароля - 8 символов',
            'confirmed' => 'Пароли не совпадают' 
            ]; 
           $success = 'Пароль изменен!';
        }else{
            $messages = [
            'required' => 'Будь лааска, заповніть всі поля',
            'current_password' => 'Пароль не вірний',
            'min:8' => 'Мінімальний розмір паролю - 8 символів',
            'confirmed' => 'Паролі не співпадають' 
            ];
            $success = 'Пароль змінено!';
        }

        $validator = $this->validate(request(), [
            'current_password' => 'required|current_password',
            'new_password' => 'required|string|min:8|confirmed',
        ], $messages);

        request()->user()->fill([
            'password' => Hash::make(request()->input('new_password'))
        ])->save();
        request()->session()->flash('success', $success);

        return redirect()->route('home');
    }

    public function orderPlan(Request $request)
    {
        if($request['plan_id'] == 1){
            return 1;
        }
        $plan = Plan::where('user_id', $request['user_id'])->where('tariff_id', $request['plan_id'])->where('status', 'ordered')->first();
        if($plan){
            return 0;
        }
        $plan = Plan::where('user_id', $request['user_id'])->where('status', 'ordered')->first() ?? new Plan;
        $plan->user_id = $request['user_id'];
        $plan->tariff_id = $request['plan_id'];
        $plan->status = 'ordered';
        $plan->save();

        $admins = User::where('user_role_id', 1)->get();

        foreach($admins as $admin){
            mail($admin->email, 'Заявка на изменение тарифного плана', 'Вам поступила заявка на изменение тарифного плана');
        }
        return $request['plan_id'];
    }

    public function orderAdv(Request $request)
    {
        session(['open_modal' => null]);
        session(['open_modal2' => null]);
        $adv = Adv::where('user_id', Auth::id())->where('advplace_id', $request['advplace_id'])->where('status', 'ordered')->first();

        $adv = Adv::create([
            'user_id' => Auth::id(),
            'advplace_id' => $request['advplace_id'],
            'href' => $request['href'] ?? '#',
            'banner' => $request['banner'],
            'term' => Carbon::now()->addMonths($request['active_time']) ?? '-',
            'status' => 'ordered'
        ]);


        if($request->file()){

            if($request['advplace_id'] == 1){
                $width = 1185;
                $height = 463;
            }else{
                $width = 790;
                $height = 180;
            }

            $messages = [
                'mimes' => 'Файл должен быть формата jpeg, png или svg',
                'dimensions' => 'Размер баннера должен быть ' . $width . 'x' . $height,
                'max' => 'Максимальный вес загружаемого файла 1 Мб'
            ];

            $validator = $this->validate($request, [
                'banner' => 'mimes:jpg,jpeg,png,svg|dimensions:width=' . $width . ',min_height='. $height . '|max:1024',
            ], $messages);

            // $banner_del = Adv::find($adv->id);

            // $files = $request->file();

            // if(($banner_del->banner) && (isset($files['banner']))){
            //     unlink(public_path($banner_del->banner));
            // }

            foreach ($request->file() as $key => $f) {
                $new_name[$key] = $f->getClientOriginalName();
                $f->move($_SERVER['DOCUMENT_ROOT'] . '/img/banners/', $new_name[$key]);
            }
            if(isset($new_name['banner'])){
                $adv->banner = '/img/banners/' . $new_name['banner'];
            }
            
            $adv->save();
            
        }

        request()->session()->flash('success', 'Заявка принята. Для выставления счета и уточнения деталей свяжется менеджер портала.');

        $admins = User::where('user_role_id', 1)->get();

        foreach($admins as $admin){
            mail($admin->email, 'Заявка на показ рекламного баннера', 'Вам поступила заявка на показ рекламного баннера');
        }
        return redirect()->route('home');
    }

    public function selectStatus(Request $request)
    {
        $vendor = User::find(Auth::id());
        $order = Order::find($request['order_id']);
        $order->order_status_id = $request['order_status_id'];
        $order->save();
        $order_status = OrderStatus::find($request['order_status_id']);

        if($order->order_status_id == 10){
            $vendor->bonus += $order->bonus;
            $vendor->save();
        }
        if(App::getLocale() == 'ru'){
            mail($order->email, 'Изменение статуса заказа', 'Статус Вашего заказа № ' . $request['order_id'] . ' изменен на ' . $order_status->name_ru);
        }else{
            mail($order->email, 'Зміна статусу замовлення', 'Статус Вашого замовлення № ' . $request['order_id'] . ' змінено на ' . $order_status->name_uk);
        }
        
        return redirect()->route('home');

    }

    private function searchClients($request){
        $sc = [];

            if($request->search_client_name){
                $sc['name'] = $request->search_client_name;
            }

            if($request->search_client_phone){
                $sc['phone'] = $request->search_client_phone;
            }

            if($request->search_client_gender){
                $sc['gender'] = $request->search_client_gender;
            }

            if($request->search_client_age){
                $sc['age'] = $request->search_client_age;
            }

            if($request->sort_by){
                $sc['sort'] = $request->sort_by;
            }

        return $sc;    
    }

    public function deleteAvatar(Request $request)
    {
        $user = User::find($request->user_id);

        if($user->avatar){
            unlink(public_path($user->avatar));
        }
        
        $user->avatar = null;
        $user->save();
    }

    public function addReview(Request $request)
    {
        if(App::getLocale() == 'ru'){
            if(!Auth::id()){
                request()->session()->flash('error', 'Только зарегистрированные пользователи могут оставлять отзывы');
                return redirect()->back();
            }

        }else{
            if(!Auth::id()){
                request()->session()->flash('error', 'Тільки зареєстровані користувачі можуть залишати відгуки');
                return redirect()->back();
            }

        }
        

        $user = User::find(Auth::id());
        VendorReview::create([
            'user_id' => Auth::id(),
            'vendor_id' => $request['vendor_id'],
            'review' => $request['review'],
            'rating' => $request['sociability'] ?? 5
        ]);

        $vendor = User::find($request['vendor_id']);

        //Offer::setOfferRating($request['offer_id']);
        if(App::getLocale() == 'ru'){
            request()->session()->flash('success', 'Отзыв добавлен');
            mail($vendor->email, 'Новый отзыв', 'Вы получили новый отзыв с оценкой ' . $request['sociability'] ?? 5 . " и текстом: \n\n" . $request['review']);
        }else{
            request()->session()->flash('success', 'Відгук додано');
            mail($vendor->email, 'Новий відгук', 'Ви отримали новий відгук з оцінкою ' . $request['sociability'] ?? 5 . " і текстом: \n\n" . $request['review']);
        }

        $vendor = User::find($request['vendor_id']);

        return redirect()->back();
    }

    public function deleteVendorReview(Request $request)
    {
        $review = VendorReview::find($request['id']);

        $review->delete();

        $count = VendorReview::where('user_id', Auth::id())->where('vendor_id', $review->vendor_id)->count();

        return $count;
    }

    public function deleteProductReview(Request $request)
    {
        $review = Review::find($request['id']);

        $review->delete();

        $count = Review::where('user_id', Auth::id())->where('product_id', $review->product_id)->count();

        return $count;
    }

    public function setActiveShop(Request $request)
    {
        Shop::where('user_id', Auth::id())->update(['is_active'=> 0]);
        Shop::where('id', $request['shop'])->update(['is_active'=> 1]);

        $shop = Shop::where('user_id', Auth::id())->where('is_active', 1)->first();

        $delivery = Delivery::where('shop_id', $shop->id)->get();

        return json_encode(['shop' => $shop, 'delivery' => $delivery], JSON_UNESCAPED_UNICODE);
    }

    public function removeShop(Request $request)
    {
    	$shop = Shop::find($request['shop_id']);
    	Delivery::where('shop_id', $request['shop_id'])->delete();
        $shop->delete();
    }

    public function addShop(Request $request)
    {
        $shop = Shop::create([
            'user_id' => $request['user_id'],
            'address' => $request['address'],
            'is_active' => 1
        ]);
        return $shop->id;
    }

    public function addDelivery(Request $request)
    {
        $delivery = Delivery::create([
            'shop_id' => $request['shop_id'],
            'price' => $request['cost'],
            'distance' => $request['radius']
        ]);
        return $delivery->id;
    }

    public function removeDelivery(Request $request)
    {
        $delivery = Delivery::find($request['del_id']);
        $delivery->delete();
    }

    public function sendMessage(Request $request)
    {
        session(['new' => Hash::make($request['email'])]);
        mail($request['email'], 'Активация аккаунта на Pomogaika', 'Пожалуйста, пройдите по ссылке, чтобы подтвердить аккаунт: ' . $_SERVER["HTTP_REFERER"] . '?hash=' . session('new') . '. Ссылка действительна 30 минут');
    }
}
