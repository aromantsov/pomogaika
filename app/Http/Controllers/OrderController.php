<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\Offer;
use App\Models\Product;
use App\Models\Cart;
use App\Models\Category;
use App\Models\Purchase;
use App\Models\Shop;
use App\Models\Delivery;
Use App\User;
use Auth;
use App;

class OrderController extends Controller
{
    public function setOrder(Request $request)
    {
        if(App::getLocale() == 'ru'){
           $messages = [
            'required' => 'Поля, отмеченные звездочками, обязательны для заполнения',
            'max' => 'Максимальная длина 255 символов',
            'email' => 'Поле "Электронная почта" должно быть валидным и-мейл адресом'
           ]; 
        }else{
           $messages = [
            'required' => 'Поля, відмічені зірочками, обов\'язкові для звповнення',
            'max' => 'Максимальна довжинв 255 символів',
            'email' => 'Поле "Електронна пошта" має бути валідною e-mail адресою'
           ];  
        }
        

         $validator = $this->validate($request, [
            'name' => 'required|string|max:255',
            //'lastname' => 'required|string|max:255',
            'email' => ['required', 'string', 'email', 'max:255'],
            'phone' => 'required|string'
        ], $messages);

        $order_data = [];
        
        $order_data['name'] = $request['name']; 
        $order_data['lastname'] = $request['lastname']; 
        $order_data['email'] = $request['email']; 
        $order_data['phone'] = $request['phone'] ?? ''; 
        $order_data['comment'] = $request['comment'] ?? '';
        $order_data['total'] = (int)$request['total'] ?? 0;
        $adr = $request['address'] ?? '';
        $order_data['ip'] = $_SERVER['REMOTE_ADDR'];
        $customer = User::find(Auth::id());
        if($customer){
            $bonus = $customer->bonus;
        }

        $array_offers = explode('&', $request['offers']);

        foreach($array_offers as $value){
        	$array_value = explode('=', $value);
        	$order_data[$array_value[0]] = $array_value[1];
        } 

        $order_data['address'] = $adr;

        $offers_array = [];
        foreach($order_data as $key => $value){
        	if(substr($key, 0, 5) == 'offer'){
                $offers_array[] = $value;
        	}
        }

        $vendors_array = [];
        foreach($order_data as $key => $value){
            if(substr($key, 0, 16) == 'delivery_status-'){
                $vendors_array[$key] = $value;
            }
        }

        $vendors = [];
        foreach($order_data as $key => $value){
            if(substr($key, 0, 7) == 'vendor-'){
                $vendors[] = $value;
            }
        }

        $vendors = array_unique($vendors);

        $pr_name = 'name_' . App::getLocale();

        $numbers = [];

        foreach($vendors as $vendor){
            $vendor_data = User::find($vendor);
            $order = Order::create([
                'user_id' => Auth::id() ?? 0,
                'name' => $order_data['name'],
                'lastname' => $order_data['lastname'],
                'email' => $order_data['email'],
                'phone' => $order_data['phone'],
                'comment' => $order_data['comment'],
                'address' => rawurldecode($order_data['address_' . $vendor]),
                'total' => $order_data['total'],
                'bonus' => $order_data['total'] / 10000,
                'payment' => $order_data['payment'],
                'ip' => $order_data['ip']
            ]);        

            $offers = [];
            $i = 0;
            $order_total = 0;
            $message_for_admin = "Зарегистрирован заказ № " . $order->id . ". Подробнее: \n";
            $message_for_vendor = "Вам поступил заказ № " . $order->id . ". Подробнее: \n"; 
            foreach($offers_array as $offer_id){
                if($order_data['vendor-' . $offer_id] != $vendor){
                    continue;
                }
                
                $offers[$i]['offer_id'] = $offer_id;
                $offers[$i]['quantity'] = $order_data['quan-' . $offer_id];
                $offers[$i]['order_id'] = $order->id;
                $offers[$i]['product_name'] = Offer::find($offer_id)->products->$pr_name;
                $offers[$i]['model'] = Offer::find($offer_id)->products->model;
                $offers[$i]['price'] = $order_data['price-' . $offer_id];
                $offers[$i]['total'] = $order_data['total-' . $offer_id];
                $order_total += $offers[$i]['total'];
                //$vendor = $order_data['vendor-' . $offer_id];
                $offers[$i]['delivery'] = $vendors_array['delivery_status-' . $vendor];
                Purchase::create($offers[$i]);

                $product = Offer::find($offer_id)->products;

                $product->popular += $offers[$i]['quantity'];
                $product->save();
                
                $order->bonus = $order_total / 10000;

                if($offers[0]['delivery'] == 'outshop'){
                    $shipping = 'продавцом';
                    $cost = (int)$order_data['hidden_cost_' . $vendor];
                }elseif($offers[0]['delivery'] == 'ourshipping'){
                    $shipping = 'pomogaika';
                    $cost = 700;
                }else{
                    $shipping = 'самовывоз';
                    $cost = 0;
                }

                $order->shipping_price = $cost;

                $order->total = $order_total + $cost;
                $order->save();
                
                if($customer){
                    $bonus += Offer::find($offer_id)->products->bonus;
                }

                $message_for_admin .= "\n" . $offers[$i]['product_name'] . ' ' . $offers[$i]['price'] . ' грн ' . $offers[$i]['quantity']  . ' шт. Итого:' . $offers[$i]['total'] . ' грн' . "\n";

                $message_for_vendor .= "\n" . $offers[$i]['product_name'] . ' ' . $offers[$i]['price'] . ' грн ' . $offers[$i]['quantity'] . ' шт. Итого:' . $offers[$i]['total'] . ' грн' . "\n";

                $i++;
            }

            if(App::getLocale() == 'ru'){
                mail($order_data['email'], 'Заказ №' . $order->id, 'Спасибо за заказ. Ваш заказ успешно принят в обработку. Вы сможете отследить статус заказа в вашем личном кабинете. Наши менеджеры могут связаться с вами для уточнения деталей');
            }else{
                mail($order_data['email'], 'Замовлення №' . $order->id, 'Дякуємо за замовлення. Ваше замовлення успішно принято в обробку. Вы зможете відслідковувати статус замовлення в вашем особистому кабінеті. Наші менеджеры можуть зв\'язатися з вами для уточнення деталей');
            }

            //$total_order = $order->total + $cost;

            if($offers[0]['delivery'] == 'inshop'){
                if(App::getLocale() == 'ru'){
                    $shipping = 'Самовывоз';
                }elseif(App::getLocale() == 'uk'){
                    $shipping = 'Самовивiз';
                }else{
                    $shipping = $offers[0]['delivery'];
                }
            }elseif($offers[0]['delivery'] == 'outshop'){
                if(App::getLocale() == 'ru'){
                    $shipping = 'Доставка поставщиком';
                }elseif(App::getLocale() == 'uk'){
                    $shipping = 'Доставка поставщиком';
                }else{
                    $shipping = $offers[0]['delivery'];
                } 
            }elseif($offers[0]['delivery'] == 'outshop'){
                    $shipping = 'Доставка Pomogaika'; 
            }

            $cost = $cost ?? 0;
                                  

            $message_for_admin .= "\nДоставка: " . $shipping . ', стоимость - ' . $cost . ' грн' . "\nИтого: $order->total грн";

            $message_for_vendor .= "\nДоставка: " . $shipping . ', стоимость - ' . $cost . ' грн' . "\nИтого: $order->total грн";

            $admins = User::where('user_role_id', 1)->get();

            foreach($admins as $admin){
                mail($admin->email, 'Заказ №' . $order->id, $message_for_admin);
            } 

            mail($vendor_data->email, 'Заказ №' . $order->id, $message_for_vendor);

            $numbers[] = $order->id;
        }
        
        if($customer){ 
           $bonus += session('bonus'); 
           $customer->bonus = $bonus;
           $customer->save(); 
        }

        session(['bonus' => null]);

        $carts = Cart::where('session_id', session('cart'))->get();

        foreach($carts as $cart){
            $cart->delete();
        }

        session(['cart' => null]);

        return implode('-', $numbers);
    }

    public function confirm($id)
    {
        return view('confirm', [
            'orders_id' => explode('-', $id),
            'categories' => Category::with('children')->where('parent_id', '0')->where('published', '1')->get(),
            'local' => App::getLocale()
        ]);
    }

    public function info($id)
    {
        return view('order_info', [
            'order' => Order::select('*', 'orders.total as total')->leftJoin('order_statuses', 'order_statuses.id', '=', 'orders.order_status_id')->leftJoin('purchases', 'purchases.order_id', '=', 'orders.id')->where('orders.id', $id)->firstOrFail(),
            'categories' => Category::with('children')->where('parent_id', '0')->where('published', '1')->get(),
            'purchases' => Purchase::leftJoin('offers', 'purchases.offer_id', '=', 'offers.id')->leftJoin('products', 'products.id', '=', 'offers.product_id')->where('purchases.order_id', $id)->get(),
            'local' => App::getLocale()
        ]);
    }

    public function getDelivery(Request $request)
    {
        session(['cost_' . $request['vendor_id'] => null]);
        session(['user_address_' . $request['vendor_id'] => $request['user_address']]);
        $shops = Shop::where('user_id', $request['vendor_id'])->get();
        return json_encode($shops, JSON_UNESCAPED_UNICODE);
    }

    public function costDelivery(Request $request)
    {
        $dis = $request['distance'] / 1000;
        $deliveries = Delivery::where('shop_id', $request['shop_id'])->orderBy('distance', 'asc')->get();

        foreach($deliveries as $delivery){
            if($dis < $delivery->distance){
                $cost = $delivery->price;
                break;
            }
            if(!isset($cost)){
                $cost = 0;
            }
        }

        //var_dump($cost);
        //var_dump(session('cost_' . $request['vendor_id']));

        if((session('cost_' . $request['vendor_id']) == null) || (session('cost_' . $request['vendor_id']) == 0) || (session('cost_' . $request['vendor_id']) > $cost)){ 
            session(['cost_' . $request['vendor_id'] => $cost]);
        }

        //var_dump(session('cost_' . $request['vendor_id']));
        return session('cost_' . $request['vendor_id']);
    }

    public function sessionCost(Request $request)
    {
        //session(['cost_' . $request['vendor_id'] => 0]);
    }

}
