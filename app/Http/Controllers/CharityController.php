<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App;

class CharityController extends Controller
{
    public function index()
    {
        return view('charity', [
        	'categories' => Category::with('children')->where('parent_id', '0')->where('published', '1')->get(),
            'local' => App::getLocale()
        ]);
    }
}
