<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Info;
use App\Models\Callback;
use App\Models\Plan;
use App\User;
use Carbon\Carbon;
use Validator;
use App;

class InfoController extends Controller
{
    public function index($id)
    {
    	return view('info', [
            'categories' => Category::with('children')->where('parent_id', '0')->where('published', '1')->get(),
            'info' => Info::where('id', $id)->firstOrFail(),
            'local' => App::getLocale()
    	]);
    }

    public function contact()
    {
        return view('contact', [
            'categories' => Category::with('children')->where('parent_id', '0')->where('published', '1')->get(),
            'local' => App::getLocale()
        ]);
    }

    public function callback(Request $request)
    {
        $messages = [
            'required' => 'Все поля длжны быть заполнены',
            'email' => 'Вы указали некорректный e-mail',
            'max' => 'Длина значений имени и и-мейла не должна превышать 191 символ',
            'min' => 'Напишите, пожалуйста, вопрос более развернуто'
        ];

        $validator = $this->validate($request, [
            'name' => 'required|max:191',
            'email' => 'required|email|max:191',
            'message' => 'required|min:10'
        ], $messages);

        Callback::create($request->all());

        $admins = User::where('user_role_id', 1)->get();

        foreach($admins as $admin){
            mail($admin->email, 'Новая заявка обратной связи', "Вам поступила заявка на обратную связь:\n\n Имя: " . $request['name'] . "\n Email: " . $request['email'] . "\n Вопрос: " . $request['message']);
        } 

        request()->session()->flash('success', 'Запрос отправлен');
        return redirect()->back();
    }
}
