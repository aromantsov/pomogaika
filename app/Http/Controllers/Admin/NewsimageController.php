<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Newsimage;

class NewsimageController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.newsimages.index', [
            'newsimages' => Newsimage::orderBy('created_at', 'desc')->paginate(10),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.newsimages.create', [
            'newsimage' => [],
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        if($request->file()){

            $newsimage = new Newsimage;

            $new_name = [];

            $messages = [
                'file' => 'Файл должен быть формата jpeg, png или svg'
            ];

            $validator = $this->validate($request, [
                'file' => 'mimes:jpg,jpeg,png,svg'
            ], $messages);

            foreach ($request->file() as $key => $f) {
                $new_name[$key] = $f->getClientOriginalName();
                $f->move($_SERVER['DOCUMENT_ROOT'] . '/img/newsimages/', $new_name[$key]);
            }
            if(isset($new_name['image'])){
                $newsimage->image = '/img/newsimages/' . $new_name['image'];
            }
            
            $newsimage->save();
            request()->session()->flash('success', 'Изображение загружено');
        }

        request()->session()->flash('error', 'Пожалуйста, загрузите изображение');
        
        return redirect()->route('admin.newsimage.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Newsimage $newsimage)
    {
        if($newsimage->image){
            unlink(public_path($newsimage->image));
        }

        $newsimage->delete();

        request()->session()->flash('success', 'Изображение удалено');

        return redirect()->route('admin.newsimage.index');
    }
}
