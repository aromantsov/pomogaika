<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\OrderStatus;
use App\Models\Purchase;
use App\Models\Offer;
use App\User;
use App;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.orders.index', [
            'orders' => Order::select('*', 'orders.id as id')->leftJoin('order_statuses', 'orders.order_status_id', '=', 'order_statuses.id')->orderBy('orders.created_at', 'desc')->paginate(10),
            'local' => App::getLocale()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        $vendor = Order::getOrderVendor($order->id);
        return view('admin.orders.edit', [
            'order' => $order,
            'user' => User::find($order->user_id) ?? 0,
            'statuses' => OrderStatus::all(),
            'purchases' => Purchase::where('order_id', $order->id)->get(),
            'offers' => Offer::with('products')->where('user_id', $vendor)->get(),
            'local' => App::getLocale()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        
        //$path = $request->file('image')->store('uploads', 'public');
        $order->update($request->except('bonus'));

        $user = User::find($order->user_id) ?? 0;

        if($user){
            $user->bonus = $request['bonus'];
            $user->save();
        }
        

        request()->session()->flash('success', 'Данные обновлены');

        return redirect()->route('admin.orders.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function autocomplete(Request $request)
    {
        $vendor = Order::getOrderVendor($request['order_id']);
        return json_encode(Offer::with('products')->where('user_id', $vendor)->get(), JSON_UNESCAPED_UNICODE);
    }

    public function savePurchase(Request $request)
    {

        $messages = [
            'required' => 'Поле Цена должно быть заполнено'
        ];

        Purchase::where('order_id', $request['order_id'])->delete();
        
        //dd($request->property);
        if($request->purchase !== null){
        $alltotal = (int)$request['shipping_price'];    
        foreach($request->purchase as $purchase){

            $validator = $this->validate($request, [
               //'property.*.name' => ['required', new IsValidProperty($request->product_id, $property['name'])],
               'purchase.*.price' => 'required',
               //'property.*.value_uk' => 'required|string|max:191'
            ], $messages);

            $prop_val = Purchase::where('offer_id', $purchase['name'])->where('order_id', $request['order_id'])->first();

            if(!$prop_val){
                $prop_val = new Purchase;
            }

            $prop_val = new Purchase;
            $prop_val->offer_id = $purchase['name'];
            $prop_val->order_id = $request['order_id'];
            $prop_val->price = $purchase['price'] ?? 0;
            $prop_val->quantity = $purchase['quantity'] ?? 1;
            $total = $prop_val->price * $prop_val->quantity;
            $prop_val->total = $total ?? 0;
            //$prop_val->value_uk = $property['value_uk'];
            $prop_val->save();

            $alltotal += $total ?? 0;
        }
        }
        
        $order = Order::find($request['order_id']);
        $order->total = $alltotal;
        $order->shipping_price = (int)$request['shipping_price'];
        $order->save();

        request()->session()->flash('success', 'Заказ успешно обновлен');
        return redirect()->back();
    }

    public function setPrice(Request $request)
    {
        $data = Offer::where('id', $request['offer'])->first();
        return $data->price;
    }
}
