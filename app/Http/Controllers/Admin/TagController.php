<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Tag;
use App;

class TagController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.tags.index', [
            'tags' => Tag::paginate(10),
            'local' => App::getLocale()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('admin.tags.create', [
            'tag' => [],
            'local' => App::getLocale()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'required' => 'Поля обязательны для заполнения',
            'max' => 'Максимальная длина - 191 символ'
        ];

        $validator = $this->validate($request, [
            'tag_ru' => 'required|string|max:191',
            'tag_uk' => 'required|string|max:191',
        ], $messages);

        Tag::create($request->all());

        request()->session()->flash('success', 'Тег успешно создан');

        return redirect()->route('admin.tag.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Tag $tag)
    {
        return view('admin.tags.edit', [
            'tag' => $tag,
            'local' => App::getLocale()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tag $tag)
    {
        $messages = [
            'required' => 'Поля обязательны для заполнения',
            'max' => 'Максимальная длина - 191 символ'
        ];

        $validator = $this->validate($request, [
            'tag_ru' => 'required|string|max:191',
            'tag_uk' => 'required|string|max:191',
        ], $messages);

        $tag->tag_ru = $request['tag_ru'];
        $tag->tag_uk = $request['tag_uk'];
        $tag->save();

        request()->session()->flash('success', 'Данные обновлены');

        return redirect()->route('admin.tag.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tag $tag)
    {
        $tag->delete();

        request()->session()->flash('success', 'Тег удален');

        return redirect()->route('admin.tag.index');
    }
}
