<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Manufacturer;
use App\Models\Product;
use App;

class ManufacturerController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.manufacturers.index', [
            'manufacturers' => Manufacturer::orderBy('brand', 'asc')->paginate(10),
            'local' => App::getLocale()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('admin.manufacturers.create', [
            'manufacturer' => [],
            'local' => App::getLocale()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $messages = [
            'required' => 'Поле не заполнено'
        ];

        $validator = $this->validate($request, [
            'brand' => 'required'
        ], $messages);
        
        $manufacturer = Manufacturer::create(['brand' => $request['brand']]);

        request()->session()->flash('success', 'Производитель успешно добавлен');
        
        return redirect()->route('admin.manufacturers.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Manufacturer $manufacturer)
    {
        return view('admin.manufacturers.edit', [
            'manufacturer' => $manufacturer,
            'local' => App::getLocale()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Manufacturer $manufacturer)
    {
        $messages = [
            'required' => 'Поле не заполнено'
        ];

        $validator = $this->validate($request, [
            'brand' => 'required'
        ], $messages);
        
        $manufacturer->brand = $request['brand'];

        $manufacturer->save();

        request()->session()->flash('success', 'Данные обновлены');
        
        return redirect()->route('admin.manufacturers.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Manufacturer $manufacturer)
    {
        $products = Product::where('manufacturer_id', $manufacturer->id)->first();
        if($products){
            request()->session()->flash('error', 'У данного производителя есть товары. Вы не можете его удалить');
            return redirect()->route('admin.manufacturers.index');
        }
        $manufacturer->delete();
        request()->session()->flash('success', 'Производитель удален');
        return redirect()->route('admin.manufacturers.index');
    }
}
