<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\News;
use App\Models\Tag;
use App;

class NewsController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.news.index', [
            'news' => News::orderBy('created_at', 'desc')->paginate(10),
            'local' => App::getLocale()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.news.create', [
            'news' => [],
            'tags' => Tag::all(),
            'taglist' => [],
            'simlist' => [],
            'similars' => News::all(),
            'local' => App::getLocale()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $news = News::create($request->except('tags', 'similar'));

        $new_name = [];
        if($request->file()){

            $messages = [
                'file' => 'Файл должен быть формата jpeg, png или svg'
            ];

            $validator = $this->validate($request, [
                'file' => 'mimes:jpg,jpeg,png,svg'
            ], $messages);

            foreach ($request->file() as $key => $f) {
                $new_name[$key] = $f->getClientOriginalName();
                $f->move($_SERVER['DOCUMENT_ROOT'] . '/img/news/', $new_name[$key]);
            }
            if(isset($new_name['picture'])){
                $news->picture = '/img/news/' . $new_name['picture'];
            }
            
            $news->save();
        }
        $news->tags()->attach($request->input('tags'));
        $news->similar()->attach($request->input('similar'));
        request()->session()->flash('success', 'Новость успешно создана');
        return redirect()->route('admin.news.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(News $news)
    {

        $tag_list = [];

        foreach($news->tags as $tag){
            $tag_list[] = $tag->id;
        }

        $sim_list = [];

        foreach($news->similar as $sim){
        	$sim_list[] = $sim->id;
        }

        return view('admin.news.edit', [
            'news' => $news,
            'tags' => Tag::all(),
            'taglist' => $tag_list,
            'simlist' => $sim_list,
            'similars' => News::all(),
            'local' => App::getLocale()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, News $news)
    {
        foreach($news->tags as $tag){
            $news->tags()->detach($tag);
        }

        foreach($news->similar as $sim){
            $news->tags()->detach($sim);
        }

        if($request->file()){

            $messages = [
                'file' => 'Файл должен быть формата jpeg, png или svg'
            ];

            $validator = $this->validate($request, [
                'file' => 'mimes:jpg,jpeg,png,svg'
            ], $messages);

            $image_del = News::find($news->id);

            $files = $request->file();

            if(($image_del->picture) && (isset($files['picture']))){
                unlink(public_path($image_del->picture));
            }

            
        }
        //$path = $request->file('image')->store('uploads', 'public');
        $news->update($request->except('picture', 'tags', 'similar'));

        $news->tags()->attach($request->input('tags'));
        $news->similar()->attach($request->input('similar'));

        if($request->file()){

            foreach ($request->file() as $key => $f) {
                $new_name[$key] = $f->getClientOriginalName();
                $f->move($_SERVER['DOCUMENT_ROOT'] . '/img/news/', $new_name[$key]);
            }
            if(isset($new_name['picture'])){
                $news->picture = '/img/news/' . $new_name['picture'];
            }
            
            $news->save();
        }

        request()->session()->flash('success', 'Данные обновлены');

        return redirect()->route('admin.news.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(News $news)
    {
        if($news->picture){
            unlink(public_path($news->picture));
        }

        foreach($news->tags as $tag){
            $news->tags()->detach($tag);
        }

        foreach($news->similar as $sim){
            $news->tags()->detach($sim);
        }

        $news->delete();

        request()->session()->flash('success', 'Новость удалена');

        return redirect()->route('admin.news.index');
    }

     public function deletePicture(Request $request)
    {
        $news = News::find($request->news_id);

        if($news->picture){
                unlink(public_path($news->picture));
            }
        
        $news->picture = null;
        $news->save();
    }
}
