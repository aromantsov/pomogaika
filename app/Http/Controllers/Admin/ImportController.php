<?php

namespace App\Http\Controllers\Admin;

use App\Models\Product;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Imports\ProductsImport;
use Maatwebsite\Excel\Facades\Excel;
use App;
use Validator;

class ImportController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index()
    {
    	return view('admin.import', [
            'local' => App::getLocale()
        ]);
    }

    public function upload(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'file' => 'required|max:100000|mimes:xlsx,xls,csv'
        ]);

        if($validator->passes()){
            \Excel::import(new ProductsImport, request()->file('file'));
            // $dataTime = date('Ymd-His');
            // $file = $request->file('file');
         //    $fileName = $dataTime . '_' . $file->getClientOriginalName();
         //    $savePath = public_path('/upload/');
         //    $new_file = $file->move($savePath, $fileName);
            return redirect()->back()->with(['success' => 'Файл успешно импортирован']);
        }else{
            return redirect()->back()->with(['errors' => $validator->errors()->all()]);
        }
    }

}
 