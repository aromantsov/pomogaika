<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Property;
use App\Rules\IsValidProperty;

class PropertyController extends Controller
{
	public function __construct()
    {
        $this->middleware('admin');
    }

    public function saveProperty(Request $request)
    {

        $messages = [
        	'required' => 'Поле должно быть заполнено'
        ];

        Property::where('product_id', $request['product_id'])->delete();
        
        //dd($request->property);
        if($request->property !== null){
        foreach($request->property as $property){

        	$validator = $this->validate($request, [
               //'property.*.name' => ['required', new IsValidProperty($request->product_id, $property['name'])],
               'property.*.value_ru' => 'required|string|max:191',
               'property.*.value_uk' => 'required|string|max:191'
            ], $messages);

            $prop_val = Property::where('attribute_id', $property['name'])->where('product_id', $request['product_id'])->first();

            if(!$prop_val){
                $prop_val = new Property;
            }

            $prop_val = new Property;
            $prop_val->attribute_id = $property['name'];
            $prop_val->product_id = $request['product_id'];
            $prop_val->value_ru = $property['value_ru'];
            $prop_val->value_uk = $property['value_uk'];
            $prop_val->save();
        }
        }
        request()->session()->flash('success', 'Характеристика успешно создана');
        return redirect()->back();
    }
}
