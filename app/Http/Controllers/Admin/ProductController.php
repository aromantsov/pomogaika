<?php

namespace App\Http\Controllers\Admin;

use App\Models\Product;
use App\Models\Category;
use App\Models\Picture;
use App\Models\Property;
use App\Models\Attribute;
use App\Models\Manufacturer;
use App\Models\Offer;
use App\Models\ProductCalculator;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.products.index', [
            'products' => Product::orderBy('id', 'desc')->paginate(10),
            'local' => App::getLocale()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.products.create', [
            'product' => [],
            'categories' => Category::with('children')->where('parent_id', 0)->get(),
            'manufacturers'=> Manufacturer::orderBy('brand', 'asc')->get(),
            'vendors' => User::where('user_role_id', 3)->get(),
            'delimiter' => '',
            'selected_calc' => [],
            'calculators' => json_decode(file_get_contents('https://pomogayka.ua/api/calculators')) ?? [],
            'local' => App::getLocale()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $product = Product::create($request->except('pictures', 'calculator_id'));

        if(is_array($request['calculator_id'])){
            for($i = 0; $i < count($request['calculator_id']) ?? 0; $i++){
                ProductCalculator::updateOrCreate(['product_id' => $product->id, 'calculator' => $request['calculator_id'][$i]]);
                $not_del[] = $request['calculator_id'][$i];
            }
        }
        
        if($request->file()){

            $files = $request->file();

            $messages = [
                'file' => 'Файл должен быть формата jpeg, png или svg'
            ];

            $validator = $this->validate($request, [
                'file' => 'mimes:jpg,jpeg,png,svg'
            ], $messages);

            foreach ($request->file() as $key => $f) {
                if($key == 'pictures'){
                    foreach($f as $one_f){
                        $new_name = $one_f->getClientOriginalName();
                        $one_f->move($_SERVER['DOCUMENT_ROOT'] . '/img/product_pictures/', $new_name);
                        Picture::create([
                            'product_id' => $product->id,
                            'image' => '/img/product_pictures/' . $new_name
                        ]);
                    }
                }else{
                    $new_name = $f->getClientOriginalName();
                    $f->move($_SERVER['DOCUMENT_ROOT'] . '/img/product/', $new_name);
                    $product->image = '/img/product/' . $new_name;
                    $product->save();
                }
            }
            
        }

        // $offer = Offer::create([
        //     'product_id' => $product->id,
        //     'user_id' => 3,
        //     'vendor_model' => $request['model'], 
        //     'price' => $request['price']
        // ]);

        request()->session()->flash('success', 'Товар успешно создан');
        return redirect()->route('admin.product.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $calc = ProductCalculator::where('product_id', $product->id)->get();
        $selected_calc = [];

        foreach($calc as $value){
            $selected_calc[] = $value['calculator'];
        }
        return view('admin.products.edit', [
            'product' => $product,
            'categories' => Category::with('children')->where('parent_id', 0)->get(),
            'manufacturers'=> Manufacturer::orderBy('brand', 'asc')->get(),
            'vendors' => User::where('user_role_id', 3)->get(),
            'delimiter' => '',
            'properties' => Property::with('attributes')->where('product_id', $product->id)->get(),
            'attributes' => Attribute::all(),
            //'pr_vendors' => User::where('user_role_id', 3)->where('product_id', $product->id)->get(),
            'offers' => Offer::with('users')->where('product_id', $product->id)->get(),
            'selected_calc' => $selected_calc,
            'calculators' => json_decode(file_get_contents('https://pomogayka.ua/api/calculators')) ?? [],
            'local' => App::getLocale()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {   
        if($request->file()){

            $messages = [
                'file' => 'Файл должен быть формата jpeg, png или svg'
            ];

            $validator = $this->validate($request, [
                'file' => 'mimes:jpg,jpeg,png,svg'
            ], $messages);

            $image_del = Product::find($product->id);

            $files = $request->file();

            if(($image_del->image) && (isset($files['image']))){
                unlink(public_path($image_del->image));
            }
            

            foreach ($request->file() as $key => $f) {
                if($key == 'pictures'){
                    foreach($f as $one_f){
                        $new_name = $one_f->getClientOriginalName();
                        $one_f->move($_SERVER['DOCUMENT_ROOT'] . '/img/product_pictures/', $new_name);
                        Picture::create([
                            'product_id' => $product->id,
                            'image' => '/img/product_pictures/' . $new_name
                        ]);
                    }
                }else{
                    $new_name = $f->getClientOriginalName();
                    $f->move($_SERVER['DOCUMENT_ROOT'] . '/img/product/', $new_name);
                    $main_image_name = $new_name;
                }
            }

            
        }

        $not_del = [];

        $product->update($request->except('pictures', 'calculator_id'));
        
        if(is_array($request['calculator_id'])){
            for($i = 0; $i < count($request['calculator_id']) ?? 0; $i++){
                ProductCalculator::updateOrCreate(['product_id' => $product->id, 'calculator' => $request['calculator_id'][$i]]);
                $not_del[] = $request['calculator_id'][$i];
            }
        }
        
        ProductCalculator::where('product_id', $product->id)->whereNotIn('calculator', $not_del)->delete();

        if($request->file()){

            if(isset($main_image_name)){
                $product->image = '/img/product/' . $main_image_name;
            }
            
            $product->save();
        }

        //$product->categories()->delete();

        // if($request->input('category_id')){
        //     $product->categories()->save($request->input('category_id'));
        // }

        request()->session()->flash('success', 'Данные обновлены');

        return redirect()->route('admin.product.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //$product->categories()->delete();
        if($product->image){
            unlink(public_path($product->image));
        }

        if($product->offers() !== null){
            $product->offers()->delete();
        }
        
        if($product->pictures() !== null){
            $product->pictures()->delete();
        }
        
        if($product->properties() !== null){
            $product->properties()->delete();
        }
        
        if($product->reviews() !== null){
            $product->reviews()->delete();
        }

        if($product->calculators() !== null){
            $product->calculators()->delete();
        }
        
        
        $product->delete();

        return redirect()->route('admin.product.index');
    }

    public function deleteImage(Request $request)
    {
        $product = Product::find($request->product_id);

        if($product->image){
                unlink(public_path($product->image));
            }
        
        $product->image = null;
        $product->save();
    }

    public function deletePicture(Request $request)
    {
        //dump($request);
        $picture = Picture::find($request->picture_id);

        if($picture && file_exists($picture)){
            unlink(public_path($picture->image));
        }

        $picture->delete();
        return 'success';
    }

    public function autocomplete(Request $request)
    {
        return json_encode(Attribute::all(), JSON_UNESCAPED_UNICODE);
    }

    public function autocomplete2(Request $request)
    {
        return json_encode(User::where('user_role_id', 3)->get(), JSON_UNESCAPED_UNICODE);
    }

    public function saveOffer(Request $request)
    {
        //dd($request->offer);

        $messages = [
            'required' => 'Поле Цена должно быть заполнено'
        ];

        Offer::where('product_id', $request['product_id'])->delete();
        
        //dd($request->property);
        if($request->offer !== null){
        foreach($request->offer as $offer){

            $validator = $this->validate($request, [
               //'property.*.name' => ['required', new IsValidProperty($request->product_id, $property['name'])],
               'offer.*.price' => 'required',
               //'property.*.value_uk' => 'required|string|max:191'
            ], $messages);

            $prop_val = Offer::where('user_id', $offer['name'])->where('product_id', $request['product_id'])->first();

            if(!$prop_val){
                $prop_val = new Offer;
            }

            $prop_val = new Offer;
            $prop_val->user_id = $offer['name'];
            $prop_val->product_id = $request['product_id'];
            $prop_val->price = $offer['price'];
            //$prop_val->value_uk = $property['value_uk'];
            $prop_val->save();
        }
        }
        $product = Product::find($request['product_id']);
        $product->price = Offer::select('price')->where('product_id', $request['product_id'])->avg('price');
        $product->price = (int)$product->price;
        $product->save();
        request()->session()->flash('success', 'Предложение успешно создано');
        return redirect()->back();
    }
}
