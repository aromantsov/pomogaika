<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use App;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function dashboard()
    {
    	return view('admin.dashboard', [
            'categories' => Category::latestCategories(5),
            'products' => \App\Models\Product::published(1)->orderBy('created_at')->get(),
            'count_categories' => Category::count(),
            'count_products' => Product::count(),
            'local' => App::getLocale()
    	]);
    }
}