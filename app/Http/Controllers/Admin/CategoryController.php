<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.categories.index', [
            'categories' => Category::orderBy('id', 'asc')->paginate(10),
            'local' => App::getLocale()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.categories.create', [
                'category' => [],
                'categories' => Category::with('children')->where('parent_id', '0')->get(),
                'delimiter' => '',
                'local' => App::getLocale()
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $category = Category::create($request->all());

        $new_name = [];
        if($request->file()){

            $messages = [
                'file' => 'Файл должен быть формата jpeg, png или svg'
            ];

            $validator = $this->validate($request, [
                'file' => 'mimes:jpg,jpeg,png,svg'
            ], $messages);

            foreach ($request->file() as $key => $f) {
                $new_name[$key] = $f->getClientOriginalName();
                $f->move($_SERVER['DOCUMENT_ROOT'] . '/img/category/' . $key . '/', $new_name[$key]);
            }
            if(isset($new_name['icon'])){
                $category->icon = '/img/category/icon/' . $new_name['icon'];
            }
            
            if(isset($new_name['image'])){
                $category->image = '/img/category/image/' . $new_name['image'];
            }
            $category->save();
            request()->session()->flash('success', 'Категория успешно создана');
        }

        return redirect()->route('admin.category.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        return view('admin.categories.edit', [
                'category' => $category,
                'categories' => Category::with('children')->where('parent_id', '0')->get(),
                'delimiter' => '',
                'local' => App::getLocale()
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        if($request->file()){

            $messages = [
                'file' => 'Файл должен быть формата jpeg, png или svg'
            ];

            $validator = $this->validate($request, [
                'file' => 'mimes:jpg,jpeg,png,svg'
            ], $messages);

            $file = $request->file();

            $image_del = Category::find($category->id);

            if($image_del->icon && isset($file['icon'])){
                unlink(public_path($image_del->icon));
            }
            if($image_del->image && isset($file['image'])){
                unlink(public_path($image_del->image));
            }

            $new_name = [];
            foreach ($request->file() as $key => $f) {
                $new_name[$key] = $f->getClientOriginalName();
                $f->move($_SERVER['DOCUMENT_ROOT'] . '/img/category/' . $key . '/', $new_name[$key]);
            }

            
        }

        $category->update($request->all());

        if($request->file()){
            if(isset($new_name['icon'])){
                $category->icon = '/img/category/icon/' . $new_name['icon'];
            }
            
            if(isset($new_name['image'])){
                $category->image = '/img/category/image/' . $new_name['image'];
            }
            
            $category->save();
        }

        request()->session()->flash('success', 'Данные обновлены');

        return redirect()->route('admin.category.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $products = Product::where('category_id', $category->id)->first();

        if($products){
             return view('admin.categories.edit', [
                'category' => $category,
                'categories' => Category::with('children')->where('parent_id', '0')->get(),
                'delimiter' => '',
                'local' => App::getLocale(),
                'error_busy' => true
            ]);
        }else{
           if($category->icon){
              unlink(public_path($category->icon));
           } 
           if($category->image){
              unlink(public_path($category->image));
           } 
           $category->delete();
           return redirect()->route('admin.category.index'); 
        }    
    }

    public function deleteIcon(Request $request)
    {
        $category = Category::find($request->category_id);

        if($category->icon){
                unlink(public_path($category->icon));
            }
        
        $category->icon = null;
        $category->save();
    }

    public function deleteImage(Request $request)
    {
        $category = Category::find($request->category_id);

        if($category->image){
                unlink(public_path($category->image));
            }
        
        $category->image = null;
        $category->save();
    }
}
