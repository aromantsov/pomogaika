<?php

namespace App\Http\Controllers\Admin\UserManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Adv;

class AdvController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.user_management.advs.index', [
            'advs' => Adv::with('users')->where('status', 'ordered')->paginate(10),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $adv = Adv::with('users')->where('id', $id)->first();
        return view('admin.user_management.advs.edit', [
             'adv' => $adv, 
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages = [
            'required' => 'Укажите дату последнего дня показа баннера' 
        ];

        $validator = $this->validate($request, [
            'term' => 'required',
        ], $messages);

        $adv = Adv::find($id);

        if($request->file()){

            $messages = [
                'mimes' => 'Файл должен быть формата jpeg, png или svg'
            ];

            $validator = $this->validate($request, [
                'banner' => 'mimes:jpg,jpeg,png,svg'
            ], $messages);

            $banner_del = Adv::find($adv->id);

            $files = $request->file();

            if(($banner_del->banner) && (isset($files['banner']))){
                unlink(public_path($banner_del->banner));
            }
            
        }

        if($request['send'] == 'Активировать'){

            $adv->advplace_id = $request['advplace_id'];
            $adv->term = $request['term'];
            $adv->href = $request['href'];
            $adv->status = 'active';
            $adv->save();



            mail($request['email'], 'Активация показа рекламного баннера', 'Заявка на показ рекламного баннера активирована.');
            request()->session()->flash('success', 'Заявка на показ рекламного баннера активирована');
        }else{
            $adv->advplace_id = $request['advplace_id'];
            $adv->term = $request['term'];
            $adv->href = $request['href'];
            $adv->save();
            request()->session()->flash('success', 'Содержание заявки изменено');
        }

        
        if($request->file()){

            foreach ($request->file() as $key => $f) {
                $new_name[$key] = $f->getClientOriginalName();
                $f->move($_SERVER['DOCUMENT_ROOT'] . '/img/banners/', $new_name[$key]);
            }
            if(isset($new_name['banner'])){
                $adv->banner = '/img/banners/' . $new_name['banner'];
            }
            
            $adv->save();
        }
        return redirect()->route('admin.user_management.adv.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $adv = Adv::find($id);
        $adv->delete();
        return redirect()->route('admin.user_management.adv.index');
    }

    public function deleteBanner(Request $request)
    {
        $adv = Adv::find($request->adv_id);

        if($adv->banner){
                unlink(public_path($adv->banner));
            }
        
        $adv->banner = null;
        $adv->save();
    }
}
