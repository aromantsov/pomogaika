<?php

namespace App\Http\Controllers\Admin\UserManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\UserRole;
use App\User;

class UserRoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.user_management.userroles.index', [
            'roles' => UserRole::paginate(10)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.user_management.userroles.create', [
            'userrole' => [],
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $request->validate([
            'name' => 'required|string|max:255',
        ]);

        UserRole::create([
            'name' => $request['name'],
        ]);

        return redirect()->route('admin.user_management.role.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $userrole = UserRole::find($id);
         return view('admin.user_management.userroles.edit', [
            'userrole' => $userrole,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $userrole = UserRole::find($id);
        $validator = $this->validate($request, [
            'name' => 'required|string|max:255',
        ]);

        $userrole->name = $request['name'];
        $userrole->save();  

        return redirect()->route('admin.user_management.role.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $userrole = UserRole::find($id);
        $users = User::where('user_role_id', $id)->first();

        if($users){
             return view('admin.user_management.userroles.edit', [
                'userrole' => $userrole,
                'error_busy' => true
            ]);
        }else{
           $userrole->delete();
           return redirect()->route('admin.user_management.role.index'); 
        }
    }
}