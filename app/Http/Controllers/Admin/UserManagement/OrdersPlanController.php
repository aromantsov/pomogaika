<?php

namespace App\Http\Controllers\Admin\UserManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Plan;
use App\Models\Tariff;

class OrdersPlanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.user_management.orders_plans.index', [
            'orders' => Plan::with('users')->with('tariffs')->where('status', 'ordered')->paginate(10),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $plan = Plan::with('users')->with('tariffs')->where('id', $id)->first();
         return view('admin.user_management.orders_plans.edit', [
             'plan' => $plan, 
             'tariffs' => Tariff::all()
         ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages = [
            'required' => 'Укажите дату последнего дня действия тарифа' 
        ];

        $validator = $this->validate($request, [
            'term' => 'required',
        ], $messages);

        if($request['send'] == 'Активировать'){
            $order = Plan::where('user_id', $request['user_id'])->where('status', 'ordered')->first();
            if($order){
                $order->delete();
            } 
  
            $plan = Plan::where('user_id', $request['user_id'])->where('status', '!=', 'ordered')->first();

            if($plan){
                $plan->tariff_id = $request['tariff_id'];
                $plan->term = $request['term'];
                $plan->save();
            }else{
                $plan = Plan::create([
                    'user_id' => $request['user_id'],
                    'tariff_id' => $request['tariff_id'],
                    'term' => $request['term'],
                    'status' => 'active'
                ]);
            }

            $tariff = Tariff::find($request['tariff_id']);

            mail($request['email'], 'Изменение тарифного плана обслуживания', 'Тарифный план ' . $tariff->name . ' активирован до ' . $request['term'] . '.');
            request()->session()->flash('success', 'Тарифный план активирован');
        }else{
            $plan = Plan::find($id);
            $plan->tariff_id = $request['tariff_id'];
            $plan->term = $request['term'];
            $plan->save();
            request()->session()->flash('success', 'Содержание заявки изменено');
        }
        return redirect()->route('admin.user_management.orderplan.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $plan = Plan::find($id);
        $plan->delete();
        return redirect()->route('admin.user_management.orderplan.index');
    }
}
