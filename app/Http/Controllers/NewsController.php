<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\News;
use App\Models\Tag;
use App;

class NewsController extends Controller
{
    public function index(){
    	return view('newslist', [
    		'newslist' => News::with('tags')->with('similar')->where('published', 1)->paginate(10),
    		'tags' => Tag::all(),
            'categories' => Category::with('children')->where('parent_id', '0')->where('published', '1')->get(),
            'local' => App::getLocale()
    	]);
    }

    public function show($id)
    {
        $news = News::where('id', $id)->firstOrFail();

        $similar = $news->similar->take(2);
        $news_tags = $news->tags;

        if(!session('views')){
        	session(['views' => true]);
        	$news->views++;
        	$news->save();
        }

        return view('news', [
    		'news' => $news,
    		'similar' => $similar,
    		'newstags' => $news_tags,
    		'tags' => Tag::all(),
            'categories' => Category::with('children')->where('parent_id', '0')->where('published', '1')->get(),
            'local' => App::getLocale()
    	]);
    }

    public function tag($id)
    {
        $tag = Tag::find($id);
        return view('search.tag', [
            'newslist' => $tag->news,
            'tags' => Tag::all(),
            'categories' => Category::with('children')->where('parent_id', '0')->where('published', '1')->get(),
            'local' => App::getLocale()
        ]);
    }

    public function search(Request $request)
    {

        return view('search.news', [
            'get' => $request,
            'newslist' => News::with('tags')->with('similar')->where('name_ru', 'like', '%' . $request['search'] . '%')->orWhere('name_uk', 'like', '%' . $request['search'] . '%')->orWhere('description_ru', 'like', '%' . $request['search'] . '%')->orWhere('description_uk', 'like', '%' . $request['search'] . '%')->where('published', 1)->paginate(10),
            'tags' => Tag::all(),
            'categories' => Category::with('children')->where('parent_id', '0')->where('published', '1')->get(),
            'local' => App::getLocale()
        ]);
    }
}
