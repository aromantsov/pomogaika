<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Product;
use App\Models\Manufacturer;
use App\Models\Adv;
use App\User;
use App;

class CategoryController extends Controller
{
    public function index()
    {
    	return view('categories', [
    		'categories' => Category::with('children')->where('parent_id', '0')->where('published', '1')->get(),
            'slider' => Adv::where('banner', '!=', NULL)->where('status', 'active')->where('advplace_id', 2)->get(),
    		'local' => App::getLocale()
    	]);
    }

    public function show($id, Request $request)
    {
    	$category_page = Category::find($id);
        
        if(!$category_page){
        	return redirect()->route('main');
        }

        $vendors = Product::join('offers', 'offers.product_id', '=', 'products.id')->select('offers.user_id as vendor_id')->where('products.category_id', $id)->distinct()->get();

        //foreach($vendors as $vendor){
            $filters['vendor'] = []; //$vendor->vendor_id;
        //}

        $manufacturers = Product::select('manufacturer_id')->where('category_id', $id)->distinct()->get();

        //foreach($manufacturers as $manufacturer){
            $filters['manufacturer'] = []; //$manufacturer->manufacturer_id;
        //} 

    	return view('category', [
            'id' => $id,
            'get' => $request,
            'filters' => $filters ?? [],
            'all_checked' => true,
            'categories' => Category::with('children')->where('parent_id', '0')->where('published', '1')->get(),
    		'category_page' => $category_page,
    		'subcategories' => Category::with('children')->where('parent_id', $id)->where('published', '1')->get(),
            'products' => Product::getCategoryProducts($id, $request['sort_by']),
            'vendors' => User::getFilterVendors($id),
            'min_price' => Product::where('category_id', $id)->min('price'),
            'max_price' => Product::where('category_id', $id)->max('price'),
            'from_price' => Product::where('category_id', $id)->min('price'),
            'to_price' => Product::where('category_id', $id)->max('price'),
            'manufacturers' => Manufacturer::getFilterManufacturers($id),
            'slider' => Adv::where('banner', '!=', NULL)->where('status', 'active')->where('advplace_id', 2)->get(),
    		'local' => App::getLocale()
    	]);
    }

    public function filters(Request $request, $id)
    {
        $category_page = Category::find($id);
        
        if(!$category_page){
            return redirect()->route('main');
        }

        //$products = Product::where('category_id', $id)->where('manufacturer_id', ($request['vendor'])->where('price', >=, $request['from_price'])->where('price', <=, $request['to_price'])->get();

        $min_price = Product::where('category_id', $id)->min('price');
        $max_price = Product::where('category_id', $id)->max('price');

        $from_price = $min_price;
        $to_price = $max_price;

        if((isset($request['from_price'])) && ($request['from_price'] > $min_price)){
            $from_price = $request['from_price'];
        }

        if((isset($request['to_price'])) && ($request['to_price'] < $max_price)){
            $to_price = $request['to_price'];
        }

        if($request['price_range']){
            $range = explode(';', $request['price_range']);
            $from_price = $range[0];
            $to_price = $range[1];
        }

        $get = $request;

        if(isset($_GET['sort_by'])){
            $_GET['sort_by'] = null;
        }

        return view('category', [
            'id' => $id,
            'get' => $get,
            'param' => $_GET,
            'filters' => $request,
            'all_checked' => $request['all'] ?? false,
            'categories' => Category::with('children')->where('parent_id', '0')->where('published', '1')->get(),
            'category_page' => $category_page,
            'subcategories' => Category::with('children')->where('parent_id', $id)->where('published', '1')->get(),
            'products' => Product::getProducts($request, $id, $from_price, $to_price, $request['sort_by']),
            'vendors' => User::getFilterVendors($id),
            'min_price' => $min_price,
            'max_price' => $max_price,
            'from_price' => $from_price,
            'to_price' => $to_price,
            'manufacturers' => Manufacturer::getFilterManufacturers($id),
            'slider' => Adv::where('banner', '!=', NULL)->where('status', 'active')->where('advplace_id', 2)->get(),
            'local' => App::getLocale()
        ]);
    }
}
