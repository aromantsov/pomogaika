<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Offer;
use App\Models\Cart;
use Auth;
use App;
use App\User;

class CartController extends Controller
{
    public function add(Request $request)
    {
    	$json = array();

    	if (isset($request['offer_id'])) {
			$offer_id = (int)$request['offer_id'];
		} else {
			$offer_id = 0;
		}

		$offer_info = Offer::with('products')->where('id', $offer_id)->first();
		
		if($offer_info){
			$quantity = $request['quantity'] ?? 1;
		}
        
        if(!session('cart')){
        	session(['cart' => substr(bin2hex(openssl_random_pseudo_bytes(26)), 0, 26)]);
        }
		

		$total = Cart::where('user_id', Auth::id() ?? 0)->where('session_id', session('cart'))->where('offer_id', $offer_id)->first();

        if(!$total){
        	Cart::create(['api_id' => 0, 'user_id' => Auth::id() ?? 0, 'session_id' => session('cart'), 'offer_id' => $offer_id, 'quantity' => $quantity]);
        }else{
        	$total->api_id = 0;
		    $total->user_id = Auth::id() ?? 0;
		    $total->session_id = session('cart');
		    $total->offer_id = $offer_id;
		    $total->quantity += $quantity;
		    $total->save();
        }
        
        $pr_title = 'name_' . App::getLocale();
        if(App::getLocale() === 'ru'){
           $json['success'] = sprintf('<a href="%s">%s</a> добавлен <a href="%s">в корзину покупок</a>!', '/ru/product' . $offer_info->product_id, $offer_info->products->$pr_title, '/ru/cart'); 
        }else{
           $json['success'] = sprintf('<a href="%s">%s</a> додано <a href="%s">до кошику покупок</a>!', '/product' . $offer_info->product_id, $offer_info->products->$pr_title, '/cart');
        }
		
		$offers_total = 0;
		
		$offers = Cart::where('user_id', Auth::id() ?? 0)->where('session_id', session('cart'))->get();

		foreach($offers as $offer){
			$offers_total += $offer->quantity;
		}	

		$json['total'] = $offers_total;

		return json_encode($json, JSON_UNESCAPED_UNICODE);
    }

    public function index()
    {
    	$cart = Cart::with('users')->where('session_id', session('cart'))->get();

    	$vendors = User::getVendors(session('cart'));

    	if(count($cart) < 1){
    		$empty_cart = true;
    	}else{
    		$empty_cart = false;
    	}

        $user = $cart[0]->users ?? null;

        if(!$user){
            $user = User::find(Auth::id()) ?? null;
        }

        if(isset($user->user_role_id) && ($user->user_role_id == 3)){
            request()->session()->flash('error', 'Корзина доступна только для покупателей');
            return redirect()->route('main');
        }

    	return view('cart', [
           'empty_cart' => $empty_cart,
           'user' => $user,
           'vendors' => $vendors, 
           'cart' => session('cart'),
           'local' => App::getLocale()
    	]);
    }

    public function remove(Cart $cart)
    {
    	$cart->delete();
    }

    public function update(Request $request, Cart $cart)
    {
        $cart->quantity = $request['quantity'];
        $cart->save();
    }

    public function setShippingMethod(Request $request, $vendor_id)
    {
        session(['vendor-' . $vendor_id => $request['delivery_status']]);
    }

    public function map(Request $request)
    {
        $startStr0 = $request['start'];
        $finishStr0 = $request['end'];

        $startStr = str_replace(", ", ",", $startStr0);
        $startStr = str_replace(",", " ", $startStr);
        $startStr = trim(str_replace(" ", "+", $startStr));
        $finishStr = str_replace(", ", ",", $finishStr0);
        $finishStr = str_replace(",", " ", $finishStr);
        $finishStr = trim(str_replace(" ", "+", $finishStr));


        $url = "https://maps.googleapis.com/maps/api/directions/json?origin=$startStr&destination=$finishStr&key=AIzaSyAW180KjqSqjH7vJsmpInHw6vuHhQ04pfI";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        $response = curl_exec($ch); 
        curl_close($ch);
        $jsonToArray = json_decode($response,1);
        //print_r($jsonToArray);
        print_r($jsonToArray['routes'][0]['legs'][0]['distance']['value']);
    }

    public function preorder(Request $request)
    {

        if($request['sort_by']){
            $products = array();
            foreach(session('api') as $offer){
                $offer['vendors'] = User::getVendorsWithSort($offer['id'], $request['sort_by']);
                $products[] = $offer;
            }
        }

        return view('preorder', [
            'products' => $products ?? session('api') ?? [],
            'get' => $request ?? null,
            'local' => App::getLocale()
        ]);
    }
}
