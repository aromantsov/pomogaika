<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use App\Models\News;
use App\Models\Adv;
use Illuminate\Http\Request;
use App;

class WelcomeController extends Controller
{
    public function main()
    {
        
    	return view('welcome', [
            'categories' => Category::where('parent_id', '0')->where('published', '1')->get(),
            'left_categories' => Category::where('parent_id', '0')->where('published', '1')->limit(21)->get(),
            'new_products' => Product::getNewProducts(),
            'sales' => Product::getSales(),
            'popular' => Category::orderBy('id', 'asc')->where('published', '1')->limit(10)->get(),
            'newslist' => News::orderBy('created_at', 'desc')->limit(6)->get(),
            'slider' => Adv::where('banner', '!=', NULL)->where('status', 'active')->where('advplace_id', 1)->get(),
            'slider2' => Adv::where('banner', '!=', NULL)->where('status', 'active')->where('advplace_id', 3)->get(),
            'local' => App::getLocale()
        ]);
    }

}
