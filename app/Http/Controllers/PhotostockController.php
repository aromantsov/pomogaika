<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Photostock;
use Auth;

class PhotostockController extends Controller
{

    public function __construct()
    {
        $this->middleware('premium_vendor');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         return view('photostock.index', [
            'photos' => Photostock::where('user_id', Auth::id())->orderBy('created_at', 'desc')->paginate(10),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('photostock.create', [
            'photo' => [],
            'user_id' => Auth::id(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        session(['open_modal' => null]);
        if($request->file()){

            $photo = new Photostock;

            $new_name = [];

            $messages = [
                'reqiured' => 'Укажите, пожалуйста, файл для загрузки',
                'mimes' => 'Файл должен быть формата jpeg',
                'dimensions' => 'Размер изображения должен быть не более 420х420 пикселей',
                'max' => 'Максимальный вес изображения 50 кбайт',
            ];

            $validator = $this->validate($request, [
                'image' => 'required|mimes:jpg,jpeg,png|dimensions:max_width=420,max_height=420|max:50'
            ], $messages);

            foreach ($request->file() as $key => $f) {
                $new_name[$key] = $f->getClientOriginalName();
                $f->move($_SERVER['DOCUMENT_ROOT'] . '/img/photostock/', $new_name[$key]);
            }
            if(isset($new_name['image'])){
                $photo->image = '/img/photostock/' . $new_name['image'];
            }
            $photo->user_id = $request['user_id'];
            $photo->save();
            request()->session()->flash('success', 'Изображение загружено');
        }

        //request()->session()->flash('error', 'Пожалуйста, загрузите изображение');
        
        return redirect()->route('photos.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Photostock $photo)
    {
         if(file_exists($photo->image)){
            unlink(public_path($photo->image));
        }

        $photo->delete();

        request()->session()->flash('success', 'Изображение удалено');

        return redirect()->route('photos.index');
    }
}
