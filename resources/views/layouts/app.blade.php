@php
use App\Http\Middleware\LocaleMiddleware;
use App\Models\Cart;
use App\User;
$n_name = 'name_' . App::getLocale();
$des = 'description_' . App::getLocale();
$vis = User::find(Auth::id());
@endphp

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="/img/main/favicon.ico" type="image/x-icon" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, viewport-fit=cover" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <meta http-equiv="x-ua-compatible" content="IE=edge" />
    @if(isset($news))
    <meta property="og:url"           content="http://helper.dimarvel.com/news/{{ $news->id }}" />
    <meta property="og:title"         content="{{ $news->$n_name }}" />
    <meta property="og:description"   content="{{ $news->$des }}" />
    <meta property="og:image"         content="{{ $news->image }}" />
    @endif

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Pomogaika Shop</title>

    <!-- Scripts --> 
    <script src="/js/jquery-3.5.1.js"></script>
    <script type="text/javascript" src="/js/slick.min.js"></script>
    <script src="{{ asset('https://cdn.ckeditor.com/4.13.0/standard/ckeditor.js') }}"></script>
    <script src="/js/jquery.maskedinput.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script src="/js/compare.js"></script>
    <script src="/js/script.js"></script>
    <!-- <script src='/js/swiper-1.js' defer></script> -->
    <script src="/js/ion.rangeSlider.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
    
    @if(isset($sales))
    @if(count($sales) < 5)
    <style>
    .line_other_product:before{
        display: none !important;
    }
    </style>
    @endif
    @endif
    @if(isset($products))
    @if(count($products) < 5)
    <style>
    .line_other_product:before{
        display: none !important;
    }
    </style>
    @endif
    @endif
    @if(isset($categories))
    @if(count($categories) < 6)
    <style>
    .line_cats_front:before{
        display: none !important;
    }
    </style>
    @endif
    @endif
    @if(isset($cat->id))
    @if(isset($products[$cat->id]))
    @if(count($products[$cat->id]) < 5)
    <style>
    .line_other_product:before{
        display: none !important;
    }
    </style>
    @endif
    @endif
    @endif

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
     <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.1/css/ion.rangeSlider.min.css" />
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/selectric.css') }}">
    <link rel="stylesheet" href="{{ asset('css/xzoom.css') }}">
    <link rel="stylesheet" type="text/css" href="/css/slick.css">
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.css">
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css">
    <link rel="stylesheet" href="/css/swiper-1.css">
</head>
<body>
    <div class="mobile_menu_hidden_block">
    <div class="heading">
        <strong class="logo">
            <a href="{{ route('main') }}"><img src="/img/main/logo.svg" alt="icon"></a>
        </strong>
        <button class="close_mobile">
            <span class="line_first"></span>
            <span class="line_second"></span>
        </button>
    </div>
    <div class="lang_enter_account">
        <div class="lang">
            <a href="<?= route('setlocale', ['lang' => 'ru']) ?>"<?= LocaleMiddleware::getLocale() === 'ru' ? 'class="active"' : '' ?>>Ru</a> | <a href="<?= route('setlocale', ['lang' => 'uk']) ?>" <?= LocaleMiddleware::getLocale() === null ? 'class="active"' : '' ?>>Ua</a>
        </div>
        <div class="login_enter">
            @auth
            <a href="{{ url('home') }}"><img src="/img/main/icon_account.svg" alt=""> @lang('app.cabinet')</a>
            @else
            <span><a href="#" class="open_modal_login"><img src="/img/main/icon_account.svg">@lang('app.enter')</a><a href="#" class="open_modal_register">/@lang('app.register')</a></span>
            @endauth
        </div>
    </div>
    <div class="other_link_header">
        <a href="{{ url('compare') }}" class="link_compare">
            <img src="/img/main/icon_compare.svg" alt="">
        </a>
        @if(($vis == null) || ($vis->user_role_id != 3))
        <a href="/home?wishlist=true" class="link_wishlist">
            <img src="/img/main/icon_wishlist_white.svg" alt="">
        </a>
        <a href="{{ route('cart') }}" class="link_cart">
            <img src="/img/main/icon_cart_white.svg" alt=""> @lang('app.cart') (<span id="count-offers-mobile">{{ Cart::countOffers() ?? 0 }}</span>)
        </a>
        @endif
    </div>
    <div class="search_form">
        <form action="{{ route('search.product') }}" method="get">
            <input type="text" name="search" placeholder="@lang('search.search')...">
            <button type="submit" class="search1"><img src="/img/main/icon_search.svg" alt=""></button>
            <button type="" class="search2"><img src="/img/main/icon_search.svg" alt=""></button>
        </form>
    </div>
    <div class="menu_mobile">
        <ul>
            <li><a href="{{ route('main') }}">@lang('app.main')</a></li>
            <li><a href="{{ route('categories') }}">@lang('account_company_info.catalog')</a></li>
            <li><a href="{{ route('is_sale') }}">@lang('app.sale')</a></li>
            <li><a href="{{ route('newslist') }}">@lang('app.news')</a></li>
            <li><a href="{{ route('info', 3) }}">@lang('app.about_portal')</a></li>
            <li><a href="{{ route('info', 2) }}">@lang('app.for_sellers')</a></li>
            <li><a href="{{ route('contact') }}">@lang('app.contacts')</a></li>
        </ul>
    </div>
</div>
    <div class="main-wrapper">
    <header>
        <div class="container">
            <div class="wrap_header">
                <div class="logo_link">
                    <a href="{{ route('main') }}">
                        <img src="/img/main/logo.svg" alt="">
                    </a>
                </div>
                <div class="menu_search_link_header">
                    <div class="menu_top_line">
                        <ul>
                            <li><a href="{{ route('main') }}">@lang('app.main')</a></li>
                            <li><a href="{{ route('categories') }}">@lang('account_company_info.catalog')</a></li>
                            <li><a href="{{ route('is_sale') }}">@lang('app.sale')</a></li>
                            <li><a href="{{ route('newslist') }}">@lang('app.news')</a></li>
                            <li><a href="{{ route('info', 3) }}">@lang('app.about_portal')</a></li>
                            <li><a href="{{ route('info', 2) }}">@lang('app.for_sellers')</a></li>
                            <li><a href="{{ route('contact') }}">@lang('app.contacts')</a></li>
                        </ul>
                        <div class="lang_enter_account">
                            <div class="lang">
                                <a href="<?= route('setlocale', ['lang' => 'ru']) ?>"<?= LocaleMiddleware::getLocale() === 'ru' ? 'class="active"' : '' ?>>Ru</a> | <a href="<?= route('setlocale', ['lang' => 'uk']) ?>" <?= LocaleMiddleware::getLocale() === null ? 'class="active"' : '' ?>>Ua</a>
                            </div>
                            <div class="login_enter">
                                @auth
                                <a href="{{ route('home') }}"><img src="/img/main/icon_account.svg" alt=""> @lang('app.cabinet')</a>
                                @else
                                <span><a href="#" class="open_modal_login"><img src="/img/main/icon_account.svg">@lang('app.enter')</a><a href="#" class="open_modal_register">/@lang('app.register')</a></span>
                                @endauth
                            </div>
                            <button class="mobile_menu">
                                <span></span>
                                <span></span>
                                <span></span>
                            </button>
                        </div>
                    </div>
                    <div class="search_top_line_link">
                        <div class="search_form">
                            <form action="{{ route('search.product') }}" method="get">
                                <input type="text" name="search" placeholder="@lang('search.search')...">
                                <button type="submit"><img src="/img/main/icon_search.svg" alt=""></button>
                            </form>
                        </div>
                        <div class="other_link_header">
                            <a href="{{ route('compare') }}" class="link_compare">
                                <img src="/img/main/icon_compare.svg" alt="">
                            </a>
                            @if(($vis == null) || ($vis->user_role_id != 3))
                            <a href="{{ route('home') }}?wishlist=true" class="link_wishlist">
                                <img src="/img/main/icon_wishlist_white.svg" alt="">
                            </a>
                            <a href="{{ route('cart') }}" class="link_cart">
                                <img src="/img/main/icon_cart_white.svg" alt=""> @lang('app.cart') (<span id="count-offers">{{ Cart::countOffers() ?? 0 }}</span>)
                            </a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div id="app"> 

    @if (Session::has('success'))
        <div class="alert alert-success">{{ Session::get('success') }}</div>
    @endif

        <main class="py-4">
            @yield('content')
        </main>
    </div>
    <footer class="mt-auto">
        <div class="container">
            <div class="line_footer_links">
                <a href="/">
                    <img src="/img/main/logo_footer.svg" alt="">
                </a>
                <ul>
                    <li><a href="{{ route('main') }}">@lang('app.main')</a></li>
                    <li><a href="{{ route('categories') }}">@lang('account_company_info.catalog')</a></li>
                    <li><a href="{{ route('is_sale') }}">@lang('app.sale')</a></li>
                    <li><a href="{{ route('newslist') }}">@lang('app.news')</a></li>
                    <li><a href="{{ route('info', 3) }}">@lang('app.about_portal')</a></li>
                    <li><a href="{{ route('info', 2) }}">@lang('app.for_sellers')</a></li>
                    <li><a href="{{ route('contact') }}">@lang('app.contacts')</a></li>
                </ul>
            </div>
        </div>
        <form action="{{ route('logout') }}" method="post" id="logout-form">
            {{ csrf_field() }}
        </form>
    </footer>
    </div>
    <div class="modal_block enter_form" style="display: @if((isset($errors)) &&($errors->any()) && (session('open_modal'))) block; @else none; @endif">
        <div class="close_modal"></div>
        <p class="name_modal">@lang('app.log_in')</p>
        <form method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}
            <input type="hidden" name="modal" value="true">
            <div class="label_form_enter_registration">
                <label>
                    <p>@lang('app.login')</p>
                    <input type="text" name="login" value="{{ old('login') }}"  class="form-control @if(isset($errors)) @error('email') is-invalid @enderror @error('phone') is-invalid @enderror @endif" value="{{ old('email') }}" required autocomplete="email" autofocus>
                    @if(isset($errors))
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                    @error('phone')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                    @endif
                </label>
            </div>
            <div class="label_form_enter_registration">
                <label>
                    <p>@lang('app.password')</p>
                    <div class="pass-form">
                    <input type="password" name="password" class="form-control pass-input @if(isset($errors)) @error('password') is-invalid @enderror @endif" name="password" required autocomplete="new-password" id="auth-password">
                    <button type="button" class="btn" onclick="visiblePassword();"><i id="pass-btn2" class="fa fa-eye-slash"></i></button>
                    </div>
                    @if(isset($errors))
                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                    @endif
                </label>
            </div>
            <div class="required_checkbox">
                <label>
                    <input type="checkbox" name="remember" value="1">
                    <span></span>
                    <p>@lang('app.remember')</p>
                </label>
            </div>
            <div>
                <a href="/password/reset">@lang('app.forgot')</a>
            </div>

            <button type="submit"><img src="/img/main/icon_register.svg" alt="">@lang('app.enter')</button>
            <div class="link_page_enter">
                <p>@lang('app.not_register'). <span class="open_register_modal open_modal_register">@lang('app.register')</span></p>
            </div>
        </form>
    </div>
    <div class="modal_block registration_form" id="regform" style="display: @if((isset($errors)) &&($errors->any()) && (session('open_modal2'))) block; @else none; @endif;">
        <div class="close_modal"></div>
        <p class="name_modal">@lang('app.register')</p>
        <form method="POST" action="{{ route('register') }}">
            {{ csrf_field() }}
            <input type="hidden" name="modal_reg" value="true">
            <div class="label_form_enter_registration">
                <label>
                    <p>@lang('app.phone') <span class="required">*</span></p>
                    <input type="tel" name="phone" class="form-control @if(isset($errors)) @error('phone') is-invalid @enderror @endif" value="{{ old('phone') }}" required autocomplete="phone" autofocus>
                    @if(isset($errors))
                    @error('phone')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                    @endif
                </label>
            </div>
            <div class="label_form_enter_registration">
                <label>
                    <p>@lang('app.password')</p>
                    <div class="pass-form">
                    <input type="password" name="password" class="form-control pass-input @if(isset($errors)) @error('password') is-invalid @enderror @endif" name="password" required autocomplete="new-password" id="reg-password">
                    <button type="button" class="btn" onclick="visiblePassword();"><i id="pass-btn" class="fa fa-eye-slash"></i></button>
                    </div>
                    @if(isset($errors))
                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                    @endif
                </label>
            </div>
            <div class="label_form_enter_registration">
                <label>
                    <p>@lang('app.password_confirm')</p>
                    <div class="pass-form">
                    <input type="password" name="password_confirmation" id="reg-confirm">
                    <button type="button" class="btn" onclick="visiblePassword();"><i id="pass-btn" class="fa fa-eye-slash"></i></button>
                    </div>
                </label>
            </div>
<!--             <div class="label_form_enter_registration">
                <label>
                    <p>Отчество</p>
                    <input type="text">
                </label>
            </div> -->
            <div class="label_form_enter_registration">
                <label>
                    <p>Email <span class="required">*</span></p>
                    <input type="email" name="email" class="form-control @if(isset($errors)) @error('email') is-invalid @enderror @endif" name="email" value="{{ old('email') }}" required autocomplete="email">
                    @if(isset($errors))
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                    @endif
                </label>
            </div>
<!--             <div class="label_form_enter_registration">
                <label>
                    <p>Телефон <span class="required"></span></p>
                    <input type="tel">
                </label>
            </div> -->
             @if(isset($errors))
                    @error('conditions')
                    <div>
                    <span>
                        <strong style="color: red;">{{ $message }}</strong>
                    </span>
                    </div>
                    @enderror
            @endif

            <div class="label_form_enter_registration">
                <p>Тип аккаунта </p>
                    <div class="line_checkbox_inputs">
                        <label>
                            <input type="radio" name="user_role_id" value="4" class="form-control" checked>
                            <span></span>
                            Покупатель
                        </label>
                        <label>
                            <input type="radio" name="user_role_id" value="3" class="form-control">
                            <span></span>
                            Поставщик
                        </label>
                    </div>
            </div>

            <div class="required_checkbox">
                <label for="conditions">
                    <input id="conditions" type="checkbox" name="conditions" value="1" class="form-control @if(isset($errors)) @error('conditions')  @enderror @endif">
                    <span></span>
                    <p>@lang('app.agree'). <a href="{{ route('info', 5) }}">@lang('app.conditions')</a></p>
                </label>
            </div>

            <button type="submit"><img src="/img/main/icon_register.svg" alt="">@lang('app.to_register')</button>
            <div class="link_page_enter">
                <p>@lang('app.registered'). <span class="open_enter_modal open_modal_login">@lang('app.login_now')</span></p>
            </div>
        </form>
    </div>
    <div class="modal_block buy">
        <div class="close_modal"></div>
        <form action="{{ route('orderadv') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <h3>1.Загрузите баннер</h3>
            <p>Баннер должен быть в формате jpeg или png, размером <span id="banner-format"></span> и весить не более 1 Мб</p>
            <label for="file" class="file">
                Выберите файл
                <input type="file" id="file" name="banner">
            </label>
            <h3>2.Вставьте ссылку</h3>
            <input type="text" class="link" name="href">
            <h3>3.Выберите период показа рекламной плоскости</h3>
                <select name="active_time" id="active_time" class="select_mounth">
                    <option value="1" selected>1 месяц</option>
                    <option value="2">2 месяца</option>
                    <option value="3">3 месяца</option>
                    <option value="4">4 месяца</option>
                    <option value="5">5 месяцев</option>
                    <option value="6">6 месяцев</option>
                </select>
                <input type="hidden" class="select_val" id="advplace_id" name="advplace_id" value="">
            <div class="summ">
                <span>Сумма: </span><strong id="last_summ">1200 грн</strong>
            </div>
            <button type="submit">Оплатить</button>
        </form>
    </div>
    <div class="overlay" style="display: @if((isset($errors)) && ($errors->any()) && ((session('open_modal')) || (session('open_modal2')))) block;@endif"></div>
    <script src="https://unpkg.com/swiper/swiper-bundle.js"></script>
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCW5nouZUrAhQHMgRioLgpq0W4xniGKGRg&libraries=places"></script>
    <script>
	var glRes = 'n';
	
	$(document).ready(function () {
	   google.maps.event.addDomListener(window, 'load', initialize);
	   google.maps.event.addDomListener(window, 'load', initialize1);
	});
	
    function deleteImage(product_id){
        var url = '/admin/product/deleteimage';
        $.ajax({
            url: url,
            method: 'post',
            data:{
                'product_id': product_id,
                '_token': '{{ csrf_token() }}',
            },
            success: function(){
                $('.product-image').remove();
            }
        });
    }

	function initialize() {
			var center = new google.maps.LatLng(49.978434, 36.270861);
            var circle = new google.maps.Circle({
                center: center,
                radius: 20000
            });	
		var input = document.getElementById('user_address_3');
		var autocomplete = new google.maps.places.Autocomplete(input);
		autocomplete.setBounds(circle.getBounds());
	}

	function initialize1() {
			var center = new google.maps.LatLng(49.978434, 36.270861);
            var circle = new google.maps.Circle({
                center: center,
                radius: 20000
            });	
		var input = document.getElementById('add_shop');
		var autocomplete = new google.maps.places.Autocomplete(input);
		autocomplete.setBounds(circle.getBounds());
	}

    function uploadExcel() {
        $('#import-excel').submit();
    }

    function toggleNavbar(){
        $('#a_side_category').toggle();
    }

    function logout(){
        if(confirm('Вы хотите выйти?')){
            $('#logout-form').submit();
        }
    }

    function addWishlist(product_id, user_id){
        var url = '/addtowishlist';
        $.ajax({
            url: url,
            method: 'post',
            data: {
                'product_id': product_id,
                'user_id': user_id,
                '_token': '{{ csrf_token() }}',
            },
            success: function(res){
                if(res == 'success'){
                    $('.alert-success').css('display', 'block').html('Товар успешно добавлен в список желаний');
                }else if(res == 'success_del'){
                    alert('Товар успешно удален из списка желаний');
                }else if(res == 'error'){
                    alert('Только зарегистрированные пользователи могут добавлять товар в список желаний');
                }else if(res == 'error2'){
                    alert('Только покупатели могут добавлять товар в список желаний');    
                }
            }
        });
        return false;
    }

    function searchForm(){
        $('#search-form').submit();
    }

    function searchClient(){
        $('#search-client').submit();
    }

    function openDescription(offer_id){
        $('show-des-' + offer_id).slideDown();
    }

    function addToCart(offer_id, quantity = 1){
        var url = "{{ route('add_to_cart') }}";
        $.ajax({ 
           url: url,
           type: 'post',
           data: {
             'offer_id': offer_id,
             'quantity': quantity,
             '_token': '{{ csrf_token() }}',
           },
           success:function(json){
             var res = JSON.parse(json);
             $('#count-offers').html(res['total']);
             $('#count-offers-mobile').html(res['total']);
             $('.alert-success').css('display', 'block').html(res['success']);
           }
        });
    }

    function removeOffer(cart_id){
        var url = '/remove_from_cart/' + cart_id;
        $.ajax({
            url: url,
            type: 'post',
            data: {
                '_token': '{{ csrf_token() }}',
                '_method': 'delete'
            }, 
            success: function(){
                localStorage.setItem('cart_name', $('#name').val());
                localStorage.setItem('cart_lastname', $('#lastname').val());
                localStorage.setItem('cart_email', $('#user_email').val());
                localStorage.setItem('cart_phone', $('#phone').val());
                localStorage.setItem('cart_comment', $('#comment').val());
                location.reload();
            }
        });
    }

    function minusQuantity(cart_id){
        var quan = +$('#quan-' + cart_id).val();
        if(quan > 1){
            quan -= 1;
            $('#quan-' + cart_id).val(quan);
        }
        calculate(cart_id);
    }

    function plusQuantity(cart_id){
        var quan = +$('#quan-' + cart_id).val();
        quan += 1;
        $('#quan-' + cart_id).val(quan);
        calculate(cart_id);
    }

    function calculate(cart_id)
    {
        var url = '/update_cart/' + cart_id;
        $.ajax({
            url: url,
            type: 'post',
            data: {
                '_token': '{{ csrf_token() }}',
                '_method': 'put',
                'quantity': $('#quan-' + cart_id).val(),
            },
            success:function(){
                localStorage.setItem('cart_name', $('#name').val());
                localStorage.setItem('cart_lastname', $('#lastname').val());
                localStorage.setItem('cart_email', $('#user_email').val());
                localStorage.setItem('cart_phone', $('#phone').val());
                localStorage.setItem('cart_comment', $('#comment').val());
                setTimeout(location.reload(), 1000);
            }
        });
    }

    function setShippingMethod(vendor_id){
        var url = '/setshipping/' + vendor_id;
        $.ajax({
            url: url,
            type: 'post',
            data:{
                'delivery_status': $('#shipping-' + vendor_id + ' input:checked').val(),
                '_token': '{{ csrf_token() }}', 
            },
            success:function(){
                localStorage.setItem('cart_name', $('#name').val());
                localStorage.setItem('cart_lastname', $('#lastname').val());
                localStorage.setItem('cart_email', $('#user_email').val());
                localStorage.setItem('cart_phone', $('#phone').val());
                localStorage.setItem('cart_comment', $('#comment').val());
                setTimeout(location.reload(), 1000);
            }
        })
    }

    function setOrder(){
        var url = '/setorder';
        if(!$('#set_order_button').hasClass('ipad-disabled')){
        $.ajax({
            url: url,
            type: 'post',
            data:{
                'name': $('#name').val(),
                'lastname': $('#lastname').val(),
                'email': $('#user_email').val(),
                'phone': $('#phone').val(),
                'comment': $('#comment').val(),
                'offers': $('#vendors-form').serialize(),
                '_token': '{{ csrf_token() }}',
                'total': $('#order-total').text(),
                //'cost': $('#user_address').val(),
            },
            beforeSend:function(){
                $('#set_order_button').attr('disabled', 'disabled');
                $('#set_order_button').addClass('ipad-disabled');
                $('#set_order_button').addClass('ipad-disabled');
            },
            success:function(res){
                $('#set_order_button').attr('disabled', 'disabled');

                setTimeout(function(){
                    $(location).attr('href', '/success/' + res);
                }, 1000);
            },
            error:function(reject){
                if( reject.status === 422 ) {
                    var errors = $.parseJSON(reject.responseText);
                    for(key in errors.errors){
                        $('.alert-danger').css('display', 'block').html(errors.errors[key]);
                        break;
                    }
                }

                $('#set_order_button').removeAttr('disabled');
                $('#set_order_button').removeClass('ipad-disabled');
            }
        });
        }
    }

    function filterSend(){
        $('#filter-form').submit();
    }

    function orderPlan(user_id, plan_id){
        var url = '/orderplan';

        $.ajax({
            url: url,
            type: 'post',
            data: {
                'user_id': user_id,
                'plan_id': plan_id,
                '_token': '{{ csrf_token() }}', 
            },
            success: function(plan_id){
                if(plan_id == 0){
                    alert('Тарифный план уже заказан');
                }else if(plan_id == 1){
                    alert('Тарифный план будет подключен автоматически после окончания срока действия нынешнего тарифного плана');
                }else{
                    alert('Заявка принята. Для выставления счета и уточнения деталей свяжется менеджер портала.');
                    $('.order_plan').html('<img src="/img/main/white_checked.svg" alt="icon">Заказать');
                    $('#order_plan_' + plan_id).html('<img src="/img/main/white_checked.svg" alt="icon">В ожидании');
                }
            }
        });
    }

    function prolongatePlan(user_id, plan_id){
        var url = '/orderplan';

        $.ajax({
            url: url,
            type: 'post',
            data: {
                'user_id': user_id,
                'plan_id': plan_id,
                '_token': '{{ csrf_token() }}', 
            },
            success: function(plan_id){
                if(plan_id == 0){
                    alert('Тарифный план уже заказан');
                }else if(plan_id == 1){
                    alert('Тарифный план будет подключен автоматически после окончания срока действия нынешнего тарифного плана');
                }else{
                    alert('Заявка принята. Для выставления счета и уточнения деталей свяжется менеджер портала.');
                    //$('.order_plan').html('<img src="/img/main/white_checked.svg" alt="icon">Заказать');
                    $('#prolongate_plan').html('<img src="/img/main/white_checked.svg" alt="icon">В ожидании');
                }
            }
        });
    }

    function modalAdv(price, advplace_id){
        $('.overlay').fadeIn(100, function () { 
            $('.buy').fadeIn(100); 
            $('#advplace_id').val(advplace_id); 
            $('#last_summ').text(price + ' грн'); 
            $('#active_time').change(function(){
                var total = price * $('#active_time option:selected').val();
                $('#last_summ').text(total + ' грн'); 
            });
            if(advplace_id == 1){
                $('#banner-format').html('1185х463');
            }else{
                $('#banner-format').html('790x180');
            }
        }); 
    }

    function orderAdv(user_id, advplace_id){
        var url = '/orderadv';

        $.ajax({
            url: url,
            type: 'post',
            data: {
                'user_id': user_id,
                'advplace_id': advplace_id,
                '_token': '{{ csrf_token() }}', 
            },
            success: function(adv_id){
                if(adv_id == 0){
                    alert('Реклама уже заказана');
                }else{
                    alert('Заявка принята. Для выставления счета и уточнения деталей свяжется менеджер портала.');
                    //$('.order_plan').html('<img src="/img/main/white_checked.svg" alt="icon">Заказать');
                    $('#adv-' + advplace_id).html('<img src="/img/main/white_checked.svg" alt="icon">В ожидании');
                }
            }
        });
    }

    function selectStatus(order_id){
        $('#status-form-' + order_id).submit();
    }

    function deleteAvatar(user_id){
        var url = '/deleteavatar';
        $.ajax({
            url: url,
            method: 'post',
            data:{
                'user_id': user_id,
                '_token': '{{ csrf_token() }}',
            },
            success: function(){
                $('.user_image_upload').html('<img src="/img/main/image_company.png" alt="">');
                $('#delbutton').remove();
            }
        });
    }

    function deleteVendorReview(review_id, vendor_id){
        if(confirm('Удалить отзыв?')){
            var url = '/deletevendorreview';
            $.ajax({
                url: url,
                method: 'post',
                data: {
                    'id': review_id,
                    '_token': '{{ csrf_token() }}',
                    '_method': 'delete'
                },
                success: function(count){
                    $('#vendorreview-' + review_id).remove();

                    if(count == 0){
                        $('#company-' + vendor_id).remove();
                    }
                    setTimeout(location.reload(), 100);
                }
            });
        }
    }

    function deleteProductReview(review_id, product_id){
        if(confirm('Удалить отзыв?')){
            var url = '/deleteproductreview';
            $.ajax({
                url: url,
                method: 'post',
                data: {
                    'id': review_id,
                    '_token': '{{ csrf_token() }}',
                    '_method': 'delete'
                },
                success: function(count){
                    $('#productreview-' + review_id).remove();

                    if(count == 0){
                        $('#product-' + product_id).remove();
                    }
                    setTimeout(location.reload(), 100);
                }
            });
        }
    }

    function v_like(review_id){
        var url = '/vlike';
        $.ajax({
            'url': url,
            'method': 'post',
            'data': {
                '_token': '{{ csrf_token() }}',
                'review_id': review_id
            },
            'dataType': 'json',
            success: function(json){
                $('#vlike-' + review_id).html(json['likes']);
                $('#vdislike-' + review_id).html(json['dislikes']);
            }
        });
    }

    function v_dislike(review_id){
        var url = '/vdislike';
        $.ajax({
            'url': url,
            'method': 'post',
            'data': {
                '_token': '{{ csrf_token() }}',
                'review_id': review_id
            },
            'dataType': 'json',
            success: function(json){
                $('#vlike-' + review_id).html(json['likes']);
                $('#vdislike-' + review_id).html(json['dislikes']);
            }
        });
    }

    function p_like(review_id){
        var url = '/plike';
        $.ajax({
            'url': url,
            'method': 'post',
            'data': {
                '_token': '{{ csrf_token() }}',
                'review_id': review_id
            },
            'dataType': 'json',
            success: function(json){
                $('#plike-' + review_id).html(json['likes']);
                $('#pdislike-' + review_id).html(json['dislikes']);
            }
        });
    }

    function p_dislike(review_id){
        var url = '/pdislike';
        $.ajax({
            'url': url,
            'method': 'post',
            'data': {
                '_token': '{{ csrf_token() }}',
                'review_id': review_id
            },
            'dataType': 'json',
            success: function(json){
                $('#plike-' + review_id).html(json['likes']);
                $('#pdislike-' + review_id).html(json['dislikes']);
            }
        });
    }

    function setActiveShop(){
        var url = '/activeshop';
        $.ajax({
            'url': url,
            'method': 'post', 
            'data': {
                '_token': '{{ csrf_token() }}',
                '_method': 'put',
                'shop': $('#active_shop option:checked').val()
            },
            'dataType': 'json',
            success: function(json){
                $('#active-address').html('<label for="shop3">'+ json['shop']['address'] + '<input type="radio" name="delivery" id="shop3" checked><span></span></label>');
                $('.add_point').html('');
                console.log(json['delivery']);
                for(key in json['delivery']){
                    $('.add_point').append('<li class="one_address"><input type="checkbox" name="delivery_addresses[]" value="' + json['delivery'][key]['price'] + '/' + json['delivery'][key]['distsnce'] + '" checked><span>' + json['delivery'][key]['price'] + ' грн</span><span>' + json['delivery'][key]['distance'] + ' км</span><div class="remove_blocks2"></div></li>');
                }
                location.reload();
            }
        });
    }

    function removeShop(shop_id){
        var url = '/removeshop';
        $.ajax({
            'url': url,
            'method': 'post', 
            'data': {
                '_token': '{{ csrf_token() }}',
                '_method': 'delete',
                'shop_id': shop_id
            },
            success: function(){
                $('#line_new_inpt_' + shop_id).remove();
            }
        });
    }

    function addShop(user_id){
        var url = '/addshop';
        $.ajax({
            'url': url,
            'method': 'post', 
            'data': {
                '_token': '{{ csrf_token() }}',
                'user_id': user_id,
                'address': $('#add_shop').val(),
            },
            success: function(shop_id){
                var res = '<div class="line_new_inpt" id="line_new_inpt_' + shop_id + '">';
                res += '<div><label><input type="checkbox" name="shops[]" value="' + $('#add_shop').val() + '" checked disabled><span></span>' + $('#add_shop').val() + ' </label></div>';
                res += '<div class="remove_blocks" id="remove-block-' + shop_id + '" onclick="removeShop(' + shop_id + ');"></div></div>';
                $('.append_new_input').append(res);
                location.reload();                          
            }
        });
    }

    function addDelivery(shop_id){
        var url = '/adddelivery';
        $.ajax({
            'url': url,
            'method': 'post', 
            'data': {
                '_token': '{{ csrf_token() }}',
                'shop_id': shop_id,
                'cost': $('#cost').val(),
                'radius':  $('#radius').val(),
            },
            success: function(del_id){
                var res = '<li class="one_address" id="one_address_' + del_id + '"><input type="checkbox" name="delivery_addresses[]" value="' + $('#cost').val() + '/' +  $('#radius').val()+ '" checked disabled><span>' + $('#cost').val() + ' грн</span><span>' +  $('#radius').val() + ' км</span><div class="remove_blocks2" id="remove-block2-' + del_id + '" onclick="removeDelivery(' + del_id + ');"></div></li>';
                    $('.append_point').append(res);
                    location.reload();   
            }
        });
    }

    function removeDelivery(del_id){
        var url = '/removedelivery';
        $.ajax({
            'url': url,
            'method': 'post', 
            'data': {
                '_token': '{{ csrf_token() }}',
                '_method': 'delete',
                'del_id': del_id
            },
            success: function(){
                $('#one_address_' + del_id).remove();
            }
        });
    }
   
    function geo_start(start, finish, shop, vendor, id, i, j) {	
		var startStr = start;
		var finishStr = finish; 
		var res = 'n';
		if(finishStr.length < 10) { alert('Введите адрес в формате: город, улица, дом!'); return false; }   
											$.ajax({
                                                url: '/map',
                                                type: 'post',
                                                data: {
                                                    'start': startStr,
                                                    'end': finishStr, 
													'_token': '{{ csrf_token() }}', 
                                                },
                                                dataType: 'json',
                                                success: function(msg){
													var url = '/costdelivery';
													$.ajax({
														url: url,
														type: 'post',
														data: {
															'_token': '{{ csrf_token() }}', 
															'shop_id': shop['id'],
															'distance': msg,
                                                            'vendor_id': id
														},
														success: function(cost){
															$('#all-routes').append('<div id="route_cost_' + shop['id'] + '">' + cost + '</div>');
															$('#mini_text_cost_' + id).text(cost + ' грн');
															$('#hidden_cost_' + id).val(cost + ' грн');
                                                            //alert(cost);														
															if(parseInt(cost) < 1 && (i == j)) {
                                                                $('#mini_text_cost_' + id).text('0 грн');
                                                                $('#hidden_cost_' + id).val('0 грн');
                                                                setTimeout(function(){
                                                                    $('#mini_text_cost_' + id).text('0 грн');
                                                                    $('#hidden_cost_' + id).val('0 грн');
                                                                    alert('Для согласования возможности и стоимости доставки по указанному адресу с вами свяжется менеджер');
                                                                    $('#mini_text_cost_' + id).text('0 грн');
                                                                    $('#hidden_cost_' + id).val('0 грн');
                                                                    //var url2 = '/sessioncost';
                                                                    // $.ajax({
                                                                    //     url: url2,
                                                                    //     type: 'post',
                                                                    //     data: {
                                                                    //         '_token': '{{ csrf_token() }}',
                                                                    //         'vendor_id': id 
                                                                    //     },
                                                                    //     success: function(){
                                                                    //         $('#mini_text_cost_' + id).text('0 грн');
                                                                    //         $('#hidden_cost_' + id).val('0 грн');
                                                                    //     }
                                                                    // })
                                                                }, 500);
																
															}
															// if(parseInt(cost) < 1) {
															// 	$('#hidden_cost_' + id).data('gres','f1');
															// }
                                                            //console.log($('#hidden_cost').data('gres'));
														},
													});
                                                
                                                },
                                                error: function() {
                                                    if($('#hidden_cost_' + id).data('gres') != 'f1'){
                                                        $('#mini_text_cost').text('0 грн');
                                                        $('#hidden_cost_' + id).val('0 грн');
                                                        alert('Для согласования возможности и стоимости доставки по указанному адресу с вами свяжется менеджер');
                                                        $('#mini_text_cost_' + id).text('0 грн');
                                                        $('#hidden_cost_' + id).val('0 грн');
                                                    }
                                                    $('#hidden_cost_' + id).data('gres','f1');
													glRes = 'Не правильный формат адреса!';
                                                }
                                            });  
											
    }

    function visiblePassword(){
        var x = document.getElementById("reg-password");
        var y = document.getElementById("reg-confirm");
        var z = document.getElementById("auth-password");
        var apf = document.getElementById("password");
        var pc = document.getElementById("password-confirm");
        if (x.type === "password") {
            x.type = "text";
            y.type = "text";
            z.type = "text";
            apf.type = "text";
            pc.type = "text";
            $('#pass-btn').removeClass('fa-eye-slash');
            $('#pass-btn').addClass('fa-eye');
            $('#pass-btn2').removeClass('fa-eye-slash');
            $('#pass-btn2').addClass('fa-eye');
        } else {
            x.type = "password";
            y.type = "password";
            z.type = "password";
            apf.type = "password";
            pc.type = "password";
            $('#pass-btn2').removeClass('fa-eye');
            $('#pass-btn2').addClass('fa-eye-slash');
        }
    }

    function sendMessage(email){
        var url = 'sendactivate';
        $.ajax({
            url: url,
            method: 'post',
            data: {
                '_token': '{{ csrf_token() }}',
                'email': email
            },
            success: function(){
                $('#sendactivate').html('<p>Сообщение отправлено</p>');
            }
        });
    }

    </script>
    <script>
        $('input[type="tel"]').mask("+38 (099) 999-99-99"); 

        $('.name_slide_label').click(function () {  
            $(this).toggleClass('open').siblings('.wrap_slide_content').slideToggle(300);   
        }); 

        // $(document).ready(function() {
        //     $("#check_all").click(function () {
        //         if (!$("#check_all").is(":checked")){
        //            //$(".checkbox").removeAttr("checked");
        //         }else{
        //            $(".checkbox").attr("checked","checked");
        //         }
        //     });
        // });

        // $(document).ready(function() {
        //     $(".checkbox").click(function () {
        //         // if (!$("#check_all").is(":checked")){
        //         //    //$(".checkbox").removeAttr("checked");
        //         // }else{
        //            $("#check_all").removeAttr("checked");
        //         //}
        //     });
        // }); 

        // $('#add_review').click(function () { 
        //     $('.overlay').fadeIn(100, function () {
        //         $('.create_reviews').fadeIn(100);  
        //     });
        //     return false; 
        // }); 

        $('.link_add_review').click(function (event) {    
            $('.overlay').fadeIn(100, function () {
                var elem = event.currentTarget;
                var vendor_id = elem.getAttribute('data-vendor');
                $('#vendor_id').attr('value', vendor_id);
                $('.create_reviews').fadeIn(100);  
            });
            return false; 
        });    

        $('.rating_options input').click(function (event) {
            var elem = event.currentTarget;
            var rating = elem.getAttribute('value');

            for(i = 1; i <= 5; i++){
                if(rating <= i){
                    $('.stars input[value=' + i + ']').css('background', 'url(/img/main/stars-grey.svg) center no-repeat');
                }

                if(rating == i){
                    $('.stars input[value=' + i + ']').css('background', 'url(/img/main/stars.svg) center no-repeat');
                }
            }
            
        });
 
    </script>
    @if((isset($get)) && ($get->wishlist == 'true'))
    <script>
    $(document).ready(function() {
        $('.tabs__content').removeClass('active');
        $('.tab_wishlist').addClass('active');
        $('.tabs__caption > li').removeClass('active');
        $('.caption_wishlist').addClass('active');
        localStorage.setItem("tab0", 2);
    });    
    </script>
    @endif
    @if((isset($get)) && ($get->import == 'done'))
    <script>
    alert('Товары успешно загружены');
    </script>
    @endif
</body>
</html>
