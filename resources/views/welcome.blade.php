@php
$cat_title = 'title_' . $local;
$pr_title = 'name_' . $local;
$des = 'description_' . $local;
@endphp

@extends('layouts.app', ['sales' => $sales, 'categories' => $popular
])

@section('content')
@if (Session::has('error'))
        <div class="alert alert-danger">{{ Session::get('error') }}</div>
    @endif
<div class="content">
        <div class="container">
        	<div class="alert alert-success" style="display: none;"></div>
            <div class="alert alert-danger" style="display: none;"></div>
            <div class="line_top_content">
                <div class="a_side_category">
                    <div class="name_section_home_cat">@lang('account_company_info.catalog') <img src="img/main/line_toggle_cat.svg" alt=""></div>
                    <div class="link_cats_a_side">
                        @forelse($left_categories as $category)
                        <a href="{{ route('category', ['id' => $category->id]) }}"><img src="{{ $category->icon ?? '/img/main/image_cat1.svg' }}" alt="">{{ $category->$cat_title }}</a>
                        @empty
                        <p>@lang('welcome.no_categories')</p>
                        @endforelse
                    </div>
                    <a href="{{ route('categories') }}" class="all_category_link blue_button_link"><img src="/img/main/line_toggle_cat.svg" alt=""> Все категории</a>
                </div>
                <div class="b_side_sliders">
                    <div class="slider_home_page swiper-container">
                        @foreach($slider as $banner)
                        <a href="{{ $banner->href }}"><img src="{{ $banner->banner }}" alt=""></a>
                        @endforeach
                        <a href=""><img src="/img/main/slider_image.jpg" alt=""></a>
                        <!-- <a href="#"><img src="img/main/slider_image.jpg" alt=""></a>
                        <a href="#"><img src="img/main/slider_image.jpg" alt=""></a>
                        <a href="#"><img src="img/main/slider_image.jpg" alt=""></a>
                        <a href="#"><img src="img/main/slider_image.jpg" alt=""></a> -->
                    </div>
<!--                     <div class="swiper-container">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide d-flex flex-column justify-content-center align-items-center">
                            <a href="#"><img src="img/main/slider_image.jpg" alt=""></a>
                        </div>
                        <div class="swiper-slide d-flex flex-column justify-content-center align-items-center">
                            <a href="#"><img src="img/main/slider_image.jpg" alt=""></a>
                        </div>
                        <div class="swiper-slide d-flex flex-column justify-content-center align-items-center">
                            <a href="#"><img src="img/main/slider_image.jpg" alt=""></a>
                        </div>
                       
                        <div class="swiper-slide d-flex flex-column justify-content-center align-items-center">
                            <a href="#"><img src="img/main/slider_image.jpg" alt=""></a>
                        </div>
                        
                        <div class="swiper-slide d-flex flex-column justify-content-center align-items-center">
                            <a href="#"><img src="img/main/slider_image.jpg" alt=""></a>
                        </div>
                    </div>
                    <div class="swiper-pagination"></div>
                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>
                    </div> -->
                    <div class="new_product_line">
                        <p class="title_product_block">@lang('welcome.news')</p>
                        <div class="slider_product_block">
                            @foreach($new_products as $product)
                            <div class="one_product_line">
                                <div class="nameplates">
                                    <div class="red_nameplate">New</div>
                                </div>
                                <div class="wishlist_compare">
                                    <a href="#" class="add_wishlist" onclick="addWishlist({{ $product->id }}, {{ Auth::id() ?? 0 }}); return false;"><img src="/img/main/icon_wishlist_blue.svg" alt=""><div class="ripple-parent">
                                            <div class="ripple"></div>
                                        </div></a>
                                </div>
                                <a href="{{ route('product', ['id' => $product['id']]) }}"><img src="{{ $product->image ?? '/img/main/default-product.jpg' }}" alt=""></a>
                                <div class="product-name" style="display: block; height: 70px;">
                                    <p>{{ $product->$pr_title }}</p>
                                </div>
                                <div class="line_price_addcart">
                                    <div class="left_price">
                                        <p class="red_price">{{ (int)$product->price }} грн</p>
                                    </div>
                                </div>
                                <a href="{{ route('product', ['id' => $product['id']]) }}" class="add_cart">Выбрать поставщика</a>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="other_product_line">
                <p class="title_product_block">@lang('welcome.actions') <a href="{{ route('is_sale') }}" class="blue_button_link"><img src="/img/main/line_toggle_cat.svg" alt=""> @lang('welcome.all_actions')</a></p>
                <div class="line_other_product">
                    @foreach($sales as $product)
                    <div class="one_product_line">
                        <div class="nameplates">
                            <div class="red_nameplate">Sale</div>
                        </div>

                        <div class="wishlist_compare">
                            <a href="" class="add_compare" onclick="compare.add({{ $product['id'] }}); return false;"><img src="img/main/icon_compare_blue.svg" alt=""><div class="ripple-parent">
                                            <div class="ripple"></div>
                                        </div></a>
                            <a href="" class="add_wishlist" onclick="addWishlist({{ $product['id'] }}, {{ Auth::id() ?? 0 }}); return false;"><img src="img/main/icon_wishlist_blue.svg" data-text="test" alt=""><div class="ripple-parent">
                                            <div class="ripple"></div>
                                        </div></a>
                        </div>
                        <a href="{{ route('product', ['id' => $product['id']]) }}"><img src="{{ $product['image'] ?? '/img/main/default-product.jpg' }}" alt=""></a>
                        <p>{{ $product[$pr_title] }}</p>
                        <div class="star_reviews">
                            <div class="line_star">
                                <img src="@if($product['rating'] >= 1)/img/main/star_yellow.svg @else /img/main/empty_star.svg @endif" alt="">
                                <img src="@if($product['rating'] >= 2)/img/main/star_yellow.svg @else /img/main/empty_star.svg @endif" alt="">
                                <img src="@if($product['rating'] >= 3)/img/main/star_yellow.svg @else /img/main/empty_star.svg @endif" alt="">
                                <img src="@if($product['rating'] >= 4)/img/main/star_yellow.svg @else /img/main/empty_star.svg @endif" alt="">
                                <img src="@if($product['rating'] >= 5)/img/main/star_yellow.svg @else /img/main/empty_star.svg @endif" alt="">
                            </div>
                            <div class="count_reviews">
                                {{ $product['reviews'] }}
                                <img src="/img/main/icon_review.svg" alt="">
                            </div>
                        </div>
                        <div class="line_price_addcart">
                            <div class="left_price">
                                <p class="old_price">{{ (int)$product['price'] }} грн</p>
                                <p class="red_price">{{ (int)$product['price'] }} грн</p>
                            </div>
                        </div>
                        <a href="{{ route('product', ['id' => $product['id']]) }}" class="add_cart">Выбрать поставщика</a>
                        @if($product['bonus'])<div class="red_price">+ {{ $product['bonus'] }} бонусных грн</div>@endif
                    </div>
                    @endforeach
                </div>
            </div>

            <div class="other_product_line">
                <p class="title_product_block">@lang('welcome.populars') <a href="{{ route('categories') }}" class="blue_button_link"><img src="/img/main/line_toggle_cat.svg" alt=""> @lang('welcome.all_categories')</a></p>
                <div class="line_cats_front popular">
                    @foreach($popular as $category)
                    <a href="{{ route('category', ['id' => $category->id]) }}">
                        <img src="{{ $category->image ?? '/img/main/default-category.jpg' }}" alt="" style="width: 147px; height: 147px;">
                        <p>{{ $category->$cat_title }}</p>
                    </a>
                    @endforeach
                </div>
            </div>
            <div class="line_image_link">
                <a href="">
                    <img src="/img/main/rekl1.jpg" alt="">
                    <p>@lang('welcome.flat')</p>
                </a>
                <a href="">
                    <img src="/img/main/rekl2.jpg" alt="">
                </a>
                <a href="">
                    <img src="/img/main/rekl1.jpg" alt="">
                    <p>@lang('welcome.flat')</p>
                </a>
            </div>
            <div class="other_product_line">
                <p class="title_product_block">@lang('news.info') <a href="{{ route('newslist') }}" class="blue_button_link"><img src="/img/main/icon_news.svg" alt=""> @lang('welcome.all_articles')</a></p>
                <div class="line_cats_front">
                    @foreach($newslist as $news)
                    <div class="one_block_news">
                        <a href="{{ route('news', ['id' => $news->id]) }}"><img src="{{ $news->picture ?? '/img/main/image_news.jpg' }}" alt=""></a>
                        <div>
                            <div>
                                <p class="name_news">{{ $news->$pr_title }}</p>
                                <p>{{ strip_tags(substr($news->$des, 0, 50)) }} ...</p>
                            </div>
                            <div class="line_views_link">
                                <div>
                                    {{ $news->views }}
                                    <img src="/img/main/icon_views.svg" alt="">
                                </div>
                                <a href="{{ route('news', ['id' => $news->id]) }}">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    <!-- <div class="one_block_news">
                        <a href=""><img src="/img/main/image_news.jpg" alt=""></a>
                        <div>
                            <div>
                                <p class="name_news">Битумная черепица</p>
                                <p>Подкладочный ковер, так же как и гидроизоляционный, продается в рулонах метровой ширины, тыльная сторона покрыта клеящим составом. Способ ...</p>
                            </div>
                            <div class="line_views_link">
                                <div>
                                    56
                                    <img src="/img/main/icon_views.svg" alt="">
                                </div>
                                <a href="">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    <div class="one_block_news">
                        <a href=""><img src="/img/main/image_news.jpg" alt=""></a>
                        <div>
                            <div>
                                <p class="name_news">Битумная черепица</p>
                                <p>Подкладочный ковер, так же как и гидроизоляционный, продается в рулонах метровой ширины, тыльная сторона покрыта клеящим составом. Способ ...</p>
                            </div>
                            <div class="line_views_link">
                                <div>
                                    56
                                    <img src="/img/main/icon_views.svg" alt="">
                                </div>
                                <a href="">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    <div class="one_block_news">
                        <a href=""><img src="/img/main/image_news.jpg" alt=""></a>
                        <div>
                            <div>
                                <p class="name_news">Битумная черепица</p>
                                <p>Подкладочный ковер, так же как и гидроизоляционный, продается в рулонах метровой ширины, тыльная сторона покрыта клеящим составом. Способ ...</p>
                            </div>
                            <div class="line_views_link">
                                <div>
                                    56
                                    <img src="/img/main/icon_views.svg" alt="">
                                </div>
                                <a href="">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    <div class="one_block_news">
                        <a href=""><img src="/img/main/image_news.jpg" alt=""></a>
                        <div>
                            <div>
                                <p class="name_news">Битумная черепица</p>
                                <p>Подкладочный ковер, так же как и гидроизоляционный, продается в рулонах метровой ширины, тыльная сторона покрыта клеящим составом. Способ ...</p>
                            </div>
                            <div class="line_views_link">
                                <div>
                                    56
                                    <img src="/img/main/icon_views.svg" alt="">
                                </div>
                                <a href="">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    <div class="one_block_news">
                        <a href=""><img src="/img/main/image_news.jpg" alt=""></a>
                        <div>
                            <div>
                                <p class="name_news">Битумная черепица</p>
                                <p>Подкладочный ковер, так же как и гидроизоляционный, продается в рулонах метровой ширины, тыльная сторона покрыта клеящим составом. Способ ...</p>
                            </div>
                            <div class="line_views_link">
                                <div>
                                    56
                                    <img src="/img/main/icon_views.svg" alt="">
                                </div>
                                <a href="">Подробнее</a>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
@endsection
