@php
$cat_title = 'title_' . $local;
$pr_title = 'name_' . $local;
@endphp

@extends('layouts.app')

@section('content')
    <div class="content">
        <div class="breadcrumbs_container">
            <div class="container">
                <div class="breadcrumbs">
                   <div class="show_overlay_hover"></div>
                    <div class="link_dropdown_menu">
                        <button class="blue_button_link" onclick="toggleNavbar();">@lang('account_company_info.catalog') <img src="/img/main/line_toggle_cat.svg" alt=""></button>
                        <div class="a_side_category" id="a_side_category">
                            <div class="link_cats_a_side">
                                @foreach($categories as $category)
                                <div class="wrap_link_category">
                                    <a href="{{ route('category', ['id' => $category->id]) }}"><img src="{{ $category->icon ?? '/img/main/image_cat1.svg' }}" alt="">{{ $category->$cat_title }}</a>
                                    <div class="wrap_submenu_section">
                                        @foreach($category->children as $category_list)
                                         <div class="one_catalog_submenu_line">
                                            <img src="{{ $category_list->image ?? '/img/main/icon_cats1.jpg' }}" alt="">
                                            <div class="list_link_submenu_name_catalog">
                                                <p class="name_cat_section">{{ $category_list->$cat_title }}</p>
                                                <div class="line_link_cat nav-menu">
                                                    @foreach($category_list->children as $catlist2)
                                                    <a href="{{ route('category', ['id' => $catlist2->id]) }}">{{ $catlist2->$cat_title }}</a>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                                @endforeach
                            </div>
                            <a href="{{ route('categories') }}" class="all_category_link blue_button_link"><img src="/img/main/line_toggle_cat.svg" alt=""> @lang('category.all_categories')</a>
                        </div>
                    </div>
                    <div class="breadcrumbs_link">
                        <a href="{{ route('main') }}">@lang('account_user_info.main')</a>
                        <span>Распродажа </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="alert alert-success" style="display: none;"></div>
        <div class="alert alert-danger" style="display: none;"></div>
        <div class="container">
            <a href="" class="image_link_add">
                <img src="/img/main/rel3.jpg" alt="">
            </a>
            <div class="categories_line_wrap">
                <p class="title_product_block_cat"></p>
            </div>
        </div>
        <div class="container">
            <div class="catalog_filter_line">
                <div class="left_filter">
                </div>
                <div class="right_result_catalog">
                    <div class="count_sort">
                        <p class="title_product_block">Распродажа</p>
                        <div class="sort_select_text">
                        </div>
                    </div>
                    <div class="line_other_product">
                    	@foreach($products as $product)
                        <div class="one_product_line">
                            <div class="nameplates">
                                @if($product['is_sale'])<div class="red_nameplate">Sale</div>@endif
                            </div>

                            <div class="wishlist_compare">
                                <a href="" class="add_compare" onclick="compare.add({{ $product['id'] }}); return false;"><img src="/img/main/icon_compare_blue.svg" alt=""></a>
                                <a href="#" class="add_wishlist" onclick="addWishlist({{ $product['id'] }}, '{{ Auth::id() ?? 0 }}'); return false;"><img src="/img/main/icon_wishlist_blue.svg" alt=""><div class="ripple-parent">
                                                        <div class="ripple"></div>
                                                    </div></a>
                            </div>
                            <a href="{{ route('product', ['id' => $product['id']]) }}"><img src="{{ $product['image'] ?? '/img/main/default-product.jpg' }}" alt="" class="prod-img" style=""></a>
                            <div style="height: 40px;">
                                <p>{{ $product[$pr_title] }}</p>
                            </div>
                            <div class="star_reviews">
                                <div class="line_star">
                                <img src="@if($product['rating'] >= 1)/img/main/star_yellow.svg @else /img/main/empty_star.svg @endif" alt="">
                                <img src="@if($product['rating'] >= 2)/img/main/star_yellow.svg @else /img/main/empty_star.svg @endif" alt="">
                                <img src="@if($product['rating'] >= 3)/img/main/star_yellow.svg @else /img/main/empty_star.svg @endif" alt="">
                                <img src="@if($product['rating'] >= 4)/img/main/star_yellow.svg @else /img/main/empty_star.svg @endif" alt="">
                                <img src="@if($product['rating'] >= 5)/img/main/star_yellow.svg @else /img/main/empty_star.svg @endif" alt="">
                                </div>
                                <div class="count_reviews">
                                    {{ $product['reviews'] }}
                                    <img src="/img/main/icon_review.svg" alt="">
                                </div>
                            </div>
                            <div class="line_price_addcart">
                                <div class="left_price">
                                    <p class="old_price">{{ (int)$product['trade_price'] ?? 0 }} грн</p>
                                    <p class="red_price">{{ (int)$product['price'] }} грн</p>
                                </div>
                            </div>
                            <a href="{{ route('product', ['id' => $product['id']]) }}" class="add_cart">Выбрать поставщика</a>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection