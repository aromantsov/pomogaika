@php
$cat_title = 'title_' . $local;
$des = 'text_' . $local;
@endphp
@extends('layouts.app')

@section('content')
<div class="content">
	<div class="breadcrumbs_container">
            <div class="container">
                <div class="breadcrumbs">
                    <div class="show_overlay_hover"></div>

                    <div class="breadcrumbs_link">
                        <a href="{{ route('main') }}">@lang('account_user_info.main')</a>
                        <span>Инфо</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="text_container">
                <h2>{{ $info->$cat_title }}</h2>
                <p>{!! htmlspecialchars_decode($info->$des) !!}</p>
            </div>
        </div>

</div>
@endsection