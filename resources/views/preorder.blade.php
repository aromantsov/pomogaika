@php
use App\Models\VendorReview;
$pr_title = 'name_' . $local;
@endphp

@extends('layouts.app')

@section('content')
<div class="container">

        <div class="wrap_account_orders">
                        @forelse($products as $product)
                        <div class="one_wrap_orders">
                        <div class="line_order_header">
                            <div>
                                <p class="mini_title">Артикул</p>
                                <a href="{{ route('product', $product['id']) }}" target="_blank">{{ $product['model'] }} - {{ $product['name'] }}</a>
                            </div>
                            <div  style="text-decoration: none !important;">
                                <p class="mini_title">Количество</p>
                                <p class="preorder-quantity" style="text-decoration: none !important;">{{ $product['quantity'] }}</p>
                            </div>
                            <div class="count_sort">
                            <div class="sort_select_text">
                                <select class="sort_select" name="" id="sort_select">
                                    <option value="price_asc" @if($get['sort_by'] == 'price_asc') selected @endif>@lang('product.by_price_asc')</option>
                                    <option value="price_desc" @if($get['sort_by'] == 'price_desc') selected @endif>@lang('product.by_price_desc')</option>
                                    <option value="popular" @if($get['sort_by'] == 'price_asc') selected @endif>@lang('product.by_popular')</option>
                                </select>
                            </div>
                            </div>
                        </div>
                        <div class="toggle_slide_orders_details" style="display: block;">
                            @forelse($product['vendors'] as $vendor)
                            @php
                            $rating = (int)VendorReview::where('vendor_id', $vendor->user_id)->avg('rating');
                            @endphp
                            <div class="line_column_orders">
                                <div class="top_info_orders">
                                    <div>
                                        <p class="mini_title">@lang('account_user_info.seller')</p>
                                        <p>{{ $vendor['name'] }}</p>
                                    </div>
                                    <div>
                                        <p class="mini_title">Цена</p>
                                        <p>{{ (int)$vendor['price'] }} грн</p>
                                    </div>
                                    <div>
                                        <p class="mini_title">Рейтинг</p>
                                        <div>
                                            <img src="@if($rating >= 1)/img/main/star_yellow.svg @else /img/main/empty_star.svg @endif" alt="" style="width: 14px;">
                                            <img src="@if($rating >= 2)/img/main/star_yellow.svg @else /img/main/empty_star.svg @endif" alt="" style="width: 14px;">
                                            <img src="@if($rating >= 3)/img/main/star_yellow.svg @else /img/main/empty_star.svg @endif" alt="" style="width: 14px;">
                                            <img src="@if($rating >= 4)/img/main/star_yellow.svg @else /img/main/empty_star.svg @endif" alt="" style="width: 14px;">
                                            <img src="@if($rating >= 5)/img/main/star_yellow.svg @else /img/main/empty_star.svg @endif" alt="" style="width: 14px;">
                                        </div>
                                    </div>
                                    <div>
                                        <a href="#" class="add_cart" id="button-cart-{{ $vendor->id }}" onclick="addToCart({{ $vendor->id }}, {{ $product['quantity'] }})">+<img src="/img/main/icon_cart_white.svg" alt="" style="width: 14px;"></a>
                                    </div>
                                </div>
                                <div class="slide_up_orders"></div>
                            </div>
                            @empty
                            Нет предложений
                            @endforelse

                        </div>
                    </div>
                    @empty
                    Нет предложений
                    @endforelse
                    </div>              
     
</div> 
<script>
	 $('#sort_select').change(function(){
        var local = '/{{ $local }}';
        if(local == '/uk'){
            local = '';
        }
        window.location.href = local + '/preorder/?sort_by=' + $('#sort_select option:selected').val();
      });  
</script>     
@endsection