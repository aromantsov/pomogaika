@php
//use App;
use App\Models\Category;
$categories = Category::with('children')->where('parent_id', '0')->where('published', '1')->get();
$local = App::getLocale();
//$cat_title = 'title_' . $local;
@endphp
@extends('layouts.app')

@section('content')
<div class="container">
	<div class="breadcrumbs_container">
            <div class="container">
                <div class="breadcrumbs">
                    <div class="breadcrumbs_link">
                        <a href="/">@lang('account_user_info.main')</a>
                        <span>@lang('errors.404')</span>
                    </div>
                </div>
            </div>
        </div>
            <div class="info_container">
                <div class="left_info_container">
                    <div>
                        <h2>@lang('errors.no_exists')</h2>
                        <p>@lang('errors.deprecate')</p>
                        <div class="info_button">
                            <a href="/" class="orange_button">@lang('errors.main_transfer')</a>
                        </div>
                    </div>
                </div>
                <div class="right_info_container">
                    <div><img src="/img/main/404.jpg" alt="image"></div>
                </div>
            </div>
        </div>
@endsection