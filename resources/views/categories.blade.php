@php
$cat_title = 'title_' . $local;
@endphp

@extends('layouts.app')

@section('content')
    <div class="content">
        <div class="breadcrumbs_container">
            <div class="container">
                <div class="breadcrumbs">
                   <div class="show_overlay_hover"></div>
                    <div class="link_dropdown_menu">
                        <button class="blue_button_link" onclick="toggleNavbar();">@lang('account_company_info.catalog') <img src="/img/main/line_toggle_cat.svg" alt=""></button>
                        <div class="a_side_category" id="a_side_category">
                            <div class="link_cats_a_side">
                                @foreach($categories as $category)
                                <div class="wrap_link_category">
                                    <a href="{{ route('category', ['id' => $category->id]) }}"><img src="{{ $category->icon ?? '/img/main/image_cat1.svg' }}" alt="">{{ $category->$cat_title }}</a>
                                    <div class="wrap_submenu_section">
                                        @foreach($category->children as $category_list)
                                         <div class="one_catalog_submenu_line">
                                            <img src="{{ $category_list->image ?? '/img/main/icon_cats1.jpg' }}" alt="">
                                            <div class="list_link_submenu_name_catalog">
                                                <p class="name_cat_section">{{ $category_list->$cat_title }}</p>
                                                <div class="line_link_cat nav-menu">
                                                    @foreach($category_list->children as $catlist2)
                                                    <a href="{{ route('category', ['id' => $catlist2->id]) }}">{{ $catlist2->$cat_title }}</a>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                                @endforeach
                            </div>
                            <a href="/categories" class="all_category_link blue_button_link"><img src="image/line_toggle_cat.svg" alt=""> @lang('category.all_categories')</a>
                        </div>
                    </div>
                    <div class="breadcrumbs_link">
                        <a href="{{ route('main') }}">@lang('account_user_info.main')</a>
                        <span>@lang('category.catalog')</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="category-slider">
            @foreach($slider as $banner)
            <div class="category-banner">
                <a href="" class="image_link_add">
                <img src="/img/main/rel3.jpg" alt="">
            </a>
            </div>
            @endforeach
            <div class="category-banner">
                <a href="" class="image_link_add">
                <img src="/img/main/rel3.jpg" alt="">
            </a>
            </div>
            </div>
            <div class="categories_line_wrap">
                <p class="title_product_block_cat">@lang('category.catalog')</p>
                <div class="line_categories">
                    @foreach($categories as $category)
                    <div class="one_catalog_line">
                        <img src="{{ $category->image ?? '/img/main/default-category.jpg' }}" alt="">
                        <div class="list_link_name_catalog">
                            <a href="{{ route('category', ['id' => $category->id]) }}" class="name_cat_section">{{ $category->$cat_title }}</a>
                            <div class="line_link_cat">
                                @foreach($category->children as $subcategory)
                                <a href="{{ route('category', ['id' => $subcategory->id]) }}">{{ $subcategory->$cat_title }}</a>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection