@php
$cat_title = 'title_' . $local;
@endphp

@extends('layouts.app')

@section('content')
<div class="container">
                <div class="breadcrumbs">
                   <div class="show_overlay_hover"></div>
                    <div class="link_dropdown_menu">
                        <button class="blue_button_link" onclick="toggleNavbar();">@lang('account_company_info.catalog') <img src="/img/main/line_toggle_cat.svg" alt=""></button>
                        <div class="a_side_category" id="a_side_category">
                            <div class="link_cats_a_side">
                                @foreach($categories as $category)
                                <div class="wrap_link_category">
                                    <a href="{{ route('category', ['id' => $category->id]) }}"><img src="{{ $category->icon ?? '/img/main/image_cat1.svg' }}" alt="">{{ $category->$cat_title }}</a>
                                    <div class="wrap_submenu_section">
                                        @foreach($category->children as $category_list)
                                         <div class="one_catalog_submenu_line">
                                            <img src="{{ $category_list->image ?? '/img/main/icon_cats1.jpg' }}" alt="">
                                            <div class="list_link_submenu_name_catalog">
                                                <p class="name_cat_section">{{ $category_list->$cat_title }}</p>
                                                <div class="line_link_cat nav-menu">
                                                    @foreach($category_list->children as $catlist2)
                                                    <a href="{{ route('category', ['id' => $catlist2->id]) }}">{{ $catlist2->$cat_title }}</a>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                                @endforeach
                            </div>
                            <a href="{{ route('categories') }}" class="all_category_link blue_button_link"><img src="/img/main/line_toggle_cat.svg" alt=""> @lang('category.all_categories')</a>
                        </div>
                    </div>
                    <div class="breadcrumbs_link">
                        <a href="{{ route('main') }}">@lang('account_user_info.main')</a>
                        <span>@lang('confirm.order_confirmation')</span>
                    </div>
                </div>
            </div>
        </div>
<div class="container">
    @foreach($orders_id as $order_id)
            <div class="info_container">
                <div class="left_info_container">
                    <div>
                        <h2>@lang('account_company_info.order') {{ $order_id }} </h2>
                        <p>@lang('confirm.thanks')</p>
                        <div class="info_button">
                            <a href="{{ route('categories') }}" class="orange_button">@lang('cart.purchases')</a>
                            <a href="{{ route('order_info', ['id' => $order_id]) }}" class="blue_button">@lang('cart.order_status')</a>
                        </div>
                    </div>
                </div>
                <div class="right_info_container">
                    <div><img src="/img/main/ok_order.jpg" alt="image"></div>
                </div>
            </div>
            @endforeach 
        </div> 
        <script>
            localStorage.clear();
        </script>     
@endsection