@php
$cat_title = 'title_' . $local;
$tag_l = 'tag_' . $local;
$name = 'name_' . $local;
$des = 'description_' . $local;
@endphp
@extends('layouts.app')

@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="content">
	<div class="breadcrumbs_container">
            <div class="container">
                <div class="breadcrumbs">
                    <div class="show_overlay_hover"></div>
                    <div class="link_dropdown_menu">
                        <button class="blue_button_link" onclick="toggleNavbar();">@lang('account_company_info.catalog') <img src="//img/main/line_toggle_cat.svg" alt=""></button>
                        <div class="a_side_category" id="a_side_category">
                            <div class="link_cats_a_side">
                                @foreach($categories as $category)
                                <div class="wrap_link_category">
                                    <a href="{{ route('category', ['id' => $category->id]) }}"><img src="{{ $category->icon ?? '//img/main/image_cat1.svg' }}" alt="">{{ $category->$cat_title }}</a>
                                    <div class="wrap_submenu_section">
                                        @foreach($category->children as $category_list)
                                         <div class="one_catalog_submenu_line">
                                            <img src="{{ $category_list->image ?? '//img/main/icon_cats1.jpg' }}" alt="">
                                            <div class="list_link_submenu_name_catalog">
                                                <p class="name_cat_section">{{ $category_list->$cat_title }}</p>
                                                <div class="line_link_cat nav-menu">
                                                    @foreach($category_list->children as $catlist2)
                                                    <a href="{{ route('category', ['id' => $catlist2->id]) }}">{{ $catlist2->$cat_title }}</a>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                                @endforeach
                            </div>
                            <a href="{{ route('categories') }}" class="all_category_link blue_button_link"><img src="//img/main/line_toggle_cat.svg" alt=""> @lang('category.all_categories')</a>
                        </div>
                    </div>

                    <div class="breadcrumbs_link">
                        <a href="{{ route('main') }}">@lang('account_user_info.main')</a>
                        <span>Контакты</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="contact_container">
                <div class="contact_list">
                    <h2>Наши контакты</h2>
                    <h3>Сервисный отдел</h3>
                    <div class="contact_items">
                        <div class="tel_contact">
                            <span>Телефоны:</span>
                            <a href="#"><img src="/img/main/mts.svg" alt="icon"> +38 (050) 500 - 50 - 05</a><a href="#"><img src="/img/main/kiivstar.svg" alt="icon">+38 (093) 930 - 90 - 30</a><a href="#"><img src="/img/main/lifecell.svg" alt="icon">+38 (068) 680 - 60 - 80</a>
                        </div>
                        <div class="graph">
                            <span>Режим работы:</span>
                            <p><img src="/img/main/grapf_green.svg" alt="icon">в будни: с 8:00 до 19:00</p>
                            <p><img src="/img/main/graph_red.svg" alt="icon">суббота: с 9:00 до 18:00</p>
                            <p><img src="/img/main/graph_red.svg" alt="icon">воскресенье: с 9:00 до 17:00</p>
                        </div>
                    </div>
                    <p>Чердак под кровлей может быть теплым или холодным, в зависимости от этого изменяется состав кровельного пирога. Но его часть от стропил и выше всегда остается неизменной. Подкладочный ковер, так же как и гидроизоляционный, продается в рулонах метровой ширины, тыльная сторона покрыта клеящим составом. Подкладочный ковер, так же как и гидроизоляционный, продается в способ Подкладочный ковер, так же как и гидроизоляционный, продается в способ</p>
                </div>
                <div class="contact_form">
                    <h2>Обратная связь</h2>
                    <h3>Отправьте нам сообщение</h3>
                    <form action="{{ route('callback') }}" method="post" required>
                        {{ csrf_field() }}
                        <input type="hidden" name="status" value="new">
                        <label for="">
                            Имя:
                            <input type="text" name="name" value="{{ @old('name') }}">
                        </label>
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                        <label for="">
                            E-mail:
                            <input type="email" name="email" value="{{ @old('email') }}" required>
                        </label>
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        <label for="">
                            Сообщение:
                            <textarea name="message" id="" cols="30" rows="10">{{ @old('message') }}</textarea>
                        </label>
                        @error('message')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        <button class="submit"><img src="/img/main/note.svg" alt="icon"> Отправить</button>
                    </form>
                </div>
            </div>
        </div>

</div>
@endsection