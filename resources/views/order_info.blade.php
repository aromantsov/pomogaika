@php
$cat_title = 'title_' . $local;
$pr_title = 'name_' . $local;

@endphp

@extends('layouts.app')

@section('content')
<div class="container">
                <div class="breadcrumbs">
                   <div class="show_overlay_hover"></div>
                    <div class="link_dropdown_menu">
                        <button class="blue_button_link" onclick="toggleNavbar();">@lang('account_company_info.catalog') <img src="/img/main/line_toggle_cat.svg" alt=""></button>
                        <div class="a_side_category" id="a_side_category">
                            <div class="link_cats_a_side">
                                @foreach($categories as $category)
                                <div class="wrap_link_category">
                                    <a href="{{ route('category', ['id' => $category->id]) }}"><img src="{{ $category->icon ?? '/img/main/image_cat1.svg' }}" alt="">{{ $category->$cat_title }}</a>
                                    <div class="wrap_submenu_section">
                                        @foreach($category->children as $category_list)
                                         <div class="one_catalog_submenu_line">
                                            <img src="{{ $category_list->image ?? '/img/main/icon_cats1.jpg' }}" alt="">
                                            <div class="list_link_submenu_name_catalog">
                                                <p class="name_cat_section">{{ $category_list->$cat_title }}</p>
                                                <div class="line_link_cat nav-menu">
                                                    @foreach($category_list->children as $catlist2)
                                                    <a href="{{ route('category', ['id' => $catlist2->id]) }}">{{ $catlist2->$cat_title }}</a>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                                @endforeach
                            </div>
                            <a href="{{ route('categories') }}" class="all_category_link blue_button_link"><img src="/img/main/line_toggle_cat.svg" alt=""> @lang('category.all_categories')</a>
                        </div>
                    </div>
                    <div class="breadcrumbs_link">
                        <a href="{{ route('main') }}">@lang('account_user_info.main')</a>
                        <span>@lang('confirm.order_confirmation')</span>
                    </div>
                </div>
            </div>
        </div>
<div class="container">
    <div class="order_item active">
                            <div class="order_show">
                                <div class="orders_item_info">
                                    <div class="open_order_item">
                                        @lang('account_company_info.order')
                                        <span class="order_number">№ {{ $order->order_id }}</span>
                                    </div>
                                    <div class="open_order_item">
                                        @lang('account_company_info.date2')
                                        <span class="order_date">{{ $order->created_at }}</span>
                                    </div>
                                    <div class="open_order_item">
                                        @lang('account_company_info.customer')
                                        <span class="customer">{{ $order->name . ' ' . $order->lastname ?? '' }}</span>
                                    </div>
                                    <div class="open_order_item">
                                        @lang('account_company_info.phone')
                                        <span>{{ $order->phone ?? '' }}</span>
                                    </div>
                                    <div class="open_order_item">
                                        @lang('account_company_info.total')
                                        <span>{{ (int)$order->total ?? '' }} грн</span>
                                    </div>
                                    <div class="open_order_item">
                                        @lang('account_company_info.shipping')
                                        <span>@if($order->delivery == 'inshop')
                                        @if($local == 'ru')
                                        Самовывоз
                                        @elseif($local == 'uk')
                                        Самовивіз
                                        @else
                                        {{ $order->delivery }}
                                        @endif
                                        @elseif($order->delivery == 'outshop')
                                        @if($local == 'ru')
                                        Доставка поставщиком
                                        @elseif($local == 'uk')
                                        Доставка постачальником
                                        @else
                                        {{ $order->delivery }}
                                        @endif
                                        @elseif($order->delivery == 'ourshipping')
                                        Доставка Pomogaika
                                        @endif</span>
                                    </div>
                                    <div class="open_order_item">
                                        @lang('account_company_info.payment')
                                        <span>@if($order->payment == 'cod')
                                        @if($local == 'ru')
                                        Наложенный платеж
                                        @elseif($local == 'uk')
                                        Післяплата
                                        @else
                                        {{ $order->payment }}
                                        @endif
                                        @elseif($order->payment == 'agreememt')
                                        @if($local == 'ru')
                                        Оплата по договору
                                        @elseif($local == 'uk')
                                        Оплата згідно договору
                                        @else
                                        {{ $order->payment }}
                                        @endif
                                        @elseif($order->payment == 'liqpay')
                                        Visa/Mastercard(Liqpay)
                                        @elseif($order->payment == 'bonus')
                                        Оплата бонусами
                                        @endif</p> </span>
                                    </div>
                                    <div class="open_order_item">
                                        @lang('account_company_info.address')
                                        <span>{{ $order->address ?? '' }}</span>
                                    </div>
                                    <div class="open_order_item">
                                        @lang('account_company_info.status')
                                        <span>{{ $order->$pr_title ?? '' }}</span>
                                    </div>
                                    <div class="open_order_item">
                                        Стоимость доставки
                                        <span>{{ (int)$order->shipping_price ?? '' }} грн</span>
                                    </div>
                                </div>
                            </div>
                            <div class="order_hidden">
                            @foreach($purchases as $product)
                                <div class="hidden_item">
                                    <span class="hidden_title">{{ $product->$pr_title }}</span>
                                    <span>{{ $product->quantity }} шт</span>
                                    <span>{{ (int)$product->total }} грн</span>
                                </div>
                                @endforeach

                            </div>
                        </div>
            
</div>      
@endsection