@php
use App\Models\Offer;
$pr_title = 'name_' . $local;
$all_total = 0;
@endphp

@extends('layouts.app')

@section('content')
<div class="alert alert-success" style="display: none;"></div>
<div class="alert alert-danger" style="display: none;"></div>
<div class="container">
            @if($empty_cart)
            <div class="cart_container">@lang('cart.empty_cart')</div>
            @else
            <div class="cart_container">
                <div class="left_cart_container">
                    <p class="title_cart_section">@lang('cart.ordering')</p>
                    <div class="line_radio_toggle">
                        @auth
                        @else
                        <label>
                            <input type="radio" name="user_status" value="new" checked>
                            <span></span>
                            @lang('cart.new_user')
                        </label>
                        <label>
                            <input type="radio" name="user_status" value="registered">
                            <span></span>
                            @lang('cart.registered')
                        </label>
                        @endauth
                    </div>
                    <div class="toggle_forms">
                        <form action="" id="user-form">
                            <div class="wrap_block_inputs">
                                <div>
                                    <p>@lang('account_company_info.name4') <span class="required">*</span></p>
                                    <input class="user_data" type="text" name="name" id="name" value="{{ $user->name ?? '' }}">
                                </div>
                                <div>
                                    <p>@lang('account_user_info.second_name')</p>
                                    <input class="user_data" type="text" name="lastname" id="lastname" value="{{ $user->lastname ?? '' }}">
                                </div>
                                <div>
                                    <p>Email <span class="required">*</span></p>
                                    <input class="user_data" type="text" name="email" id="user_email" value="{{ $user->email ?? '' }}">
                                </div>
                                <div>
                                    <p>@lang('account_company_info.phone2') <span class="required">*</span></p>
                                    <input class="user_data" type="tel" name="phone" id="phone" value="{{ $user->phone ?? '' }}">
                                </div>
                                <div>
                                    <p>@lang('cart.comment')  </p>
                                    <textarea class="user_data" name="comment" id="comment"></textarea>
                                </div>
                                @auth
                                Вы накопили {{ (int)$user->bonus ?? 0 }} бонусных грн. По поводу их начисления и списания с вами свяжется менеджер.<br/>
                                @if(session('bonus'))
                                Вам будет начислено {{ session('bonus') }} баллов дополнительно
                                @endif
                                @endauth
                            </div>
                            <div class="line_link_button">
                                <a href="/">@lang('cart.purchases')</a>
                                <button type="button" id="set_order_button">@lang('cart.order_payment')</button>
                            </div>
                        </form>
                        <form method="POST" action="{{ route('login') }}" id="cart-login-form">
                            <div class="wrap_block_inputs">
                            	@csrf
                                <div class="form-group row">
                                    <p>@lang('cart.login')</p>
                                    <input id="email" type="text" class="form-control @error('login') is-invalid @enderror" name="login" value="{{ old('email') }}" required autocomplete="email" autofocus @error('email') style="border-color: #e3342f;" @enderror>
                                </div>
                                @error('email')
                                    <span class="invalid-feedback" role="alert" style="display: block;">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <div class="form-group row">
                                    <p>@lang('account_user_info.password')</p>
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" @error('password') style="border-color: #e3342f;" @enderror>
                                </div>
                                @error('password')
                                    <span class="invalid-feedback" role="alert" style="display: block;">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="line_link_button">
                                <a href="">@lang('cart.purchases')</a>
                                <button type="submit" value="">Войти</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="right_cart_container">
                    <p class="title_cart_section">@lang('cart.cart')</p>
                    <form action="" id="vendors-form">
                    	@foreach($vendors as $vendor)
                    	@php
                    	$offers = Offer::getOffer($cart, $vendor->vendor_id);
                    	@endphp
                        <div class="wrap_block_inputs">
                        	@php
                        	$total = 0;
                        	@endphp
                        	@foreach($offers as $offer)
                            @php
                            $offer_total = (int)$offer->price * $offer->quantity;
                            $total += $offer_total;
                            @endphp
                            <div class="one_product_in_cart">
                                <div class="icon_cart_product">
                                    <img src="{{ $offer->image ?? '/img/main/default-product.jpg' }}" alt="">
                                </div>
                                <div class="name_cart_product">
                                    <p style="text-align: center;">{{ $offer->$pr_title ?? '' }}</p>
                                    @if($offer->bonus)<p class="red-price">(+ {{ (int)$offer->bonus }} бонусных грн для зарегистрированных пользователей)</p>@endif
                                </div>
                                <div class="price_product">
                                    <p>{{ (int)$offer->price ?? 0 }} грн</p>
                                </div>
                                <div class="plus_minus">
                                    <input type="hidden" name="offer-{{ $offer->offer_id }}" value="{{ $offer->offer_id }}">
                                    <input type="hidden" name="price-{{ $offer->offer_id }}" value="{{ (int)$offer->price ?? 0 }}">
                                    <input type="hidden" name="vendor-{{ $offer->offer_id }}" value="{{ $vendor->vendor_id ?? 0 }}">
                                    <input type="hidden" name="total-{{ $offer->offer_id }}" value="{{ (int)$offer->price * $offer->quantity ?? 0 }}">
                                    <div class="minus" id="minus-{{ $offer->offer_id }}" onclick="minusQuantity({{ $offer->id }}); return false;">-</div>
                                    <input type="text" name="quan-{{ $offer->offer_id }}" value="{{ $offer->quantity }}" id="quan-{{ $offer->id }}" onchange="calculate({{ $offer->id }});">
                                    <div class="plus" id="plus-{{ $offer->offer_id }}" onclick="plusQuantity({{ $offer->id }}); return false;">+</div>
                                </div>
                                <div class="total_price">
                                    <p>{{ (int)$offer->price * $offer->quantity ?? 0 }} грн</p>
                                </div>
                                <div class="remove_product" id="remove-offer-{{ $offer->id }}">
                                    <a href="#" onclick="removeOffer({{ $offer->id }}); return false;">
                                        <img src="/img/main/red_trash.svg" alt="">
                                    </a>
                                </div>
                            </div>
                            @endforeach
                            <div class="delivery_settings">
                                <div class="top_click_settings" id="shipping-{{ $vendor->vendor_id }}" onchange="setShippingMethod({{ $vendor->vendor_id }});">
                                    <p>@lang('account_company_info.shipping')</p>
                                    
                                    
                                    <label>
                                        <input type="radio" name="delivery_status-{{ $vendor->vendor_id }}" value="inshop" @if((!session('vendor-' . $vendor->vendor_id)) || (session('vendor-' . $vendor->vendor_id) == 'inshop'))) checked @endif @if($vendor->gender == 'outshop') disabled @endif>
                                        <span></span>
                                        @lang('account_company_info.inshop')@if($vendor->gender == 'outshop')<span class="text_open cart-add-text" data-text=" У данного поставщика этот способ доставки недоступен">i</span>@endif
                                    </label>
                                    
                                    <label>
                                        <input type="radio" name="delivery_status-{{ $vendor->vendor_id }}" value="outshop" @if(session('vendor-' . $vendor->vendor_id) == 'outshop') checked @php $total += session('cost_' . $vendor->vendor_id) ?? 0; @endphp @endif @if($vendor->gender == 'inshop') disabled @endif>
                                        <span></span>
                                        @lang('cart.outshop') @if($vendor->gender == 'inshop')<span class="text_open cart-add-text" style="margin-left: 5px;" data-text=" У данного поставщика этот способ доставки недоступен">i</span>@endif
                                    </label>
                                    
                                    <label>
                                        <input type="radio" name="delivery_status-{{ $vendor->vendor_id }}" value="ourshipping" @if(session('vendor-' . $vendor->vendor_id) == 'ourshipping') checked @php $total += 700; @endphp @endif>
                                        <span></span>
                                        @lang('cart.ourshipping')
                                    </label>
                                </div>
                                <div class="change_delivery_type">
                                    @if((!session('vendor-' . $vendor->vendor_id)) || (session('vendor-' . $vendor->vendor_id) == 'inshop'))
                                    <div class="delivery_self">
                                        <div class="wrap_delivery_flex">
                                            <p class="min_text_del">@lang('account_company_info.shop_address')</p>
                                            <select name="address_{{ $vendor->vendor_id }}" id="user_address_{{ $vendor->vendor_id }}" class="form-control">
                                            @foreach($vendor->shops as $shop)
                                                <option value="{{ $shop->address }}">{{ $shop->address }}</option>
                                            @endforeach
                                            </select>
                                            <div>
                                                <p class="mini_text_cart">@lang('cart.shipping_cost')</p>
                                                <p>0 грн</p>
                                            </div>
                                        </div>
                                    </div>
                                    <script>
                                        $('#set_order_button').click(function(){
                                                setOrder();
                                                return false;
                                        });

                                        function mobileButton(){
                                                console.log('test');
                                                setOrder();
                                                return false;
                                        }
                                    </script>
                                    @endif
                                    @if(session('vendor-' . $vendor->vendor_id) == 'outshop')
                                    <div class="delivery_self">
                                        <div class="wrap_delivery_flex">
                                            <p class="min_text_del">@lang('cart.shipping_address')</p>
                                            <input type="text" name="address_{{ $vendor->vendor_id }}" id="user_address_{{ $vendor->vendor_id }}" value="{{ session('user_address_' . $vendor->vendor_id) }}" required>
                                            <div>
                                                <p class="mini_text_cart">@lang('cart.shipping_cost')</p>
                                                <p id="mini_text_cost_{{ $vendor->vendor_id }}">{{ session('cost_' . $vendor->vendor_id) ?? 0 }} грн</p>
                                                <input type="hidden" name="hidden_cost_{{ $vendor->vendor_id }}" id="hidden_cost_{{ $vendor->vendor_id }}" value="{{ session('cost_' . $vendor->vendor_id) ?? 0 }} грн" placeholder="Адрес" onchange="location.reload();" required="required">
                                            </div>
                                            <div style="display: none;" id="all-routes"></div>
                                        </div>
                                    </div>
                                    <script>
                                        $('#set_order_button').click(function(){
                                            if($('#user_address_{{ $vendor->vendor_id }}').val() == ''){
                                                alert('Введите, пожалуйста, адрес');
                                                return false;
                                            }else{
                                                setOrder();
                                                return false;
                                            }
                                        });

                                        function mobileButton(){
                                            if($('#user_address_{{ $vendor->vendor_id }}').val() == ''){
                                                alert('Введите, пожалуйста, адрес');
                                                return false;
                                            }else{
                                                setOrder();
                                                return false;
                                            }
                                        }

                                        $('#user_address_{{ $vendor->vendor_id }}').change(function(){
                                            var url = '/shopaddresses';
                                            var vendor = '{{ $vendor->vendor_id }}';
                                            $.ajax({
                                                url: url,
                                                type: 'post',
                                                data: {
                                                    'vendor_id': '{{ $vendor->vendor_id }}',
                                                    '_token': '{{ csrf_token() }}', 
                                                    'user_address': $('#user_address_{{ $vendor->vendor_id }}').val(),
                                                },
                                                dataType: 'json',
                                                success: function(json){
                                                    $('#hidden_cost_{{ $vendor->vendor_id }}').data('gres','0');
                                                    var cost = 0;
                                                    var i = 1;
                                                    var j = json.length;
                                                    for(key in json){
                                                        var start = json[key]['address'];
                                                        var finish = $('#user_address_{{ $vendor->vendor_id }}').val();
                                                        //alert({{ $vendor->vendor_id }});
                                                        geo_start(start, finish, json[key], vendor, {{ $vendor->vendor_id }}, i, j);
                                                        var delcost = $('#mini_text_cost_{{ $vendor->vendor_id }}').text();

                                                        console.log($('#hidden_cost_{{ $vendor->vendor_id }}').data('gres'));

                                                        localStorage.setItem('cart_name', $('#name').val());
                                                        localStorage.setItem('cart_lastname', $('#lastname').val());
                                                        localStorage.setItem('cart_email', $('#user_email').val());
                                                        localStorage.setItem('cart_phone', $('#phone').val());
                                                        localStorage.setItem('cart_comment', $('#comment').val());
														
                                                        setInterval(function(){                                                           
                                                            if($('#mini_text_cost_{{ $vendor->vendor_id }}').text() != delcost){
																
                                                               location.reload();
															   
                                                            }
                                                        }, 500);
                                                        i++;
                                                    }
                                                },
                                                error: function(){
                                                        $('#mini_text_cost_{{ $vendor->vendor_id }}').text('0 грн');
                                                        $('#hidden_cost_{{ $vendor->vendor_id }}').val('0 грн');											
														setTimeout(function(){
                                                                    alert('Для согласования возможности и стоимости доставки по указанному адресу с вами свяжется менеджер');
                                                                    $('#mini_text_cost_{{ $vendor->vendor_id }}').text('0 грн');
                                                                    $('#hidden_cost_{{ $vendor->vendor_id }}').val('0 грн');
                                                                }, 100);
													console.log(managerCounter);
                                                }
                                            });
                                            //location.reload();
                                        });


                                    </script>
                                    @endif
                                    @if(session('vendor-' . $vendor->vendor_id) == 'ourshipping')
                                    <div class="delivery_self">
                                        <div class="wrap_delivery_flex">
                                            <p class="min_text_del">@lang('cart.shipping_address')</p>
                                            <input type="text" name="address_{{ $vendor->vendor_id }}" id="user_address_{{ $vendor->vendor_id }}" placeholder="Адрес" value="{{ $user->address ?? '' }}" required="required">
                                            <div>
                                                <p class="mini_text_cart">@lang('cart.shipping_cost')</p>
                                                <p>700 грн</p>
                                            </div>
                                        </div>
                                    </div>
                                     <script>
                                        $('#set_order_button').click(function(){
                                            if($('#user_address_{{ $vendor->vendor_id }}').val() == ''){
                                                alert('Введите, пожалуйста, адрес');
                                                return false;
                                            }else{
                                                setOrder();
                                                return false;
                                            }
                                        });

                                        function mobileButton(){
                                            console.log('test');
                                            if($('#user_address_{{ $vendor->vendor_id }}').val() == ''){
                                                alert('Введите, пожалуйста, адрес');
                                                return false;
                                            }else{
                                                setOrder();
                                                return false;
                                            }
                                        }
                                    </script>
                                    @endif
                                </div>
                            </div>
                            <div class="about_company_in_cart">
                                <div>
                                    <input type="text" style="visibility: hidden;">
                                </div>
                                <div class="cart_continue">
                                    @if(($vendor->plan != null) && ($vendor->plan->tariffs->public_profile))
                                    <a href="/by_vendor/{{ $vendor->vendor_id }}">@lang('cart.continue')</a>
                                    @endif
                                    <div>
                                        <p class="min_text_del">@lang('cart.cost')</p>
                                        <p class="total_price" id="total_price_{{ $vendor->vendor_id }}">{{ $total }} грн</p>
                                    </div>
                                </div>
                            </div>
                            <div class="top_click_settings" onchange="">
                                    <p>@lang('account_company_info.payment')</p>

                                    <label>
                                        <input type="radio" name="payment" value="cod" checked>
                                        <span></span>
                                        @lang('cart.cod')
                                    </label>
                                    <label>
                                        <input type="radio" name="payment" value="agreement">
                                        <span></span>
                                        @lang('cart.agreement')
                                    </label>
                                    <!-- <label>
                                        <input type="radio" name="payment" value="liqpay" disabled>
                                        <span></span>
                                        Visa/MasterCard(Liqpay)
                                    </label> -->
                                    @auth
                                    @if($user->bonus)
                                    <label>
                                        <input type="radio" name="payment" value="bonus">
                                        <span></span>
                                        Оплата бонусами
                                    </label>
                                    @endif
                                    @endauth
                                </div>
                        </div>
                        @php
                        $all_total += $total;
                        @endphp
                        @endforeach
                        <div class="order_offer">
                            @lang('cart.all_total'):
                            <span><span id="order-total">{{ $all_total }}</span> грн</span>
                        </div>
                    </form>
                    <div class="line_link_button mobile">
                        <a href="{{ route('main') }}">@lang('cart.purchases')</a>
                        <button form="user-form" type="button" id="set_order_button2" onclick="mobileButton();">@lang('cart.order_payment')</button>
                    </div>
                </div>
            </div>
            @endif
        </div>
        <div class="modal_block text_hint">
            <div class="close_modal"></div>
            <p class="append_text"></p>
        </div>
        @auth
        @else
        <script>
            if(localStorage.getItem('cart_name')){
                $('#name').val(localStorage.getItem('cart_name'));
            }
            
            if(localStorage.getItem('cart_lastname')){
                $('#lastname').val(localStorage.getItem('cart_lastname'));
            }

            if(localStorage.getItem('cart_email')){
                $('#user_email').val(localStorage.getItem('cart_email'));
            }
            
            if(localStorage.getItem('cart_phone')){
                $('#phone').val(localStorage.getItem('cart_phone'));
            }
            
            if(localStorage.getItem('cart_comment')){
               $('#comment').val(localStorage.getItem('cart_comment')); 
            }
            
        </script>
        @endauth
        <script>
            $('.user_data').change(function(){
                localStorage.setItem('cart_name', $('#name').val());
                localStorage.setItem('cart_lastname', $('#lastname').val());
                localStorage.setItem('cart_email', $('#user_email').val());
                localStorage.setItem('cart_phone', $('#phone').val());
                localStorage.setItem('cart_comment', $('#comment').val());
            });

            $('input[name=\'user_status\']').change(function(){
                localStorage.setItem('user_status', $('input[name=\'user_status\']:checked').val());;
            });

            if(localStorage.getItem('user_status') == 'registered'){
                $('input[name="user_status"][value="registered"]').prop('checked', true);
                $('#user-form').hide();
                $('#cart-login-form').show();
            }
        </script>
        @auth
        <script>
            localStorage.setItem('user_status', null);
            $('#user-form').show();
            $('#cart-login-form').hide();
        </script>
        @endauth

@endsection