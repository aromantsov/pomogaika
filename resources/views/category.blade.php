@php
$cat_title = 'title_' . $local;
$pr_title = 'name_' . $local;
@endphp

@extends('layouts.app')

@section('content')
    <div class="content">
        <div class="breadcrumbs_container">
            <div class="container">
                <div class="breadcrumbs">
                   <div class="show_overlay_hover"></div>
                    <div class="link_dropdown_menu">
                        <button class="blue_button_link" onclick="toggleNavbar();">@lang('account_company_info.catalog') <img src="/img/main/line_toggle_cat.svg" alt=""></button>
                        <div class="a_side_category" id="a_side_category">
                            <div class="link_cats_a_side">
                                @foreach($categories as $category)
                                <div class="wrap_link_category">
                                    <a href="{{ route('category', ['id' => $category->id]) }}"><img src="{{ $category->icon ?? '/img/main/image_cat1.svg' }}" alt="">{{ $category->$cat_title }}</a>
                                    <div class="wrap_submenu_section">
                                        @foreach($category->children as $category_list)
                                         <div class="one_catalog_submenu_line">
                                            <img src="{{ $category_list->image ?? '/img/main/icon_cats1.jpg' }}" alt="">
                                            <div class="list_link_submenu_name_catalog">
                                                <p class="name_cat_section">{{ $category_list->$cat_title }}</p>
                                                <div class="line_link_cat nav-menu">
                                                    @foreach($category_list->children as $catlist2)
                                                    <a href="{{ route('category', ['id' => $catlist2->id]) }}">{{ $catlist2->$cat_title }}</a>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                                @endforeach
                            </div>
                            <a href="{{ route('categories') }}" class="all_category_link blue_button_link"><img src="/img/main/line_toggle_cat.svg" alt=""> @lang('category.all_categories')</a>
                        </div>
                    </div>
                    <div class="breadcrumbs_link">
                        <a href="{{ route('main') }}">@lang('account_user_info.main')</a>
                        <a href="{{ route('categories') }}">@lang('category.catalog')</a>
                        <span>{{ $category_page->$cat_title }}</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="alert alert-success" style="display: none;"></div>
        <div class="alert alert-danger" style="display: none;"></div>
        <div class="container">
            <div class="category-slider">
            @foreach($slider as $banner)
            <div class="category-banner">
                <a href="" class="image_link_add">
                <img src="/img/main/rel3.jpg" alt="">
            </a>
            </div>
            @endforeach
            <div class="category-banner">
                <a href="" class="image_link_add">
                <img src="/img/main/rel3.jpg" alt="">
            </a>
            </div>
            </div>
            <div class="categories_line_wrap">
                <p class="title_product_block_cat">{{ $category_page->$cat_title }}</p>
                <div class="line_categories">
                    @foreach($category_page->children as $subcategory)
                    <div class="one_catalog_line">
                        <img src="{{ $subcategory->image ?? '/img/main/default-category.jpg' }}" alt="">
                        <div class="list_link_name_catalog">
                            <a href="{{ route('category', ['id' => $subcategory->id]) }}" class="name_cat_section">{{ $subcategory->$cat_title }}</a>
                            <div class="line_link_cat">
                                @foreach($subcategory->children as $subcategory2)
                                <a href="{{ route('category', ['id' => $subcategory2->id]) }}">{{ $subcategory2->$cat_title }}</a>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="container">
            <div class="catalog_filter_line">
                <div class="left_filter">
                    <p class="title_filter_block">@lang('category.filters')</p>
                    <div class="wrap_filter">
                        <form action="{{ route('filters', $id) }}" method="get"  id="filter-form" onchange="filterSend()">
                            <input type="hidden" name="sort_by" value="{{ $filters['sort_by'] ?? '' }}">
                            <!-- <div class="slide_title_label"> 
                                <div class="wrap_slide_content">
                                <label>
                                    <input type="checkbox" name="all" value="true" id ="check_all" class="checkbox" @if($all_checked) checked @endif>
                                    <span></span>
                                    @lang('category.check_all')
                                </label>
                                </div>
                            </div> -->
                            @if(count($vendors) > 1)
                            <div class="slide_title_label">
                                <p class="name_slide_label">@lang('account_user_info.seller'):</p>
                                <div class="wrap_slide_content">
                                    @foreach($vendors as $vendor)
                                    <label>
                                        <input type="checkbox" name="vendor[]" value="{{ $vendor->id }}"  class="checkbox" @if(in_array($vendor->id, $filters['vendor'] ?? [])) checked @endif>
                                        <span></span>
                                        {{ $vendor->name }}
                                    </label>
                                    @endforeach
                                    <!-- <label>
                                        <input type="checkbox">
                                        <span></span>
                                        «Абсолют»
                                    </label>
                                    <label>
                                        <input type="checkbox">
                                        <span></span>
                                        «Петрович Харьков»
                                    </label>
                                    <label>
                                        <input type="checkbox">
                                        <span></span>
                                        «Куб»
                                    </label> -->
                                </div>
                            </div>
                            @endif
                            @if(count($manufacturers) > 1)
                            <div class="slide_title_label">
                                <p class="name_slide_label">@lang('category.brand'):</p>
                                <div class="wrap_slide_content">
                                    @foreach($manufacturers as $manufacturer)
                                    <label>
                                        <input type="checkbox" name="manufacturer[]" value="{{ $manufacturer->id }}"  class="checkbox" @if(in_array($manufacturer->id, $filters['manufacturer'] ?? [])) checked @endif>
                                        <span></span>
                                        {{ $manufacturer->brand }}
                                    </label>
                                    @endforeach
                                    <!-- <label>
                                        <input type="checkbox">
                                        <span></span>
                                        VOLLE
                                    </label>
                                    <label>
                                        <input type="checkbox">
                                        <span></span>
                                        Treker
                                    </label>
                                    <label>
                                        <input type="checkbox">
                                        <span></span>
                                        Момент
                                    </label> -->
                                </div>
                            </div>
                            @endif
                            <div class="slide_title_label">
                                <p class="name_slide_label">@lang('account_company_info.price'):</p>
                                <div class="wrap_slide_content">
                                    <div class="wrap_range_inputs">
                                        <input type="text" name="from_price" class="from_input" value="{{ (int)$from_price }}">
                                        <input type="text" name="to_price" class="to_input" value="{{ (int)$to_price }}">
                                    </div>
                                    <input type="text" class="js-range-slider" name="price_range" data-type="double" data-min="{{ $min_price }}" data-max="{{ $max_price }}" data-from="{{ $from_price }}" data-to="{{ $to_price }}" data-grid="false">
                                </div>
                            </div>
                            <!-- <div class="slide_title_label">
                                <p class="name_slide_label">Размер/Объем:</p>
                                <div class="wrap_slide_content">
                                    <div class="label_weight">
                                        <label>
                                            <input type="checkbox">
                                            <span></span>
                                            1 кг
                                        </label>
                                        <label>
                                            <input type="checkbox">
                                            <span></span>
                                            10 кг
                                        </label>
                                        <label>
                                            <input type="checkbox">
                                            <span></span>
                                            25 кг
                                        </label>
                                        <label>
                                            <input type="checkbox">
                                            <span></span>
                                            50 кг
                                        </label>
                                        <label>
                                            <input type="checkbox">
                                            <span></span>
                                            100 кг
                                        </label>
                                        <label>
                                            <input type="checkbox">
                                            <span></span>
                                            150 кг
                                        </label>
                                    </div>
                                </div> -->
                            <!-- </div> -->
                        </form>
                    </div>
                </div>
                <div class="right_result_catalog">
                    <div class="count_sort">
                        <p class="title_product_block">@lang('category.products'):</p>
                        <div class="sort_select_text">
                            @if($products)
                            <p>@lang('account_company_info.sort'):</p>
                            <select class="sort_select" name="" id="sort_select">
                                <option value="price_asc" @if(isset(($filters['sort_by'])) && ($filters['sort_by'] == 'price_asc')) selected @endif>@lang('category.by_more_price')</option>
                                <option value="price_desc" @if(isset(($filters['sort_by'])) && ($filters['sort_by'] == 'price_desc'))  selected @endif>@lang('category.by_less_price')</option>
                                <option value="popular"  @if(isset(($filters['sort_by'])) && ($filters['sort_by'] == 'popular'))  selected @endif>@lang('category.by_popular')</option>
                            </select>
                            @endif
                        </div>
                    </div>
                    <div class="line_other_product">
                    	@foreach($products as $product)
                        <div class="one_product_line">
                            <div class="nameplates">
                            @if($product['is_sale'])<div class="red_nameplate">Sale</div>@endif
                            @if($product['is_new'])<div class="red_nameplate">New</div>@endif
                            </div>

                            <div class="wishlist_compare">
                                <a href="" class="add_compare" onclick="compare.add({{ $product['id'] }}); return false;"><img src="/img/main/icon_compare_blue.svg" alt=""></a>
                                <a href="#" class="add_wishlist" onclick="addWishlist({{ $product['id'] }}, {{ Auth::id() ?? 0 }}); return false;"><img src="/img/main/icon_wishlist_blue.svg" alt="">
                                <div class="ripple-parent">
                                        <div class="ripple"></div>
                                    </div></a>
                            </div>
                            <a href="{{ route('product', ['id' => $product['id']]) }}"><img src="{{ $product['image'] ?? '/img/main/default-product.jpg' }}" alt="" class="prod-img" style=""></a>
                            <div style="height: 40px;">
                                <p>{{ $product[$pr_title] }}</p>
                            </div>
                            <div class="star_reviews">
                                <div class="line_star">
                                <img src="@if($product['rating'] >= 1)/img/main/star_yellow.svg @else /img/main/empty_star.svg @endif" alt="">
                                <img src="@if($product['rating'] >= 2)/img/main/star_yellow.svg @else /img/main/empty_star.svg @endif" alt="">
                                <img src="@if($product['rating'] >= 3)/img/main/star_yellow.svg @else /img/main/empty_star.svg @endif" alt="">
                                <img src="@if($product['rating'] >= 4)/img/main/star_yellow.svg @else /img/main/empty_star.svg @endif" alt="">
                                <img src="@if($product['rating'] >= 5)/img/main/star_yellow.svg @else /img/main/empty_star.svg @endif" alt="">
                                </div>
                                <div class="count_reviews">
                                    {{ $product['reviews'] }}
                                    <img src="/img/main/icon_review.svg" alt="">
                                </div>
                            </div>
                            <div class="line_price_addcart">
                                <div class="left_price">
                                    @if($product['is_sale'])<p class="old_price">{{ (int)$product['trade_price'] ?? 0 }} грн</p>@endif
                                    <p class="red_price">{{ (int)$product['price'] }} грн</p>

                                </div>
                            </div>
                            <a href="{{ route('product', ['id' => $product['id']]) }}" class="add_cart">Выбрать поставщика</a>
                            @if($product['bonus'])<div class="red_price">+ {{ $product['bonus'] }} бонусных грн</div>@endif
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
      $('#sort_select').change(function(){
        var local = '/{{ $local }}';
        if(local == '/uk'){
            local = '';
        }
        var req = local + '/filters/{{ $category_page->id }}/?{{ http_build_query($param ?? [], "", "&") }}' + '&sort_by=' + $('#sort_select option:selected').val();
        req = req.replace(/&amp;/g, '&');
        window.location.href = req;
      });  
    </script>
@endsection