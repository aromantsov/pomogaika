@extends('admin.layouts.app')

@section('content')

<div class="container">
	@component('admin.components.breadcrumb')
	@slot('title') Редактировать заявку @endslot
	@slot('parent') Главная страница @endslot
	@slot('active') Заявки на обратную связь @endslot
	@endcomponent
	<hr/>
	<form action="{{ route('admin.callback.update', $callback) }}" method="post" class="form-horizontal">
		{{ csrf_field() }}
		<input type="hidden" name="_method" value="put">
		<label for="">Имя</label>
        <input type="text" class="form-control" name="name" placeholder="Имя" value="{{ $callback->name ?? '' }}">
        <label for="">E-mail</label>
        <input type="email" class="form-control" name="email" placeholder="E-mail" value="{{ $callback->email ?? '' }}">
        <label for="">Сообщение</label>
        <textarea name="message" id="message" cols="30" rows="10" class="form-control">{{ $callback->message ?? '' }}</textarea>
        <label for="">Статус</label>
        <select name="status" id="" class="form-control">
        <option value="new" @if($callback->status == 'new') selected @endif>Новый</option>
        <option value="inwork" @if($callback->status == 'inwork') selected @endif>В работе</option>
        <option value="wait" @if($callback->status == 'wait') selected @endif>Ожидание</option>
        <option value="closed" @if($callback->status == 'closed') selected @endif>Закрыт</option>
        </select>
        <hr/>
        <input type="submit" class="btn btn-primary">  
	</form>
</div>

@endsection