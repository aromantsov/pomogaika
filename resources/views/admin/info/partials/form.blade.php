@php
$pr_title = 'title_' . $local;
@endphp

<label for="">Наименование страницы (на русском языке)</label>
<input type="text" class="form-control" name="title_ru" placeholder="Наименование страницы (на русском языке)" value="{{ $info->title_ru ?? '' }}" required>

<label for="">Наименование страницы (на украинском языке)</label>
<input type="text" class="form-control" name="title_uk" placeholder="Наименование страницы (на украинском языке)" value="{{ $info->title_uk ?? '' }}" required> 


<label for="description-ru">Текст (на русском языке)</label>
<textarea name="text_ru" id="description-ru" cols="30" rows="10" class="form-control">{{ $info->text_ru ?? '' }}</textarea>


<label for="description-ru">Текст (на украинском языке)</label>
<textarea name="text_uk" id="description-uk" cols="30" rows="10" class="form-control">{{ $info->text_uk ?? '' }}</textarea>
<script type="text/javascript">

$(document).ready(function(){
    CKEDITOR.replace( 'description-ru' );
    CKEDITOR.replace( 'description-uk' );
});
 </script>

<hr>
<input type="submit" class="btn btn-primary" value="Сохранить">