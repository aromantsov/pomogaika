@extends('admin.layouts.app')

@section('content')

<div class="container">
	@component('admin.components.breadcrumb')
	@slot('title') Создать информационную страницу @endslot
	@slot('parent') Главная страница @endslot
	@slot('active') Информационная страница @endslot
	@endcomponent
	<hr>
	<form action="{{ route('admin.info.store') }}" method="post" class="form-horizontal" enctype="multipart/form-data">
		{{ csrf_field() }}
		@include('admin.info.partials.form')
	</form>
</div>
@endsection