@extends('admin.layouts.app')

@section('content')

<div class="container">
	@component('admin.components.breadcrumb')
	@slot('title') Создать характеристику @endslot
	@slot('parent') Главная страница @endslot
	@slot('active') Характеристики @endslot
	@endcomponent
	<hr/>
	<form action="{{ route('admin.attribute.store') }}" method="post" class="form-horizontal">
		{{ csrf_field() }}
		@include('admin.attributes.partials.form')
	</form>
</div>

@endsection