@php
$attr_title = 'name_' . $local;
@endphp

@extends('admin.layouts.app')

@section('content')

<div class="container">
	@component('admin.components.breadcrumb')
	@slot('title') Список Характеристик @endslot
	@slot('parent') Главная страница @endslot
	@slot('active') Характеристики @endslot
	@endcomponent
	<hr/>
	@if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if (Session::has('success'))
        <div class="alert alert-success">{{ Session::get('success') }}</div>
    @endif
	<a href="{{ route('admin.attribute.create') }}" class="btn btn-primary pull-right"><i class="fa fa-plus-square-o"> Создать характеристику</i></a>
	<table class="table table-striped">
		<thead>
			<th>Название</th>
			<th>Статус</th>
			<th class="text-right">Действия</th>
		</thead>
		<tbody>
			@forelse($attributes as $attribute)
			<tr>
				<td>{{ $attribute->$attr_title }}</td>
				<td>{{ $attribute->published }}</td>
				<td class="text-right">
				    <form action="{{ route('admin.attribute.destroy', $attribute) }}" onsubmit="if(confirm('Delete?')){return true}else{return false}" method="post">
            			<input type="hidden" name="_method" value="DELETE"> 
            			{{ csrf_field() }}
            			<a href="@if(in_array($attribute->id, [10])) # @else {{ route('admin.attribute.edit', $attribute) }} @endif" class="btn btn-default" @if(in_array($attribute->id, [6, 7, 8, 9, 10])) disabled @endif><i class="fa fa-edit"></i></a>
            			<button type="submit" class="btn" @if(in_array($attribute->id, [6, 7, 8, 9, 10]))) disabled @endif><i class="fa fa-trash"></i></button>
            		</form></td> 
			</tr>
			@empty
			<tr>
				<td colspan="3" class="text-center">Нет характеристик</td>
			</tr>
			@endforelse
		</tbody>
		<tfoot>
			<tr>
				<td colspan="3">
					<ul class="pagination full-right">
						{{ $attributes->links() }}
					</ul>
				</td>
			</tr>
		</tfoot>
	</table>
</div>

@endsection