<label for="">Название на русском языке</label>
<input type="text" class="form-control" name="name_ru" placeholder="Название на русском языке" value="{{ $attribute->name_ru ?? '' }}" required>
<label for="">Название на украинском языке</label>
<input type="text" class="form-control" name="name_uk" placeholder="Название на украинском языке" value="{{ $attribute->name_uk ?? '' }}" required>
<hr/>
<input type="submit" class="btn btn-primary">   