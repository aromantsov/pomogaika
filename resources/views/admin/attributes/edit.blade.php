@extends('admin.layouts.app')

@section('content')

<div class="container">
	@component('admin.components.breadcrumb')
	@slot('title') Редактировать характеристику @endslot
	@slot('parent') Главная страница @endslot
	@slot('active') Характеристики @endslot
	@endcomponent
	<hr/>
	<form action="{{ route('admin.attribute.update', $attribute) }}" method="post" class="form-horizontal">
		{{ csrf_field() }}
		<input type="hidden" name="_method" value="put">
		@include('admin.attributes.partials.form')
	</form>
</div>

@endsection