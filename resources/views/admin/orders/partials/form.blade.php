

<label for="">Статус</label>
<select name="order_status_id" id="" class="form-control">
<option value="0" disabled @if(!isset($order)) selected @endif>--Выберите статус--</option>
@foreach($statuses as $status)
<option value="{{ $status->id }}" @if($order->order_status_id == $status->id) selected @endif>{{ $status->name_ru }}</option>
@endforeach

</select>

<label for="">Имя клиента</label>
<input type="text" class="form-control" name="name" placeholder="Имя клиента" value="{{ $order->name ?? '' }}" required>

<label for="">Фамилия клиента</label>
<input type="text" class="form-control" name="lastname" placeholder="Фамилия клиента" value="{{ $order->lastname ?? '' }}" required>

<label for="">Email клиента</label>
<input type="email" class="form-control" name="email" placeholder="Email клиента" value="{{ $order->email ?? '' }}" required>

<label for="">Телефон клиента</label>
<input type="tel" class="form-control" name="phone" placeholder="Телефон клиента" value="{{ $order->phone ?? '' }}" required> 

<label for="">Адрес доставки</label>
<input type="text" class="form-control" name="address" placeholder="Адрес доставки" value="{{ $order->address ?? '' }}"> 

<label for="">Итоговая сумма</label>
<input type="text" class="form-control" name="total" placeholder="Итоговая сумма" value="{{ (int)$order->total ?? 0 }} грн"> 

@if($user)
<label for="">Количество бонусов у клиента</label>
<input type="text" class="form-control" name="bonus" placeholder="Количество бонусов у клиента" value="{{ $user->bonus ?? 0 }}"> 
@endif

<label for="">Способ оплаты</label>
<select name="payment" id="" class="form-control">
@if(isset($order->id))
    <option value="cod" @if($order->payment == 'cod') selected @endif>Наложенный платеж</option>
    <option value="agreement" @if($order->payment == 'agreement') selected @endif>Оплата согласно договору</option>
    <option value="liqpay" @if($order->payment == 'liqpay') selected @endif>Liqpay</option>
    <option value="bonus" @if($order->payment == 'bonus') selected @endif>Оплата бонксами</option>
@else
    <option value="cod">Наложенный платеж</option>
    <option value="agreement">Оплата согласно договору</option>
    <option value="liqpay">Liqpay</option>
    <option value="bonus">Оплата бонксами</option>
@endif
</select>

<label for="">IP-адрес, с которого поступил заказ</label>
<input type="text" class="form-control" name="ip" placeholder="IP-адрес, с которого поступил заказ" value="{{ $order->ip ?? '' }}" disabled>


<label for="description-ru">Комментарий</label>
<textarea name="comment" id="comment" cols="30" rows="10" class="form-control">{{ $order->comment ?? '' }}</textarea>
<script type="text/javascript">

$(document).ready(function(){
    CKEDITOR.replace( 'comment' );
});
 </script>

<hr>
<input type="submit" class="btn btn-primary">