@extends('admin.layouts.app')

@section('content')
<div class="container">
	@component('admin.components.breadcrumb')
	@slot('title') Редактировать заказ @endslot
	@slot('parent') Главная страница @endslot
	@slot('active') Заказы @endslot
	@endcomponent
	<hr>
	@if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if (Session::has('success'))
        <div class="alert alert-success">{{ Session::get('success') }}</div>
    @endif
	<form action="{{ route('admin.orders.update', $order) }}" method="post" class="form-horizontal"  enctype="multipart/form-data">
		<input type="hidden" name="_method" value="put">
		{{ csrf_field() }}
		@include('admin.orders.partials.form')
	</form>
	<a href="#modal3" class="btn btn-default open_modal" style="margin: 10px;">Товары в заказе</a>
</div>

<div id="modal3" class="modal_div">

    <span class="modal_close">X</span>
    <div class="tab-pane" id="tab-offer">
              <div class="table-responsive">
              	<form action="{{ route('admin.order.savepurchase') }}" method="post" id="purchase-form">
              		{{ csrf_field() }}
              		<input type="hidden" name="order_id" value="{{ $order->id }}"> 
                <table id="offer" class="table table-striped table-bordered table-hover" style="width: 100%;">
                  <thead>
                    <tr>
                      <td class="text-left" style="max-width: 30%">Товар</td>
                      <td class="text-left">Цена</td>
                      <td class="text-left" style="max-width: 10%">Количество</td>
                      <td class="text-left">Итого</td>
                      <td class="text-right">Действие</td>
                    </tr>
                  </thead>
                  <tbody>
                  @php
                  $offer_row = 0;
                  $attr_name = 'name_' . $local; 
                  @endphp
                  @foreach($purchases as $purchase)
                  <tr id="offer-row{{ $offer_row }}">
                    <td class="text-left" style="max-width: 30%">
                        <select name="purchase[{{ $offer_row }}][name]" id="purchase-{{ $offer_row }}-name" onchange="setPrice({{ $offer_row}});">
                          <option value="0"></option>
                          @foreach($offers as $offer)
                          <option value="{{ $offer->id }}" style="max-width: 25%" @if($offer->id == $purchase->offer_id) selected @endif>{{ $offer->products->name_ru ?? '' }}</option>
                          @endforeach
                        </select>
                      </td>
                      <td class="text-left" style="width: 40%;"><div class="input-group">
                        <input type="text" name="purchase[{{ $offer_row }}][price]" rows="5" placeholder="Введите значение" class="form-control" value="{{ $purchase->price }}" id="purchase-{{ $offer_row }}-price" onchange="calcTotal({{ $offer_row }});"></td>
                      <td class="text-left" style="max-width: 10%"><div class="input-group">  
                        <input type="text" name="purchase[{{ $offer_row }}][quantity]" id="purchase-{{ $offer_row }}-quantity" onchange="calcTotal({{ $offer_row }});" rows="5" placeholder="Введите значение" class="form-control" value="{{ $purchase->quantity }}"></td>
                      <td class="text-left" style="width: 40%;"><div class="input-group">  
                        <input type="text" name="purchase[{{ $offer_row }}][total]" rows="5" placeholder="Введите значение" class="form-control" value="{{ $purchase->total }}" id="purchase-{{ $offer_row }}-total" disabled></td>
                    <td class="text-right"><button type="button" onclick="$('#offer-row{{ $offer_row }}').remove();" data-toggle="tooltip" title="Удалить" class="btn btn-danger" @if(count($purchases) == 1) disabled @endif><i class="fa fa-minus-circle"></i></button></td>
                  </tr>
                  @php
                  $offer_row++;
                  @endphp
                  @endforeach
                    </tbody>
                  
                  <tfoot>
                    <tr>
                      <td colspan="4"></td>
                      <td class="text-right"><button type="button" onclick="addOffer();" data-toggle="tooltip" title="" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                    </tr>
                  </tfoot>
                </table>
                <label for="">Стоимость доставки</label>
                <input type="text" class="form-control" name="shipping_price" placeholder="Стоимость доставки" value="{{ (int)$order->shipping_price ?? 0 }} грн" style="margin-bottom: 10px"> 
                </form>
                <button type="submit" class="btn btn-default" onclick="$('#purchase-form').submit();">Сохранить</button>
              </div>
            </div>
</div>
<div id="overlay"></div>
            <script>
              var offer_row = {{ $offer_row }}; 

    function addOffer() {
      var url = '/admin/order/autocomplete';
      $.ajax({
         url: url,
         method: 'post',
         data:{
                '_token': '{{ csrf_token() }}',
                'order_id': '{{ $order->id }}'
            },   
         success: function(res){
          var attrs = JSON.parse(res);
         
        var html  = '<tr id="offer-row' + offer_row + '">';
        html += '  <td class="text-left" style="max-width: 30%"><select name="purchase[' + offer_row + '][name]" id="purchase-' + offer_row + '-name" onchange="setPrice(' + offer_row + ')">';
        html += '<option value="0"></option>';
        for(var attr in attrs){
        	if(typeof(attrs[attr]['products']) != "undefined" && attrs[attr]['products'] !== null){
        		var name = attrs[attr]['products']['name_ru'];
        	}else{
        		var name = '';
        	}
        	html += '<option value="' + attrs[attr]['id'] + '">' + name + '</option>';
        }
        html += '</select></td>';
        html += '  <td class="text-left">';
        html += '<div class="input-group"><input type="text" name="purchase[' + offer_row + '][price]" value="" rows="5" placeholder="" class="form-control" id="purchase-' + offer_row + '-price" onchange="calcTotal(' + offer_row + ');"></div>';
        html += '  </td>';
        html += '  <td class="text-left" style="max-width: 10%">';
        html += '<div class="input-group"><input type="text" name="purchase[' + offer_row + '][quantity]" value="1" rows="5" placeholder=""  id="purchase-' + offer_row + '-quantity" class="form-control" onchange="calcTotal(' + offer_row + ');"></div>';
        html += '  </td>';
        html += '  <td class="text-left">';
        html += '<div class="input-group"><input type="text" name="purchase[' + offer_row + '][total]" value="" rows="5" placeholder="" class="form-control" id="purchase-' + offer_row + '-total" disabled></div>';
        html += '  </td>';
        html += '  <td class="text-right"><button type="button" onclick="$(\'#offer-row' + offer_row + '\').remove();" data-toggle="tooltip" title="" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
        html += '</tr>';

        $('#offer tbody').append(html);

        offer_row++;
    }
    });
  }

  function savepurchases(){
  	var url = '/admin/purchase/savepurchase';
  	var overlay = $('#overlay');
    var modal = $('.modal_div');
  	$.ajax({
  		url: url,
  		method: 'post',
  		data: $('#purchase-form').serialize(),
  		success: function(res){
  			console.log(res);
            modal 
            .animate({opacity: 0, top: '45%'}, 200, 
            function(){ 
                $(this).css('display', 'none');
                overlay.fadeOut(400);
  		    });
  	    }        
    });
    return false;
  }

  function setPrice(row){
  	var url = '/admin/order/setprice';
  	var offer = $('#purchase-' + row + '-name option:selected').val();
    $.ajax({
    	url: url,
    	method: 'post',
    	data: {
    		'_token': '{{ csrf_token() }}',
            'offer': offer
    	},
    	success: function(price){
            $('#purchase-' + row + '-price').val(price);
            $('#purchase-' + row + '-quantity').val(1);
            $('#purchase-' + row + '-total').val(price);
    	}
    })
  }

  function calcTotal(row){
  	var quantity = parseInt($('#purchase-' + row + '-quantity').val());
  	console.log(quantity);
  	var price = parseInt($('#purchase-' + row + '-price').val());
  	console.log(price);
  	var total = price * quantity;
  	$('#purchase-' + row + '-total').val(total);
  }
    </script>



@endsection