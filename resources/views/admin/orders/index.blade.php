
@extends('admin.layouts.app')

@section('content')

<div class="container">
	@component('admin.components.breadcrumb')
	@slot('title') Список заказов @endslot
	@slot('parent') Главная страница @endslot
	@slot('active') Заказы @endslot
	@endcomponent
	<hr/>
	@if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if (Session::has('success'))
        <div class="alert alert-success">{{ Session::get('success') }}</div>
    @endif
	<!-- <a href="" class="btn btn-primary pull-right"><i class="fa fa-plus-square-o"> Создать товар</i></a> -->
	<table class="table table-striped">
		<thead>
			<th>№ заказа</th>
			<th>Покупатель</th>
			<th>Итоговая сумма</th>
			<th>Статус</th>
			<th class="text-right">Действия</th>
		</thead>
		<tbody>
			@forelse($orders as $order)
			<tr>
				<td>{{ $order->id }}</td>
				<td>{{ $order->name . ' ' . $order->lastname }}</td>
				<td>{{ $order->total . ' грн' }}</td>
				<td>{{ $order->name_ru }}</td>
				<td class="text-right">
            			<a href="{{ route('admin.orders.edit', $order) }}" class="btn btn-default"><i class="fa fa-edit"></i></a>
                </td>
			</tr>
			@empty
			<tr>
				<td colspan="3" class="text-center">Нет заказов</td>
			</tr>
			@endforelse
		</tbody>
		<tfoot>
			<tr>
				<td colspan="3">
					<ul class="pagination full-right">
						{{ $orders->links() }}
					</ul>
				</td>
			</tr>
		</tfoot>
	</table>
</div>

@endsection