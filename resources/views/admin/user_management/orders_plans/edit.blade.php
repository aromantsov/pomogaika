@extends('admin.layouts.app')

@section('content')
<div class="container">
	@component('admin.components.breadcrumb')
	@slot('title') Редактирование заявки на изменение тарифного плана @endslot
	@slot('parent') Панель управления @endslot
	@slot('active') Заявка на изменение тарифного плана @endslot
	@endcomponent
	<hr>
	<form action="{{ route('admin.user_management.orderplan.update', $plan->id) }}" method="post" class="form-horizontal">
		<input type="hidden" name="_method" value="put">
		<input type="hidden" name="user_id" value="{{ $plan->users->id ?? '' }}">
		{{ csrf_field() }}
    @if($errors->any())
    <div class="alert alert-danger">
	    <ul>
		    @foreach($errors->all() as $error)
		    <li>{{ $error }}</li>
		    @endforeach
	    </ul>
    </div>
    @endif
    <label for="">Имя пользователя</label>
    <input type="text" class="form-control" placeholder="Имя пользователя" value="{{ $plan->users->name ?? '' }}" disabled>
    <label for="">Email</label>
    <input type="email" class="form-control" name="email" placeholder="Email" value="{{ $plan->users->email ?? '' }}" disabled>
    <label for="">Телефон</label>
    <input type="tel" class="form-control" placeholder="Телефон" value="{{ $plan->users->phone ?? '' }}" disabled>
    <label for="">Подключаемый тариф</label>
    <select name="tariff_id" class="form-control" id="">
	<option value="0" disabled>--Подключаемый тариф--</option>
	@foreach($tariffs as $tariff)
    <option value="{{ $tariff->id }}" @if($plan->tariffs->id == $tariff->id) selected @endif>{{ $tariff->name }}</option>
	@endforeach
	</select>
	<label for="">Действителен до</label>
	<input type="date" class="form-control" name="term" placeholder="" value="{{ $plan->term }}" required>
</select>
<hr>
<input type="submit" name="send" class="btn btn-primary" value="Сохранить">
<input type="submit" name="send" class="btn btn-primary" value="Активировать">
	</form>
</div>
@endsection