@extends('admin.layouts.app')

@section('content')
<div class="container">
	@component('admin.components.breadcrumb')
	@slot('title') Редактирование заявки на показ рекламного баннера @endslot
	@slot('parent') Панель управления @endslot
	@slot('active') Заявка на показ рекламного баннера @endslot
	@endcomponent
	<hr>
	<form action="{{ route('admin.user_management.adv.update', $adv->id) }}" method="post" class="form-horizontal" enctype="multipart/form-data">
		<input type="hidden" name="_method" value="put">
		<input type="hidden" name="user_id" value="{{ $adv->users->id ?? '' }}">
		{{ csrf_field() }}
    @if($errors->any())
    <div class="alert alert-danger">
	    <ul>
		    @foreach($errors->all() as $error)
		    <li>{{ $error }}</li>
		    @endforeach
	    </ul>
    </div>
    @endif
    <label for="">Имя пользователя</label>
    <input type="text" class="form-control" placeholder="Имя пользователя" value="{{ $adv->users->name ?? '' }}" disabled>
    <label for="">Email</label>
    <input type="email" class="form-control" name="email" placeholder="Email" value="{{ $adv->users->email ?? '' }}" disabled>
    <label for="">Телефон</label>
    <input type="tel" class="form-control" placeholder="Телефон" value="{{ $adv->users->phone ?? '' }}" disabled>
    <label for="">Подключаемый тариф</label>
    <select name="advplace_id" class="form-control" id="">
	<option value="0" disabled>--Место для рекламного баннера--</option>
    <option value="1" @if($adv->advplace_id == 1) selected @endif>Рекламный банер (большой) на главной странице</option>
    <option value="2" @if($adv->advplace_id == 2) selected @endif>Рекламный банер (маленький) на главной странице</option>
    <option value="3" @if($adv->advplace_id == 3) selected @endif>Размещение товара в разделе "Новинки" на главной странице</option>
	</select>
    <p>Заявлен срок действия до: {{ $adv->term }}</p>
	<label for="">Действителен до</label>
	<input type="date" class="form-control" name="term" placeholder="" value="{{ $adv->term }}" required>
	<label for="banner">Баннер</label>
    <input type="file" class="form-control" name="banner">

    @if(isset($adv->banner))
    <div class="adv-banner">
        <div style="margin: 10px;">
            <img src="{{ $adv->banner }}" alt="" style="height: 100px; width: 100px;">
        </div>
        <div>
            <button type="button" id="del-img" onclick="deleteAdvBanner({{ $adv->id }})">Удалить изображение</button>
        </div>
    </div>
    @endif

    <label for="">Ссылка на баннере</label>
	<input type="text" class="form-control" name="href" placeholder="http://" value="{{ $adv->href ?? '#' }}" required>
<hr>
<input type="submit" name="send" class="btn btn-primary" value="Сохранить">
<input type="submit" name="send" class="btn btn-primary" value="Активировать">
	</form>
</div>
@endsection