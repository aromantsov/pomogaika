@extends('admin.layouts.app')

@section('content')
<div class="container">
	@component('admin.components.breadcrumb')
	@slot('title') Список заявок на показ рекламных баннеров @endslot
	@slot('parent') Главная страница @endslot
	@slot('active') Заявки на показ рекламных баннеров @endslot
	@endcomponent
	<hr>
	@if($errors->any())
    <div class="alert alert-danger">
	    <ul>
		    @foreach($errors->all() as $error)
		    <li>{{ $error }}</li>
		    @endforeach
	    </ul>
    </div>
    @endif
    @if (Session::has('success'))
        <div class="alert alert-success">{{ Session::get('success') }}</div>
    @endif
	<!-- <a href="" class="btn btn-primary pull-right"><i class="fa fa-plus-square-o"> Создать пользователя</i></a> -->
	<table class="table table-striped">
		<thead>
			<th>Имя пользователя</th>
			<th>Место для баннера</th>
			<th class="text-right">Действия</th>
		</thead>
		<tbody>
			@forelse($advs as $order)
            <tr>
            	<td>{{ $order->users->name }}</td>
            	<td>{{ $order->advplace_id }}</td> 
            	<td class="text-right">
            		<form action="{{ route('admin.user_management.adv.destroy', $order->id) }}" onsubmit="if(confirm('Delete?')){return true}else{return false}" method="post">
            			<input type="hidden" name="_method" value="DELETE"> 
            			{{ csrf_field() }}
            			<a href="{{ route('admin.user_management.adv.edit', $order->id) }}" class="btn btn-default"><i class="fa fa-edit"></i></a>
            			<button type="submit" class="btn"><i class="fa fa-trash"></i></button>
            		</form></td>
            </tr>
			@empty
            <tr>
            	<td colspan="3" class="text-center"><h2>Нет заявок</h2></td>
            </tr>
			@endforelse
			<tfoot>
				<tr>
					<td colspam="3">
						<ul class="pagination pull-right">
							{{ $advs->links() }} 
						</ul>
					</td>
				</tr>
			</tfoot>
		</tbody>
	</table>
</div>
@endsection