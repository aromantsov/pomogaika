@extends('admin.layouts.app')

@section('content')
<div class="container">
	@component('admin.components.breadcrumb')
	@slot('title') Редактирование роли пользователей @endslot
	@slot('parent') Панель управления @endslot
	@slot('active') Роли пользователей @endslot
	@endcomponent
	@if(isset($error_busy))
	<div style="color: red; font-size: 18px;">Эта роль присвоена пользователям. Удалить ее нельзя</div>
	@endif
	<hr>
	<form action="{{ route('admin.user_management.role.update', $userrole->id) }}" method="post" class="form-horizontal">
		<input type="hidden" name="_method" value="put">
		{{ csrf_field() }}
    @include('admin.user_management.userroles.partials.form')
	</form>
</div>
@endsection