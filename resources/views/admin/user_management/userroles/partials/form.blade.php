@if($errors->any())
<div class="alert alert-danger">
	<ul>
		@foreach($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
@endif
<label for="">Название роли пользователя</label>
<input type="text" class="form-control" name="name" placeholder="Название роли пользователя" value="@if(old('name')){{ old('name') }}@else{{ $userrole->name ?? '' }}@endif" required>
<hr>
<input type="submit" name="some_name" class="btn btn-primary" value="Сохранить">