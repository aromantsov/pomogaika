@extends('admin.layouts.app')

@section('content')
<div class="container">
	@component('admin.components.breadcrumb')
	@slot('title') Список ролей польователей @endslot
	@slot('parent') Главная страница @endslot
	@slot('active') Роли пользователей @endslot
	@endcomponent
	<hr>
	<a href="{{ route('admin.user_management.role.create') }}" class="btn btn-primary pull-right"><i class="fa fa-plus-square-o"> Создать роль пользователей</i></a>
	<table class="table table-striped">
		<thead>
			<th>Название</th>
			<th class="text-right">Action</th>
		</thead>
		<tbody>
			@forelse($roles as $userrole)
            <tr>
            	<td>{{ $userrole->name }}</td>
            	<td class="text-right">
            		<form action="{{ route('admin.user_management.role.destroy', $userrole->id) }}" onsubmit="if(confirm('Delete?')){return true}else{return false}" method="post">
            			<input type="hidden" name="_method" value="DELETE"> 
            			{{ csrf_field() }}
            			<a href="{{ route('admin.user_management.role.edit', $userrole->id) }}" class="btn btn-default"><i class="fa fa-edit"></i></a>
            			<button type="submit" class="btn" @if($userrole->id < 4) disabled @endif><i class="fa fa-trash"></i></button>
            		</form></td>
            </tr>
			@empty
            <tr>
            	<td colspan="3" class="text-center"><h2>Нет ролей</h2></td>
            </tr>
			@endforelse
			<tfoot>
				<tr>
					<td colspam="3">
						<ul class="pagination pull-right">
							{{ $roles->links() }} 
						</ul>
					</td>
				</tr>
			</tfoot>
		</tbody>
	</table>
</div>
@endsection