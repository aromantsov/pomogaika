@extends('admin.layouts.app')

@section('content')
<div class="container">
	@component('admin.components.breadcrumb')
	@slot('title') Создание пользователя @endslot
	@slot('parent') Панель управления @endslot
	@slot('active') Пользователи @endslot
	@endcomponent
	<hr>
	<form action="{{ route('admin.user_management.user.store') }}" method="post" class="form-horizontal">
		{{ csrf_field() }}
		@include('admin.user_management.users.partials.form')
	</form>
</div>
@endsection