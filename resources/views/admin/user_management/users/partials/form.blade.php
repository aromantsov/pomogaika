@if($errors->any())
<div class="alert alert-danger">
	<ul>
		@foreach($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
@endif
<label for="">Имя пользователя</label>
<input type="text" class="form-control" name="name" placeholder="Имя пользователя" value="@if(old('name')){{ old('name') }}@else{{ $user->name ?? '' }}@endif" required>
<label for="">Email</label>
<input type="email" class="form-control" name="email" placeholder="Email" value="@if(old('email')){{ old('email') }}@else{{ $user->email ?? '' }}@endif" required>
<label for="">Пароль</label>
<input type="password" class="form-control" name="password" placeholder="Пароль">
<label for="">Подтверждение пароля</label>
<input type="password" class="form-control" name="password_confirmation" placeholder="Подтверждение пароля">
<label for="">Статус пользователя</label>
<select name="user_role_id" id="user_role_id" class="form-control">
	<option value="0" disabled>--Статус пользователя--</option>
	@foreach($roles as $user_role)
    <option value="{{ $user_role->id }}" @if(isset($user) && (is_object($user)) && ($user_role->id == $user->user_role_id)) selected @endif>{{ $user_role->name }}</option>
	@endforeach
</select>
<label for="">Количество бонусов, грн</label>
<input type="text" class="form-control" name="bonus" placeholder="Количество бонусов, грн" value="{{ $user->bonus ?? '' }}">
<hr>
<input type="submit" name="some_name" class="btn btn-primary" value="Сохранить">