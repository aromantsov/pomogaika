<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script src="{{ asset('https://cdn.ckeditor.com/4.13.0/standard/ckeditor.js') }}"></script>
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/modal.js') }}" defer></script>
    <script src="/js/jquery.maskedinput.min.js"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/modal.css') }}" rel="stylesheet">
     <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">@lang('welcome.login')</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">@lang('welcome.register')</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        @lang('auth.logout')
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
    <script>
    function deleteImage(product_id){
        var url = '/admin/product/deleteimage';
        $.ajax({
            url: url,
            method: 'post',
            data:{
                'product_id': product_id,
                '_token': '{{ csrf_token() }}',
            },
            success: function(){
                $('.product-image').remove();
            }
        });
    }

    function deleteIcon(category_id){
        var url = '/admin/category/deleteicon';
        $.ajax({
            url: url,
            method: 'post',
            data:{
                'category_id': category_id,
                '_token': '{{ csrf_token() }}',
            },
            success: function(){
                $('.category-icon').remove();
            }
        });
    }

    function deleteCatImage(category_id){
        var url = '/admin/category/deleteimage';
        $.ajax({
            url: url,
            method: 'post',
            data:{
                'category_id': category_id,
                '_token': '{{ csrf_token() }}',
            },
            success: function(){
                $('.category-image').remove();
            }
        });
    }

    function deletePicture(picture_id){
        var url = '/admin/product/deletepicture';
        $.ajax({
            url: url,
            method: 'post',
            data:{
                'picture_id': picture_id,
                '_token': '{{ csrf_token() }}',
            },
            success: function(res){
                if(res == 'success'){
                    $('.product-picture').remove();
                }
            }
        });
    }

    function deleteNewsPicture(news_id){
        var url = '/admin/news/deletepicture';
        $.ajax({
            url: url,
            method: 'post',
            data:{
                'news_id': news_id,
                '_token': '{{ csrf_token() }}',
            },
            success: function(){
                $('#news-picture-' + news_id).remove();
            }
        });
    }

    function deleteAdvBanner(adv_id){
        var url = '/admin/user_management/adv/deletebanner';
        $.ajax({
            url: url,
            method: 'post',
            data:{
                'adv_id': adv_id,
                '_token': '{{ csrf_token() }}',
            },
            success: function(){
                $('.adv-banner').remove();
            }
        });
    }

    function uploadData() {
        $('#import').submit();
    }
    </script>
    <script>
        $('input[type="tel"]').mask("+38 (999) 999-99-99"); 
    </script>
</body>
</html>
