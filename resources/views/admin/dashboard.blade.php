@extends('admin.layouts.app')

@php
$cat_title = 'title_' . $local;
$pr_title = 'name_' . $local;
@endphp

@section('content')
   <div class="container">
   	   <div class="row">
   	   	<div class="col-sm-3">
   	   		<div class="jumbotron">
   	   			<a href="/admin/category"><span class="label label-primary">@lang('category.count') {{ $count_categories }}</span></a>
   	   		</div>
   	   	</div>
            <div class="col-sm-3">
               <div class="jumbotron">
                  <a href="/admin/product"><span class="label label-primary">Количество товаров {{ $count_products }}</span></a>
               </div>
            </div>
            <div class="col-sm-3">
               <div class="jumbotron">
                  <a href="/admin/user_management/user"><span class="label label-primary">Управление пользователями (в т. ч. поставщиками)</span></a>
               </div>
            </div>
            <div class="col-sm-3">
               <div class="jumbotron">
                  <a href="/admin/user_management/role"><span class="label label-primary">Роли пользователей</span></a>
               </div>
            </div>
   	   </div>
         <div class="row">
            <div class="col-sm-3">
               <div class="jumbotron">
                  <a href="/admin/attribute"><span class="label label-primary">Характеристики</span></a>
               </div>
            </div>
            <div class="col-sm-3">
               <div class="jumbotron">
                  <a href="/admin/news"><span class="label label-primary">Новости</span></a>
               </div>
            </div>
            <div class="col-sm-3">
               <div class="jumbotron">
                  <a href="/admin/tag"><span class="label label-primary">Теги новостей</span></a>
               </div>
            </div>
            <div class="col-sm-3">
               <div class="jumbotron">
                  <a href="/admin/newsimage"><span class="label label-primary">Изображения в новостях</span></a>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-sm-3">
               <div class="jumbotron">
                  <a href="/admin/orders"><span class="label label-primary">Заказы</span></a>
               </div>
            </div>
            <div class="col-sm-3">
               <div class="jumbotron">
                  <a href="/admin/manufacturers"><span class="label label-primary">Производители</span></a>
               </div>
            </div>
            <div class="col-sm-3">
               <div class="jumbotron">
                  <a href="/admin/user_management/orderplan"><span class="label label-primary">Заявки на изменение тарифного плана</span></a>
               </div>
            </div>
            <div class="col-sm-3">
               <div class="jumbotron">
                  <a href="/admin/user_management/adv"><span class="label label-primary">Заявки на показ рекламных баннеров</span></a>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-sm-3">
               <div class="jumbotron">
                  <a href="/admin/info"><span class="label label-primary">Информационные страницы</span></a>
               </div>
            </div>
            <div class="col-sm-3">
               <div class="jumbotron">
                  <a href="/admin/review"><span class="label label-primary">Отзывы о товарах</span></a>
               </div>
            </div>
            <div class="col-sm-3">
               <div class="jumbotron">
                  <a href="/admin/vendorreview"><span class="label label-primary">Отзывы о продавцах</span></a>
               </div>
            </div>
            <div class="col-sm-3">
               <div class="jumbotron">
                  <a href="/admin/callback"><span class="label label-primary">Заявки на обратную связь</span></a>
               </div>
            </div>
         </div>
   	   <div class="row">
   	   	<div class="col-sm-6">
   	   		<a href="{{ route('admin.category.create') }}" class="btn btn-block btn-default">@lang('category.create')</a>
   	   		@foreach($categories as $category)
               <a href="{{ route('admin.category.edit', $category) }}" class="list-group-item">
                  <h4 class="list-group-item-heading">{{ $category->$cat_title }}</h4>
               </a>
                @endforeach
   	   	</div>
            <div class="col-sm-6">
               <a href="{{ route('admin.product.create') }}" class="btn btn-block btn-default">Создать товар</a>
               @foreach($products as $product)
               <a href="{{ route('admin.product.edit', $product) }}" class="list-group-item">
                  <h4 class="list-group-item-heading">{{ $product->$pr_title }}</h4>
               </a>
               @endforeach
            </div>
   	   </div>
   </div>
@endsection