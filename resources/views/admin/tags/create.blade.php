@extends('admin.layouts.app')

@section('content')

<div class="container">
	@component('admin.components.breadcrumb')
	@slot('title') Создать тег новостей @endslot
	@slot('parent') Главная страница @endslot
	@slot('active') Теги новостнй @endslot
	@endcomponent
	<hr>
	<form action="{{ route('admin.tag.store') }}" method="post" class="form-horizontal" enctype="multipart/form-data">
		{{ csrf_field() }}
		@include('admin.tags.partials.form')
	</form>
</div>
@endsection