<label for="">Тег (на русском языке)</label>
<input type="text" class="form-control" name="tag_ru" placeholder="Тег (на русском языке)" value="{{ $tag->tag_ru ?? '' }}" required>

<label for="">Тег (на украинском языке)</label>
<input type="text" class="form-control" name="tag_uk" placeholder="Тег (на украинском языке)" value="{{ $tag->tag_uk ?? '' }}" required> 

<hr>
<input type="submit" class="btn btn-primary" value="Сохранить">