@extends('admin.layouts.app')

@section('content')

<div class="container">
	@component('admin.components.breadcrumb')
	@slot('title') Загрузить изображение новости @endslot
	@slot('parent') Главная страница @endslot
	@slot('active') Изображения новостей @endslot
	@endcomponent
	<hr>
	<form action="{{ route('admin.newsimage.store') }}" method="post" class="form-horizontal" enctype="multipart/form-data">
		{{ csrf_field() }}
		<label for="image">Изображение</label>
        <input type="file" class="form-control" name="image">
        <hr>
        <input type="submit" class="btn btn-primary" value="Загрузить">
	</form>
</div>
@endsection