@extends('admin.layouts.app')

@section('content')
   <div class="container">
         @if(isset($error_nofile))
         <div class="row" style="color: red; font-size: 18px;">Укажите, пожалуйста, файл для загрузки</div>
         @endif
   	   <div class="row">
   	   	<form action="{{ route('admin.upload') }}" method="post" enctype="multipart/form-data" id="import" class="form-horizontal">
                     {{ csrf_field() }}
                     <table class="form">
                        <tr>
                           <td>
                              Импорт из XLS, XLSX или CSV файла
                           </td>
                        </tr>
                        <tr>
                           <td>Загружаемый файл<br /><br /><input type="file" name="file" id="upload" /></td>
                        </tr>
                        <tr>
                           <td class="buttons"><a onclick="uploadData();" class="btn btn-primary"><span>Импорт</span></a></td>
                        </tr>
                     </table>
                  </form>
   	   </div>
   </div>
@endsection