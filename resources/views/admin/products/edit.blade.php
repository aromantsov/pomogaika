@extends('admin.layouts.app')

@section('content')
<div class="container">
	@component('admin.components.breadcrumb')
	@slot('title') Редактировать товар @endslot
	@slot('parent') Главная страница @endslot
	@slot('active') Товары @endslot
	@endcomponent
	<hr>
	@if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if (Session::has('success'))
        <div class="alert alert-success">{{ Session::get('success') }}</div>
    @endif
	<form action="{{ route('admin.product.update', $product) }}" method="post" class="form-horizontal"  enctype="multipart/form-data">
		<input type="hidden" name="_method" value="put">
		{{ csrf_field() }}
		@include('admin.products.partials.form')
	</form>
	<a href="#modal1" class="btn btn-default open_modal" style="margin: 10px;">Редактирование характеристик товара</a>
	<a href="#modal2" class="btn btn-default open_modal" style="margin: 10px;">Поставщики данного товара</a>
</div>

<div id="modal1" class="modal_div">

    <span class="modal_close">X</span>
    @include('admin.products.partials.properties')

</div>

<div id="modal2" class="modal_div">

    <span class="modal_close">X</span>
    @include('admin.products.partials.offers')

</div>
<div id="overlay"></div>
@endsection