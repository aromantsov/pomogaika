@extends('admin.layouts.app')

@section('content')

<div class="container">
	@component('admin.components.breadcrumb')
	@slot('title') Создать товар @endslot
	@slot('parent') Главная страница @endslot
	@slot('active') Товары @endslot
	@endcomponent
	<hr>
	<form action="{{ route('admin.product.store') }}" method="post" class="form-horizontal" enctype="multipart/form-data">
		{{ csrf_field() }}
		@include('admin.products.partials.form')
	</form>
</div>
@endsection