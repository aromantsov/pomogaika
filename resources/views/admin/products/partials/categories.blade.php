@php
$pr_title = 'title_' . $local;
@endphp

@foreach($categories as $category)

  <option value="{{$category->id ?? ""}}"

    @isset($product['id'])
        @if ($category->id == $product['category_id'])
          selected="selected"
        @endif
    @endisset

    >
    {!! $delimiter ?? "" !!}{{$category->$pr_title ?? ""}}
  </option>

  @if (count($category->children) > 0)

    @include('admin.products.partials.categories', [
      'categories' => $category->children,
      'delimiter'  => ' - ' . $delimiter
    ])

  @endif
@endforeach