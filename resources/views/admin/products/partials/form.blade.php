<label for="">Статус</label>
<select name="published" id="" class="form-control">
@if(isset($product->id))
    <option value="0" @if($product->published == 0) selected @endif>Не опубликован</option>
    <option value="1" @if($product->published == 1) selected @endif>Опубликован</option>
@else
    <option value="0">Не опубликован</option>
    <option value="1">Опубликован</option>
@endif
</select>

<label for="">Новый</label>
<select name="is_new" id="" class="form-control">
@if(isset($product->id))
    <option value="0" @if($product->is_new == 0) selected @endif>Нет</option>
    <option value="1" @if($product->is_new == 1) selected @endif>Да</option>
@else
    <option value="0">Нет</option>
    <option value="1">Да</option>
@endif
</select>

<label for="">Распродажа</label>
<select name="is_sale" id="" class="form-control">
@if(isset($product->id))
    <option value="0" @if($product->is_sale == 0) selected @endif>Нет</option>
    <option value="1" @if($product->is_sale == 1) selected @endif>Да</option>
@else
    <option value="0">Нет</option>
    <option value="1">Да</option>
@endif
</select>

<label for="">Артикул поставщика</label>
<input type="text" class="form-control" name="model" placeholder="Артикул поставщика" value="{{ $product->model ?? '' }}" required>

<label for="">Артикул производителя</label>
<input type="text" class="form-control" name="sku" placeholder="Артикул производителя" value="{{ $product->sku ?? '' }}" required>

<!-- <label for="">Производитель (будет переделано в Select)</label>
<input type="text" class="form-control" name="manufacturer_id" placeholder="Производитель" value="{{ $product->manufacturer_id ?? '' }}" required> -->

<label for="">Производитель</label>
<select name="manufacturer_id" id="" class="form-control">
    <option value="0" disabled @if(!isset($product->manufacturer_id)) selected @endif>--Выберите производителя--</option>
    @foreach($manufacturers as $manufacturer)
    <option value="{{ $manufacturer->id }}" @if((isset($product->manufacturer_id)) && ($product->manufacturer_id == $manufacturer->id))selected @endif>{{ $manufacturer->brand }}</option>
    @endforeach
</select>

<label for="">Цена (грн.)</label>
<input type="text" class="form-control" name="price" placeholder="Цена (грн.)" value="{{ $product->price ?? 0 }}" disabled>

<label for="">Базовая цена до распродажи (грн.)</label>
<input type="text" class="form-control" name="trade_price" placeholder="Базовая цена до распродажи (грн.)" value="{{ $product->trade_price ?? 0 }}">

<label for="">Количество бонусов, которое приносит покупка товара</label>
<input type="text" class="form-control" name="bonus" placeholder="Количество бонусов, за которые можно купить товар" value="{{ $product->bonus ?? 0 }}" required>

<label for="">Основное изображение</label>
<input type="file" class="form-control" name="image">

@if(isset($product->image))
<div class="product-image">
    <div style="margin: 10px;">
        <img src="{{ $product->image }}" alt="" style="height: 100px; width: 100px;">
    </div>
    <div>
	    <button type="button" id="del-img" onclick="deleteImage({{ $product->id }})">Удалить изображение</button>
    </div>
</div>
@endif

<label for="pictures">Дополнительные изображения</label>
<input type="file" class="form-control" name="pictures[]" multiple>

@if(isset($product->pictures))
<div style="display: flex;">
@foreach($product->pictures as $picture)
<div class="product-picture" id="product-picture-{{ $picture->id }}">
    <div style="margin: 10px;">
        <img src="{{ $picture->image }}" alt="" style="height: 100px; width: 100px;">
    </div>
    <div>
        <button type="button" onclick="deletePicture({{ $picture->id }})">Удалить изображение</button>
    </div>
</div>
@endforeach
</div>
@endif

<label for="">Видео (id видео с Youtube, хвост ссылки https://youtu.be/{то, что нужно вставить})</label>
<input type="text" class="form-control" name="video" placeholder="Видео" value="{{ $product->video ?? '' }}">

@if(isset($product->video))
    <div style="margin: 10px;">
        <iframe width="560" height="315" src="https://www.youtube.com/embed/{{ $product->video }}" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
@endif

<label for="">Единица измерения</label>
<input type="text" class="form-control" name="unit" placeholder="Единица измерения" value="{{ $product->unit ?? '' }}">

<label for="">Порядок сортировки</label>
<input type="text" class="form-control" name="sort_order" placeholder="Порядок сортировки" value="{{ $product->sort_order ?? '' }}">

<label for="">Наименование товара (на русском языке)</label>
<input type="text" class="form-control" name="name_ru" placeholder="Наименование товара (на русском языке)" value="{{ $product->name_ru ?? '' }}" required>

<label for="">Наименование товара (на украинском языке)</label>
<input type="text" class="form-control" name="name_uk" placeholder="Наименование товара (на украинском языке)" value="{{ $product->name_uk ?? '' }}" required> 

<label for="">Категория товара</label>
<select name="category_id" id="" class="form-control">
	<option value="0">--Без категории--</option>
	@include('admin.products.partials.categories', ['categories' => $categories])
</select>

<label for="">Калькуляторы, в которых участвует товар (мультивыбор)</label>
<select name="calculator_id[]" id="" class="form-control" multiple>
    <option value="0">--Без калькулятора--</option>
    @foreach($calculators as $key => $value)
    <option value="{{ $key }}" @if(in_array($key, $selected_calc)) selected @endif>{{ $value }}</option>
    @endforeach
</select>

<label for="description-ru">Описание товара (на русском языке)</label>
<textarea name="description_ru" id="description-ru" cols="30" rows="10" class="form-control">{{ $product->description_ru ?? '' }}</textarea>


<label for="description-ru">Описание товара (на украинском языке)</label>
<textarea name="description_uk" id="description-uk" cols="30" rows="10" class="form-control">{{ $product->description_uk ?? '' }}</textarea>
<script type="text/javascript">

$(document).ready(function(){
    CKEDITOR.replace( 'description-ru' );
    CKEDITOR.replace( 'description-uk' );
});
 </script>

<label for="meta_title_ru">Мета название (на русском языке)</label>
<input type="text" name="meta_name_ru" id="meta_title_ru" cols="30" rows="10" class="form-control" value="{{ $product->meta_name_ru ?? '' }}">

<label for="meta_title_uk">Мета название (на украинском языке)</label>
<input type="text" name="meta_name_uk" id="meta_title_uk" cols="30" rows="10" class="form-control" value="{{ $product->meta_name_uk ?? '' }}">

<label for="meta_h1_ru">Мета тег h1 (на русском языке)</label>
<input type="text" name="meta_h1_ru" id="meta_h1_ru" cols="30" rows="10" class="form-control" value="{{ $product->meta_h1_ru ?? '' }}">

<label for="meta_h1_uk">Мета тег h1 (на украинском языке)</label>
<input type="text" name="meta_h1_uk" id="meta_h1_uk" cols="30" rows="10" class="form-control" value="{{ $product->meta_h1_uk ?? '' }}">

<label for="meta_description_ru">Мета описание (на русском языке)</label>
<input type="text" name="meta_description_ru" id="meta_description_ru" cols="30" rows="10" class="form-control" value="{{ $product->meta_description_ru ?? '' }}">

<label for="meta_description_uk">Мета описание (на украинском языке)</label>
<input type="text" name="meta_description_uk" id="meta_description_uk" cols="30" rows="10" class="form-control" value="{{ $product->meta_description_uk ?? '' }}">

<label for="keywords-ru">Ключевые слова (на русском языке)</label>
<input type="text" name="meta_keywords_ru" id="keywords-ru" cols="30" rows="10" class="form-control" value="{{ $product->meta_keywords_ru ?? '' }}">

<label for="keywords-uk">Ключевые слова (на украинском языке)</label>
<input type="text" name="meta_keywords_uk" id="keywords-uk" cols="30" rows="10" class="form-control" value="{{ $product->meta_keywords_uk ?? '' }}">
<hr>
<input type="submit" class="btn btn-primary">


  