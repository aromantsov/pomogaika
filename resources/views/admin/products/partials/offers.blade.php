              <div class="tab-pane" id="tab-vendor">
              <div class="table-responsive">
              	<form action="{{ route('admin.product.offer') }}" method="post" id="offer-form">
              		{{ csrf_field() }}
              		<!-- <input type="hidden" name="_method" value="put"> -->
              		<input type="hidden" name="product_id" value="{{ $product->id }}"> 
                <table id="vendor" class="table table-striped table-bordered table-hover">
                  <thead>
                    <tr>
                      <td class="text-left">Поставщик</td>
                      <td class="text-left">Цена</td>
                      <td class="text-right">Действие</td>
                    </tr>
                  </thead>
                  <tbody>
                  @php
                  $vendor_row = 0; 
                  @endphp
                  @foreach($offers as $offer)
                  <tr id="vendor-row{{ $vendor_row }}">
                    <td class="text-left" style="width: 40%;">
                        <select name="offer[{{ $vendor_row }}][name]" id="">
                          <option value="0"></option>
                          @foreach($vendors as $vendor)
                          <option value="{{ $vendor->id }}" @if($vendor->id == $offer->user_id) selected @endif>{{ $vendor->name }}</option>
                          @endforeach
                        </select>
                    </td>
                      <!-- <td class="text-left" style="width: 40%;"><div class="input-group"><span class="input-group-addon"><img src="/img/main/rus.png" title="ru" /></span> -->
                        <td><textarea name="offer[{{ $vendor_row }}][price]" rows="5" placeholder="Введите значение" class="form-control">{{ $offer->price }}</textarea></td>
                    <td class="text-right"><button type="button" onclick="$('#vendor-row{{ $vendor_row }}').remove();" data-toggle="tooltip" title="Удалить" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
                  </tr>
                  @php
                  $vendor_row++;
                  @endphp
                  @endforeach
                    </tbody>
                  
                  <tfoot>
                    <tr>
                      <td colspan="2"></td>
                      <td class="text-right"><button type="button" onclick="addVendor();" data-toggle="tooltip" title="" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                    </tr>
                  </tfoot>
                </table>
                </form>
                <button type="submit" class="btn btn-default" onclick="$('#offer-form').submit();">Сохранить</button>
              </div>
            </div>
            <script>
              var vendor_row = {{ $vendor_row }}; 

    function addVendor() {
      var url = '/admin/product/autocomplete2';
      $.ajax({
         url: url,
         method: 'post',
         data:{
                '_token': '{{ csrf_token() }}',
            },   
         success: function(res){
          var attrs = JSON.parse(res);
          console.log(attrs);
         
        var html  = '<tr id="vendor-row' + vendor_row + '">';
        html += '  <td class="text-left" style="width: 20%;"><select name="offer[' + vendor_row + '][name]">vendor_row';
        html += '<option value="0"></option>';
        for(var attr in attrs){
        	html += '<option value="' + attrs[attr]['id'] + '">' + attrs[attr]['name'] + '</option>';
        }
        html += '</select></td>';
        html += '  <td class="text-left">';
        html += '<div class="input-group"><textarea name="offer[' + vendor_row + '][price]" rows="5" placeholder="" class="form-control"></textarea></div>';
        html += '  </td>';
        html += '  <td class="text-right"><button type="button" onclick="$(\'#vendor-row' + vendor_row + '\').remove();" data-toggle="tooltip" title="" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
        html += '</tr>';

        $('#vendor tbody').append(html);

        vendor_row++;
    }
    });
  }

  function saveOffers(){
  	var url = '/admin/product/offer';
  	var overlay = $('#overlay');
    var modal = $('.modal_div');
  	$.ajax({
  		url: url,
  		method: 'post',
  		data: $('#offer-form').serialize(),
  		success: function(res){
  			console.log(res);
            modal 
            .animate({opacity: 0, top: '45%'}, 200, 
            function(){ 
                $(this).css('display', 'none');
                overlay.fadeOut(400);
  		    });
  	    }        
    });
    return false;
  }
    </script>