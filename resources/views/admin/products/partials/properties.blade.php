              <div class="tab-pane" id="tab-attribute">
              <div class="table-responsive">
              	<form action="{{ route('admin.property.saveproperty') }}" method="post" id="property-form">
              		{{ csrf_field() }}
              		<input type="hidden" name="_method" value="put">
              		<input type="hidden" name="product_id" value="{{ $product->id }}"> 
                <table id="attribute" class="table table-striped table-bordered table-hover">
                  <thead>
                    <tr>
                      <td class="text-left">Характеристика</td>
                      <td class="text-left">Свойство характеристики</td>
                      <td class="text-right">Действие</td>
                    </tr>
                  </thead>
                  <tbody>
                  @php
                  $attribute_row = 0;
                  $attr_name = 'name_' . $local; 
                  @endphp
                  @foreach($properties as $property)
                  <tr id="attribute-row{{ $attribute_row }}">
                    <td class="text-left" style="width: 40%;">
                        <select name="property[{{ $attribute_row }}][name]" id="">
                          <option value="0"></option>
                          @foreach($attributes as $attribute)
                          <option value="{{ $attribute->id }}" @if($attribute->id == $property->attribute_id) selected @endif>{{ $attribute->$attr_name }}</option>
                          @endforeach
                        </select>
                      </td>
                      <td class="text-left" style="width: 40%;"><div class="input-group"><span class="input-group-addon"><img src="/img/main/rus.png" title="ru" /></span>
                        <textarea name="property[{{ $attribute_row }}][value_ru]" rows="5" placeholder="Введите значение" class="form-control">{{ $property->value_ru }}</textarea>
                        <div class="input-group"><span class="input-group-addon"><img src="/img/main/ukr.png" title="ukr" /></span>
                        <textarea name="property[{{ $attribute_row }}][value_uk]" rows="5" placeholder="Введите значение" class="form-control">{{ $property->value_uk }}</textarea>
                      </div></td>
                    <td class="text-right"><button type="button" onclick="$('#attribute-row{{ $attribute_row }}').remove();" data-toggle="tooltip" title="Удалить" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
                  </tr>
                  @php
                  $attribute_row++;
                  @endphp
                  @endforeach
                    </tbody>
                  
                  <tfoot>
                    <tr>
                      <td colspan="2"></td>
                      <td class="text-right"><button type="button" onclick="addAttribute();" data-toggle="tooltip" title="" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                    </tr>
                  </tfoot>
                </table>
                </form>
                <button type="submit" class="btn btn-default" onclick="$('#property-form').submit();">Сохранить</button>
              </div>
            </div>
            <script>
              var attribute_row = {{ $attribute_row }}; 

    function addAttribute() {
      var url = '/admin/product/autocomplete';
      $.ajax({
         url: url,
         method: 'post',
         data:{
                '_token': '{{ csrf_token() }}',
            },   
         success: function(res){
          var attrs = JSON.parse(res);
         
        var html  = '<tr id="attribute-row' + attribute_row + '">';
        html += '  <td class="text-left" style="width: 20%;"><select name="property[' + attribute_row + '][name]">attribute_row';
        html += '<option value="0"></option>';
        for(var attr in attrs){
        	html += '<option value="' + attrs[attr]['id'] + '">' + attrs[attr]['name_{{ $local }}'] + '</option>';
        }
        html += '</select></td>';
        html += '  <td class="text-left">';
        html += '<div class="input-group"><span class="input-group-addon"><img src="/img/main/rus.png" title="ru" /></span><textarea name="property[' + attribute_row + '][value_ru]" rows="5" placeholder="" class="form-control"></textarea></div>';
        html += '<div class="input-group"><span class="input-group-addon"><img src="/img/main/ukr.png" title="uk" /></span><textarea name="property[' + attribute_row + '][value_uk]" rows="5" placeholder="" class="form-control"></textarea></div>';
        html += '  </td>';
        html += '  <td class="text-right"><button type="button" onclick="$(\'#attribute-row' + attribute_row + '\').remove();" data-toggle="tooltip" title="" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
        html += '</tr>';

        $('#attribute tbody').append(html);

        attribute_row++;
    }
    });
  }

  function saveProperties(){
  	var url = '/admin/property/saveproperty';
  	var overlay = $('#overlay');
    var modal = $('.modal_div');
  	$.ajax({
  		url: url,
  		method: 'post',
  		data: $('#property-form').serialize(),
  		success: function(res){
  			console.log(res);
            modal 
            .animate({opacity: 0, top: '45%'}, 200, 
            function(){ 
                $(this).css('display', 'none');
                overlay.fadeOut(400);
  		    });
  	    }        
    });
    return false;
  }
    </script>