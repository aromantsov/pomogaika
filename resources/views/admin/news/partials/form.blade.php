@php
$pr_title = 'name_' . $local;
$tag_title = 'tag_' . $local;
@endphp
<label for="">Статус</label>
<select name="published" id="" class="form-control">
@if(isset($news->id))
    <option value="0" @if($news->published == 0) selected @endif>Не опубликован</option>
    <option value="1" @if($news->published == 1) selected @endif>Опубликован</option>
@else
    <option value="0">Не опубликован</option>
    <option value="1">Опубликован</option>
@endif
</select>

<label for="picture">Главное изображение</label>
<input type="file" class="form-control" name="picture">

@if(isset($news->picture))
<div class="news-picture">
    <div style="margin: 10px;">
        <img src="{{ $news->picture }}" alt="" style="height: 100px; width: 100px;">
    </div>
    <div>
        <button type="button" id="del-img" onclick="deleteNewsPicture({{ $news->id }})">Удалить изображение</button>
    </div>
</div>
@endif

<label for="">Наименование новости (на русском языке)</label>
<input type="text" class="form-control" name="name_ru" placeholder="Наименование новости (на русском языке)" value="{{ $news->name_ru ?? '' }}" required>

<label for="">Наименование новости (на украинском языке)</label>
<input type="text" class="form-control" name="name_uk" placeholder="Наименование новости (на украинском языке)" value="{{ $news->name_uk ?? '' }}" required> 

<!-- <label for="">Категория товара</label>
<select name="category_id" id="" class="form-control">
	<option value="0">--Без категории--</option>
</select> -->

<label for="">Теги</label>
@forelse($tags as $tag)
<div><input type="checkbox" name="tags[]" value="{{ $tag->id }}" id="tag-{{ $tag->id }}" @if(in_array($tag->id, $taglist)) checked @endif)>
<label for="tag-{{ $tag->id }}">{{ $tag->$tag_title }}</label></div>
@empty
<p>нет тегов</p>
@endforelse

<label for="">Связанные статьи</label>
@forelse($similars as $similar)
<div><input type="checkbox" name="similar[]" value="{{ $similar->id }}" id="similar-{{ $similar->id }}" @if(isset($news->id)) @if($news->id == $similar->id) disabled @endif @endif @if(in_array($similar->id, $simlist))checked @endif>
<label for="similar-{{ $similar->id }}">{{ $similar->$pr_title }}</label></div>
@empty
<p>нет статей</p>
@endforelse

<label for="description-ru">Текст новости (на русском языке)</label>
<textarea name="description_ru" id="description-ru" cols="30" rows="10" class="form-control">{{ $news->description_ru ?? '' }}</textarea>


<label for="description-ru">Текст новости (на украинском языке)</label>
<textarea name="description_uk" id="description-uk" cols="30" rows="10" class="form-control">{{ $news->description_uk ?? '' }}</textarea>
<script type="text/javascript">

$(document).ready(function(){
    CKEDITOR.replace( 'description-ru' );
    CKEDITOR.replace( 'description-uk' );
});
 </script>

<label for="meta_title_ru">Мета название (на русском языке)</label>
<input type="text" name="meta_name_ru" id="meta_title_ru" cols="30" rows="10" class="form-control" value="{{ $news->meta_name_ru ?? '' }}">

<label for="meta_title_uk">Мета название (на украинском языке)</label>
<input type="text" name="meta_name_uk" id="meta_title_uk" cols="30" rows="10" class="form-control" value="{{ $news->meta_name_uk ?? '' }}">

<label for="meta_h1_ru">Мета тег h1 (на русском языке)</label>
<input type="text" name="meta_h1_ru" id="meta_h1_ru" cols="30" rows="10" class="form-control" value="{{ $news->meta_h1_ru ?? '' }}">

<label for="meta_h1_uk">Мета тег h1 (на украинском языке)</label>
<input type="text" name="meta_h1_uk" id="meta_h1_uk" cols="30" rows="10" class="form-control" value="{{ $news->meta_h1_uk ?? '' }}">

<label for="meta_description_ru">Мета описание (на русском языке)</label>
<input type="text" name="meta_description_ru" id="meta_description_ru" cols="30" rows="10" class="form-control" value="{{ $news->meta_description_ru ?? '' }}">

<label for="meta_description_uk">Мета описание (на украинском языке)</label>
<input type="text" name="meta_description_uk" id="meta_description_uk" cols="30" rows="10" class="form-control" value="{{ $news->meta_description_uk ?? '' }}">

<label for="keywords-ru">Ключевые слова (на русском языке)</label>
<input type="text" name="meta_keywords_ru" id="keywords-ru" cols="30" rows="10" class="form-control" value="{{ $news->meta_keywords_ru ?? '' }}">

<label for="keywords-uk">Ключевые слова (на украинском языке)</label>
<input type="text" name="meta_keywords_uk" id="keywords-uk" cols="30" rows="10" class="form-control" value="{{ $news->meta_keywords_uk ?? '' }}">
<hr>
<input type="submit" class="btn btn-primary" value="Сохранить">