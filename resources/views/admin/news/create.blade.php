@extends('admin.layouts.app')

@section('content')

<div class="container">
	@component('admin.components.breadcrumb')
	@slot('title') Создать новость @endslot
	@slot('parent') Главная страница @endslot
	@slot('active') Новости @endslot
	@endcomponent
	<hr>
	<form action="{{ route('admin.news.store') }}" method="post" class="form-horizontal" enctype="multipart/form-data">
		{{ csrf_field() }}
		@include('admin.news.partials.form')
	</form>
</div>
@endsection