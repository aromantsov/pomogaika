@extends('admin.layouts.app')

@section('content')
<div class="container">
	@component('admin.components.breadcrumb')
	@slot('title') Редактировать новость @endslot
	@slot('parent') Главная страница @endslot
	@slot('active') Новости @endslot
	@endcomponent
	<hr>
	@if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if (Session::has('success'))
        <div class="alert alert-success">{{ Session::get('success') }}</div>
    @endif
	<form action="{{ route('admin.news.update', $news) }}" method="post" class="form-horizontal"  enctype="multipart/form-data">
		<input type="hidden" name="_method" value="put">
		{{ csrf_field() }}
		@include('admin.news.partials.form')
	</form>
</div>

@endsection
