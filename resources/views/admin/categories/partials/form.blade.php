<label for="">Статус</label>
<select name="published" id="" class="form-control">
@if(isset($category->id))
<option value="0" @if($category->published == 0) selected="" @endif>Выключена</option>
<option value="1" @if($category->published == 1) selected="" @endif>Включена</option>
@else
<option value="0">Выключена</option>
<option value="1">Включена</option>
@endif
</select>
<label for="">Название на русском языке</label>
<input type="text" class="form-control" name="title_ru" placeholder="Название на русском языке" value="{{ $category->title_ru ?? '' }}" required>
<label for="">Название на украинском языке</label>
<input type="text" class="form-control" name="title_uk" placeholder="Название на украинском языке" value="{{ $category->title_uk ?? '' }}" required>
<label for="">Родительская категория</label>
<select name="parent_id" id="" class="form-control">
	<option value="0">--Без родительской категории--</option>
	@include('admin.categories.partials.categories', ['categories'=>$categories])
</select>
<label for="">Иконка</label>
<input type="file" class="form-control" name="icon">

@if(isset($category->icon))
<div class="category-icon">
    <div style="margin: 10px;">
        <img src="{{ $category->icon }}" alt="" style="height: 100px; width: 100px;">
    </div>
    <div>
	    <button type="button" id="del-icon" onclick="deleteIcon({{ $category->id }})">Удалить изображение</button>
    </div>
</div>
@endif

<label for="">Изображение</label>
<input type="file" class="form-control" name="image">

@if(isset($category->image))
<div class="category-image">
    <div style="margin: 10px;">
        <img src="{{ $category->image }}" alt="" style="height: 100px; width: 100px;">
    </div>
    <div>
	    <button type="button" id="del-img" onclick="deleteCatImage({{ $category->id }})">Удалить изображение</button>
    </div>
</div>
@endif
<hr/>
<input type="submit" class="btn btn-primary">    