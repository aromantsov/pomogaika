@extends('admin.layouts.app')

@php
$cat_title = 'title_' . $local;
@endphp

@section('content')

<div class="container">
	@component('admin.components.breadcrumb')
	@slot('title') @lang('category.list') @endslot
	@slot('parent') @lang('category.main_page') @endslot
	@slot('active') @lang('category.categories') @endslot
	@endcomponent
	<hr/>
	@if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if (Session::has('success'))
        <div class="alert alert-success">{{ Session::get('success') }}</div>
    @endif
	<a href="{{ route('admin.category.create') }}" class="btn btn-primary pull-right"><i class="fa fa-plus-square-o"> @lang('category.create')</i></a>
	<table class="table table-striped">
		<thead>
			<th>@lang('category.name')</th>
			<th>@lang('category.status')</th>
			<th class="text-right">@lang('category.action')</th>
		</thead>
		<tbody>
			@forelse($categories as $category)
			<tr>
				<td>{{ $category->$cat_title }}</td>
				<td>{{ $category->published }}</td>
				<td class="text-right">
				    <form action="{{ route('admin.category.destroy', $category) }}" onsubmit="if(confirm('Delete?')){return true}else{return false}" method="post">
            			<input type="hidden" name="_method" value="DELETE"> 
            			{{ csrf_field() }}
            			<a href="{{ route('admin.category.edit', $category) }}" class="btn btn-default"><i class="fa fa-edit"></i></a>
            			<button type="submit" class="btn"><i class="fa fa-trash"></i></button>
            		</form></td>
			</tr>
			@empty
			<tr>
				<td colspan="3" class="text-center">@lang('category.no_categories')</td>
			</tr>
			@endforelse
		</tbody>
		<tfoot>
			<tr>
				<td colspan="3">
					<ul class="pagination full-right">
						{{ $categories->links() }}
					</ul>
				</td>
			</tr>
		</tfoot>
	</table>
</div>

@endsection