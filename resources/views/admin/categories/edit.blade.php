@extends('admin.layouts.app')

@section('content')

<div class="container">
	@component('admin.components.breadcrumb')
	@slot('title') Редактировать категорию @endslot
	@slot('parent') Главная страница @endslot
	@slot('active') Категории @endslot
	@endcomponent
	<hr/>
	@if(isset($error_busy))
	<div style="color: red; font-size: 18px;">У этой категории есть товары. Поэтому она не может быть удалена</div>
	@endif
	<form action="{{ route('admin.category.update', $category) }}" method="post" class="form-horizontal" enctype="multipart/form-data">
		<input type="hidden" name="_method" value="put">
		{{ csrf_field() }}
		@include('admin.categories.partials.form')
	</form>
</div>

@endsection