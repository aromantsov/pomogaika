@extends('admin.layouts.app')

@section('content')

<div class="container">
	@component('admin.components.breadcrumb')
	@slot('title') Создать категорию @endslot
	@slot('parent') Главная страница @endslot
	@slot('active') Категории @endslot
	@endcomponent
	<hr/>
	<form action="{{ route('admin.category.store') }}" method="post" class="form-horizontal" enctype="multipart/form-data">
		{{ csrf_field() }}
		@include('admin.categories.partials.form')
	</form>
</div>

@endsection