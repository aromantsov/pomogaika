@extends('admin.layouts.app')

@section('content')

<div class="container">
	@component('admin.components.breadcrumb')
	@slot('title') Создать производителя @endslot
	@slot('parent') Главная страница @endslot
	@slot('active') Производители @endslot
	@endcomponent
	<hr>
	<form action="{{ route('admin.manufacturers.store') }}" method="post" class="form-horizontal">
		{{ csrf_field() }}
		@include('admin.manufacturers.partials.form')
	</form>
</div>
@endsection