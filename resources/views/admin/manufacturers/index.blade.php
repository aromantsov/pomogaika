
@extends('admin.layouts.app')

@section('content')

<div class="container">
	@component('admin.components.breadcrumb')
	@slot('title') Список производителей @endslot
	@slot('parent') Главная страница @endslot
	@slot('active') Производители @endslot
	@endcomponent
	<hr/>
	@if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if (Session::has('success'))
        <div class="alert alert-success">{{ Session::get('success') }}</div>
    @endif
    @if (Session::has('error'))
        <div class="alert alert-danger">{{ Session::get('error') }}</div>
    @endif
	<a href="{{ route('admin.manufacturers.create') }}" class="btn btn-primary pull-right"><i class="fa fa-plus-square-o"> Создать производителя</i></a>
	<table class="table table-striped">
		<thead>
			<th>Название</th>
			<th class="text-right">Действия</th>
		</thead>
		<tbody>
			@forelse($manufacturers as $manufacturer)
			<tr>
				<td>{{ $manufacturer->brand}}</td>
				<td class="text-right">
				    <form action="{{ route('admin.manufacturers.destroy', $manufacturer) }}" onsubmit="if(confirm('Delete?')){return true}else{return false}" method="post">
            			<input type="hidden" name="_method" value="DELETE"> 
            			{{ csrf_field() }}
            			<a href="{{ route('admin.manufacturers.edit', $manufacturer) }}" class="btn btn-default"><i class="fa fa-edit"></i></a>
            			<button type="submit" class="btn"><i class="fa fa-trash"></i></button>
            		</form></td>
			</tr>
			@empty
			<tr>
				<td colspan="3" class="text-center">Нет производителей</td>
			</tr>
			@endforelse
		</tbody>
		<tfoot>
			<tr>
				<td colspan="3">
					<ul class="pagination full-right">
						{{ $manufacturers->links() }}
					</ul>
				</td>
			</tr>
		</tfoot>
	</table>
</div>

@endsection