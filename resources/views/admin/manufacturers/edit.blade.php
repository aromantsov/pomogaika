@extends('admin.layouts.app')

@section('content')
<div class="container">
	@component('admin.components.breadcrumb')
	@slot('title') Редактировать производителя @endslot
	@slot('parent') Главная страница @endslot
	@slot('active') Производитель @endslot
	@endcomponent
	<hr>
	@if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if (Session::has('success'))
        <div class="alert alert-success">{{ Session::get('success') }}</div>
    @endif
	<form action="{{ route('admin.manufacturers.update', $manufacturer) }}" method="post" class="form-horizontal"  enctype="multipart/form-data">
		<input type="hidden" name="_method" value="put">
		{{ csrf_field() }}
		@include('admin.manufacturers.partials.form')
	</form>

</div>

@endsection