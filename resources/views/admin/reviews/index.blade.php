@extends('admin.layouts.app')

@section('content')

<div class="container">
	@component('admin.components.breadcrumb')
	@slot('title') Список отзывов о товаре @endslot
	@slot('parent') Главная страница @endslot
	@slot('active') Отзывы о товаре @endslot
	@endcomponent
	<hr/>
	@if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if (Session::has('success'))
        <div class="alert alert-success">{{ Session::get('success') }}</div>
    @endif
	<table class="table table-striped">
		<thead>
			<th>Товар</th>
			<th>Пользователь</th>
			<th class="text-right">Действия</th>
		</thead>
		<tbody>
			@forelse($reviews as $review)
			<tr>
				<td>{{ $review->products->name_ru }}</td>
				<td>{{ $review->users->name . ' ' . $review->users->lastname }}</td>
				<td class="text-right">
				    <form action="{{ route('admin.review.destroy', $review) }}" onsubmit="if(confirm('Delete?')){return true}else{return false}" method="post">
            			<input type="hidden" name="_method" value="DELETE"> 
            			{{ csrf_field() }}
            			<a href="{{ route('admin.review.edit', $review) }}" class="btn btn-default"><i class="fa fa-edit"></i></a>
            			<button type="submit" class="btn"><i class="fa fa-trash"></i></button>
            		</form></td>
			</tr>
			@empty
			<tr>
				<td colspan="3" class="text-center">Нет отзывов</td>
			</tr>
			@endforelse
		</tbody>
		<tfoot>
			<tr>
				<td colspan="3">
					<ul class="pagination full-right">
						{{ $reviews->links() }}
					</ul>
				</td>
			</tr>
		</tfoot>
	</table>
</div>

@endsection