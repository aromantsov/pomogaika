@extends('admin.layouts.app')

@section('content')
<div class="container">
	@component('admin.components.breadcrumb')
	@slot('title') Редактировать отзыв о товаре @endslot
	@slot('parent') Главная страница @endslot
	@slot('active') Отзывы о товаре @endslot
	@endcomponent
	<hr>
	@if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if (Session::has('success'))
        <div class="alert alert-success">{{ Session::get('success') }}</div>
    @endif
	<form action="{{ route('admin.review.update', $review) }}" method="post" class="form-horizontal">
		<input type="hidden" name="_method" value="put">
		{{ csrf_field() }}
        

<label for="">Товар</label>
<input type="text" class="form-control" value="{{ $review->products->name_ru ?? '' }}" disabled>

<label for="">Пользователь</label>
<input type="text" class="form-control" value="{{ $review->users->name . ' ' . $review->users->lastname ?? '' }}" disabled>

<label for="">Рейтинг</label>
<input type="text" class="form-control" value="{{ $review->rating ?? '' }}" disabled> 

<label for="">Преимущества</label>
<input type="text" class="form-control" name="positives" placeholder="Преимущества" value="{{ $review->positives ?? '' }}"> 

<label for="">Недостатки</label>
<input type="text" class="form-control" name="negatives" placeholder="Недостатки" value="{{ $review->negatives ?? '' }}"> 

<label for="description-ru">Отзыв</label>
<textarea name="text" id="review" cols="30" rows="10" class="form-control">{{ $review->text ?? '' }}</textarea>
<hr>
<input type="submit" class="btn btn-primary" value="Сохранить">
	</form>
</div>

@endsection
