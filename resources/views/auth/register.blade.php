@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">@lang('welcome.register')</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">@lang('app.phone')</label>

                            <div class="col-md-6">
                                <input type="tel" name="phone" class="form-control @error('phone') is-invalid @enderror" value="{{ old('phone') }}" required autocomplete="phone" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">E-mail</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">@lang('auth.password')</label>

                            <div class="col-md-6 pass-form">
                                <input type="password" name="password" class="form-control pass-input @if(isset($errors)) @error('password') is-invalid @enderror @endif" name="password" required autocomplete="new-password" id="password">
                                <button type="button" class="btn" onclick="visiblePassword();"><i id="pass-btn2" class="fa fa-eye-slash"></i></button>

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">@lang('auth.confirm')</label>

                            <div class="col-md-6 pass-form">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                <button type="button" class="btn" onclick="visiblePassword();"><i id="pass-btn2" class="fa fa-eye-slash"></i></button>

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                        </div>

                        <div class="form-group row">
                            <p class="col-md-4 col-form-label text-md-right">Тип аккаунта </p>
                            <div class="line_checkbox_inputs col-md-6">
                                <label>
                                    <input type="radio" name="user_role_id" value="4" checked>
                                    <span></span>
                                    Покупатель
                                </label>
                                <label>
                                    <input type="radio" name="user_role_id" value="3">
                                    <span></span>
                                    Поставщик
                                </label>
                            </div>
                        </div>

                        <div class="required_checkbox">
                            <label for="conditions">
                                <input type="checkbox" name="conditions" value="1" id="conditions" required>
                                <span></span>
                                <p>@lang('app.agree'). <a href="{{ route('info', 5) }}">@lang('app.conditions')</a></p>
                            </label>
                            @error('conditions')
                            <div>
                            <span>
                                <strong style="color: red;">{{ $message }}</strong>
                            </span>
                            </div>
                            @enderror
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    @lang('welcome.register')
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
