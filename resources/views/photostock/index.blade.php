@extends('layouts.app')

@section('content')

<div class="container">
	<hr/>
	@if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if (Session::has('success'))
        <div class="alert alert-success">{{ Session::get('success') }}</div>
    @endif
	<a href="{{ route('photos.create') }}" class="btn btn-primary pull-right"><i class="fa fa-plus-square-o"> Загрузить изображение</i></a>
	<a href="{{ route('home') }}?import=true" class="btn btn-primary pull-right"> Назад</a>
	<table class="table table-striped">
		<thead>
			<th>Изображение</th>
			<th>URL</th>
			<th class="text-right">Действия</th>
		</thead>
		<tbody>
			@forelse($photos as $image)
			<tr>
				<td><img src="{{ $image->image }}" alt="" class="imagestock" style="width: 50px; height: 50px;"></td>
				<td>{{ 'http://' . $_SERVER['SERVER_NAME'] . '/' . $image->image }}</td>
				<td class="text-right">
				    <form action="{{ route('photos.destroy', $image) }}" onsubmit="if(confirm('Delete?')){return true}else{return false}" method="post">
            			<input type="hidden" name="_method" value="DELETE"> 
            			{{ csrf_field() }}
            			<button type="submit" class="btn"><i class="fa fa-trash"></i></button>
            		</form></td>
			</tr>
			@empty
			<tr>
				<td colspan="3" class="text-center">Нет изображений</td>
			</tr>
			@endforelse
		</tbody>
		<tfoot>
			<tr>
				<td colspan="3">
					<ul class="pagination full-right">
						{{ $photos->links() }}
					</ul>
				</td>
			</tr>
		</tfoot>
	</table>
</div>

@endsection