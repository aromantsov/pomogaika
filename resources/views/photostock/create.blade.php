@extends('layouts.app')

@section('content')

<div class="container">
	<hr>
		@if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
	<form action="{{ route('photos.store') }}" method="post" class="form-horizontal" enctype="multipart/form-data">
		{{ csrf_field() }}
		<input type="hidden" name="user_id" value="{{ $user_id }}">
		<label for="image">Изображение</label>
        <input type="file" class="form-control" name="image" required>
        <hr>
        <input type="submit" class="btn btn-primary" value="Загрузить">
	</form>
</div>
@endsection