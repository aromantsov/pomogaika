@php
use Carbon\Carbon;
$cat_title = 'title_' . $local;
$pr_title = 'name_' . $local;
@endphp

@extends('layouts.app')

@section('content')
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        @if (Session::has('error'))
        <div class="alert alert-danger">{{ Session::get('error') }}</div>
        @endif

        @if (Session::has('success'))
        <div class="alert alert-success">{{ Session::get('success') }}</div>
        @endif

        @if ($new_user)
        <div class="alert alert-success">
            Спасибо за регистрацию. Пожалуйста, подтвердите электронную почту. Подробности в письме
        </div>
        @endif
        @if ($hash)
        <div class="alert alert-success">
            Аккаунт успешно активирован
        </div>
        @endif

        <div class="container">
                <div class="border_user_account">
                    <div class="top_user_account">
                        <div class="left_top_user_account">
                            <div>
                                <div class="user_image_upload">
                                    <img src="{{ $user->avatar ?? '/img/main/image_company.png' }}" alt="">
                                </div>
                                @if($user->avatar)
                                    <div style="margin-right: 20px;">
                                        <button type="button" class="btn btn-primary" disabled>Удалить фото</button>
                                    </div>
                                @endif
                            </div>
                            <div class="info-firm">
                            <div class="line_name_user_account">
                                <p>@lang('account_company_info.firm')</p>
                                <p class="big_name_in_account">{{ $user->name }}</p>
                            </div>
                            <div class="line_name_user_account">
                                <p>@lang('account_company_info.rating')</p>
                                <div class="line_rating_reviews_account">
                                    <div class="rating_reviews_link">
                                        <div class="star_rating">
                                            <div class="star_rvs">
                                            @if($rating >=1)<img src="/img/main/star_yellow.svg" alt="">@else<img src="/img/main/empty_star.svg" alt="">@endif
                                            @if($rating >=2)<img src="/img/main/star_yellow.svg" alt="">@else<img src="/img/main/empty_star.svg" alt="">@endif
                                            @if($rating >=3)<img src="/img/main/star_yellow.svg" alt="">@else<img src="/img/main/empty_star.svg" alt="">@endif
                                            @if($rating >=4)<img src="/img/main/star_yellow.svg" alt="">@else<img src="/img/main/empty_star.svg" alt="">@endif
                                            @if($rating >=5)<img src="/img/main/star_yellow.svg" alt="">@else<img src="/img/main/empty_star.svg" alt="">@endif
                                        </div>
                                        </div>
                                        <div>
                                            <span>{{ $rating ?? 0 }}</span>
                                            <img src="/img/main/icon_review_blue.svg" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div class="info-firm">
                                <div class="line_name_user_account">
                                <p>Количество бонусов</p>
                                <p class="big_name_in_account">{{ $user->bonus }}</p>
                            </div>
                            </div>
                            @if(!$user->is_activate)
                            <div class="info-firm" id="sendactivate" style="margin-left: 15px;">
                                <button type="button" class="btn btn-primary href_pointer" onclick="sendMessage('{{ $user->email }}')">Отправить письмо для активации аккаунта</button>
                            </div>
                            @endif
                        </div>
                        <div class="right_top_user_account">
                            <a href="{{ route('edithome') }}" class="edit_button_form">
                                <img src="/img/main/icon_pencil.svg" alt="">
                            </a>
                            <button class="link_exit_account" id="logout-button" onclick="logout(); return false;">
                                <img src="/img/main/exit_account_icon.svg" alt="">
                            </button>
                        </div>
                    </div>
                    <div class="tabs">
                    <div class="blue_tab_link">
                        <ul class="tabs__caption">
                            <li class="active">@lang('account_company_info.main_info')</li>
                            <li>@lang('account_company_info.orders')</li>
                            <li id="tab-product-catalog">@lang('account_company_info.catalog')</li>
                            <li id="tab-customers">@lang('account_company_info.clients')</li>
                            <li>@lang('account_company_info.reviews')</li>
                            <li>@lang('account_company_info.adv')</li>
                            <li>@lang('account_company_info.tariff')</li>
                        </ul>
                    </div>
                    <div class="tabs__content active">
                    <form action="{{ route('updatehomevendor', $user) }}" method="post" id="update-form" enctype="multipart/form-data">
                    <div class="inputs_about_users">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="put" id="update-home">
                        <div class="one_section_inputs">
                            <div>
                                <p>@lang('account_company_info.name') <span class="text_open" data-text="Максимум 191 символ">i</span></p>
                                <input type="text" name="name" value="{{ $user->name }}">
                            </div>
                            <div>
                                <p>@lang('account_company_info.address') <span class="text_open" data-text="Максимум 191 символ">i</span></p>
                                <input type="text" name="address" value="{{ $user->address ?? ''}}"  @if(!empty($user->address))disabled @endif>
                            </div>
                        </div>
                        <div class="one_section_inputs">
                            <div>
                                <p>@lang('account_company_info.site') <span class="text_open" data-text="Максимум 191 символ">i</span></p>
                                <input type="text" name="lastname" value="{{ $user->lastname ?? '' }}"  @if(!empty($user->lastname))disabled @endif>
                            </div>
                            <div>
                                <p>@lang('account_company_info.phone') <span class="text_open" data-text="Максимум 191 символ">i</span></p>
                                <input type="tel" name="phone" value="{{ $user->phone ?? '' }}"  @if(!empty($user->phone))disabled @endif>
                            </div>
                        </div>
                        <div class="one_section_inputs">
                            <div>
                                <p>@lang('account_company_info.shippings') <span class="text_open" data-text="Выберите способ доставки">i</span></p>
                                <div class="line_checkbox_inputs">
                                    <label>
                                        <input type="checkbox" name="delivery_type[]" value="inshop" @if($user->gender != 'outshop') checked @endif>
                                        <span></span>
                                        @lang('account_company_info.inshop')
                                    </label>
                                    <label>
                                        <input type="checkbox" name="delivery_type[]" value="outshop" @if($user->gender != 'inshop') checked @endif>
                                        <span></span>
                                        @lang('account_company_info.outshop')
                                    </label>
                                </div>
                            </div>
                             <div>
                                <p>Загрузить логотип <span class="text_open" data-text="Загрузите логотип в формате jpeg, png и нажмите Редактировать">i</span></p>
                                <input type="file" name="avatar" value="">
                            </div>
                        </div>
                        @if(($plan != null) && ($plan->tariffs->profile == 1))
                        <div class="one_section_inputs big_section">
                            <div>
                                <p>@lang('account_company_info.description') </p>
                                <textarea name="description" @if(!empty($user->description))disabled @endif>{{ $user->description }}</textarea>
                            </div>
                        </div>
                        @endif
                        <div class="one_section_inputs big_section">
                            <div>
                                <p>@lang('account_company_info.shipping_conditions') <span class="text_open" data-text="">i</span></p>
                                <div class="line_add_blocks delivery">
                                    <div class="one_add_blocks">
                                        <p class="green_text">@lang('account_company_info.inshop')</p>
                                        <p>@lang('account_company_info.shop_address') <span class="text_open" data-text="Если из данного адреса будет осуществляться доставка, адрес должен быть записан в формате: город, улица, дом.
                                            Образец: Харьков, ул. Сумская, 20">i</span></p>
                                        <div class="input_add_addres">
                                            <input type="text" id="add_shop" class="pac-target-input">
                                            <div class="button_add" id="button_add1" onclick="addShop({{ $user->id }});">
                                                <div></div>
                                            </div>
                                        </div>
                                        <div class="append_new_input">
<!--                                            Пихаются данные из script.js-->
                                        @foreach($shops as $shop)
                                        <div class="line_new_inpt" id="line_new_inpt_{{ $shop->id }}">
                                            <div>  
                                                <label><input type="checkbox" name="shops[]" value="{{ $shop->address ?? '' }}" checked><span></span>{{ $shop->address ?? '' }} </label>
                                            </div>
                                        <div class="remove_blocks" id="remove-block-{{ $shop->id }}" onclick="removeShop({{ $shop->id }});"></div>  
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                                    <div class="one_add_blocks">

                                        <p class="green_text">Адресная доставка</p>
                                        <p>Адрес склада/магазина <span class="text_open" data-text="">i</span></p>
                                        <select class="select_block" id="active_shop" style="width: 100%;" onchange="setActiveShop();"> 
                                            @foreach($shops as $shop)
                                            <option value="{{ $shop->id }}" @if($shop->id == $active_shop->id) selected @endif>{{ $shop->address ??'' }}</option>
                                            @endforeach
                                        </select>
                                        <div class="adress_list">
                                            <div class="adress_item line_new_inpt" id="active-address">
                                                <label for="shop3">
                                                    {{ $active_shop->address ?? '' }}
                                                    <input type="radio" name="delivery" id="shop3" checked>
                                                    <span></span>
                                                </label>
                                            </div>
                                            <div class="adress_item_inline">
                                                <label for="">
                                                    Стоимость доставки
                                                    <input type="text" id="cost">
                                                </label>
                                                <label for="">
                                                    Радиус удаления
                                                    <input type="text" id="radius">
                                                </label>
                                                <div class="button_add" id="button_add2" onclick="addDelivery({{ $active_shop->id ?? 0 }});">
                                                    <div></div>
                                                </div>
                                            </div>
                                            <ul class="add_point">
                                                @foreach($delivery_addresses as $delivery)
                                                <li class="one_address" id="one_address_{{ $delivery->delivery_id }}"><input type="checkbox" name="delivery_addresses[]" value="{{ $delivery->price . '/' . $delivery->distance }}" checked><span>{{ $delivery->price }} грн</span><span>{{ $delivery->distance }} км</span><div class="remove_blocks2" id="remove-block2-{{ $delivery->delivery_id }}" onclick="removeDelivery({{ $delivery->delivery_id }});"></div></li>
                                                @endforeach
                                            </ul>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="margin-top: 15px;" class="button_list">
{{--                            <label for=""></label>--}}
                            <button type="submit" class="btn btn-primary" >Сохранить</button>
                            <a href="/home" class="btn btn-primary">Отменить</a>
                        </div>
                    </div>
                    </form>
                    </div>
                        <div class="company_change_password">
                            <div class="change_password_button"  style="color: white;"><img src="/img/main/save_form_account.svg" alt=""> @lang('account_company_info.to_change_password')</div>
                        </div>
                    <div class="tabs__content">
                    <div class="orders_parent">
                        <div class="order-items">
                        @foreach($orders as $order)
                        <div class="order_item active">
                            <div class="order_show">
                                <div class="orders_item_info">
                                    <div class="open_order_item">
                                        @lang('account_company_info.order')
                                        <span class="order_number">№ {{ $order['order_number'] }}</span>
                                    </div>
                                    <div class="open_order_item">
                                        @lang('account_company_info.date2')
                                        <span class="order_date">{{ $order['order_time'] }}</span>
                                    </div>
                                    <div class="open_order_item">
                                        @lang('account_company_info.customer')
                                        <span class="customer">{{ $order['lastname'] . ' ' . $order['name'] ?? '' }}</span>
                                    </div>
                                    <div class="open_order_item">
                                        @lang('account_company_info.phone')
                                        <span>{{ $order['phone'] ?? '' }}</span>
                                    </div>
                                    <div class="open_order_item">
                                        @lang('account_company_info.total')
                                        <span>{{ $order['total'] ?? '' }} грн</span>
                                    </div>
                                    <div class="open_order_item">
                                        @lang('account_company_info.shipping')
                                        <span> @if($order['delivery'] == 'inshop')
                                        @if($local == 'ru')
                                        Самовывоз
                                        @elseif($local == 'uk')
                                        Самовивіз
                                        @else
                                        {{ $order['delivery'] }}
                                        @endif
                                        @elseif($order['delivery'] == 'outshop')
                                        @if($local == 'ru')
                                        Доставка поставщиком
                                        @elseif($local == 'uk')
                                        Доставка постачальником
                                        @else
                                        {{ $order['delivery'] }}
                                        @endif
                                        @elseif($order['delivery'] == 'ourshipping')
                                        Доставка Pomogaika
                                        @endif </span>
                                    </div>
                                    <div class="open_order_item">
                                        @lang('account_company_info.payment')
                                        <span>@if($order['payment'] == 'cod')
                                        @if($local == 'ru')
                                        Наложенный платеж
                                        @elseif($local == 'uk')
                                        Післяплата
                                        @else
                                        {{ $order['payment'] }}
                                        @endif
                                        @elseif($order['payment'] == 'agreement')
                                        @if($local == 'ru')
                                        Оплата по договору
                                        @elseif($local == 'uk')
                                        Оплата згідно договору
                                        @else
                                        {{ $order['payment'] }}
                                        @endif
                                        @elseif($order['payment'] == 'liqpay') 
                                        Visa/Mastercard(Liqpay)
                                        @elseif($order['payment'] == 'bonus')
                                        Оплата бонусами
                                        @endif</span>
                                    </div>
                                    <div class="open_order_item">
                                        @lang('account_company_info.address')
                                        <span>{{ $order['address'] ?? '' }}</span>
                                    </div>
                                    <div class="open_order_item">
                                        Cтоимость доставки
                                        <span>{{ (int)$order['shipping_price'] ?? '' }} грн</span>
                                    </div>
                                    <div class="open_order_item">
                                        @lang('account_company_info.status')
                                        <form action="{{ route('selectstatus') }}" method="post" id="status-form-{{ $order['order_number'] }}">
                                        	{{ csrf_field() }}
                                        	<input type="hidden" name="_method" value="put">
                                        	<input type="hidden" name="order_id" value="{{ $order['order_number'] }}">
                                        	<select name="order_status_id" class="select_status" onchange="selectStatus({{ $order['order_number'] }})" @if($order['order_status_id'] == 10) disabled @endif>
                                        		@foreach($order_statuses as $status)
                                        		<option value="{{ $status->id }}" @if($status->id == $order['order_status_id']) selected @endif>{{ $status->$pr_title }}</option>
                                        		@endforeach
                                        	</select>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="order_hidden">
                                @foreach($order['products'] as $product)
                                <div class="hidden_item">
                                    <span class="hidden_title">{{ $product->$pr_title }}</span>
                                    <span>{{ $product->quantity }} шт</span>
                                    <span>{{ $product->total }} грн</span>
                                </div>
                                @endforeach

                            </div>
                        </div>
                        @endforeach
                        </div>
                        </div>
                    </div>    
                    <div class="tabs__content">
                        <div class="wrap_catalog_add">
                        <div class="line_search_inputs_buttons">
                            <form action="{{ route('home') }}" method="get" id="search-form">
                                <div class="left_search_inputs">
                                    <div>
                                        <p>@lang('account_company_info.name2')</p>
                                        <input type="text" name="search_name" value="{{ $get->search_name ?? '' }}" onchange="searchForm();">
                                    </div>
                                    <div>
                                        <p>@lang('account_company_info.model')</p>
                                        <input type="text" name="search_model" value="{{ $get->search_model ?? '' }}" onchange="searchForm();">
                                    </div>
                                    <div>
                                        <p>@lang('account_company_info.category')</p>
                                        <select name="search_category" onchange="searchForm();">
                                            <option value="0">@lang('account_company_info.all')</option>
                                            @foreach($search_categories as $category)
                                            <option value="{{ $category->id }}" @if($category->id == $get->search_category) selected @endif>{{ $category->$cat_title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div>
                                        <p>@lang('account_company_info.subcategory')</p>
                                        <select name="search_subcategory" onchange="searchForm();">
                                            <option value="0">@lang('account_company_info.all')</option>
                                            @foreach($search_subcategories as $category)
                                            <option value="{{ $category->id }}" @if($category->id == $get->search_subcategory) selected @endif>{{ $category->$cat_title }}</option>
                                                @foreach($category->children as $subcategory)
                                                <option value="{{ $subcategory->id }}" @if($subcategory->id == $get->search_subcategory) selected @endif>--{{ $subcategory->$cat_title }}</option>
                                                @endforeach
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </form>
                            <div class="upload_add_new">
                            	@if(($plan != null) && ($plan->tariffs->import == 1))
                                <div class="button_open_load_modal xlsx-import-button">
                                    <img src="/img/main/upload_icon.svg" alt="">
                                </div>
                                @endif
                                <a href="{{ route('edithome') }}" class="add_new_product">
                                    <img src="/img/main/product_plus.svg" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="list_catalog_products">
                            <table>
                                <thead>
                                    <tr>
                                        <td>№</td>
                                        <td>@lang('account_company_info.model')</td>
                                        <td>@lang('account_company_info.photo')</td>
                                        <td>@lang('account_company_info.name3')</td>
                                        <td>@lang('account_company_info.price')</td>
                                        <td>@lang('account_company_info.category')</td>
                                        <td>@lang('account_company_info.category')</td>
                                        <td></td>
                                    </tr>
                                </thead>
                                <tbody>
                                	@php
                                	$i = 1;
                                	@endphp
                                	@foreach($products as $product)
                                    <tr>
                                        <td>{{ $i }}</td>
                                        <td>{{ $product['model'] }}</td>
                                        <td><img class="cabinet-product" src="{{ $product['image'] ?? '/img/main/default-product.jpg' }}" alt=""></td>
                                        <td>{{ $product['name_' . $local] }}. @if(!$product['published']) (Товар находится на модерации) @endif</td>
                                        <td>{{ (int)$product['price'] }} грн</td>
                                        <td>{{ $product['category_' . $local] }}</td>
                                        <td>{{ $product['subcategory_' . $local] ?? '' }}</td>
                                        <td><a href="/product/{{ $product['product_id'] }}" class="more_product">@lang('account_company_info.more')</a></td>
                                    </tr>
                                    @php
                                	$i++;
                                	@endphp
                                    @endforeach
                                </tbody>
                            </table>

                        </div>
                        </div>
                    </div>
                    <div class="tabs__content">
                        <div class="wrap_catalog_add">
                        <div class="line_search_inputs_buttons section_reviews">
                            <form action="{{ route('home') }}" method="get" id="search-client">
                                <div class="left_search_inputs">
                                    <div>
                                        <p>@lang('account_company_info.name4')</p>
                                        <input type="text" name="search_client_name" placeholder="@lang('account_company_info.search')..." value="{{ $get->search_client_name ?? '' }}" onchange="searchClient()">
                                    </div>
                                    <div>
                                        <p>@lang('account_company_info.phone')</p>
                                        <input type="text" name="search_client_phone" placeholder="@lang('account_company_info.search')..." value="{{ $get->search_client_phone ?? ''}}" onchange="searchClient()">
                                    </div>
                                    <div>
                                        <p>@lang('account_company_info.gender')</p>
                                        <select name="search_client_gender" onchange="searchClient()">
                                            <option value="all">@lang('account_company_info.all')</option>
                                            <option value="female" @if($get->search_client_gender == 'female') selected @endif>@lang('account_company_info.female')</option>
                                            <option value="male" @if($get->search_client_gender == 'male') selected @endif>@lang('account_company_info.male')</option>
                                        </select>
                                    </div>
                                    <div>
                                        <p>@lang('account_company_info.age')</p>
                                        <select name="search_client_age" onchange="searchClient()">
                                            <option value="0">@lang('account_company_info.all')</option>
                                            @foreach($ages as $key => $age)
                                            <option value="{{ $age->age }}" @if($get->search_client_age == $age->age) selected @endif>{{ $age->age }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div>
                                        <p>@lang('account_company_info.sort')</p>
                                        <select name="sort_by" onchange="searchClient()">
                                            <option value="name" @if($get->sort_by=='name') selected @endif>@lang('account_company_info.by_name')</option>
                                            <option value="reviews" @if($get->sort_by=='reviews') selected @endif>@lang('account_company_info.by_reviews')</option>
                                            <option value="age" @if($get->sort_by=='age') selected @endif>@lang('account_company_info.by_age')</option>
                                        </select>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="list_catalog_products">
                            <table>
                                <thead>
                                    <tr>
                                        <td>№</td>
                                        <td>@lang('account_company_info.photo')</td>
                                        <td>@lang('account_company_info.fio')</td>
                                        <td>@lang('account_company_info.phone')</td>
                                        <td>@lang('account_company_info.gender')</td>
                                        <td>@lang('account_company_info.age')</td>
                                        <td>@lang('account_company_info.last_order')</td>
                                        <td>@lang('account_company_info.orders')</td>
                                        <td>@lang('account_company_info.reviews2')</td>
                                        <td></td>
                                    </tr>
                                </thead>
                                <tbody>
                                	@php
                                	$i = 1;
                                	@endphp
                                	@foreach($clients as $client)
                                    <tr>
                                        <td>{{ $i }}</td>
                                        <td><img src="{{ $client->avatar ?? '/img/main/unitaz_icon.jpg' }}" alt="" style="height: 40px; width: 40px;"></td>
                                        <td>{{ $client->name . ' ' . $client->lastname ?? '' }}</td>
                                        <td>{{ $client->phone }}</td>
                                        <td>@if($client->gender == 'male')
                                        @if($local == 'ru')
                                        Мужской
                                        @elseif($local == 'uk')
                                        Чоловічий
                                        @else
                                        {{ $client->gender }}
                                        @endif
                                        @elseif($client->gender == 'female')
                                        @if($local == 'ru')
                                        Женский
                                        @elseif($local == 'uk')
                                        Жіночий
                                        @else
                                        {{ $client->gender }}
                                        @endif
                                        @else
                                        {{ '' }}
                                        @endif</td>
                                        <td>{{ floor((strtotime(date('d-m-Y')) - strtotime($client->birthdate))/(60*60*24*365.2421896)) ?? '' }}</td> 
                                        <td>{{ $client->last_order }}</td>
                                        <td>{{ $client->orders }}</td>
                                        <td><span>{{ $client->reviews }} <span class="green_circle"></span></span></td>
                                        <!-- <td><a href="#" class="more_product" onclick="toProductCatalog();">@lang('account_company_info.more')</a></td> -->
                                    </tr>
                                    @php
                                	$i++;
                                	@endphp
                                    @endforeach
                                    <!-- <tr>
                                        <td>2</td>
                                        <td><img src="/img/main/unitaz_icon.jpg" alt=""></td>
                                        <td>Beatrice Diaz</td>
                                        <td>+38 (050) 500 50 07</td>
                                        <td>женский</td>
                                        <td>32</td>
                                        <td>01.01.2019</td>
                                        <td>4</td>
                                        <td><span>2 <span class="green_circle"></span></span></td>
                                        <td><a href="account_company_add-product.html" class="more_product">Подробнее</a></td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td><img src="/img/main/unitaz_icon.jpg" alt=""></td>
                                        <td>Beatrice Diaz</td>
                                        <td>+38 (050) 500 50 07</td>
                                        <td>женский</td>
                                        <td>32</td>
                                        <td>01.01.2019</td>
                                        <td>4</td>
                                        <td>2</td>
                                        <td><a href="account_company_add-product.html" class="more_product">Подробнее</a></td>
                                    </tr> -->
                                </tbody>
                            </table>

                        </div>
                    </div>
                    </div>
                    <div class="tabs__content">
                    	<div class="reviews_about_company">
                        @forelse($vendor_reviews as $review)
                        <div class="one_line_review_wrap">
                            <div class="left_reviews_date_like_dislike">
                                <p class="date_rvs">{{ $review->created_at }}</p>
                                <div class="line_like_dislike">
                                    <div class="like">
                                        <img src="/img/main/icon_like.svg" alt="">
                                        <span>{{ $review->likes }}</span>
                                    </div>
                                    <div class="dislike">
                                        <img src="/img/main/icon_dislike.svg" alt="">
                                        <span>{{ $review->dislikes }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="one_review_on_page">
                                <div class="avatar_stars_user">
                                    <div class="avatar_user">
                                        <img src="{{ $review->users->avatar ?? '/img/main/image_company.png' }}" alt="">
                                    </div>
                                    <div class="star_reviews_name">
                                        <p class="name_rvs">{{ $review->users->name ?? '' . ' ' . $review->users->lastname ?? '' }}</p>
                                        <div class="star_rvs">
                                            @if($review->rating >=1)<img src="/img/main/star_yellow.svg" alt="">@else<img src="/img/main/empty_star.svg" alt="">@endif
                                            @if($review->rating >=2)<img src="/img/main/star_yellow.svg" alt="">@else<img src="/img/main/empty_star.svg" alt="">@endif
                                            @if($review->rating >=3)<img src="/img/main/star_yellow.svg" alt="">@else<img src="/img/main/empty_star.svg" alt="">@endif
                                            @if($review->rating >=4)<img src="/img/main/star_yellow.svg" alt="">@else<img src="/img/main/empty_star.svg" alt="">@endif
                                            @if($review->rating >=5)<img src="/img/main/star_yellow.svg" alt="">@else<img src="/img/main/empty_star.svg" alt="">@endif
                                        </div>
                                    </div>
                                </div>
                                <div class="info_about_review">
                                    <span>i</span>
                                    Отзыв:
                                </div>
                                <p>{{ $review->review }}</p>
                            </div>
                        </div>
                        @empty
                        Нет отзывов
                        @endforelse

                    </div>
                    </div>
                    <div class="tabs__content">
                    	<div class="container_wrap">
                        <div class="line_r_texts">
                            <div class="left_r_text">
                                <p class="title_ab_banner">@lang('account_company_info.adv1')</p>
                                <p class="name_op">@lang('account_company_info.description2')</p>
                                <p>@lang('account_company_info.adv1_description')</p>
                                <p class="name_op">@lang('account_company_info.cost')</p>
                                <p>500 @lang('account_company_info.grn')</p>
                                <p class="name_op">@lang('account_company_info.conditions')</p>
                                <p>@lang('account_company_info.file1')</p>
                                <div>
                                    @if(in_array(1, $ordered_adv))
                                    <a href="#" class="button_show_form_ad"  id="adv-1" onclick="modalAdv(500, 1); return false;"><img src="/img/main/white_checked.svg" alt="">В ожидании</a>
                                    @else
                                    <a href="#" class="button_show_form_ad"  id="adv-1" onclick="modalAdv(500, 1); return false;"><img src="/img/main/white_checked.svg" alt="">@lang('account_company_info.adv_order')</a>
                                    @endif
                                </div>
                            </div>
                            <div class="right_image_r">
                                <p class="title_ab_banner">@lang('account_company_info.adv1')</p>
                                <img src="/img/main/banner_place3.png" alt="">
                            </div>
                        </div>
                        <div class="line_r_texts">
                            <div class="left_r_text">
                                <p class="title_ab_banner">@lang('account_company_info.adv2')</p>
                                <p class="name_op">@lang('account_company_info.description2')</p>
                                <p>@lang('account_company_info.adv2_description')</p>
                                <p class="name_op">@lang('account_company_info.cost')</p>
                                <p>300 @lang('account_company_info.grn')</p>
                                <p class="name_op">@lang('account_company_info.conditions')</p>
                                <p>@lang('account_company_info.file2')</p>
                                <div>
                                    @if(in_array(2, $ordered_adv))
                                    <a href="#" class="button_show_form_ad"  id="adv-2" onclick="modalAdv(300, 2); return false;"><img src="/img/main/white_checked.svg" alt="">В ожидании</a>
                                    @else
                                    <a href="#" class="button_show_form_ad"  id="adv-2" onclick="modalAdv(300, 2); return false;"><img src="/img/main/white_checked.svg" alt="">@lang('account_company_info.adv_order')</a>
                                    @endif
                                </div>
                            </div>
                            <div class="right_image_r">
                                <p class="title_ab_banner">@lang('account_company_info.adv2')</p>
                                <img src="/img/main/banner_place2.png" alt="">
                            </div>
                        </div>
                        <div class="line_r_texts">
                            <div class="left_r_text">
                                <p class="title_ab_banner">@lang('account_company_info.adv3')</p>
                                <p class="name_op">@lang('account_company_info.description2')</p>
                                <p>@lang('account_company_info.adv3_description')</p>
                                <p class="name_op">@lang('account_company_info.cost')</p>
                                <p>300 @lang('account_company_info.grn')</p>
                                <div>
                                    @if(in_array(3, $ordered_adv))
                                    <a href="#" class="button_show_form_ad"  id="adv-3" onclick="modalAdv(300, 3); return false;"><img src="/img/main/white_checked.svg" alt="">В ожидании</a>
                                    @else
                                    <a href="#" class="button_show_form_ad"  id="adv-3" onclick="modalAdv(300, 3); return false;"><img src="/img/main/white_checked.svg" alt="">@lang('account_company_info.adv_order')</a>
                                    @endif
                                </div>
                            </div>
                            <div class="right_image_r">
                                <p class="title_ab_banner">@lang('account_company_info.adv3')</p>
                                <img src="/img/main/banner_place1.png" alt="">
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="tabs__content">
                        <div class="tarif_container">
                            <div class="your_plan">
                                @php
                                $tar = $plan->tariffs->id ?? 1;
                                $ordered_tar = $ordered_plan->tariffs->id ?? 0;
                                @endphp
                                <h2>@lang('account_company_info.your_tafiff'):</h2>
                                <div class="your_plan_block">
                                    <ul>
                                        <li>
                                            <span>@lang('account_company_info.tariff')</span>
                                            <p>{{ $plan->tariffs->name ?? 'Basic' }}</p>
                                        </li>
                                        <li>
                                             <span>@lang('account_company_info.cost')</span>
                                             <p>{{ $plan->tariffs->price ?? 0 }} грн/мес</p>
                                        </li>
                                        <li>
                                            <span>@lang('account_company_info.activation_date')</span>
                                            <p>{{ $plan->created_at ?? $user->created_at }}</p>
                                        </li>
                                        <li>
                                            <span>@lang('account_company_info.ending_date')</span>
                                            <p>@if(isset($plan)){{ Carbon::parse($plan->term)->addDays(1) ?? '-' }}@else - @endif</p>
                                        </li>
                                    </ul>
                                    @php
                                    $plan_tar = $plan->tariff_id ?? 1;
                                    @endphp
                                    @if($ordered_tar == $plan_tar)
                                    <a href="#" id="prolongate_plan" onclick="prolongatePlan({{ $user->id }}, {{ $plan->tariff_id ?? 1 }}); return false;"><img src="/img/main/white_checked.svg" alt="icon">В ожидании</a>
                                    @else
                                    <a href="#" id="prolongate_plan" onclick="prolongatePlan({{ $user->id }}, {{ $plan->tariff_id ?? 1 }}); return false;">@lang('account_company_info.prolongate')</a>
                                    @endif
                                </div>
                            </div>
                            <div class="our_plans">
                                <h2>@lang('account_company_info.our_tariffs'):</h2>
                                <p>@lang('account_company_info.adv1_description')</p>
                            </div>
                            <div class="tarif_table">
                                <div class="tarif_name">
                                    <div class="functional">
                                        <h2>@lang('account_company_info.functional')</h2>
                                        <ul>
                                            <li>@lang('account_company_info.big_profile')</li>
                                            <li>@lang('account_company_info.products_import')</li>
                                            <li>@lang('account_company_info.customers_message')</li>
                                            <li>@lang('account_company_info.public_profiles')</li>
                                            <li>@lang('account_company_info.hot_offers')</li>
                                            <li>@lang('account_company_info.personal_manager')</li>
                                            <li>@lang('account_company_info.month_rating')</li>
                                        </ul>
                                    </div>
                                    @foreach($tariffs as $tariff)
                                    <div class="{{ $tariff->name }}">
                                        <h2>{{ ucfirst($tariff->name) }}</h2>
                                        <ul>
                                            <li>@if($tariff->profile)<img src="/img/main/green_checked.svg" alt="icon">@endif</li>
                                            <li>@if($tariff->import)<img src="/img/main/green_checked.svg" alt="icon">@endif</li>
                                            <li>@if($tariff->messages)<img src="/img/main/green_checked.svg" alt="icon">@endif</li>
                                            <li>@if($tariff->public_profile)<img src="/img/main/green_checked.svg" alt="icon">@endif</li>
                                            <li>@if($tariff->hot)<img src="/img/main/green_checked.svg" alt="icon">@endif</li>
                                            <li>@if($tariff->manager)<img src="/img/main/green_checked.svg" alt="icon">@endif</li>
                                            <li>@if($tariff->month_rating)<img src="/img/main/green_checked.svg" alt="icon">@endif</li>
                                        </ul>
                                    </div>
                                    @endforeach
                                </div>
                                <div class="tarif_count">
                                    <div class="count_tarif">
                                        <h2>@lang('account_company_info.cost')</h2>
                                    </div>
                                    
                                    @foreach($tariffs as $tariff)
                                    <div class="tarif_{{ $tariff->name }}">
                                        <span class="mobile_tarif">{{ ucfirst($tariff->name) }}</span>
                                        <h2>{{ $tariff->price }} грн/мес</h2>
                                        @if($tariff->id == $tar)
                                        <a href="#" class="active"><img src="/img/main/white_checked.svg" alt="icon">@lang('account_company_info.active')</a>
                                        @else
                                        @if($tariff->id == $ordered_tar)
                                        <a href="#" class="order_plan" id="order_plan_{{ $tariff->id }}"  onclick="orderPlan({{ $user->id }}, {{ $tariff->id }}); return false;"><img src="/img/main/white_checked.svg" alt="icon">В ожидании</a>
                                        @else
                                        <a href="#" class="order_plan" id="order_plan_{{ $tariff->id }}" onclick="orderPlan({{ $user->id }}, {{ $tariff->id }}); return false;"><img src="/img/main/white_checked.svg" alt="icon">@lang('account_company_info.to_order')</a>
                                        @endif
                                        @endif
                                    </div>
                                    @endforeach
<!--                                     <div class="tarif_gold">
                                        <span class="mobile_tarif">Gold</span>
                                        <h2>1 000 грн/мес</h2>
                                        <a href="#"><img src="/img/main/white_checked.svg" alt="icon">Заказать</a>
                                    </div>
                                    <div class="tarif_platinum">
                                        <span class="mobile_tarif">Platinum</span>
                                        <h2>2 000 грн/мес</h2>
                                        <a href="#"><img src="/img/main/white_checked.svg" alt="icon"> Заказать</a>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
        </div>
    </div>
    <div class="overlay" @if($get->import) style="display:block;" @endif></div>
    <div class="modal_block text_hint">
        <div class="close_modal"></div>
        <p class="append_text"></p>
    </div>

    <div class="modal_block change_password">
        <div class="close_modal"></div>
        <p class="name_modal">@lang('account_company_info.change_password')</p>
        <form action="{{ route('changepassword') }}" method="post">
            {{ csrf_field() }}
            <div class="one_section_inputs">
                <div>
                    <p>@lang('account_company_info.old_password'):</p>
                    <input type="password" name="current_password">
                </div>
                <div>
                    <p>@lang('account_company_info.new_password'):</p>
                    <input type="password" name="new_password">
                </div>
                <div>
                    <p>@lang('account_company_info.confirm_password'):</p>
                    <input type="password" name="new_password_confirmation">
                </div>
            </div>
            <button class="blue_button_link" type="submit"><img src="/img/main/save_form_account.svg" alt=""> @lang('account_company_info.to_change_password')</button>
        </form>
    </div>

    @if(($plan != null) && ($plan->tariffs->import == 1))
    <div class="modal_block xlsx-import" @if($get->import) style="display:block;" @endif>
        <div class="close_modal"></div>
        <p class="name_modal">Импорт товаров</p>
        <form action="{{ route('upload') }}" method="post" enctype="multipart/form-data" id="import-excel" class="form-horizontal">
            {{ csrf_field() }}
            <input type="hidden" name="vendor_id" value="{{ $user->id }}">
            <table class="form">
                <tr>
                    <td>
                        @lang('account_company_edit.file_for_import')
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="{{ route('info', 6) }}" target="_blank">Инструкция по загрузке</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        Изображения загружаются по ссылке на него. Если вы не имеете ссылок, можете воспользоваться нашим <a href="{{ route('photos.index') }}">фотохостингом</a>
                    </td>
                </tr>
                <tr>
                    <td>@lang('account_company_edit.uploading_file')<br /><br /><input type="file" name="file" id="upload" /></td>
                </tr>
                <tr>
                    <td class="buttons"><a onclick="uploadExcel();" class="btn btn-primary"><span>@lang('account_company_edit.import')</span></a></td>
                </tr>
            </table>
        </form>
    </div>
    @endif
@endsection
