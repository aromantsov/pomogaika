@php
$cat_title = 'title_' . $local;
$tag_l = 'tag_' . $local;
$name = 'name_' . $local;
$des = 'description_' . $local;
@endphp
@extends('layouts.app')

@section('content')
<div class="content">
	<div class="breadcrumbs_container">
            <div class="container">
                <div class="breadcrumbs">
                    <div class="show_overlay_hover"></div>
                    <div class="link_dropdown_menu">
                        <button class="blue_button_link" onclick="toggleNavbar();">@lang('account_company_info.catalog') <img src="/img/main/line_toggle_cat.svg" alt=""></button>
                        <div class="a_side_category" id="a_side_category">
                            <div class="link_cats_a_side">
                                @foreach($categories as $category)
                                <div class="wrap_link_category">
                                    <a href="{{ route('category', ['id' => $category->id]) }}"><img src="{{ $category->icon ?? '/img/main/image_cat1.svg' }}" alt="">{{ $category->$cat_title }}</a>
                                    <div class="wrap_submenu_section">
                                        @foreach($category->children as $category_list)
                                         <div class="one_catalog_submenu_line">
                                            <img src="{{ $category_list->image ?? '/img/main/icon_cats1.jpg' }}" alt="">
                                            <div class="list_link_submenu_name_catalog">
                                                <p class="name_cat_section">{{ $category_list->$cat_title }}</p>
                                                <div class="line_link_cat nav-menu">
                                                    @foreach($category_list->children as $catlist2)
                                                    <a href="{{ route('category', ['id' => $catlist2->id]) }}">{{ $catlist2->$cat_title }}</a>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                                @endforeach
                            </div>
                            <a href="/categories" class="all_category_link blue_button_link"><img src="/img/main/line_toggle_cat.svg" alt=""> @lang('category.all_categories')</a>
                        </div>
                    </div>

                    <div class="breadcrumbs_link">
                        <a href="/">@lang('account_user_info.main')</a>
                        <span>@lang('charity.charity')</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <a href="" class="image_link_add">
                <img src="/img/main/rel3.jpg" alt="">
            </a>
            <div class="wrap_static_page charity_page">
                <p class="title_news_block">@lang('charity.charity')</p>
                <p class="mini_title_news">@lang('charity.about')</p>
                <div class="charity_text">
                    <p>@lang('charity.attic')</p>
                    <div class="comp_charity">
                        <p class="name_company_charity">@lang('charity.company') №1</p>
                        <div class="line_text_img_charity">
                            <div class="img_charity">
                                <img src="/img/main/img_charity.jpg" alt="">
                            </div>
                            <div class="text_charity">
                                <div>
                                    <p class="title_charity">@lang('charity.prolisok')</p>
                                    <p>@lang('charity.attic')</p>
                                </div>
                                <div class="click_charity">@lang('charity.help')</div>
                            </div>
                        </div>
                    </div>
                    <div class="comp_charity">
                        <p class="name_company_charity">@lang('charity.company') №2</p>
                        <div class="line_text_img_charity">
                            <div class="img_charity">
                                <img src="/img/main/img_charity.jpg" alt="">
                            </div>
                            <div class="text_charity">
                                <div>
                                    <p class="title_charity">@lang('charity.prolisok')</p>
                                    <p>@lang('charity.attic')</p>
                                </div>
                                <div class="click_charity">@lang('charity.help')</div>
                            </div>
                        </div>
                    </div>
                    <div class="comp_charity">
                        <p class="name_company_charity">@lang('charity.company') №3</p>
                        <div class="line_text_img_charity">
                            <div class="img_charity">
                                <img src="/img/main/img_charity.jpg" alt="">
                            </div>
                            <div class="text_charity">
                                <div>
                                    <p class="title_charity">@lang('charity.prolisok')</p>
                                    <p>@lang('charity.attic')</p>
                                </div>
                                <div class="click_charity">@lang('charity.help')</div>
                            </div>
                        </div>
                    </div>
                    <div class="comp_charity">
                        <p class="name_company_charity">@lang('charity.company') №4</p>
                        <div class="line_text_img_charity">
                            <div class="img_charity">
                                <img src="/img/main/img_charity.jpg" alt="">
                            </div>
                            <div class="text_charity">
                                <div>
                                    <p class="title_charity">@lang('charity.prolisok')</p>
                                    <p>@lang('charity.attic')</p>
                                </div>
                                <div class="click_charity">@lang('charity.help')</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
@endsection