@php
use App\Models\Property;
$cat_title = 'title_' . $local;
$pr_title = 'name_' . $local;
$val = 'value_' . $local;
$des = 'description_' . $local;
@endphp

@extends('layouts.app')

@section('content')
    <div class="content">
        <div class="breadcrumbs_container">
            <div class="container">
                <div class="breadcrumbs">
                   <div class="show_overlay_hover"></div>
                    <div class="link_dropdown_menu">
                        <button class="blue_button_link" onclick="toggleNavbar();">@lang('account_company_info.catalog') <img src="/img/main/line_toggle_cat.svg" alt=""></button>
                        <div class="a_side_category" id="a_side_category">
                            <div class="link_cats_a_side">
                                @foreach($categories as $category)
                                <div class="wrap_link_category">
                                    <a href="{{ route('category', ['id' => $category->id]) }}"><img src="{{ $category->icon ?? '/img/main/image_cat1.svg' }}" alt="">{{ $category->$cat_title }}</a>
                                    <div class="wrap_submenu_section">
                                        @foreach($category->children as $category_list)
                                         <div class="one_catalog_submenu_line">
                                            <img src="{{ $category_list->image ?? '/img/main/icon_cats1.jpg' }}" alt="">
                                            <div class="list_link_submenu_name_catalog">
                                                <p class="name_cat_section">{{ $category_list->$cat_title }}</p>
                                                <div class="line_link_cat nav-menu">
                                                    @foreach($category_list->children as $catlist2)
                                                    <a href="{{ route('category', ['id' => $catlist2->id]) }}">{{ $catlist2->$cat_title }}</a>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                                @endforeach
                            </div>
                            <a href="{{ route('categories') }}" class="all_category_link blue_button_link"><img src="/img/main/line_toggle_cat.svg" alt=""> @lang('category.all_categories')</a>
                        </div>
                    </div>
                    <div class="breadcrumbs_link">
                        <a href="{{ route('main') }}">@lang('account_user_info.main')</a>
                        <a href="{{ route('categories') }}">@lang('category.catalog')</a>
                        <span>@lang('compare.compare')</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <a href="" class="image_link_add">
                <img src="/img/main/rel3.jpg" alt="">
            </a>
            <div class="wrap_comparement_section">
                <p class="big_name_title">@lang('compare.compare')</p>
                @foreach($cats as $cat)    
                <div class="one_comparement_category">
                    <div class="line_name_cats_add_button">
                        <p>{{ $cat->$cat_title ?? 'Товары без категории' }}</p>
                        @isset($cat->id)<a href="/category/{{ $cat->id }}"><img src="/img/main/icon_white_plus.svg" alt="">Добавить еще</a>@endisset
                    </div>
                    <div class="table_product_comparement">
                        <div class="row_compare_products">
                            <div class="col_compare_products">

                            </div>
                            @foreach($products[$cat->id ?? 0] as $product)
                            <div class="col_compare_products">
                                <a href="/product/{{ $product->id }}"><img src="{{ $product->image ?? '/img/main/default-product.jpg' }}" alt=""></a>
                                <a href="/removecompare/{{ $product->id }}" class="remove_coparement_products">
                                    <img src="/img/main/icon_comparement_remove.svg" alt="">
                                </a>
                            </div>
                            @endforeach
                            <!-- <div class="col_compare_products">
                                <img src="/img/main/product_slider_image2.jpg" alt="">
                                <a href="" class="remove_coparement_products">
                                    <img src="/img/main/icon_comparement_remove.svg" alt="">
                                </a>
                            </div> -->
                        </div>
                        <div class="row_compare_products">
                            <div class="col_compare_products">

                            </div>
                            @foreach($products[$cat->id ?? 0] as $product)
                            <div class="col_compare_products">
                                <p>{{ $product->$pr_title }}</p>
                            </div>
                            @endforeach
                        </div>
                        <div class="toggle_block_more">
                            <div class="row_compare_products">
                                <div class="col_compare_products">
                                    <p>Цена</p>
                                </div>
                                @foreach($products[$cat->id ?? 0] as $product)
                                <div class="col_compare_products">
                                    <p>{{ (int)$product->price . ' грн' }}</p>
                                </div>
                                @endforeach
                                <!-- <div class="col_compare_products">
                                    <p>Суха</p>
                                </div> -->
                            </div>
                            <div class="row_compare_products">
                                <div class="col_compare_products">
                                    <p>Производитель</p>
                                </div>
                                @foreach($products[$cat->id ?? 0] as $product)
                                <div class="col_compare_products">
                                    <p>{{ $product->manufacturers->brand ?? '' }}</p>
                                </div>
                                @endforeach
                                <!-- <div class="col_compare_products">
                                    <p>Суха</p>
                                </div> -->
                            </div>
                            @foreach($props[$cat->id ?? 0] as $property)
                            <div class="row_compare_products">
                                <div class="col_compare_products">
                                    <p>{{ $property->attributes->$pr_title }}</p>
                                </div>
                                @foreach($products[$cat->id ?? 0] as $product)
                                @php
                                $prop = Property::select($val)->where('attribute_id', $property->attributes->id)->where('product_id', $product->id)->first();
                                @endphp
                                <div class="col_compare_products">
                                    <p>{{ $prop[$val] }}</p>
                                </div>
                                @endforeach
                                <!-- <div class="col_compare_products">
                                    <p>Суха</p>
                                </div> -->
                            </div>
                            @endforeach
                        </div>
                    </div>
                            <div class="click_more_about">
                                <p data-text-close="Свернуть" data-text-open="@lang('account_company_info.more')"><span>@lang('account_company_info.more')</span> <img src="/img/main/arrow_down.svg" alt=""></p>
                            </div>
                        <!-- </div> -->
                @endforeach  
            </div>
        </div>
    </div>
@endsection