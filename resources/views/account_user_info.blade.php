@php
$cat_title = 'title_' . $local;
$pr_title = 'name_' . $local;
@endphp

@extends('layouts.app')

@section('content')
@if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        @if ($new_user)
        <div class="alert alert-success">
            Спасибо за регистрацию. Пожалуйста, подтвердите электронную почту. Подробности в письме
        </div>
        @endif
        @if ($hash)
        <div class="alert alert-success">
            Аккаунт успешно активирован
        </div>
        @endif
    <div class="content">
        <div class="breadcrumbs_container">
            <div class="container">
                <div class="breadcrumbs">
                   <div class="show_overlay_hover"></div>
                    <div class="link_dropdown_menu">
                        <button class="blue_button_link" onclick="toggleNavbar();">@lang('account_company_info.catalog') <img src="/img/main/line_toggle_cat.svg" alt=""></button>
                        <div class="a_side_category" id="a_side_category">
                            <div class="link_cats_a_side">
                                @foreach($categories as $category)
                                <div class="wrap_link_category">
                                    <a href="{{ route('category', ['id' => $category->id]) }}"><img src="{{ $category->icon ?? '/img/main/image_cat1.svg' }}" alt="">{{ $category->$cat_title }}</a>
                                    <div class="wrap_submenu_section">
                                        @foreach($category->children as $category_list)
                                         <div class="one_catalog_submenu_line">
                                            <img src="{{ $category_list->image ?? '/img/main/icon_cats1.jpg' }}" alt="">
                                            <div class="list_link_submenu_name_catalog">
                                                <p class="name_cat_section">{{ $category_list->$cat_title }}</p>
                                                <div class="line_link_cat nav-menu">
                                                    @foreach($category_list->children as $catlist2)
                                                    <a href="{{ route('category', ['id' => $catlist2->id]) }}">{{ $catlist2->$cat_title }}</a>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                                @endforeach
                            </div>
                            <a href="/categories" class="all_category_link blue_button_link"><img src="/img/main/line_toggle_cat.svg" alt=""> @lang('account_user_info.all_categories')</a>
                        </div>
                    </div>
                    <div class="breadcrumbs_link">
                        <a href="{{ route('main') }}">@lang('account_user_info.main')</a>
                        <span>@lang('account_user_info.user'): {{ $user->name }} {{ $user->lastname }} {{ $user->patronimic }}</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <form action="{{ route('updatehome', $user) }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="put" id="update-home">
                <div class="border_user_account">
                    <div class="top_user_account">
                        <div class="left_top_user_account">
                            <div class="user_image_upload">
                                <img src="{{ $user->avatar ?? '/img/main/image_company.png' }}" alt="">
                            </div>
                            @if($user->avatar)
                            <div id="delbutton" style="margin-right: 20px;">
                                <button type="button" class="btn btn-primary" onclick="deleteAvatar({{ $user->id }})" disabled>Удалить фото</button>
                            </div>
                            @endif
                            <div class="info-firm">
                            <div class="line_name_user_account">
                                <p>@lang('account_company_info.customer')</p>
                                <p class="big_name_in_account">{{ $user->name }} {{ $user->lastname }} {{ $user->patronimic }}</p>
                            </div>
                            <div class="line_name_user_account">
                                <p>Email</p>
                                <p class="big_name_in_account">{{ $user->email }}</p>
                            </div>
                            </div>
                            <div class="info-firm" style="margin-left: 15px;">
                            <div class="line_name_user_account">
                                <p>@lang('account_company_info.phone')</p>
                                <p class="big_name_in_account">{{ $user->phone }}</p>
                            </div>
                            <div class="line_name_user_account">
                                <p>Бонусных грн</p>
                                <p class="big_name_in_account">{{ (int)$user->bonus }}</p>
                            </div>
                            </div>
                            @if(!$user->is_activate)
                            <div class="info-firm" id="sendactivate" style="margin-left: 15px;">
                                <button type="button" class="btn btn-primary href_pointer" onclick="sendMessage('{{ $user->email }}')">Отправить письмо для активации аккаунта</button>
                            </div>
                            @endif
                        </div>
                        <div class="right_top_user_account">
                            @if($user->user_role_id == 1)
                            <a href="{{ route('admin.index') }}">
                                @lang('account_user_info.adminpanel')
                            </a>
                            @endif
                            <a href="{{ route('edithome') }}" class="edit_button_form">
                                <img src="/img/main/icon_pencil.svg" alt="">
                            </a>
                            <button class="link_exit_account" id="logout-button" onclick="logout(); return false;">
                                <img src="/img/main/exit_account_icon.svg" alt="">
                            </button>
                        </div>
                    </div>
                   <div class="tabs">
                    <div class="blue_tab_link">
                    <ul class="tabs__caption">
                        <li class="active">@lang('account_user_info.personal_data')</li>
                        <li>@lang('account_user_info.actual_orders')</li>
                        <li class="caption_wishlist">@lang('account_user_info.wishlist')</li>
                        <li>@lang('account_user_info.my_reviews')</li>
                        <li>@lang('account_user_info.order_history')</li>
                    </ul>
                    </div>
                    <div class="tabs__content active">
                    <div class="inputs_about_users">
                        <div class="one_section_inputs">
                            <div>
                                <p>@lang('account_user_info.second_name') <span class="text_open" data-text="Для изменения данных нажмите Редактировать">i</span></p>
                                <input type="text" name="lastname" value="{{ $user->lastname }}" @if(!empty($user->lastname))disabled @endif>
                            </div>
                            <div>
                                <p>@lang('account_company_info.name4')* <span class="text_open" data-text="Для изменения данных нажмите Редактировать">i</span></p>
                                <input type="text" name="name" value="{{ $user->name }}">
                            </div>
                            <div>
                                <p>@lang('account_user_info.patronimic') <span class="text_open" data-text="Для изменения данных нажмите Редактировать">i</span></p>
                                <input type="text" name="patronimic" value="{{ $user->patronimic }}" @if(!empty($user->patronimic))disabled @endif>
                            </div>
                        </div>
                        <div class="one_section_inputs">
                            <div>
                                <p>@lang('account_company_info.gender') <span class="text_open" data-text="Для изменения данных нажмите Редактировать">i</span></p>
                                <div class="line_checkbox_inputs">
                                    <label>
                                        <input type="radio" name="gender_input" @if($user->gender == 'male') checked @endif>
                                        <span></span>
                                        @lang('account_company_info.male')
                                    </label>
                                    <label>
                                        <input type="radio" name="gender_input" @if($user->gender == 'female') checked @endif>
                                        <span></span>
                                        @lang('account_company_info.female')
                                    </label>
                                </div>
                            </div>
                            <div>
                                <p>@lang('account_user_info.birthdate') <span class="text_open" data-text="Для изменения данных нажмите Редактировать">i</span></p>
                                <input type="date" name="birthdate" value="{{ $user->birthdate }}"  @if(!empty($user->birthdate))disabled @endif>
                            </div>
                            <div>
                                <p>@lang('account_company_info.phone') <span class="text_open" data-text="Для изменения данных нажмите Редактировать">i</span></p>
                                <input type="tel" name="phone" value="{{ $user->phone }}"  @if(!empty($user->phone))disabled @endif>
                            </div>
                            <div>
                                <p>Загрузить аватар <span class="text_open" data-text="Загрузите аватар в формате jpeg, png и нажмите Редактировать">i</span></p>
                                <label for="file_up" class="file_up">
                                    <input type="file" name="avatar" value="" id="file_up">
                                </label>
                            </div>
                        </div>
                        <div class="one_section_inputs">
                            <div>
                                <p>Email <span class="text_open" data-text="Для изменения данных нажмите Редактировать">i</span></p>
                                <input type="email" name="email" value="{{ $user->email }}">
                            </div>
                            <div>
                                <p>@lang('account_user_info.default_address') <span class="text_open" data-text="Для изменения данных нажмите Редактировать">i</span></p>
                                <input type="text" name="address" value="{{ $user->address }}"  @if(!empty($user->address))disabled @endif>
                            </div>
                            <div>
                                <p>@lang('account_user_info.password') <span class="text_open" data-text="Для изменения пароля">i</span></p>
                                <div class="change_password_button"><img src="/img/main/save_form_account.svg" alt=""> @lang('account_company_info.to_change_password')</div>
                            </div>
                            <div class="button_list_user">
                                <p>Сохранить или отменить</p>
                                <div>
                                    <button type="submit" class="btn btn-primary">Сохранить</button>
                                    <a href="/home" class="btn btn-primary">Отменить</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="tabs__content">
                        <div class="wrap_account_orders">
                        @foreach($actual as $order)
                        <div class="one_wrap_orders">
                        <div class="line_order_header">
                            <div>
                                <p class="mini_title">@lang('account_company_info.order')</p>
                                <p>@lang('account_company_info.order') № {{ $order['order_id'] }} @lang('account_company_info.from') {{ $order['order_time'] }}</p>
                            </div>
                            <div>
                                <p class="mini_title">@lang('account_company_info.status')</p>
                                <p>{{ $order[$pr_title] }}</p>
                            </div>
                            <div>
                                <p class="mini_title">@lang('account_company_info.payment')</p>
                                <p>@if($order['payment'] == 'cod')
                                        @if($local == 'ru')
                                        Наложенный платеж
                                        @elseif($local == 'uk')
                                        Післяплата
                                        @else
                                        {{ $order['payment'] }}
                                        @endif
                                        @elseif($order['payment'] == 'agreement')
                                        @if($local == 'ru')
                                        Оплата по договору
                                        @elseif($local == 'uk')
                                        Оплата згідно договору
                                        @else
                                        {{ $order['payment'] }}
                                        @endif
                                        @elseif($order['payment'] == 'liqpay') 
                                        Visa/Mastercard(Liqpay)
                                        @endif</p> 
                            </div>
                            <div>
                                <p class="mini_title">@lang('account_company_info.order_total')</p>
                                <p>{{ (int)$order['total'] }} грн</p>
                            </div>
                            <div>
                                <p class="mini_title">Стоимость доставки</p>
                                <p>{{ (int)$order['shipping_price'] }} грн</p>
                            </div>
                            <div>
                                <p class="click_down_orders">@lang('account_company_info.more') <img src="/img/main/arrow_down.svg" alt=""></p>
                            </div>
                        </div>
                        <div class="toggle_slide_orders_details">
                            @foreach($order['vendors'] as $vendor)
                            <div class="line_column_orders">
                                <div class="top_info_orders">
                                    <div>
                                        <p class="mini_title">@lang('account_user_info.seller')</p>
                                        <p>{{ $vendor['name'] }}</p>
                                    </div>
                                    <div>
                                        <p class="mini_title">@lang('account_company_info.shipping')</p>
                                        <p>
                                        @if($vendor['delivery'] == 'inshop')
                                        @if($local == 'ru')
                                        Самовывоз
                                        @elseif($local == 'uk')
                                        Самовивіз
                                        @else
                                        {{ $vendor['delivery'] }}
                                        @endif
                                        @elseif($vendor['delivery'] == 'outshop')
                                        @if($local == 'ru')
                                        Доставка поставщиком
                                        @elseif($local == 'uk')
                                        Доставка постачальником
                                        @else
                                        {{ $vendor['delivery'] }}
                                        @endif
                                        @elseif($vendor['delivery'] == 'ourshipping')
                                        Доставка Pomogaika
                                        @endif    
                                        </p>
                                    </div>
                                    <div>
                                        <a href="" class="link_add_review" data-vendor="{{ $vendor['user_id'] }}">@lang('account_user_info.send_review')</a>
                                    </div>
                                </div>
                                <!-- <div class="bottom_info_orders"> -->
                                    @foreach($vendor['products'] as $product)
                                    <div class="bottom_info_orders">
                                    <div>
                                        <img src="{{ $product->image ?? '/img/main/default-product.jpg' }}" alt="">
                                    </div>
                                    <div>
                                        <p>{{ $product->$pr_title ?? '' }}</p>
                                    </div>
                                    <div>{{ $product->quantity }} шт</div>
                                    <div>{{ $product->total }} грн</div>
                                    </div>
                                    @endforeach
                                <!-- </div> -->
                                <div class="slide_up_orders"></div>
                            </div>
                            @endforeach
                            <div class="slide_up_orders">@lang('account_user_info.less') <img src="/img/main/arrow_down.svg" alt=""></div>
                        </div>
                    </div>
                    @endforeach
                    </div>
                    </div>
                    <div class="tabs__content tab_wishlist">
                        <div class="wrap_account_wishlist">
                        @foreach($cats as $cat)    
                        <div class="one_wishlist_account">
                            <div class="name_category_click">
                                <p>{{ $cat->$cat_title }} (@lang('account_user_info.products'): {{ count($products[$cat->id]) }})</p>
                            </div>
                            <div class="product_wishlist_line">
                                <div class="line_other_product">
                                    @foreach($products[$cat->id] as $product)
                                    <div class="one_product_line">
                                        <div class="nameplates">
                                            @if($product['is_sale'])<div class="red_nameplate">Sale</div>@endif
                                            @if($product['is_new'])<div class="red_nameplate">New</div>@endif
                                        </div>

                                        <div class="wishlist_compare">
                                            <a href="" class="add_compare" onclick="compare.add({{ $product['product_id'] }}); return false;"><img src="img/main/icon_compare_blue.svg" alt=""><div class="ripple-parent">
                                            <div class="ripple"></div>
                                        </div></a>
                                            <a href="#" class="add_wishlist" onclick="addWishlist({{ $product['product_id'] }}, {{ Auth::id() ?? 0 }}); setTimeout(location.reload(), 1000); return false;"><img src="/img/main/icon_wishlist_blue.svg" alt="">
                                            <div class="ripple-parent">
                                                    <div class="ripple"></div>
                                                </div></a>
                                        </div>
                                        <img src="{{ $product['image'] ?? '/img/main/default-product.jpg' }}" alt="">
                                        <p>{{ $product[$pr_title] }}</p>
                                        <div class="star_reviews">
                                            <div class="line_star">
                                                <img src="@if($product['rating'] >= 1)/img/main/star_yellow.svg @else /img/main/empty_star.svg @endif" alt="">
                                <img src="@if($product['rating'] >= 2)/img/main/star_yellow.svg @else /img/main/empty_star.svg @endif" alt="">
                                <img src="@if($product['rating'] >= 3)/img/main/star_yellow.svg @else /img/main/empty_star.svg @endif" alt="">
                                <img src="@if($product['rating'] >= 4)/img/main/star_yellow.svg @else /img/main/empty_star.svg @endif" alt="">
                                <img src="@if($product['rating'] >= 5)/img/main/star_yellow.svg @else /img/main/empty_star.svg @endif" alt="">
                                            </div>
                                            <div class="count_reviews">
                                                {{ $product['reviews'] }}
                                                <img src="/img/main/icon_review.svg" alt="">
                                            </div>
                                        </div>
                                        <div class="line_price_addcart">
                                            <div class="left_price">
                                                @if($product['is_sale'])<p class="old_price">{{ (int)$product['price'] }} грн</p>@endif
                                                <p class="red_price">{{ (int)$product['price'] }} грн</p>
                                            </div>
                                        </div>
                                        <a href="{{ route('product', ['id' => $product['id']]) }}" class="add_cart">Выбрать поставщика</a>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>


                        
                    </div>
                    <div class="tabs__content">
                    	<div class="wrap_account_review">
                        <div class="one_review_account">
                        <div class="name_category_click">
                            <p>Отзывы о товарах (Отзывов: {{ count($reviews) }})</p>
                        </div>
                        <div class="account_my_reviews">
                            <div class="reviews_about_company">
                                @foreach($reviews as $review)
                                <div class="one_wrap_toggle_review" id="company-{{ $vendor['id'] }}" id="company-{{ $review['id'] }}">

                                    <p class="name_reviews_product">{{ $review['products'][$pr_title] }} <span>(Отзывов: {{ count($review['reviews']) }})</span></p>
                                    @foreach($review['reviews'] as $one_review)
                                    <div class="one_line_review_wrap" id="productreview-{{ $one_review->id }}">
                                        <div class="left_reviews_date_like_dislike">
                                            <p class="date_rvs">{{ $one_review->created_at }}</p>
                                            <div class="line_like_dislike">
                                                <div class="like">
                                                    <img src="/img/main/icon_like.svg" alt="">
                                                    <span>{{ $one_review->likes }}</span>
                                                </div>
                                                <div class="dislike">
                                                    <img src="/img/main/icon_dislike.svg" alt="">
                                                    <span>{{ $one_review->dislikes }}</span>
                                                </div>
                                            </div>
                                            <a href="" class="remove_review" onclick="deleteProductReview({{ $one_review->id }}, {{ $review['products']['id'] }}); return false;">
                                                <img src="/img/main/red_trash.svg" alt="">
                                            </a>
                                        </div>
                                        <div class="one_review_on_page">
                                            <div class="line_dignity_limitations">
                                                <div class="dignity_block">
                                                    <div>
                                                        <div class="icon_dd_border">
                                                            +
                                                        </div>
                                                        <p>Достоинства:</p>
                                                    </div>
                                                    <p>{{ $one_review->positives }}</p>
                                                </div>
                                                <div class="limitations_block">
                                                    <div>
                                                        <div class="icon_dd_border">
                                                            -
                                                        </div>
                                                        <p>Недостатки:</p>
                                                    </div>
                                                    <p>{{ $one_review->negatives }}</p>
                                                </div>
                                            </div>
                                            <div class="info_stars_user">
                                                <div class="info_about_review">
                                                    <span>i</span>
                                                    Отзыв:
                                                </div>
                                                <div class="star_rvs">
                                                    @if($one_review->rating >=1)<img src="/img/main/star_yellow.svg" alt="">@else<img src="/img/main/empty_star.svg" alt="">@endif
                                                    @if($one_review->rating >=2)<img src="/img/main/star_yellow.svg" alt="">@else<img src="/img/main/empty_star.svg" alt="">@endif
                                                    @if($one_review->rating >=3)<img src="/img/main/star_yellow.svg" alt="">@else<img src="/img/main/empty_star.svg" alt="">@endif
                                                    @if($one_review->rating >=4)<img src="/img/main/star_yellow.svg" alt="">@else<img src="/img/main/empty_star.svg" alt="">@endif
                                                    @if($one_review->rating >=5)<img src="/img/main/star_yellow.svg" alt="">@else<img src="/img/main/empty_star.svg" alt="">@endif
                                                </div>
                                            </div>

                                            <p>{{ $one_review->text }}</p>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>


                    <div class="one_review_account ">
                        <div class="name_category_click">
                            <p>Отзывы о компаниях (Компаний: {{ count($vendor_reviews) }})</p>
                        </div>

                        <div class="account_my_reviews">
                            <div class="reviews_about_company">
                                @foreach($vendor_reviews as $vendor)
                                <div class="one_wrap_toggle_review rewiews_about_company_click" id="company-{{ $vendor['id'] }}">
                                    <p class="name_reviews_product">{{ $vendor['vendors']['name'] }} <span>( Отзывов: {{ count($vendor['reviews']) }})</span></p>
                                    <div class="review_about_company_slide">
                                    	@foreach($vendor['reviews'] as $review)
                                        <div class="one_line_review_wrap" id="vendorreview-{{ $review->id }}">
                                            <div class="left_reviews_date_like_dislike">
                                                <p class="date_rvs">{{ $review->created_at }}</p>
                                                <div class="line_like_dislike">
                                                    <div class="like">
                                                        <img src="/img/main/icon_like.svg" alt="">
                                                        <span>{{ $review->likes }}</span>
                                                    </div>
                                                    <div class="dislike">
                                                        <img src="/img/main/icon_dislike.svg" alt="">
                                                        <span>{{ $review->dislikes }}</span>
                                                    </div>
                                                </div>
                                                <a href="#" class="remove_review" onclick="deleteVendorReview({{ $review->id }}, {{ $vendor['id'] }}); return false;">
                                                    <img src="/img/main/red_trash.svg" alt="">
                                                </a>
                                            </div>
                                            <div class="one_review_on_page">
                                                <div class="info_stars_user">
                                                    <div class="info_about_review">
                                                        <span>i</span>
                                                        Отзыв:
                                                    </div>
                                                    <div class="star_rvs">
                                                        @if($review->rating >=1)<img src="/img/main/star_yellow.svg" alt="">@else<img src="/img/main/empty_star.svg" alt="">@endif
                                                        @if($review->rating >=2)<img src="/img/main/star_yellow.svg" alt="">@else<img src="/img/main/empty_star.svg" alt="">@endif
                                                        @if($review->rating >=3)<img src="/img/main/star_yellow.svg" alt="">@else<img src="/img/main/empty_star.svg" alt="">@endif
                                                        @if($review->rating >=4)<img src="/img/main/star_yellow.svg" alt="">@else<img src="/img/main/empty_star.svg" alt="">@endif
                                                        @if($review->rating >=5)<img src="/img/main/star_yellow.svg" alt="">@else<img src="/img/main/empty_star.svg" alt="">@endif
                                                    </div>
                                                </div>

                                                <p>{{ $review->review }}</p>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    </div>
                    </div>
                    <div class="tabs__content">
                        <div class="wrap_account_orders">
                        @foreach($orders as $order)
                        <div class="one_wrap_orders">
                        <div class="line_order_header">
                            <div>
                                <p class="mini_title">@lang('account_company_info.order')</p>
                                <p>@lang('account_company_info.order') № {{ $order['order_id'] }} от {{ $order['order_time'] }}</p>
                            </div>
                            <div>
                                <p class="mini_title">@lang('account_company_info.status')</p>
                                <p>{{ $order[$pr_title] }}</p>
                            </div>
                            <div>
                                <p class="mini_title">@lang('account_company_info.payment')</p>
                                <p>@if($order['payment'] == 'cod')
                                        @if($local == 'ru')
                                        Наложенный платеж
                                        @elseif($local == 'uk')
                                        Післяплата
                                        @else
                                        {{ $order['payment'] }}
                                        @endif
                                        @elseif($order['payment'] == 'agreement')
                                        @if($local == 'ru')
                                        Оплата по договору
                                        @elseif($local == 'uk')
                                        Оплата згідно договору
                                        @else
                                        {{ $order['payment'] }}
                                        @endif
                                        @elseif($order['payment'] == 'liqpay')
                                        Visa/Mastercard(Liqpay)
                                        @elseif($order['payment'] == 'bonus')
                                        Оплата бонусами
                                        @endif</p> 
                            </div>
                            <div>
                                <p class="mini_title">@lang('account_company_info.order_total')</p>
                                <p>{{ (int)$order['total'] }} грн</p>
                            </div>
                            <div>
                                <p class="mini_title">Стоимость доставки</p>
                                <p>{{ (int)$order['shipping_price'] }} грн</p>
                            </div>
                            <div>
                                <p class="click_down_orders">@lang('account_company_info.more') <img src="/img/main/arrow_down.svg" alt=""></p>
                            </div>
                        </div>
                        <div class="toggle_slide_orders_details">
                            @foreach($order['vendors'] as $vendor)
                            <div class="line_column_orders">
                                <div class="top_info_orders">
                                    <div>
                                        <p class="mini_title">@lang('account_user_info.seller')</p>
                                        <p>{{ $vendor['name'] }}</p>
                                    </div>
                                    <div>
                                        <p class="mini_title">@lang('account_company_info.shipping')</p>
                                        <p>@if($vendor['delivery'] == 'inshop')
                                        @if($local == 'ru')
                                        Самовывоз
                                        @elseif($local == 'uk')
                                        Самовивіз
                                        @else
                                        {{ $vendor['delivery'] }}
                                        @endif
                                        @elseif($vendor['delivery'] == 'outshop')
                                        @if($local == 'ru')
                                        Доставка поставщиком
                                        @elseif($local == 'uk')
                                        Доставка постачальником
                                        @else
                                        {{ $vendor['delivery'] }}
                                        @endif
                                        @elseif($vendor['delivery'] == 'ourshipping')
                                        Доставка Pomogaika
                                        @endif</p>
                                    </div>
                                    <div>
                                        <a href="" class="link_add_review" data-vendor="{{ $vendor['user_id'] }}">@lang('account_user_info.send_review')</a>
                                    </div>
                                </div>
                                @foreach($vendor['products'] as $product)
                                <div class="bottom_info_orders">
                                    <div>
                                        <img src="{{ $product->image ?? '/img/main/default-product.jpg' }}" alt="">
                                    </div>
                                    <div>
                                        <p>{{ $product->$pr_title ?? '' }}</p>
                                    </div>
                                    <div>{{ $product->quantity }} шт</div>
                                    <div>{{ $product->total }} грн</div>
                                </div>
                                @endforeach
                                <div class="slide_up_orders"></div>
                            </div>
                            @endforeach
                            <div class="slide_up_orders">@lang('account_user_info.less') <img src="/img/main/arrow_down.svg" alt=""></div>
                        </div>
                    </div>
                    @endforeach
                    </div>
                    </div> 
                    </div>  
                </div>
            </form>
        </div>
    </div>

 
    <div class="overlay"></div>
    <div class="modal_block text_hint">
        <div class="close_modal"></div>
        <p class="append_text"></p>
    </div>

    <div class="modal_block change_password">
        <div class="close_modal"></div>
        <p class="name_modal">@lang('account_company_info.change_password')</p>
        <form action="{{ route('changepassword') }}" method="post">
            {{ csrf_field() }}
            <div class="one_section_inputs">
                <div>
                    <p>@lang('account_company_info.old_password'):</p>
                    <input type="password" name="current_password">
                </div>
                <div>
                    <p>@lang('account_company_info.new_password'):</p>
                    <input type="password" name="new_password">
                </div>
                <div>
                    <p>@lang('account_company_info.confirm_password'):</p>
                    <input type="password" name="new_password_confirmation">
                </div>
            </div>
            <button class="blue_button_link" type="submit"><img src="/img/main/save_form_account.svg" alt=""> @lang('account_company_info.to_change_password')</button>
        </form>
    </div>

    <div class="modal_block create_reviews">
        <div class="close_modal"></div>
        <p class="name_modal">Оставить отзыв</p>
        <form action="{{ route('addvendorreview') }}" method="post">
            <input type="hidden" name="vendor_id" id="vendor_id" value="">
            {{ csrf_field() }}
            <div class="one_section_inputs">
                <div>
                    <p class="reviews_title">Отзыв</p>
                    <input type="text" name="review">
                </div>
                <div class="rating_options stars">
                    <input type="radio" name="sociability" id="one" value="1">
                    <label for="one"></label>
                    <input type="radio" name="sociability" id="two" value="2">
                    <label for="two"></label>
                    <input type="radio" name="sociability" id="three" value="3">
                    <label for="three"></label>
                    <input type="radio" name="sociability" id="four" value="4">
                    <label for="four"></label>
                    <input type="radio" name="sociability" id="five" value="5">
                    <label for="five"></label>
                </div>
            </div>
            <button type="submit" class="blue_button_link"><img src="/img/main/note.svg" alt=""> Отправить</button>
        </form>
    </div>
@endsection
