@php
$cat_title = 'title_' . $local;
$tag_l = 'tag_' . $local;
$name = 'name_' . $local;
$des = 'description_' . $local;
@endphp
@extends('layouts.app')

@section('content')
<div class="content">
	<div class="breadcrumbs_container">
            <div class="container">
                <div class="breadcrumbs">
                    <div class="show_overlay_hover"></div>
                    <div class="link_dropdown_menu">
                        <button class="blue_button_link" onclick="toggleNavbar();">@lang('account_company_info.catalog') <img src="/img/main/line_toggle_cat.svg" alt=""></button>
                        <div class="a_side_category" id="a_side_category">
                            <div class="link_cats_a_side">
                                @foreach($categories as $category)
                                <div class="wrap_link_category">
                                    <a href="{{ route('category', ['id' => $category->id]) }}"><img src="{{ $category->icon ?? '/img/main/image_cat1.svg' }}" alt="">{{ $category->$cat_title }}</a>
                                    <div class="wrap_submenu_section">
                                        @foreach($category->children as $category_list)
                                         <div class="one_catalog_submenu_line">
                                            <img src="{{ $category_list->image ?? '/img/main/icon_cats1.jpg' }}" alt="">
                                            <div class="list_link_submenu_name_catalog">
                                                <p class="name_cat_section">{{ $category_list->$cat_title }}</p>
                                                <div class="line_link_cat nav-menu">
                                                    @foreach($category_list->children as $catlist2)
                                                    <a href="{{ route('category', ['id' => $catlist2->id]) }}">{{ $catlist2->$cat_title }}</a>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                                @endforeach
                            </div>
                            <a href="{{ route('categories') }}" class="all_category_link blue_button_link"><img src="/img/main/line_toggle_cat.svg" alt=""> @lang('category.all_categories')</a>
                        </div>
                    </div>

                    <div class="breadcrumbs_link">
                        <a href="{{ route('main') }}">@lang('account_user_info.main')</a>
                        <a href="{{ route('categories') }}">@lang('category.catalog')</a>
                        <span>@lang('news.news')</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <a href="" class="image_link_add">
                <img src="/img/main/rel3.jpg" alt="">
            </a>
            <div class="wrap_static_page">
                <p class="title_news_block">@lang('news.info')</p>
                <div class="left_static_block">
                    <p class="mini_title_news">@lang('news.article')</p>
                    @foreach($newslist as $news)
                    <div class="one_news_catalog">
                        <div class="mini_image_news">
                            <img src="{{ $news->picture ?? '/img/main/news_mini_image1.jpg' }}" alt="">
                        </div>
                        <div class="top_line_news_mini">
                            <div class="count_views">
                                {{ $news->views }}
                                <img src="/img/main/icon_views.svg" alt="">
                            </div>
                            <p class="date_mini_news">{{ $news->created_at }}</p>
                            <div class="line_tags">
                                @php
                                $i = 0;
                                @endphp
                                @foreach($news->tags as $tag)
                                <div class="one_tags @if($i = 0) active @endif">{{ $tag->$tag_l }}</div>
                                @php
                                $i++;
                                @endphp
                                @endforeach
                            </div>
                        </div>
                        <div class="title_one_mini_news">{{ $news->$name }}</div>
                        <div>{{ substr(strip_tags($news->$des), 0, 100) . '...' }}</div>
                        <div class="right_link_more">
                            <a href="{{ route('news', $news->id) }}">@lang('news.more')</a>
                        </div>
                    </div>
                    @endforeach
                </div>    
                <div class="right_static_block">
                    <p class="mini_title_news">@lang('news.search')</p>
                    <div class="mobile_tab_tag_form">
                        <div class="tab_form_tag active"><img src="/img/main/icon_search.svg" alt=""> @lang('news.article_search')</div>
                        <div class="tab_form_tag"><span>#</span> @lang('news.tag_search')</div>
                    </div>
                    <div class="from_tab_wrap">
                        <div class="form_search active">
                            <p>@lang('news.article_search')</p>
                            <form action="/search/news" method="get">
                                <input type="text" name="search" value="{{ $get['search'] }}">
                                <button type="submit"><img src="/img/main/icon_search.svg" alt=""></button>
                            </form>
                        </div>
                        <div class="line_tags">
                            <p>@lang('news.tag_search')</p>
                             @php
                                $i = 0;
                                @endphp
                                @foreach($tags as $tag)
                                <div class="one_tags @if($i = 0) active @endif">{{ $tag->$tag_l }}</div>
                                @php
                                $i++;
                                @endphp
                                @endforeach
                        </div>
                    </div>
                </div>
                <ul class="pagination">
                    {{ $newslist->links() }}
                </ul>
            </div>
        </div>
</div>
@endsection