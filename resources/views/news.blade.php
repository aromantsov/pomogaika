@php
$cat_title = 'title_' . $local;
$tag_l = 'tag_' . $local;
$name = 'name_' . $local;
$des = 'description_' . $local;
@endphp
@extends('layouts.app')

@section('content')
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v10.0" nonce="2hTutCUS"></script>
<div class="content">
	<div class="breadcrumbs_container">
            <div class="container">
                <div class="breadcrumbs">
                    <div class="show_overlay_hover"></div>
                    <div class="link_dropdown_menu">
                        <button class="blue_button_link" onclick="toggleNavbar();">@lang('account_company_info.catalog') <img src="//img/main/line_toggle_cat.svg" alt=""></button>
                        <div class="a_side_category" id="a_side_category">
                            <div class="link_cats_a_side">
                                @foreach($categories as $category)
                                <div class="wrap_link_category">
                                    <a href="{{ route('category', ['id' => $category->id]) }}"><img src="{{ $category->icon ?? '//img/main/image_cat1.svg' }}" alt="">{{ $category->$cat_title }}</a>
                                    <div class="wrap_submenu_section">
                                        @foreach($category->children as $category_list)
                                         <div class="one_catalog_submenu_line">
                                            <img src="{{ $category_list->image ?? '//img/main/icon_cats1.jpg' }}" alt="">
                                            <div class="list_link_submenu_name_catalog">
                                                <p class="name_cat_section">{{ $category_list->$cat_title }}</p>
                                                <div class="line_link_cat nav-menu">
                                                    @foreach($category_list->children as $catlist2)
                                                    <a href="{{ route('category', ['id' => $catlist2->id]) }}">{{ $catlist2->$cat_title }}</a>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                                @endforeach
                            </div>
                            <a href="/categories" class="all_category_link blue_button_link"><img src="//img/main/line_toggle_cat.svg" alt=""> @lang('category.all_categories')</a>
                        </div>
                    </div>

                    <div class="breadcrumbs_link">
                        <a href="/">@lang('account_user_info.main')</a>
                        <a href="/categories">@lang('category.catalog')</a>
                        <span>{{ $news->$name }}</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <a href="" class="image_link_add">
                <img src="/img/main/rel3.jpg" alt="">
            </a>
            <div class="wrap_static_page">
                <p class="title_news_block">@lang('news.info')</p>
                <div class="left_static_block">
                    <p class="mini_title_news">@lang('news.article')</p>
                    <div class="one_news_page">
                        <div class="mini_image_news_page">
                            <img src="{{ $news->picture ?? '/img/main/news_mini_image1.jpg' }}" alt="">
                        </div>
                        <div class="top_line_news_mini">
                            <div class="count_views">
                                {{ $news->views }}
                                <img src="/img/main/icon_views.svg" alt="">
                            </div>
                            <p class="date_mini_news">{{ $news->created_at }}</p>
                            <div class="line_tags">
                                @php
                                $i = 0;
                                @endphp
                                @foreach($newstags as $tag)
                                <div class="one_tags @if($i = 0) active @endif">{{ $tag->$tag_l }}</div>
                                @php
                                $i++;
                                @endphp
                                @endforeach
                            </div>
                        </div>
                        <p class="title_one_mini_news">{{ $news->$name }}</p>
                        <div>{!!  htmlspecialchars_decode($news->$des) !!}</div>
                        <div class="social_tags_return">
                            <div class="social_tag_share">
                                @lang('news.post')
                                <!-- <a href=""><img src="/img/main/icon_facebook.svg" alt=""></a> -->
                                <div class="" data-href="http://helper.dimarvel.com/news/{{ $news->id }}" data-layout="" data-size=""><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fhelper.dimarvel.com%2Fnews%2F{{ $news->id }}&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore"><img src="/img/main/icon_facebook.svg" alt=""></a></div>
                                <!-- <a href=""><img src="/img/main/icon_tvitter.svg" alt=""></a>
                                <a href=""><img src="/img/main/icon_telegram.svg" alt=""></a> -->
                            </div>
                            <a href="{{ route('newslist') }}">@lang('news.back')</a>
                        </div>
                    </div>
                    <div class="similar_news">
                        @php
                        $i = 0;
                        @endphp
                        @foreach($similar as $s_news)
                        <div class="one_similar_news">
                            <div class="image_similar_news">
                                <img src="{{ $s_news->picture ?? '/img/main/news_mini_image1.jpg' }}" alt="">
                            </div>
                            <div class="text_similar_news">
                                <p class="title_one_mini_news">{{ $s_news->$name }}</p>
                                <p style="width: 100%;">{!! substr(htmlspecialchars_decode($s_news->$des), 0, 100) . '...' !!}</p>
                                <div class="line_watch_more">
                                    <div class="count_views">
                                        {{ $s_news->views }}
                                        <img src="/img/main/icon_views.svg" alt="">
                                    </div>
                                    <a href="{{ route('news', $s_news->id) }}">@lang('news.more')</a>
                                </div>
                            </div>
                        </div>
                        @php
                        $i++;
                        if(@i < 1) break;
                        @endphp
                        @endforeach
                    </div>
                </div>
                <div class="right_static_block">
                    <p class="mini_title_news">@lang('news.search')</p>
                    <div class="mobile_tab_tag_form">
                        <div class="tab_form_tag active"><img src="/img/main/icon_search.svg" alt=""> @lang('news.article_search')</div>
                        <div class="tab_form_tag"><span>#</span> @lang('news.tag_search')</div>
                    </div>
                    <div class="from_tab_wrap">
                        <div class="form_search active">
                            <p>@lang('news.article_search')</p>
                            <form action="/search/news" method="get">
                                <input type="text" name="search">
                                <button type="submit"><img src="/img/main/icon_search.svg" alt=""></button>
                            </form>
                        </div>
                        <div class="line_tags">
                            <p>@lang('news.article_search')</p>
                                @php
                                $i = 0;
                                @endphp
                                @foreach($tags as $tag)
                                <div class="one_tags @if($i = 0) active @endif">{{ $tag->$tag_l }}</div>
                                @php
                                $i++;
                                @endphp
                                @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>

</div>
@endsection