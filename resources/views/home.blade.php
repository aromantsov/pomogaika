@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">@lang('auth.dashboard')</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @lang('auth.login')!
                </div>
                @if(($role == 1) || ($role == 2))
                <div><a href="{{ route('admin.index') }}">Административная панель</a></div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
