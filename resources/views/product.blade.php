@php
use App\User;
use App\Models\VendorReview;
$cat_title = 'title_' . $local;
$pr_title = 'name_' . $local; 
$prop = 'value_' . $local; 
$des = 'description_' . $local; 
@endphp
@extends('layouts.app')

@section('content')
<div class="content">
        <div class="breadcrumbs_container">
            <div class="container">
                <div class="breadcrumbs">
                    <div class="show_overlay_hover"></div>
                    <div class="link_dropdown_menu">
                        <button class="blue_button_link" onclick="toggleNavbar();">@lang('account_company_info.catalog') <img src="/img/main/line_toggle_cat.svg" alt=""></button>
                        <div class="a_side_category" id="a_side_category">
                            <div class="link_cats_a_side">
                                @foreach($categories as $category)
                                <div class="wrap_link_category">
                                    <a href="{{ route('category', ['id' => $category->id]) }}"><img src="{{ $category->icon ?? '/img/main/image_cat1.svg' }}" alt="">{{ $category->$cat_title }}</a>
                                    <div class="wrap_submenu_section">
                                        @foreach($category->children as $category_list)
                                         <div class="one_catalog_submenu_line">
                                            <img src="{{ $category_list->image ?? '/img/main/icon_cats1.jpg' }}" alt="">
                                            <div class="list_link_submenu_name_catalog">
                                                <p class="name_cat_section">{{ $category_list->$cat_title }}</p>
                                                <div class="line_link_cat nav-menu">
                                                    @foreach($category_list->children as $catlist2)
                                                    <a href="{{ route('category', ['id' => $catlist2->id]) }}">{{ $catlist2->$cat_title }}</a>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                                @endforeach
                            </div>
                            <a href="{{ route('categories') }}" class="all_category_link blue_button_link"><img src="/img/main/line_toggle_cat.svg" alt=""> @lang('category.all_categories')</a>
                        </div>
                    </div>

                    <div class="breadcrumbs_link">
                        <a href="{{ route('main') }}">@lang('account_user_info.main')</a>
                        <a href="{{ route('categories') }}">@lang('category.catalog')</a>
                        <span>{{ $product->$pr_title }}</span>
                    </div>
                </div>
            </div>
        </div>
        @if (Session::has('error'))
        <div class="alert alert-danger">{{ Session::get('error') }}</div>
        @endif
        <div class="alert alert-success" style="display: none;"></div>
        <div class="alert alert-danger" style="display: none;"></div>
        <div class="container">
            <div class="product_card">
                <div class="top_card_info">
                    <div class="left_card_product">
                        <p class="name_product_in_card">{{ $product->$pr_title }}</p>
                        <div class="rating_reviews_link">
                            <div>
                                <img src="@if($rating >= 1)/img/main/star_yellow.svg @else /img/main/empty_star.svg @endif" alt="">
                                <img src="@if($rating >= 2)/img/main/star_yellow.svg @else /img/main/empty_star.svg @endif" alt="">
                                <img src="@if($rating >= 3)/img/main/star_yellow.svg @else /img/main/empty_star.svg @endif" alt="">
                                <img src="@if($rating >= 4)/img/main/star_yellow.svg @else /img/main/empty_star.svg @endif" alt="">
                                <img src="@if($rating >= 5)/img/main/star_yellow.svg @else /img/main/empty_star.svg @endif" alt="">
                            </div>
                            <div>
                                <span>{{ $reviews_count ?? 0 }}</span>
                                <img src="/img/main/icon_review_blue.svg" alt="">
                                <a href="#" id="add_review">@lang('product.send_review')</a>
                            </div>
                        </div>
                    </div>
                    <div class="right_card_product">
                        @if($product['bonus'])<div class="red_price">+ {{ $product['bonus'] }} бонусных грн</div>@endif 
                        <div><span>Арт:</span> {{ $product->model }}</div>
                        <a href="#" class="add_compare" onclick="compare.add('{{ $product->id }}'); return false;"><img src="/img/main/icon_compare_blue.svg" alt=""></a>
                        <a href="#" class="add_wishlist" onclick="addWishlist({{ $product->id }}, {{ Auth::id() ?? 0 }})"><img src="/img/main/icon_wishlist_blue.svg" alt="">
                        <div class="ripple-parent">
                                                        <div class="ripple"></div>
                                                    </div></a>
                    </div>
                </div>
                <div class="tabs">
                <div class="blue_tab_link">
                    <ul class="tabs__caption">
                        <li class="active">@lang('product.prices')</li>
                        <li>@lang('product.attributes')</li>
                        <li>@lang('product.reviews')</li>
                        <li>@lang('product.sp')</li>
                        <!-- <li>@lang('product.similar')</li> -->
                    </ul>
                </div>
                <div class="image_poroducts_detais">
                    <div class="left_slider_product">
                    	<!--- Таб -->
                        <div class="tabs__content xzoom-container active">
                            <img class="xzoom" id="xzoom-default" src="{{ $product->image ?? '/img/main/default-product.jpg' }}" xoriginal="{{ $product->image ?? '/img/main/product_slider_image.jpg' }}" />
                            <div class="xzoom-thumbs">
                                @foreach($pictures as $picture)
                                <a href="{{ $product->image }}"><img class="xzoom-gallery" width="80" src="{{ $product->image }}" xpreview="{{ $product->image }}" title=""></a>
                                @endforeach
                            </div>
                            @if($product->video)
                            <div class="video">
                                <iframe width="100%" src="https://www.youtube.com/embed/{{ $product->video }}" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                            @endif
                        </div>
                        <!-- Конец таба -->
                        <div class="tabs__content characteristics">
                            <p class="title_characteristics">@lang('product.attributes'):</p>
                            <div class="line_characteristics">
                                <p>Производитель</p>
                                <span>{{ $product->manufacturers->brand ?? '' }}</span>
                            </div>
                            @foreach($properties as $property)
                            <div class="line_characteristics">
                                <p>{{ $property->attributes->$pr_title }}</p>
                                <span>{{ $property->$prop }}</span>
                            </div>
                            @endforeach
                            <div>{!! htmlspecialchars_decode($product->$des) !!}</div>
                        </div>
                        <!-- Конец второго таба -->
                        <div class="tabs__content list_reviews_page">
                            <p class="title_reviews_on_page">@lang('product.product_reviews'):</p>
                            @foreach($reviews as $review)
                            <div class="one_review_on_page">
                                <div class="avatar_stars_user">
                                    <div class="avatar_user">
                                        <img src="/img/main/image_company.png" alt="">
                                    </div>
                                    <div class="star_reviews_name">
                                        <p class="date_rvs">{{ $review->created_at }}</p>
                                        <p class="name_rvs">{{ $review->username }}</p>
                                        <div class="star_rvs">
                                            <img src="@if($review->rating >= 1)/img/main/star_yellow.svg @else /img/main/empty_star.svg @endif" alt="">
                                            <img src="@if($review->rating >= 2)/img/main/star_yellow.svg @else /img/main/empty_star.svg @endif" alt="">
                                            <img src="@if($review->rating >= 3)/img/main/star_yellow.svg @else /img/main/empty_star.svg @endif" alt="">
                                            <img src="@if($review->rating >= 4)/img/main/star_yellow.svg @else /img/main/empty_star.svg @endif" alt="">
                                            <img src="@if($review->rating >= 5)/img/main/star_yellow.svg @else /img/main/empty_star.svg @endif" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="line_dignity_limitations">
                                    <div class="dignity_block">
                                        <div>
                                            <div class="icon_dd_border">
                                                +
                                            </div>
                                            <p>@lang('product.positives'):</p>
                                        </div>
                                        <p>{{ $review->positives }}</p>
                                    </div>
                                    <div class="limitations_block">
                                        <div>
                                            <div class="icon_dd_border">
                                                -
                                            </div>
                                            <p>@lang('product.negatives'):</p>
                                        </div>
                                        <p>{{ $review->negatives }}</p>
                                    </div>
                                </div>
                                <div class="info_about_review">
                                    <span>i</span>
                                    @lang('product.review'):
                                </div>
                                <p>{{ $review->text }}</p>
                                <div class="line_like_dislike_link">
                                    <div class="line_like_dislike">
                                        <div class="like" onclick="p_like({{ $review->id }});">
                                            <img src="/img/main/icon_like.svg" alt="">
                                            <span id="plike-{{ $review->id }}">{{ $review->likes }}</span>
                                        </div>
                                        <div class="dislike" onclick="p_dislike({{ $review->id }});">
                                            <img src="/img/main/icon_dislike.svg" alt="">
                                            <span id="pdislike-{{ $review->id }}">{{ $review->dislikes }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            
                            
                            
                       
                        </div>
                        <!-- Конец третьего таба  -->
                         <div class="tabs__content delivery_description">
                            <p class="title_reviews_on_page">@lang('product.sp'):</p>
                            <div>
                                <div class="line_icon_text_dp">
                                    <div>
                                        <img src="/img/main/gray_p.svg" alt="">
                                    </div>
                                    <p>@lang('product.payment_conditions'):</p>
                                </div>
                                <div>
                                    @lang('product.sp_description')
                                </div>
                            </div>
                            <div>
                                <div class="line_icon_text_dp">
                                    <div>
                                        <img src="/img/main/gray_d.svg" alt="">
                                    </div>
                                    <p>@lang('product.shipping_conditions'):</p>
                                </div>
                                <div>
                                    @lang('product.sp_description')
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="right_info_suggestions">
                        <div class="count_sort">
                            <p class="count_suggestions">@lang('product.offers'): {{ count($offers) }}</p>
                            <div class="sort_select_text">
                                <p>@lang('product.sort'):</p>
                                <select class="sort_select" name="" id="sort_select">
                                    <option value="price_asc" @if($get['sort_by'] == 'price_asc') selected @endif>@lang('product.by_price_asc')</option>
                                    <option value="price_desc" @if($get['sort_by'] == 'price_desc') selected @endif>@lang('product.by_price_desc')</option>
                                    <option value="popular" @if($get['sort_by'] == 'popular') selected @endif>@lang('product.by_popular')</option>
                                </select>
                            </div>
                        </div>
                        <div class="suggestions_company">
                            <!----------------------------------Предложение товара---------------------------------------------->
                            @foreach($offers as $offer)
                            @php
                            $rating = (int)VendorReview::where('vendor_id', $offer->users->id)->avg('rating');
                            $reviews = VendorReview::with('users')->where('vendor_id', $offer->users->id)->get();
                            @endphp
                            <div class="one_suggestion_company">
                                <p class="name_suggestion">{{ $offer->users->name }}</p>
                                <div class="rating_reviews_link">
                                    <div>
                                        <img src="@if ($rating >= 1)/img/main/star_yellow.svg @else /img/main/empty_star.svg @endif" alt="">
                                        <img src="@if ($rating >= 2)/img/main/star_yellow.svg @else /img/main/empty_star.svg @endif" alt="">
                                        <img src="@if ($rating >= 3)/img/main/star_yellow.svg @else /img/main/empty_star.svg @endif" alt="">
                                        <img src="@if ($rating >= 4)/img/main/star_yellow.svg @else /img/main/empty_star.svg @endif" alt="">
                                        <img src="@if ($rating >= 5)/img/main/star_yellow.svg @else /img/main/empty_star.svg @endif" alt="">
                                    </div>
                                    <div>
                                        <span>{{ $rating }}</span>
                                        <img src="/img/main/icon_review_blue.svg" alt="">
                                        <a href="#" class="read_slide_review">@lang('product.read_reviews')</a>
                                    </div>
                                </div>
                                <!----------------------------блок с отзывами-->
                                <div class="hide_slide_reviews">
                                    <div class="wrap_slide_review">
                                        @foreach($reviews as $review)
                                        <!--------------------------------------один отзыв-->
                                        <div class="one_review_block">
                                            <div class="date_star_review">
                                                <div>
                                                    <span>{{ $review->created_at }}</span>
                                                    <p>{{ $review->users->name . ' ' . $review->users->lastname }}</p>
                                                </div>
                                                <div>
                                                    <img src="@if ($review->rating >= 1)/img/main/star_yellow.svg @else /img/main/empty_star.svg @endif" alt="">
                                                    <img src="@if ($review->rating >= 2)/img/main/star_yellow.svg @else /img/main/empty_star.svg @endif" alt="">
                                                    <img src="@if ($review->rating >= 3)/img/main/star_yellow.svg @else /img/main/empty_star.svg @endif" alt="">
                                                    <img src="@if ($review->rating >= 4)/img/main/star_yellow.svg @else /img/main/empty_star.svg @endif" alt="">
                                                    <img src="@if ($review->rating >= 5)/img/main/star_yellow.svg @else /img/main/empty_star.svg @endif" alt="">
                                                </div>
                                            </div>
                                            <div class="info_about_review">
                                                <span>i</span>
                                                Отзыв:
                                            </div>
                                            <p>{{ $review->review }}</p>
                                            <div class="line_like_dislike_link">
                                                <div class="line_like_dislike">
                                                    <div class="like" onclick="v_like({{ $review->id }});">
                                                        <img src="/img/main/icon_like.svg" alt="">
                                                        <span id="vlike-{{ $review->id }}">{{ $review->likes }}</span>
                                                    </div>
                                                    <div class="dislike" onclick="v_dislike({{ $review->id }});">
                                                        <img src="/img/main/icon_dislike.svg" alt="">
                                                        <span id="vdislike-{{ $review->id }}">{{ $review->dislikes }}</span>
                                                    </div>
                                                </div>
                                                <!-- <a href="">@lang('product.more')</a> -->
                                            </div>
                                        </div>
                                        @endforeach
                                        <!--------------------------------------!один отзыв!-->
                                    </div>
                                </div>
                                <!--------------------------------!блок с отзывами!-->
                                <div class="suggestion_product_description_cart">
                                    <span>{{ $product->$pr_title }}</span>
                                    <div>{{ (int)$offer->price }} грн @if(($visitor == null) || ($visitor->user_role_id != 3))<a href="#" class="add_cart" id="button-cart-{{ $offer->id }}" onclick="addToCart({{ $offer->id }})">+<img src="/img/main/icon_cart_white.svg" alt=""></a>@endif</div> 
                                </div>
                                <div class="info_payment_delivery">
                                    <div>
                                        <img src="/img/main/automobile_icon.svg" alt="">
                                        <div>
                                            <p>@lang('account_company_info.shipping')</p>
                                            @if(($offer->users->gender == 'outshop') || ($offer->users->gender == 'both'))
                                            <p class="green_text">@lang('account_company_info.outshop')</p>
                                            @endif
                                            @if(($offer->users->gender == 'inshop') || ($offer->users->gender == 'both'))
                                            <p class="green_text">@lang('account_company_info.inshop')</p>
                                            @endif
                                        </div>
                                    </div>
                                    <div>
                                        <img src="/img/main/payment_icon.svg" alt="">
                                        <div>
                                            <p>@lang('account_company_info.payment')</p>
                                            <p class="green_text">@lang('product.cashless')</p>
                                            <p class="green_text">@lang('product.cash')</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="wrap_slide_about_company">
                                    <div class="hide_slide" id="show-des-{{ $offer->id }}">
                                        <div class="wrap_about_company">
                                            <div class="left_image_company">
                                                <img src="{{ $offer->users->avatar ?? '/img/main/image_company.png'}}" alt="">
                                            </div>
                                            <div class="right_text_company">
                                                <p class="mini_name_company">{{ $offer->users->name }}</p>
                                                <div class="line_company_info">
                                                    <p><img src="/img/main/icon_location_blue.svg" alt="">{{ $offer->users->address }}</p>
                                                    <a href="tel:{{ $offer->users->phone }}"><img src="/img/main/icon_tel_blue.svg" alt="">{{ $offer->users->phone }}</a>
                                                    <a href="{{ $offer->users->lastname }}" target="_blank"><img src="/img/main/icon_url_blue.svg" alt="">{{ $offer->users->lastname }}</a>
                                                </div>
                                                <div class="description_about_company">
                                                    {{ $offer->users->description }}
                                                </div>
                                                @php
                                                $user = User::find($offer->users->id);
                                                @endphp
                                                <!-- <div class="footer_line_about_company">
                                                    <p><span>@lang('product.portal'): </span>{{ count($user->offers) }} @lang('product.products') <a href="">@lang('product.all_products')</a></p>
                                                </div>  -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="more_about_company">
                                        <a href="#" onclick="openDescription({{ $offer->id }}); return false;"><span>@lang('product.more')</span><img src="/img/main/arrow_down.svg" alt=""></a>
                                    </div>
                                </div>
                            </div>
                            @endforeach

                            <!------------------------------!Предложение товара!---------------------------------------->


                            





                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>

    <div class="overlay"></div>
    <div class="modal_block text_hint">
        <div class="close_modal"></div>
        <p class="append_text"></p>
    </div>

    <div class="modal_block create_reviews" style="display: none;">
        <div class="close_modal"></div>
        <p class="name_modal">@lang('product.send_review')</p>
        <form action="{{ route('addreview') }}" method="post">
            {{ csrf_field() }}
            <input type="hidden" name="product_id" id="product_id" value="{{ $product->id }}">
            <p class="title_product">{{ $product->$pr_title }}</p>
            <div class="one_section_inputs">
                <div>
                    <p class="dignity">@lang('product.positives')</p>
                    <input type="text" name="positives">
                </div>
                <div>
                    <p class="limitations">@lang('product.negatives')</p>
                    <input type="text" name="negatives">
                </div>
                <div>
                    <p class="reviews_title">@lang('product.review')</p>
                    <input type="text" name="text" required>
                </div>
                <div class="rating_options stars">
                    <input type="radio" name="sociability" id="one" value="1">
                    <label for="one"></label>
                    <input type="radio" name="sociability" id="two" value="2">
                    <label for="two"></label>
                    <input type="radio" name="sociability" id="three" value="3">
                    <label for="three"></label>
                    <input type="radio" name="sociability" id="four" value="4">
                    <label for="four"></label>
                    <input type="radio" name="sociability" id="five" value="5">
                    <label for="five"></label>
                </div>
            </div>
            <button class="blue_button_link"><img src="image/note.svg" alt=""> @lang('product.send')</button>
        </form>
    </div>
    <script>
      $('#sort_select').change(function(){
        var local = '/{{ $local }}';
        if(local == '/uk'){
            local = '';
        }
        window.location.href = local + '/product/{{ $product->id }}/?sort_by=' + $('#sort_select option:selected').val();
      });  
    </script>

    @endsection