<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'title' => 'Строительный маркетплейс',
    'login' => 'Войти',
    'register' => 'Регистрация',
    'home' => 'Домашняя страница',
    'no_categories' => 'Нет категорий',
    'news' => 'Новинки',
    'action' => 'Акционные товары',
    'all_actions' => 'Все акционные товары',
    'popular' => 'Популярные категории',
    'all_categories' => 'Все категории',
    'flat' => 'Квартира в новострое',
    'all_articles' => 'Все статьи',
];