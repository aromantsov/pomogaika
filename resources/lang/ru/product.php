<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'send_review' => 'Оставить отзыв',
    'prices' => 'Цены и предложения',
    'attributes' => 'Характеристики',
    'reviews' => 'Отзывы',
    'sp' => 'Оплата и доставка',
    'similar' => 'Связанные товары',
    'product_reviews' => 'Отзывы о товаре',
    'positives' => 'Достоинства',
    'negatives' => 'Недостатки',
    'review' => 'Отзыв',
    'more' => 'Подробнее',
    'payment_conditions' => 'Условия оплаты',
    'sp_description' => '<p>Если доставка оплаченного товара не будет произведена - вы можете обращаться с этими документами в соответсвующие инстанции, например в общество защиты прав потребителей.</p>
                                    <p>Доказательством отправки товара с нашей стороны является квитанция транспортной компании, подтверждающая что товар был Вам отправлен.</p>
                                    <p>Получая товар в офисе курьерской компании вы подписываете документы, подтверждающие получение Вами товара.</p>
                                    <p>Ответственность за доставку и целостность товара после отправки несёт транспортная компания. Все товары проходят осмотр перед отправкой в другой город.</p>
                                    <p>При отправке товар может быть застрахован курьерской компанией, так им образом у вас будет гарантия целостности получаемого товара.</p>',
    'shipping_conditions' => 'Условия доставки',  
    'offers' => 'Предложений',
    'sort' => 'Сортировка',
    'by_price_asc' => 'По возрастанию цены',
    'by_price_desc' => 'По убыванию цены',
    'by_popular' => 'По популярности',
    'read_reviews' => 'Читать отзывы',
    'cashless' => 'Безналичный расчет',
    'cash' => 'Наличный с предоплатой',
    'portal' => 'На портале представлено',
    'products' => 'товаров',
    'all_products' => 'Просмотреть все товары поставщика',
    'vendor'=> 'Поставщик',
    'select_vendor' => 'Выберите поставщика',
    'send' => 'Отправить'


];