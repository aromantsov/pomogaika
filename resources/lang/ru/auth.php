<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Данные не соответствуют',
    'throttle' => 'Слишком много попыток авторизоваться. Пожалуйста, попробуйте через :seconds секунд.',
    'name' => 'Имя пользователя',
    'password' => 'Пароль',
    'confirm' => 'Подтверждение пароля',
    'remember' => 'Запомнить меня',
    'forgot' => 'Забыли пароль?',
    'reset' => 'Восстановление пароля',
    'link' => 'Отправить ссылку',
    'logout' => 'Выйти',
    'dashboard' => 'Панель управления',
    'login' => 'Вы вошли на сайт'
];
