<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'main' => 'Главная страница',
    'sale' => 'Распродажа',
    'news' => 'Новости',
    'about_portal' => 'О портале',
    'for_sellers' => 'Для продавцов',
    'contacts' => 'Контакты',
    'cabinet' => 'Личный кабинет',
    'enter' => 'Войти',  
    'register' => 'Регистрация', 
    'cart' => 'Моя корзина',
    'log_in' => 'Вход', 
    'login' => 'Электронная почта или телефон',
    'password' => 'Пароль',
    'remember' => 'Запомнить меня',
    'forgot' => 'Забыли пароль?',
    'not_register' => 'Я не зарегистрированный пользователь',  
    'phone' => 'Телефон',
    'password_confirm' => 'Подтверждение пароля',
    'agree' => 'Я согласен с условиями обработки данных',
    'conditions' => 'Условия',  
    'to_register' => 'Зарегистрироваться',
    'registered' => 'Я зарегистрированный пользователь',
    'login_now' => 'Войти сейчас', 
    'add_wishlist' => 'Товар успешно добавлен в список желаний',
    'remove_wishlist' => 'Товар успешно удален из списка желаний',
    'registered_only' => 'Только зарегистрированные пользователи могут добавлять товар в список желаний',
    'want_login' => 'Вы хотите войти?'            
];