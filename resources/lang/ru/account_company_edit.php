<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'add_product' => 'Добавление товара',
    'search_product' => 'Поиск существующего товара',
    'product_search' => 'Поиск товаров в каталоге',
    'params' => 'Параметры каталога',
    'product_name_ru' => 'Название товара (на русском языке)',
    'product_name_uk' => 'Название товара (на украинском языке)',
    'select_category' => 'Выбрать категорию',
    'file_for_import' => 'Импорт из XLS, XLSX или CSV файла. Шаблон файла можно скачать <a href="/files/layout.xlsx">здесь</a>',
    'uploading_file' => 'Загружаемый файл',
    'import' => 'Импорт',
];