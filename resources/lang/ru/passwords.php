<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'В пароле должно быть как минимум восемь символов и сопоставимое подтверждение.',
    'reset' => 'Ваш пароль был сброшен!',
    'sent' => 'Ссылка для восстановления пароля отправлена на Ваш e-mail!',
    'token' => 'Токен восстановления пароля не действителен.',
    'user' => "Пользователь с данным e-mail не найден.",

];
