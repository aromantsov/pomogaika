<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'send_review' => 'Оставить отзыв',
    'positives' => 'Достоинства',
    'negatives' => 'Недостатки',
    'review' => 'Отзыв',
    'send' => 'Отправить',

];