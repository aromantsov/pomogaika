<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'create' => 'Создать категорию',
    'count' => 'Категорий',
    'list' => 'Список категорий',
    'main_page' => 'Главная страница',
    'categories' => 'Категории',
    'name' => 'Наименование',
    'status' => 'Статус',
    'action' => 'Действия',
    'no_categories' => 'Нет категорий',
    'all_categories' => 'Все категории',
    'catalog' => 'Каталог',
    'filters' => 'Фильтр',
    'check_all' => 'Отметить все',
    'brand' => 'Бренд',
    'products' => 'Товары',
    'by_more_price' => 'По возрастанию цены',
    'by_less_price' => 'По убыванию цены',
    'by_popular' => 'По популярности',
];