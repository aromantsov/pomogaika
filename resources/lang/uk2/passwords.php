<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Пароль має містити від 8 символів та мати підтвердження.',
    'reset' => 'Ваш пароль було скинуто!',
    'sent' => 'Ми відправили вам посилання для відновлення паролю на e-mail!',
    'token' => 'Токен відновлення паролю не дійсни й.',
    'user' => "Користувача з даним e-mail не знайдено.",

];
