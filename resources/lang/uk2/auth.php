<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Дані не співпадають.',
    'throttle' => 'Дуже багато спроб авторизуватися. Будь ласка, спробуйте ще через :seconds секунд.',
    'name' => 'Ім\'я користувача',
    'password' => 'Пароль',
    'confirm' => 'Підтвердження паролю',
    'remember' => 'Запам\'ятати мене',
    'forgot' => 'Забули пароль?',
    'reset' => 'Відновлення паролю',
    'link' => 'Відправити посилання',
    'logout' => 'Вийти',
    'dashboard' => 'Панель керування',
    'login' => 'Ви увійшли на сайт'
];