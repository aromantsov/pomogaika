<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'create' => 'Створити категорію',
    'count' => 'Категорій',
    'list' => 'Список категорій',
    'main_page' => 'Головна сторінка',
    'categories' => 'Категорії',
    'name' => 'Найменування',
    'status' => 'Статус',
    'action' => 'Дії',
    'no_categories' => 'Нема категорій',

];