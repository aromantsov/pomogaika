<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'all_categories' => 'Все категории',
    'main' => 'Главная',
    'user' => 'Пользователь',
    'adminpanel' => 'Админпанель',
    'personal_data' => 'Личные данные',
    'actual_orders' => 'Мои актуальные заказы',
    'wishlist' => 'Список желаний',
    'my_reviews' => 'Мои отзывы',
    'order_history' => 'История покупок',
    'second_name' => 'Фамилия',
    'patronimic' => 'Отчество',
    'birthdate' => 'Дата рождения',
    'default_address' => 'Адрес доставки',
    'password' => 'Пароль',
    'seller' => 'Продавец',
    'send_review' => 'Оставить отзыв о продавце',
    'less' => 'Свернуть',
    'products' => 'Товаров',
    'from' => 'от',
];