<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    '404' => 'Ошибка 404',
    'no_exists' => 'Извините, такой страницы не существует',
    'deprecate' => 'Возможно она устарела, была удалена или был введен неверный адрес.
                            Перейдите на главную страницу сайта или воспользуйтесь поиском.',
    'main_transfer' => 'Перейти на главную',
    '505' => 'Ошибка 505',
    'error' => 'Ой, что то пошло не так',
    'server' => 'Вы не виноваты! Это произошло по нашей вине, очень скоро мы все исправим.
                            Перейдите на главную страницу сайта или воспользуйтесь поиском. ',                       
];