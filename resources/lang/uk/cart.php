<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'empty_cart' => 'Корзина пуста',
    'ordering' => 'Оформление заказа',
    'new_user' => 'Новый пользователь',
    'registered' => 'Уже зарегистрирован',
    'comment' => 'Комментарий',
    'purchases' => 'Продолжить покупки',
    'order_payment' => 'Оформить и оплатить',
    'login' => 'Электронная почта или телефон',
    'enter' => 'Войти',
    'cart' => 'Корзина',
    'outshop' => 'Доставка продавцом',
    'ourshipping' => 'Доставка от Pomogayka Shop',
    'shipping_cost' => 'Стоимость доставки',
    'shipping_address' => 'Адрес доставки',
    'continue' => 'Продолжить покупать у данного продавца',
    'cost' => 'К оплате',
    'cod' => 'Наложенный платеж',
    'agreement' => 'Оплата согласно договору',
    'all_total' => 'Общая стоимость заказа',
];