<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'order_confirmation' => 'Подтверждение заказа',
    'thanks' => 'Спасибо за заказ. Ваш заказ успешно принят в обработку. Вы сможете отследить
                            статус заказа в вашем личном кабинете. Наши менеджеры могут связаться с вами
                            для уточнения деталей ',
    'order_status' => 'Статус заказа',                        
];