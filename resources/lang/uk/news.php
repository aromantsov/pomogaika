<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'info' => 'Полезная информация',
    'article' => 'Статья',
    'post' => 'Поделиться',
    'back' => 'Назад ко всем статьям',
    'more' => 'Подробнее',
    'search' => 'Поиск',
    'article_search' => 'Поиск по статьям',
    'tag_search' => 'Поиск по тегам',
    'news' => 'news'
];