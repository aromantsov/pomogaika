<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTariffsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tariffs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->integer('price');
            $table->tinyInteger('profile')->default(0);
            $table->tinyInteger('import')->default(0);
            $table->tinyInteger('messages')->default(0);
            $table->tinyInteger('public_profile')->default(0);
            $table->tinyInteger('hot')->default(0);
            $table->tinyInteger('manager')->default(0);
            $table->tinyInteger('month_rating')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tariffs');
    }
}
