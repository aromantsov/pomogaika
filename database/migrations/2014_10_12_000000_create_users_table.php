<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->default('');
            $table->string('lastname')->default('');
            $table->string('patronimic')->default('');
            $table->string('phone')->default('');
            $table->date('birthdate')->default('2000-01-01');
            $table->string('address')->default('');
            $table->text('avatar')->nullable();
            $table->string('gender')->default('');
            $table->string('description')->default('');
            $table->decimal('bonus', 10, 4)->default(0);
            $table->string('shops')->default('');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->integer('user_role_id')->default(4);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
