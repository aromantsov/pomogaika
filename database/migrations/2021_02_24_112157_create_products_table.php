<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('model');
            $table->string('sku');
            $table->integer('manufacturer_id');
            $table->decimal('price', 10, 2);
            $table->decimal('trade_price', 10, 2);
            $table->string('unit')->nullable();
            $table->tinyInteger('published')->default(0);
            $table->integer('sort_order')->nullable();
            $table->text('image')->default();
            $table->text('video')->nullable();
            $table->string('name_ru');
            $table->string('name_uk');
            $table->integer('category_id')->nullable();
            $table->text('description_ru')->nullable();
            $table->text('description_uk')->nullable();
            $table->string('meta_name_ru')->nullable();
            $table->string('meta_name_uk')->nullable();
            $table->string('meta_h1_ru')->nullable();
            $table->string('meta_h1_uk')->nullable();
            $table->string('meta_description_ru')->nullable();
            $table->string('meta_description_uk')->nullable();
            $table->string('meta_keywords_ru')->nullable();
            $table->string('meta_keywords_uk')->nullable();
            $table->timestamps();
            $table->integer('is_sale')->default(0);
            $table->integer('is_new')->default(0);
            $table->integer('bonus')->default(0);
            $table->integer('popular')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
