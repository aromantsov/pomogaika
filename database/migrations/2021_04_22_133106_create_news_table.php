<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('published')->default(0);
            $table->integer('views')->default(0);
            $table->string('picture')->nullable();
            $table->string('name_ru');
            $table->string('name_uk');
            $table->text('description_ru');
            $table->text('description_uk');
            $table->string('meta_name_ru')->nullable();
            $table->string('meta_name_uk')->nullable();
            $table->string('meta_h1_ru')->nullable();
            $table->string('meta_h1_uk')->nullable();
            $table->string('meta_description_ru')->nullable();
            $table->string('meta_description_uk')->nullable();
            $table->string('meta_keywords_ru')->nullable();
            $table->string('meta_keywords_uk')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
